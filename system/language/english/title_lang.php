<?php

//page title array
$lang['business_start_title'] = 'Business Start-Ups Ipswich, London | Niche Accountants';
$lang['business_partnerships_title'] = 'Partnership Ipswich & London | Niche Accountants';
$lang['business_company_title'] = 'Limited Companies Ipswich, London | Niche Accountants';
$lang['business_liability_title'] = 'LLPs in Ipswich and London | Niche Accountants';
$lang['business_employment_title'] = 'Self Employed Ipswich & London | Niche Accountants';
$lang['services_annual_title'] = 'Annual Accounts Ipswich & London | Niche Accountants';
$lang['services_accounting_title'] = 'Accounting Software Ipswich, London | Niche Accountants';
$lang['services_bookkeeping_title'] = 'Bookkeeping Ipswich and London | Niche Accountants';
$lang['services_consultancy_title'] = 'Business Consultancy Ipswich, London | Niche Accountants';
$lang['services_payroll_title'] = 'Payroll Services Ipswich & London | Niche Accountants';
$lang['services_tax_return_title'] = 'Self Assessment Ipswich and London | Niche Accountants';
$lang['services_corporation_title'] = 'Corporation Tax Ipswich and London | Niche Accountants';
$lang['services_vat_title'] = 'VAT Returns Ipswich and London | Niche Accountants';
$lang['digital_tax_title'] = 'Tax Help in Ipswich and London | Making Tax Digital';


//page meta description array
$lang['business_start_desc'] = 'Business Start-Up Ipswich, London – Niche Accountants advise on setting up a company and help with tax, registration and finance applications';

$lang['business_partnerships_desc'] = 'Partnerships Ipswich & London | Niche Accountants can guide you through the partnership set-up process and provide expert support to businesses in Suffolk ';

$lang['business_company_desc'] = 'Companies Ipswich, London – High-quality tax help, expert accounting and payroll solutions for small businesses, provided by Niche Accountants';

$lang['business_liability_desc'] = 'LLPs in Ipswich and London | Niche Accountants offer a full range of accounting services to small businesses, including limited liability partnerships';

$lang['business_employment_desc'] = 'Self-Employed Ipswich & Felixstowe – Niche Accountants provide help with tax returns, book-keeping and VAT for sole traders and business partnerships.';

$lang['services_annual_desc'] = 'Annual accounts Ipswich & London – Niche Accountants provide a full range of top-quality accounting services to small businesses, including filing accounts';

$lang['services_accounting_desc'] = 'Accounting Software Ipswich, London – Niche Accountants work with leading cloud software providers, including Xero and Quickbooks, to help small businesses';

$lang['services_bookkeeping_desc'] = 'Bookkeeping Ipswich and London – Niche Accountants provide quality accountancy services for small businesses, including invoice preparation and processing';

$lang['services_consultancy_desc'] = 'Business Consultancy Ipswich, London – Niche Accountants offer a wide range of services for small businesses, including overall strategy and tax planning';

$lang['services_payroll_title'] = 'Payroll Services Ipswich & London – Niche Accountants offer a full package of payroll services for small businesses, including payslips and PAYE returns';

$lang['services_tax_return_desc'] = 'Self Assessment Ipswich and London – Niche Accountants help the self-employed, plus company directors and business partners, to file their tax returns ';

$lang['services_corporation_desc'] = 'Corporation Tax Ipswich and London – Niche Accountants provide top-quality corporation tax services for individuals, partnerships and limited companies';

$lang['services_vat_desc'] = 'VAT Returns Ipswich and London – Niche Accountants offer a full range of VAT services for small businesses, including submitting your returns online';

$lang['digital_tax_desc'] = 'Tax Help in Ipswich and London – Niche Accountants’ guide to Making Tax Digital, and how it will affect businesses, landlords and the self-employed';
// $lang['/login_desc'] = 'This is login page.';
// $lang['/register_desc'] = 'This is register page.';


$lang['business_start_keywords'] = 'Business start-ups Ipswich, London';
$lang['business_partnerships_keywords'] = 'Partnership Ipswich & London';
$lang['business_company_keywords'] = 'Limited companies Ipswich, London';
$lang['business_liability_keywords'] = ' LLPs in Ipswich and London';
$lang['business_employment_keywords'] = 'employed Ipswich & London';
$lang['services_annual_keywords'] = 'Annual accounts Ipswich & London';
$lang['services_accounting_keywords'] = 'Accounting software Ipswich, London';
$lang['services_bookkeeping_keywords'] = 'Bookkeeping Ipswich and London';
$lang['services_consultancy_keywords'] = 'Business consultancy Ipswich, London ';
$lang['services_payroll_keywords'] = 'Payroll Services Ipswich & London';
$lang['services_tax_return_keywords'] = 'Self assessment Ipswich and London';
$lang['services_corporation_keywords'] = 'Corporation Tax Ipswich and London';
$lang['services_vat_keywords'] = 'VAT Returns Ipswich and London ';
$lang['digital_tax_keywords'] = 'Tax help in Ipswich and London';


?>
