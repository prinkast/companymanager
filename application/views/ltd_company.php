
<?php $this->load->view ('header');?>
<!--form end-->

  <div class="container">
    <div class="row">
    <div class="page-heading row">
		<div class="col-sm-12">	 
			<h3 class="col-sm-6" style="margin-bottom:20px;">LTD Company Information</h3>
		</div>

	</div> 
	
     </div>
     <div class="row">
      <div id="offer-popup"  class="col-md-12 offer-popup-sec">
					<form action="#" method="POST" id="ltd_registration_form" name="ltd_registration_form">
						<div class="bs-example shp_cart_page">
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="#collapseOne" class="collapsed">Company Name</a>
										</h4>
									</div>
									<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
										<span class="form-inline" role="form">
											<div class="panel-body">
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Company Name:</label> 												
													<input class="form-control" type="text" name="rt" id="rt" value="<?php echo $comp_info->company_name;?>">
												</div>
											</div>
										</span>
									</div>
								</div> <!-- PANEL ENDS -->							
							
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="#" class="collapsed">Select a Registered Office Address</a>
										</h4>
									</div>
									<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">

										<span class="form-inline" role="form">
											<div class="panel-body">
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Prefill Address:</label> 
													<select class="form-control" name="ldt_reg_form[register][reg_add_prefix]" id="get_address_select">
													<option value="0">-------------Select-------------  </option>
													<option value="1">1 St Saviours Wharf, 23 Mill Street, London, SE1 2BE</option>
													<option value="2">40 Bloomsbury Way, Lower Ground Floor, London SE1 2BE</option>
													<option value="3">101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
													</select>
													
												</div>
											</div>
										</span>
									</div>
								</div> <!-- PANEL ENDS -->
								
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="#"class="collapsed">Registered Office Address</a>
										</h4>
									</div>
									<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
									

										<span class="form-inline" role="form">
											<div class="panel-body">
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Building name/number:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_building_name]" id="reg_building_name" value="<?php echo $comp_reg->name_number;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Street:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_street]" id="reg_street" value="<?php echo $comp_reg->street;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Address 3:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_address1]" id="reg_address1" value="<?php echo $comp_reg->address1;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Town:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_town]" id="reg_town" value="<?php echo $comp_reg->town;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">County:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_county]" id="reg_county" value="<?php echo $comp_reg->county;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Postcode:</label> 
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_postcode]" id="reg_postcode" value="<?php echo $comp_reg->postcode;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Country:</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_country]" id="reg_country" value="<?php echo $comp_reg->country;?>">
												</div>
											</div>
										</span>
									</div>
								</div> <!-- PANEL ENDS -->
		
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Person Director</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		<?php //var_dump($comp_director);die;?>
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Title:</label> 
															<select class="form-control">
															 <option>Mr</option>
															 <option>Mrs</option>
															 <option>Miss</option>
															 <option>Ms</option>
															 <option>Dr</option>
															 <option>Prof</option>
															 <option>Master</option>
															 <option>Sir</option>
															 <option>Lord</option>
															 <option>Rev</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Middle name:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box dob">
															<label for="Title ">Date of birth:</label> 
															<div class="col-sm-3">
															<select class="form-control">
															  <option>1</option>
															  <option>2</option>
															  <option>3</option>
															  <option>4</option>
															  <option>5</option>
															  <option>6</option>
															  <option>7</option>
															  <option>8</option>
															  <option>9</option>
															  <option>10</option>
															  <option>11</option>
															  <option>12</option>
															  <option>13</option>
															  <option>14</option>
															  <option>15</option>
															  <option>16</option>
															  <option>17</option>
															  <option>18</option>
															  <option>19</option>
															  <option>20</option>
															  <option>21</option>
															  <option>22</option>
															  <option>23</option>
															  <option>24</option>
															  <option>25</option>
															  <option>26</option>
															  <option>27</option>
															  <option>28</option>
															  <option>29</option>
															  <option>30</option>
															</select>
														   </div>
														   <div class="col-sm-3">
															<select class="form-control">
															  <option>1</option>
															  <option>2</option>
															  <option>3</option>
															  <option>4</option>
															  <option>5</option>
															  <option>6</option>
															  <option>7</option>
															  <option>8</option>
															  <option>9</option>
															  <option>10</option>
															  <option>11</option>
															  <option>12</option>
															 </select>
															</div>
															<div class="col-sm-5">
																<select class="form-control">
																  <option>1980</option>
																  <option>1981</option>
																  <option>1982</option>
																  <option>1983</option>
																  <option>1984</option>
																  <option>1985</option>
																  <option>1987</option>
																  <option>1988</option>
																  <option>1989</option>
																  <option>1990</option>
																  <option>1991</option>
																  <option>1992</option>
																  <option>1993</option>
																  <option>1994</option>
																  <option>1995</option>    
															   	</select>
															   </div>
														</div>
														
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Nationality:</label> 
															<select class="form-control">
																<option>--- Select ---</option>
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Occupation:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country of Residence:</label> 
																																														<select class="form-control">																																																<option>--- Select ---</option>
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Select a Director Service Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Prefill Address:</label> 
															<select class="form-control">
															 <option>--- Select ---</option>
															 <option>WC1</option>
															 <option>SE1</option>
															 <option>EH2</option> 
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Service Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<select class="form-control">
															 <option>--- Select ---</option>
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option> 
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Residential Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
													    <p class="text-note">The residential address must be the address where you live.</p>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<select class="form-control">
															 <option>England</option>
															 <option>United Kingdom</option>
															 <option>Spain</option>
															 <option>USA</option> 
															</select>
														</div>
													 </div>
													</span>
												</div>
											</div> <!-- PANEL ENDS -->
											
											
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Consent to act</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
													<div class="form-group col-sm-12 box">
															<label class="checkbox"> <input type="checkbox" class="required">
															 <span>The subscribers (shareholders) confirm that the person named has consented to
															 act as director</span>
															</label>
														</div>
													</div>
												 </span>
												</div>
										</div> <!-- PANEL ENDS -->
											
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Person Shareholder</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Title:</label> 
															<select class="form-control">
															 <option>Mr</option>
															 <option>Mrs</option>
															 <option>Miss</option>
															 <option>Ms</option>
															 <option>Dr</option>
															 <option>Prof</option>
															 <option>Master</option>
															 <option>Sir</option>
															 <option>Lord</option>
															 <option>Rev</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Middle name:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text">
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Registered Office Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County of Residence:</label> 
															<select class="form-control">
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<select class="form-control">
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
													
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Nationality:</label> 
															<select class="form-control">
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Allotment of Shares</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
													    <div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">Share Currency:</label> 
															<select class="form-control">
															 <option>GBP &pound;</option>
															 <option>USD $</option>
															 <option>EURO &euro;</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 col-sm-offset-2 box">
															<label for="Title ">Share Class:</label> 
															<input class="form-control" type="text" placeholder="ORDINARY">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Number of shares:</label> 
															<input class="form-control" type="text" placeholder="1">
															<p>By default we recommend 1 share per shareholder. You can easily allocate extra shares after incorporation.</p>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Value per share:</label> 
															<input class="form-control" type="text" placeholder="1">
															<p>By default we recommend a share value of 1. The number of shares and value per share limits your company's liability.</p>
														</div>
														
													 </div>
													</span>
												</div>
											</div> <!-- PANEL ENDS -->
											
											
											<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Security</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">First three letters of Town of  birth*:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">Last three digits of Telehone number*:</label> 
															<input class="form-control" type="text">
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">Fisrt three letters of Eye colour*:</label> 
															<input class="form-control" type="text">
														</div>
													</div>
												 </span>
												</div>
											</div> <!-- PANEL ENDS -->
											
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a href="#" class="collapsed">Company Name</a>
										</h4>
									</div>
									<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
										<span class="form-inline" role="form">
											<div class="panel-body">
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Company Name:</label> 
													<input class="form-control" type="text" name="" id="reg_building_name" value="00adfrt">
												</div>
											</div>
										</span>
									</div>
								</div> <!-- PANEL ENDS -->	
			
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Person</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Title:</label> 
															<select class="form-control" name="ltd_psc[psc][psc_title]">
															<option value="Mr">Mr</option>
															<option value="Mrs">Mrs</option>
															<option value="Miss">Miss</option>
															<option value="Ms">Ms</option>
															<option value="Dr">Dr</option>
															<option value="Prof">Prof</option>
															<option value="Master">Master</option>
															<option value="Sir">Sir</option>
															<option value="Lord">Lord</option>
															<option value="Rev">Rev</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="hidden" name="ltd_psc[psc][psc_edit_id]" id="psc_edit_id" value="0">
															<input class="form-control" type="hidden" name="ltd_psc[psc][psc_id_status]" id="psc_id_status" value="1">
															<input class="form-control" type="text" name="ltd_psc[psc][psc_first_name]" id="psc_first_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Middle name:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_middle_name]" id="psc_middle_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_last_name]" id="psc_last_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box dob">
															<label for="Title ">Date of birth:</label> 
															<div class="col-sm-3">
															<select class="form-control" name="ltd_psc[psc][psc_dob_day]">
															 <option value="1">1</option>
															 <option value="2">2</option>
															 <option value="3">3</option>
															 <option value="4">4</option>
															 <option value="5">5</option>
															 <option value="6">6</option>
															 <option value="7">7</option>
															 <option value="8">8</option>
															 <option value="9">9</option>
															 <option value="10">10</option>
															 <option value="11">11</option>
															 <option value="12">12</option>
															 <option value="13">13</option>
															 <option value="14">14</option>
															 <option value="15">15</option>
															 <option value="16">16</option>
															 <option value="17">17</option>
															 <option value="18">18</option>
															 <option value="19">19</option>
															 <option value="20">20</option>
															 <option value="21">21</option>
															 <option value="22">22</option>
															 <option value="23">23</option>
															 <option value="24">24</option>
															 <option value="25">25</option>
															 <option value="26">26</option>
															 <option value="27">27</option>
															 <option value="28">28</option>
															 <option value="29">29</option>
															 <option value="30">30</option>
															 <option value="31">31</option>
															</select>
														   </div>
														   <div class="col-sm-3">
															<select class="form-control" name="ltd_psc[psc][psc_dob_month]">
															  <option>1</option>
															  <option>2</option>
															  <option>3</option>
															  <option>4</option>
															  <option>5</option>
															  <option>6</option>
															  <option>7</option>
															  <option>8</option>
															  <option>9</option>
															  <option>10</option>
															  <option>11</option>
															  <option>12</option>
															 </select>
															</div>
															<div class="col-sm-5">
																<select class="form-control" name="ltd_psc[psc][psc_dob_year]">
																  <option>1980</option>
																  <option>1981</option>
																  <option>1982</option>
																  <option>1983</option>
																  <option>1984</option>
																  <option>1985</option>
																  <option>1987</option>
																  <option>1988</option>
																  <option>1989</option>
																  <option>1990</option>
																  <option>1991</option>
																  <option>1992</option>
																  <option>1993</option>
																  <option>1994</option>
																  <option>1995</option>    
															   	</select>
															   </div>
														</div>
														
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Nationality:</label> 
															<select class="form-control" name="ltd_psc[psc][psc_nationality]">
																<option>--- Select ---</option>
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Occupation:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_occupation]" id="psc_occupation">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country of Residence:</label> 
															<select class="form-control" name="ltd_psc[psc][psc_country_res]">
																<option>--- Select ---</option>
																<option value="AFG">Afghanistan</option>
																<option value="ALA">Åland Islands</option>
																<option value="ALB">Albania</option>
																<option value="DZA">Algeria</option>
																<option value="ASM">American Samoa</option>
																<option value="AND">Andorra</option>
																<option value="AGO">Angola</option>
																<option value="AIA">Anguilla</option>
																<option value="ATA">Antarctica</option>
																<option value="ATG">Antigua and Barbuda</option>
																<option value="ARG">Argentina</option>
																<option value="ARM">Armenia</option>
																<option value="ABW">Aruba</option>
																<option value="AUS">Australia</option>
																<option value="AUT">Austria</option>
																<option value="AZE">Azerbaijan</option>
																<option value="BHS">Bahamas</option>
																<option value="BHR">Bahrain</option>
																<option value="BGD">Bangladesh</option>
																<option value="BRB">Barbados</option>
																<option value="BLR">Belarus</option>
																<option value="BEL">Belgium</option>
																<option value="BLZ">Belize</option>
																<option value="BEN">Benin</option>
																<option value="BMU">Bermuda</option>
																<option value="BTN">Bhutan</option>
																<option value="BOL">Bolivia, Plurinational State of</option>
																<option value="BES">Bonaire, Sint Eustatius and Saba</option>
																<option value="BIH">Bosnia and Herzegovina</option>
																<option value="BWA">Botswana</option>
																<option value="BVT">Bouvet Island</option>
																<option value="BRA">Brazil</option>
																<option value="IOT">British Indian Ocean Territory</option>
																<option value="BRN">Brunei Darussalam</option>
																<option value="BGR">Bulgaria</option>
																<option value="BFA">Burkina Faso</option>
																<option value="BDI">Burundi</option>
																<option value="KHM">Cambodia</option>
																<option value="CMR">Cameroon</option>
																<option value="CAN">Canada</option>
																<option value="CPV">Cape Verde</option>
																<option value="CYM">Cayman Islands</option>
																<option value="CAF">Central African Republic</option>
																<option value="TCD">Chad</option>
																<option value="CHL">Chile</option>
																<option value="CHN">China</option>
																<option value="CXR">Christmas Island</option>
																<option value="CCK">Cocos (Keeling) Islands</option>
																<option value="COL">Colombia</option>
																<option value="COM">Comoros</option>
																<option value="COG">Congo</option>
																<option value="COD">Congo, the Democratic Republic of the</option>
																<option value="COK">Cook Islands</option>
																<option value="CRI">Costa Rica</option>
																<option value="CIV">Côte d'Ivoire</option>
																<option value="HRV">Croatia</option>
																<option value="CUB">Cuba</option>
																<option value="CUW">Curaçao</option>
																<option value="CYP">Cyprus</option>
																<option value="CZE">Czech Republic</option>
																<option value="DNK">Denmark</option>
																<option value="DJI">Djibouti</option>
																<option value="DMA">Dominica</option>
																<option value="DOM">Dominican Republic</option>
																<option value="ECU">Ecuador</option>
																<option value="EGY">Egypt</option>
																<option value="SLV">El Salvador</option>
																<option value="GNQ">Equatorial Guinea</option>
																<option value="ERI">Eritrea</option>
																<option value="EST">Estonia</option>
																<option value="ETH">Ethiopia</option>
																<option value="FLK">Falkland Islands (Malvinas)</option>
																<option value="FRO">Faroe Islands</option>
																<option value="FJI">Fiji</option>
																<option value="FIN">Finland</option>
																<option value="FRA">France</option>
																<option value="GUF">French Guiana</option>
																<option value="PYF">French Polynesia</option>
																<option value="ATF">French Southern Territories</option>
																<option value="GAB">Gabon</option>
																<option value="GMB">Gambia</option>
																<option value="GEO">Georgia</option>
																<option value="DEU">Germany</option>
																<option value="GHA">Ghana</option>
																<option value="GIB">Gibraltar</option>
																<option value="GRC">Greece</option>
																<option value="GRL">Greenland</option>
																<option value="GRD">Grenada</option>
																<option value="GLP">Guadeloupe</option>
																<option value="GUM">Guam</option>
																<option value="GTM">Guatemala</option>
																<option value="GGY">Guernsey</option>
																<option value="GIN">Guinea</option>
																<option value="GNB">Guinea-Bissau</option>
																<option value="GUY">Guyana</option>
																<option value="HTI">Haiti</option>
																<option value="HMD">Heard Island and McDonald Islands</option>
																<option value="VAT">Holy See (Vatican City State)</option>
																<option value="HND">Honduras</option>
																<option value="HKG">Hong Kong</option>
																<option value="HUN">Hungary</option>
																<option value="ISL">Iceland</option>
																<option value="IND">India</option>
																<option value="IDN">Indonesia</option>
																<option value="IRN">Iran, Islamic Republic of</option>
																<option value="IRQ">Iraq</option>
																<option value="IRL">Ireland</option>
																<option value="IMN">Isle of Man</option>
																<option value="ISR">Israel</option>
																<option value="ITA">Italy</option>
																<option value="JAM">Jamaica</option>
																<option value="JPN">Japan</option>
																<option value="JEY">Jersey</option>
																<option value="JOR">Jordan</option>
																<option value="KAZ">Kazakhstan</option>
																<option value="KEN">Kenya</option>
																<option value="KIR">Kiribati</option>
																<option value="PRK">Korea, Democratic People's Republic of</option>
																<option value="KOR">Korea, Republic of</option>
																<option value="KWT">Kuwait</option>
																<option value="KGZ">Kyrgyzstan</option>
																<option value="LAO">Lao People's Democratic Republic</option>
																<option value="LVA">Latvia</option>
																<option value="LBN">Lebanon</option>
																<option value="LSO">Lesotho</option>
																<option value="LBR">Liberia</option>
																<option value="LBY">Libya</option>
																<option value="LIE">Liechtenstein</option>
																<option value="LTU">Lithuania</option>
																<option value="LUX">Luxembourg</option>
																<option value="MAC">Macao</option>
																<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
																<option value="MDG">Madagascar</option>
																<option value="MWI">Malawi</option>
																<option value="MYS">Malaysia</option>
																<option value="MDV">Maldives</option>
																<option value="MLI">Mali</option>
																<option value="MLT">Malta</option>
																<option value="MHL">Marshall Islands</option>
																<option value="MTQ">Martinique</option>
																<option value="MRT">Mauritania</option>
																<option value="MUS">Mauritius</option>
																<option value="MYT">Mayotte</option>
																<option value="MEX">Mexico</option>
																<option value="FSM">Micronesia, Federated States of</option>
																<option value="MDA">Moldova, Republic of</option>
																<option value="MCO">Monaco</option>
																<option value="MNG">Mongolia</option>
																<option value="MNE">Montenegro</option>
																<option value="MSR">Montserrat</option>
																<option value="MAR">Morocco</option>
																<option value="MOZ">Mozambique</option>
																<option value="MMR">Myanmar</option>
																<option value="NAM">Namibia</option>
																<option value="NRU">Nauru</option>
																<option value="NPL">Nepal</option>
																<option value="NLD">Netherlands</option>
																<option value="NCL">New Caledonia</option>
																<option value="NZL">New Zealand</option>
																<option value="NIC">Nicaragua</option>
																<option value="NER">Niger</option>
																<option value="NGA">Nigeria</option>
																<option value="NIU">Niue</option>
																<option value="NFK">Norfolk Island</option>
																<option value="MNP">Northern Mariana Islands</option>
																<option value="NOR">Norway</option>
																<option value="OMN">Oman</option>
																<option value="PAK">Pakistan</option>
																<option value="PLW">Palau</option>
																<option value="PSE">Palestinian Territory, Occupied</option>
																<option value="PAN">Panama</option>
																<option value="PNG">Papua New Guinea</option>
																<option value="PRY">Paraguay</option>
																<option value="PER">Peru</option>
																<option value="PHL">Philippines</option>
																<option value="PCN">Pitcairn</option>
																<option value="POL">Poland</option>
																<option value="PRT">Portugal</option>
																<option value="PRI">Puerto Rico</option>
																<option value="QAT">Qatar</option>
																<option value="REU">Réunion</option>
																<option value="ROU">Romania</option>
																<option value="RUS">Russian Federation</option>
																<option value="RWA">Rwanda</option>
																<option value="BLM">Saint Barthélemy</option>
																<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
																<option value="KNA">Saint Kitts and Nevis</option>
																<option value="LCA">Saint Lucia</option>
																<option value="MAF">Saint Martin (French part)</option>
																<option value="SPM">Saint Pierre and Miquelon</option>
																<option value="VCT">Saint Vincent and the Grenadines</option>
																<option value="WSM">Samoa</option>
																<option value="SMR">San Marino</option>
																<option value="STP">Sao Tome and Principe</option>
																<option value="SAU">Saudi Arabia</option>
																<option value="SEN">Senegal</option>
																<option value="SRB">Serbia</option>
																<option value="SYC">Seychelles</option>
																<option value="SLE">Sierra Leone</option>
																<option value="SGP">Singapore</option>
																<option value="SXM">Sint Maarten (Dutch part)</option>
																<option value="SVK">Slovakia</option>
																<option value="SVN">Slovenia</option>
																<option value="SLB">Solomon Islands</option>
																<option value="SOM">Somalia</option>
																<option value="ZAF">South Africa</option>
																<option value="SGS">South Georgia and the South Sandwich Islands</option>
																<option value="SSD">South Sudan</option>
																<option value="ESP">Spain</option>
																<option value="LKA">Sri Lanka</option>
																<option value="SDN">Sudan</option>
																<option value="SUR">Suriname</option>
																<option value="SJM">Svalbard and Jan Mayen</option>
																<option value="SWZ">Swaziland</option>
																<option value="SWE">Sweden</option>
																<option value="CHE">Switzerland</option>
																<option value="SYR">Syrian Arab Republic</option>
																<option value="TWN">Taiwan, Province of China</option>
																<option value="TJK">Tajikistan</option>
																<option value="TZA">Tanzania, United Republic of</option>
																<option value="THA">Thailand</option>
																<option value="TLS">Timor-Leste</option>
																<option value="TGO">Togo</option>
																<option value="TKL">Tokelau</option>
																<option value="TON">Tonga</option>
																<option value="TTO">Trinidad and Tobago</option>
																<option value="TUN">Tunisia</option>
																<option value="TUR">Turkey</option>
																<option value="TKM">Turkmenistan</option>
																<option value="TCA">Turks and Caicos Islands</option>
																<option value="TUV">Tuvalu</option>
																<option value="UGA">Uganda</option>
																<option value="UKR">Ukraine</option>
																<option value="ARE">United Arab Emirates</option>
																<option value="GBR">United Kingdom</option>
																<option value="USA">United States</option>
																<option value="UMI">United States Minor Outlying Islands</option>
																<option value="URY">Uruguay</option>
																<option value="UZB">Uzbekistan</option>
																<option value="VUT">Vanuatu</option>
																<option value="VEN">Venezuela, Bolivarian Republic of</option>
																<option value="VNM">Viet Nam</option>
																<option value="VGB">Virgin Islands, British</option>
																<option value="VIR">Virgin Islands, U.S.</option>
																<option value="WLF">Wallis and Futuna</option>
																<option value="ESH">Western Sahara</option>
																<option value="YEM">Yemen</option>
																<option value="ZMB">Zambia</option>
																<option value="ZWE">Zimbabwe</option>
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Prefill</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Prefill Address:</label> 
															<select class="form-control" name="ltd_psc[psc][psc_pre_address]" id="select_psc_pre_address">
															<option value="">-------------Select-------------  </option>
															<option value="1">1 St Saviours Wharf, 23 Mill Street, London, SE1 2BE</option>
															<option value="2">40 Bloomsbury Way, Lower Ground Floor, London SE1 2BE</option>
															<option value="3">101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
															</select>
														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Service Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_name]" id="psc_building_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_street]" id="psc_building_street">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_address3]" id="psc_pre_address">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_town]" id="psc_building_town">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_aounty]" id="psc_building_aounty">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_postcode]" id="psc_building_postcode">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_country]" id="psc_building_country">
															

														</div>
													</div>
												</span>
											</div>
										</div> <!-- PANEL ENDS -->
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Residential Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
													    <p class="text-note">The residential address must be the address where you live.</p>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_name]" id="psc_building_res_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_street]" id="psc_building_res_street">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_address3]" id="psc_building_res_address3">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_town]" id="psc_building_res_town">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_county]" id="psc_building_res_county">
														</div> 
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_postcode]" id="psc_building_res_postcode">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_country]" id="psc_building_res_country">
														</div>
													 </div>
													 
													</span>
												</div>
											</div> <!-- PANEL ENDS -->
											
											<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#" class="collapsed">Nature of Control</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
												     <p class="text-note">What is the nature of the person's control over this company?</p>
													    <div class="col-sm-12 box list-content">
															  <ul>
															    <li><a onclick="showHideFinal('own_share','own_share1','own_share2','own_share3')">Ownership of shares</a>
																<ul>
																<div id="own_share" style="display: none;">
																	<li><a id="" onclick="showHide('show_id','show_id1','show_id2')">The person holds shares</a>
																		<ul id="show_id" style="display:none">
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 25% but not more than 50% of shares"> More than 25% but not more than 50% of shares</li>
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 50% but not more than 75% of shares"> More than 50% but not more than 75% of shares</li>
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 75% of shares"> More than 75% of shares</li>
																		</ul>
																	</li>
																	
																	<li><a onclick="showHide('show_id1','show_id','show_id2')">The members of the firm hold shares</a>
																		<div id="show_id1" style="display:none">
																		<p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
																		<ul>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		</div>
																	</li>
															       
																	<li><a onclick="showHide('show_id2','show_id','show_id1')">The trustees of a trust hold shares</a>
																		<div id="show_id2" style="display:none">
																		<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
																		<ul>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		<div>
																	</div></div></li>
																	</div>
																</ul>															      
															</li>
																
															    <li><a onclick="showHideFinal('own_share1','own_share','own_share2','own_share3')">Ownership of voting rights</a>
															      <ul>
																  <div style="display:none" id="own_share1"> 
															        <li><a onclick="showHide('show_id3','show_id4','show_id5')">The person holds voting rights</a>
															         <ul id="show_id3" style="display:none">
															           <li><input type="radio" name="ltd_psc[psc][psc_Ownership_voting]" value="More than 25% but not more than 50% of voting rights"> More than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_Ownership_voting]" value="More than 50% but not more than 75% of voting rights"> More than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_Ownership_voting]" value="More than 75% of voting rights"> More than 75% of voting rights</li>
															         </ul>
															       </li>
															       
															       <li><a onclick="showHide('show_id4','show_id3','show_id5')">The members of a firm hold voting rights</a>
																     <div id="show_id4" style="display:none">
																	 <p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
															         <ul>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															       
															       <li><a onclick="showHide('show_id5','show_id3','show_id3')">The trustees of a trust hold voting rights</a>
															        <div id="show_id5" style="display:none">
																	<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
															         <ul>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															     </div></ul>															      
															    </li>
															    
															    <li><a onclick="showHideFinal('own_share2','own_share3','own_share1','own_share')">Right to appoint or remove the majority of the board of directors</a>
															      <div style="display:none" id="own_share2">
																  <ul>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has the right to appoint or remove the majority of the board of directors of the company"> The person has the right to appoint or remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company"> The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company"> The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company</li>
															     </ul>
																	</div>																 
															    </li>
															    
															    <li><a onclick="showHideFinal('own_share3','own_share2','own_share1','own_share')">Has significant influence or control</a>
															      <div style="display:none" id="own_share3">
																  <ul>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has significant influence or control over the company"> The person has significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company"> The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company"> The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company</li>
															     </ul>
																</div>
															    </li>
															  </ul> 
														</div>
													</div>
												  </span>
												</div>
											</div> <!-- PANEL ENDS -->
											
											
											
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="collapsed">Person</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Title:</label> 
															<select class="form-control" name="ltd_secretary[Secretary][secretary_title]">
															<option value="Mr">Mr</option>
															<option value="Mrs">Mrs</option>
															<option value="Miss">Miss</option>
															<option value="Ms">Ms</option>
															<option value="Dr">Dr</option>
															<option value="Prof">Prof</option>
															<option value="Master">Master</option>
															<option value="Sir">Sir</option>
															<option value="Lord">Lord</option>
															<option value="Rev">Rev</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="hidden" name="ltd_secretary[Secretary][secretary_is_status]" id="secretary_is_status" value="1">
															<input class="form-control" type="hidden" name="ltd_secretary[Secretary][secretary_edit_id]" id="secretary_edit_id" value="0">
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_first_name]" id="secretary_first_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Middle name:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_middle_name]" id="secretary_middle_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_last_name]" id="secretary_last_name">
														</div>
													</div>
												</span>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="collapsed">Prefill</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Prefill Address:</label> 
															<select class="form-control" name="ltd_secretary[Secretary][secretary_pre_address]" id="select_secretary_pre_adrress">
															<option value="">-------------Select-------------  </option>
															<option value="1">1 St Saviours Wharf, 23 Mill Street, London, SE1 2BE</option>
															<option value="2">40 Bloomsbury Way, Lower Ground Floor, London SE1 2BE</option>
															<option value="3">101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
															</select>
														</div>
													</div>
												</span>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="collapsed">Service Address</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
		
												<span class="form-inline" role="form">
													<div class="panel-body">
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_name]" id="secretary_building_name">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_street]" id="secretary_building_street">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_address3]" id="secretary_pre_address">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_town]" id="secretary_building_town">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_county]" id="secretary_building_county">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_postcode]" id="secretary_building_postcode">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_country]" id="secretary_building_country">
														</div>
													</div>
												</span>
											</div>
										</div>
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="collapsed">Consent to act</a>
												</h4>
											</div>
											<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
													<div class="form-group col-sm-12 box">
															<label class="checkbox"> <input type="checkbox" class="required" name="ltd_secretary[Secretary][secretary_subscription_confirm]">
															 <span>The subscribers (shareholders) confirm that the person named has consented to
															 act as secretary</span>
															</label>
														</div>
													</div>
												 </span>
												</div>
										</div>
										
										<div class="panel panel-default">
										   <div class="panel-heading">
												<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="false" class="collapsed">Memorandum &amp; Articles</a>
												</h4>
										   </div>
										   <div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
													 <div class="panel-body">
														  <div class="form-group col-sm-12 col-xs-12 box">
															   <label for="Title ">Type:</label> 
															   <label class="radio"> <input type="radio" name="ltd_memorandum[memorandum][memorandum_type]" value="Use Standard Memorandum &amp; Articles">
																<span>Use Standard Memorandum &amp; Articles</span>
															   </label>
															   <label class="radio"> <input type="radio" name="ltd_memorandum[memorandum][memorandum_type]" value="Upload Custom Memorandum &amp; Articles" onclick="hideForm();">
																<span>Upload Custom Memorandum &amp; Articles</span>
															   </label>
														  </div>
													 </div>
												</span>
										   </div>
										</div>
										<div class="col-md-12">
											<button type="submit" class="btn btn-primary pull-left" onclick="">Update</button>
											<button type="submit" class="btn btn-primary pull-right" onclick="">Cancel</button>
										</div>
							</div>
						</div>
					</form>
				</div>
    </div>
     </div>
	<?php $this->load->view ('footer');?>
  
