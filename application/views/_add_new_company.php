<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/left_nav');?>
 <div class="company-container company_manager">
        <div class="container">
		<form  action="https://secure.wp3.rbsworldpay.com/wcc/purchase"
					method="POST" id="my-payment-form" name="my-payment-form">
			<div class="row select_comp_info">
				<div class="col-sm-12 head_title">
					<div class="new_title_row">
						<h4 class="panel-title">Add a New Company</h4>
						<p class="panel-txt">Choose the type of company and the service required</p>
						<span class="fa fa-caret-down"></span>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="title-default">
						Company Type
					</div>
				</div>
				<div class="col-sm-12">
					<div class="row radio_btn_style over_hover">
						<div class="col-sm-4">
							<div class="new_title_row">
								<h4 class="panel-title">Limited Companies Only</h4>
								<p class="panel-txt">Import Company House data using your company number</p>
								<span class="fa fa-caret-down"></span>
							</div>
							<div class="form-group" id="reg_com_1" onclick="company_info('company_info_div','hide_search_box','reg_com_1');">
								<input name="radioGroup1" id="radio11" value="option1" type="radio">
								<ul>
									<li>My company is already registered.</li>
								</ul>
							</div>
						</div> 
						<div class="col-sm-4">
							<div class="new_title_row">
									<h4 class="panel-title">New Limited Company</h4>
									<p class="panel-txt">Tell us the name of your new company.</p>
									<span class="fa fa-caret-down"></span>
							</div>
							<div class="form-group" id="reg_com_2" onclick="company_info('company_info_div','search_box','reg_com_2');">
								<input name="radioGroup1" id="radio12" value="option2" type="radio">
								<ul>
									<li >My Company is not yet registered.</li>
								</ul>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="new_title_row">
								<h4 class="panel-title">International or Sole Traders</h4>
								<p class="panel-txt">Use this option if you are not a UK Limited company</p>
								<span class="fa fa-caret-down"></span>
							</div>
							<div class="form-group" id="reg_com_3" onclick="company_info('company_info_div','search_box','reg_com_3');">
								<input name="radioGroup1" id="radio13" value="option3" type="radio">
								<ul>
									<li>My Company is not a UK Limited Company.</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row custom_marign" id="company_info_div" style="display:none"> 
						<div class="form-group col-sm-6 col-xs-12 box">
							<label for="First Name">Company Name</label>
							<input type="text" class="form-control" id="company_name" name="company_name">
						</div>
						<div class="form-group col-sm-6 col-xs-12 box search_box" id="search_box">
							<label for="First Name">Company Number</label>
							<input type="text" class="form-control" id="cmp_no" name="cmp_no">
							<div class="search__it" onclick="xml_import_data();">
								<input type="button">
								<i class="fa fa-search"></i>
							</div>
						</div>
						<div class="form-group col-sm-6 col-xs-12 box">
							<label for="First Name">Trading Name</label>
							<input type="text" class="form-control" id="trading_name" name="trading_name">
							<input type="hidden" class="form-control" id="import_yes" name="import_yes">
							<input type="hidden" class="form-control" id="fomation_vlaue" name="fomation_vlaue" value="">
						</div>
					</div>
				</div>
            </div>
            <div class="row">
				<div class="col-sm-12">
					<div class="title-default">
						Location Required
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio1" value="option1" type="radio" onclick="locationReq('radio1','first_map');">
						<ul>
							<li><b>London (Westend) Office</b></li>
							<li>85 Great Portland Street</li>
							<li>First Floor</li>
							<li>London</li>
							<li>W1W 7LT</li>
						</ul>
					</div>
					<div class="map_box" id="first_map" >
						<iframe onclick="locationReq('radio1','first_map');" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.7118975277845!2d-0.14439238417189904!3d51.51850137963695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ad59616a0d3%3A0x8028b376bec1fbe7!2s85+Great+Portland+St%2C+Marylebone%2C+London+W1W+7LT%2C+UK!5e0!3m2!1sen!2sin!4v1516599591109" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio5" value="option1" type="radio" onclick="locationReq('radio5','fifth_map');">
						<ul>
							<li><b>London (City) Office</b></li>
							<li>63/66 Hatton Garden</li>
							<li>Fifth Floor, Suite 23</li>
							<li>London</li>
							<li>EC1N 8LE</li>
						</ul>
					</div>
					<div class="map_box" id="fifth_map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.5771051764245!2d-0.11101908394354211!3d51.520974079637305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b4e892ff33f%3A0x6693879933cb6f9f!2s63-66+Hatton+Garden%2C+London+EC1N+8LE%2C+UK!5e0!3m2!1sen!2sin!4v1524648153054" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio2" value="option1" type="radio" onclick="locationReq('radio2','second_map');">
						<ul>
							<li><b>London (WC1) Office</b></li>
							<li>40 Bloomsbury Way</li>
							<li>Lower Ground Floor</li>
							<li>London</li>
							<li>WC1A 2SE</li>
						</ul>
					</div>
					<div class="map_box" id="second_map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.7667409427318!2d-0.12717908394367602!3d51.517495279636776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b330a18b48b%3A0xba975c063662ce3d!2s40+Bloomsbury+Way%2C+London+WC1A+2SE%2C+UK!5e0!3m2!1sen!2sin!4v1524647963434" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio6" value="option1" type="radio" onclick="locationReq('radio6','sixth_map');">
						<ul>
							<li><b>Dublin (Central) Office</b></li>
							<li>45 Dawson Street</li>
							<li>Dublin 2</li>
							<li style="opacity:0">empty</li>
							<li style="opacity:0">empty</li>
							
							
						</ul>
					</div>
					<div class="map_box" id="sixth_map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2382.122937746805!2d-6.260700384636144!3d53.341055479977925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48670e9bc2775a89%3A0x2e919f8799ce9cc!2s45+Dawson+St%2C+Dublin+2%2C+D02+VP62%2C+Ireland!5e0!3m2!1sen!2sin!4v1523862902591" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio3" value="option1" type="radio" onclick="locationReq('radio3','third_map');">
						<ul>
							<li><b>Edinburgh (New Town) Office</b></li>
							<li>64a Cumberland Street</li>
							<li>Edinburgh</li>
							<li>EH3 6RE</li>
						</ul>
					</div>
					<div class="map_box" id="third_map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2233.4859787294326!2d-3.2047124838042027!3d55.95828058060881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4887c7945f63a1f3%3A0x38f1c6c0c944ee20!2s64A+Cumberland+St%2C+Edinburgh+EH3+6RE%2C+UK!5e0!3m2!1sen!2sin!4v1524648222238" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<div class="col-sm-6 map_outer_box">
					<div class="address_box">
						<input name="radioGroup" id="radio4" value="option1" type="radio" onclick="locationReq('radio4','fourth_map');">
						<ul>
							<li><b>Edinburgh (Central) Office</b></li>
							<li>1O1 Rose Street Lane</li>
							<li>Edinburgh</li>
							<li>EH2 3JG</li>
						</ul>
					</div>
					<div class="map_box" id="fourth_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2233.839425635838!2d-3.2013656838044047!3d55.95215498060719!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4887c790cd77a88d%3A0xfda7bd57dd88aa38!2s101+Rose+St+S+Ln%2C+Edinburgh+EH2+3JG%2C+UK!5e0!3m2!1sen!2sin!4v1524648328755" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
            </div>
			<input type="hidden"  id="free_formation_include2" name="free_formation_include2">
			<input type="hidden"  id="loctn_req" value="" name="loctn_req">
			<input type="hidden"  id="free_formation_include4" name="free_formation_include4">
			<?php $user_id = $this->session->userdata('logged_in');  ?>
			<input type="hidden"  id="user_id" value="<?php echo  $user_id['id'];?>" name="user_id">
			<div class="row select_add_serv">
				<div class="col-sm-12">
					<div class="title-default">
						Address Services
					</div>
				</div>
				<div class="col-sm-3 div"  id="box_button110">
				
					<a class="box_button" id="box_button1" onclick="product_div('Registered Office Address','box_button1','first_item','product_name1','1');">
						
					<h3 style="font-size:26px;" id="first_item">&pound;29.99</h3>
					<hr>
					
					<p>Registered Office Address</p>
					<input type="hidden"  id="product_price1" value="29.99" name="product_price">
					<input type="hidden"  id="product_name1" name="product_name">	
					<input type="hidden"  id="subscription_time1" name="subscription_time" value="1 Year Subscription">
					</a>
					<select class="form-control" name="select" id="select1" onchange="select_service('select1','first_item','product_price1','subscription_time1');">
						<option value="29.99">1 Year Subscription</option>
					</select>
										
				</div>
				<div class="col-sm-3 div" id="box_button111">
					<a class="box_button" id="box_button2" onclick="product_div('Registered Office and Director Service Address','box_button2','second_item','product_name2','2');">
					
					<h3 style="font-size:26px;" id="second_item">&pound;39.99</h3>
					<hr>
					<p>Registered Office and Director Service Address</p>
					<input type="hidden"  id="product_price2" value="39.99" name="product_price">
					<input type="hidden"   id="product_name2" value="" name="product_name">
					<input type="hidden"  id="subscription_time2" value="1 Year Subscription" name="subscription_time">	
					
					</a>
					<select class="form-control" name="select" id="select2" onchange="select_service('select2','second_item','product_price2','subscription_time2');">
						<option value="39.99">1 Year Subscription</option>
					</select>
										
				</div>
				<div class="col-sm-3 div" id="box_button112">
					<a class="box_button" id="box_button3" onclick="product_div('Virtual Business Address','box_button3','third_item','product_name3','3');">
					<h3 style="font-size:26px;" id="third_item">&pound;99.99</h3>
					<hr>
					<p>Virtual Business Address</p>
					<input type="hidden"  id="product_price3" value="99.99" name="product_price">
					<input type="hidden"  id="product_name3" name="product_name">
					<input type="hidden"  id="subscription_time3" value="1 Year Subscription" name="subscription_time">
					</a>	
					<select class="form-control" name="select" id="select3" onchange="select_service('select3','third_item','product_price3','subscription_time3');" >
						<option value="99.99#1 Year Subscription">1 Year Subscription</option>
						<option value="69.99#6 Months Subscription">6 Months Subscription</option>
						<option value="39.99#3 Months Subscription">3 Months Subscription</option>
						<option value="19.99#1 Month Subscription">1 Month Subscription</option>
					</select>
					
							
				</div>
				<div class="col-sm-3 div" id="box_button113">
					<a class="box_button" id="box_button4" onclick="product_div('Virtual Business Plus','box_button4','fourth_item','product_name4','4');">
					
					<h3 style="font-size:26px;" id="fourth_item">&pound;129.99</h3>
					<hr>
					<p>Virtual Business Plus</p>
					<input type="hidden"  id="product_price4" value="129.99" name="product_price">	
					<input type="hidden"  id="product_name4" name="product_name">	
					<input type="hidden"  id="subscription_time4" value="1 Year Subscription" name="subscription_time">	
					
					</a><br>
					<select class="form-control" name="select" id="select4" onchange="select_service('select4','fourth_item','product_price4','subscription_time4');">
						<option value="129.99#1 Year Subscription">1 Year Subscription</option>
						<option value="79.99#6 Months Subscription">6 Months Subscription</option>
						<option value="49.99#3 Months Subscription">3 Months Subscription</option>
						<option value="24.99#1 Month Subscription">1 Month Subscription</option>
					</select>	
							
				</div>
				<div id="company_formation" style="display:none">
				<div class="col-sm-12 title_txt">Include a free company formation</div>
				<div class="col-sm-3"  onclick="include_com_formation('include_yes');">
					<span class="box_button" id="include_yes">
						<h3 class="btn_types">Yes</h3>
					</span>
				</div>
				<div class="col-sm-3"  onclick="include_com_formation('include_no');">
					<span class="box_button" id="include_no">
						<h3 class="btn_types">No</h3>
					</span>
				</div>
				</div>
				<div id="company_formation1" style="display:none">
				<div class="col-sm-12 title_txt">Include a free company formation</div>
				<div class="col-sm-3"  onclick="include_com_formation('include_yes1');">
					<span class="box_button" id="include_yes1">
						<h3 class="btn_types">Yes</h3>
					</span>
				</div>
				<div class="col-sm-3"  onclick="include_com_formation('include_no1');">
					<span class="box_button" id="include_no1">
						<h3 class="btn_types">No</h3>
					</span>
				</div>
				</div>
			</div>
			<div class="row select_add_serv">
				<div class="col-sm-12">
					<div class="title-default">
						Telephone Answering and Website Hosting
					</div>
				</div>
				<div class="col-sm-6">
					<a class="box_button" id="box_button5" onclick="product_div('Telephone Answering Service','box_button5','fifth_item','product_name5','5');">
						<h3 style="font-size:26px;" id="fifth_item">£129.99</h3>
						<hr>
						<h3 class="txt_types">Include a telephone answering service</h3>
						<input type="hidden"  id="product_price5" value="129.99" name="product_price">	
						<input type="hidden"  id="product_name5" name="product_name">	
						<input type="hidden"  id="subscription_time5" value="1 Year Subscription" name="subscription_time">	
						<input type="hidden"  id="free_formation_include5" name="free_formation_include5">
					</a>
					<select class="form-control" name="select" id="select5" onchange="select_service('select5','fifth_item','product_price5','subscription_time5');">
						<option value="129.99#1 Year Subscription">1 Year Subscription</option>
						<option value="79.99#6 Months Subscription">6 Months Subscription</option>
						<option value="49.99#3 Months Subscription">3 Months Subscription</option>
						<option value="24.99#1 Month Subscription">1 Month Subscription</option>
					</select>
						
				</div>
				<div class="col-sm-6">  
					<a class="box_button" id="box_button6" onclick="product_div('Website Hosting','box_button6','sixth_item','product_name14','0');">
						<h3 style="font-size:26px;" id="sixth_item">£99.99</h3>
						<hr>
						<h3 class="txt_types">Include website hosting with my order</h3>
						<input type="hidden"  id="product_name14" name="product_name"> 
						<input type="hidden"  id="product_price14" value="99.99" name="product_price">
						<input type="hidden"  id="subscription_time14" value="website hosting" name="subscription_time">							
					</a>
				</div>
			</div>
			<div class="row select_add_serv">
				<div class="col-sm-12">
					<div class="title-default">
						Combined Packages
					</div>
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button7" onclick="product_div('Virtual Business Address & Telephone Answering service','box_button7','seventh_item','product_name6','6');">
					<h3 style="font-size:26px;" id="seventh_item">&pound;179.99</h3>
					<hr>
					<p>Virtual Business Address<br>+<br>Telephone Answering service</p>
					<input type="hidden"  id="product_price6" value="179.99" name="product_price">	
					<input type="hidden"  id="product_name6" name="product_name"> 	
					<input type="hidden"  id="subscription_time6" value="1 Year Subscription" name="subscription_time">	
					<input type="hidden"  id="free_formation_include6" name="free_formation_include6">
					</a>
					<select class="form-control" name="select" id="select6" onchange="select_service('select6','seventh_item','product_price6','subscription_time6');">
							<option value="179.99#1 Year Subscription">1 Year Subscription</option>
							<option value="109.99#6 Months Subscription">6 Months Subscription</option>
							<option value="69.99#3 Months Subscription">3 Months Subscription</option>
							<option value="34.99#1 Month Subscription">1 Month Subscription</option>
					</select>			
							
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button8" onclick="product_div('Virtual Business Plus & Telephone Answering service','box_button8','eight_item','product_name7','7');">
					<h3 style="font-size:26px;" id="eight_item">&pound;199.99</h3>
					<hr>
					<p>Virtual Business Plus<br>+<br>Telephone Answering service</p>
					<input type="hidden"  id="product_price7" value="199.99" name="product_price">	
					<input type="hidden"  id="product_name7" name="product_name">	
					<input type="hidden"  id="subscription_time7" value="1 Year Subscription" name="subscription_time">	
					<input type="hidden"  id="free_formation_include7" name="free_formation_include7">
					</a>
					<select class="form-control" name="select" id="select7" onchange="select_service('select7','eight_item','product_price7','subscription_time7');">
						<option value="199.99#1 Year Subscription">1 Year Subscription</option>
						<option value="129.99#6 Months Subscription">6 Months Subscription</option>
						<option value="79.99#3 Months Subscription">3 Months Subscription</option>
						<option value="39.99#1 Month Subscription">1 Month Subscription</option>
					</select>				
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button9" onclick="product_div('Virtual Business Plus & Website Hosting','box_button9','ninth_item','product_name8','8');">
					<h3 style="font-size:26px;" id="ninth_item">&pound;199.99</h3>
					<hr>
					<p>Virtual Business Plus<br>+<br>Website Hosting</p>
					<input type="hidden"  id="product_price8" value="199.99" name="product_price">	
					<input type="hidden"  id="product_name8" name="product_name">	
					<input type="hidden"  id="subscription_time8" value="1 Year Subscription" name="subscription_time">	
					<input type="hidden"  id="free_formation_include8" name="free_formation_include8">	
					</a>
					<select class="form-control" name="select" id="select8" onchange="select_service('select8','ninth_item','product_price8','subscription_time8');">
						<option value="199.99#1 Year Subscription">1 Year Subscription</option>
						<option value="129.99#6 Months Subscription">6 Months Subscription</option>
						<option value="79.99#3 Months Subscription">3 Months Subscription</option>
						<option value="39.99#1 Month Subscription">1 Month Subscription</option>
					</select>		
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button10" onclick="product_div('Virtual Business Plus & Legal Document Pack','box_button10','tenth_item','product_name9','9');">
					<h3 style="font-size:26px;" id="tenth_item">&pound;199.99</h3>
					<hr>
					<p>Virtual Business Plus<br>+<br>Legal Document Pack</p>
					<input type="hidden"  id="product_price9" value="199.99" name="product_price">
					<input type="hidden"  id="product_name9" name="product_name">
					<input type="hidden"  id="subscription_time9" value="1 Year Subscription" name="subscription_time">
					</a>
					<select class="form-control" name="select" id="select9" onchange="select_service('select9','tenth_item','product_price9','subscription_time9');">
						<option value="199.99#1 Year Subscription">1 Year Subscription</option>
						</select>

				</div>
			</div>
			<div class="row select_add_serv">
				<div class="col-sm-12">
					<div class="title-default">
						Additional Services
					</div>
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button11" onclick="product_div('Company Register','box_button11','11_item','product_name10','0');">
					<h3 style="font-size:26px;" id="11_item">&pound;24.99</h3>
					<hr>
						<p class="txt_types">Company Register</p>
					<input type="hidden"  id="product_price10" value="24.99" name="product_price">
					<input type="hidden"  id="product_name10" name="product_name">
					<input type="hidden"  id="subscription_time10" value="1 Year Subscription" name="subscription_time">
					</a>
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button12" onclick="product_div('Certificate of Good Standing (Standard Service)','box_button12','12_item','product_name11','0');">
					<h3 style="font-size:26px;" id="12_item">&pound;39.99</h3>
					<hr>
						<p class="txt_types">Certificate of Good Standing</p>
					<input type="hidden"  id="product_price11" value="39.99" name="product_price">
					<input type="hidden"  id="product_name11" name="product_name" >
					<input type="hidden"  id="subscription_time11" value="1 Year Service" name="subscription_time">
					</a>
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button13" onclick="product_div('Apostille Document Service','box_button13','13_item','product_name12','0');">
					<h3 style="font-size:26px;" id="13_item">&pound;124.99</h3>
					<hr>
						<p class="txt_types">International Document Pack</p>
					<input type="hidden"  id="product_price12" value="124.99" name="product_price">
					<input type="hidden"  id="product_name12" name="product_name">
					<input type="hidden"  id="subscription_time12" value="Full set of company documents" name="subscription_time">
					</a>
				</div>
				<div class="col-sm-3">
					<a class="box_button" id="box_button14" onclick="product_div('Certificate of Good Standing with Apostille (Standard Service)','box_button14','14_item','product_name13','0');">
					<h3 style="font-size:26px;" id="14_item">&pound;89.99</h3>
					<hr>
						<p class="txt_types">Certificate of Good Standing with Apostille</p>
					<input type="hidden"  id="product_price13" value="89.99" name="product_price">
					<input type="hidden"  id="product_name13" name="product_name">
					<input type="hidden"  id="subscription_time13" value="1 Year Service" name="subscription_time">
					</a>
				</div>
			</div>
			
			<div class="row select_add_serv">
				<div class="col-sm-12">
					<div class="title-default">
						Checkout Securely With WorldPay
					</div>
				</div>
				<div class="col-sm-4">
					<a class="box_pay">
						<label class="checkbox checkbox-mailingaddress"> 
							<input class="required" type="checkbox" id="agree_tnc" name="agree_tnc" value="I agree to Terms and Conditions"> 
							<span>I agree to <a href="#" data-target="#myModal_terms" data-toggle="modal">Terms and Conditions.</a></span>
						</label>
					</a>
				</div>
				<div class="col-sm-3">
					<a class="box_pay">
						<img src="https://thelondonoffice.com/beta/assets/images/credit-card-icons.png">
					</a>
				</div>
				<input type="hidden" name="instId" value="1068922" id="instId">
				<input type="hidden" name="cartId" value="abc123" id="cartId">
				<input type="hidden" name="currency" value="GBP">
				<input type="hidden" name="amount" id="amount" value="" >
				<input type="hidden" name="testMode" value="0"> 
				<input type="hidden" name="name" value="AUTHORISED">
				<input type="hidden" name="cost_amt" id="cost_amt" value="">
				<input type="hidden" name="deposite" id="deposite" value="">
				<input type="hidden" name="return_url"
				value="https://theoffice.support/admin/home/new_company">
				<div class="col-sm-5 button_selection">
					<a class="box_pay">
						<div class="total-amt last-btn">
							<select class="form-control" name="select" id="select_pay_option" onchange="select_payment_option('new');" >
								<option value="1" id="pay_1">Pay £0.00 securely online</option>
								<option value="2" id="pay_2">Pay £0.00 by Invoice / bank transfer</option>
							</select>
							<div id="change_button">
								<i class="fa fa-lock"></i>
								<input type="button" name="check_out" id="place_order" onclick="order_service();" class="btn check_out" value="Checkout Securely">
							</div>
							<span id="total_pric" style="display:none"></span>
						</div>
					</a>
				</div>
			</div>	
			</form>			
        </div>
    </div>
	<div class="modal fade" id="myModal_ltd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body popup-body txt-value">
			<h5>A £20.00 postal deposit will be added to cover mailing costs.</h5>
			<a onclick="modal_dismiss();" class="btn btn-primary">That’s OK</a>
		  </div>
		</div>
	  </div>
	</div>
<script src="<?= base_url(); ?>/assets/js/company.js"></script>
<?php $this->load->view('modal');?>
<?php $this->load->view('footer');?>