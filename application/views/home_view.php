<?php //echo "<pre>"; print_r($user_orders); die('testing'); ?>

<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/left_nav');?>
<?php $uri_test = $this->uri->segment(2); 
$uri = $_SERVER['REQUEST_URI'];?>
<?php $session_data = $this->session->userdata('logged_in');
$role_id = $session_data['role_id'];
?>
<section id="content_info" <?php (($uri_test == "searchResult") ? "echo class='top_content_info'" : "echo class =''"); ?>>
<?php  $url_renew = $this->uri->segment(2);
	if($message){
		 echo '<script type="text/javascript">';
		 echo 'setTimeout(function () { swal("Document Uploaded");';
		 echo '}, 2000);</script>';
	} 
?>
<?php $this->load->view ('includes/search_bar');?>
<div class="loader_background" id="loader_background" style="display:none;">
	<div class="loader" id="loader">
	</div>
</div>
<?php if( $this->uri->segment(2) == "searchResult" ||  $this->uri->segment(2) == "renewalRecordsNew"){ ?>
<div class="diector_view company__results"><span>Company Results<span></span></span></div>
<?php }?> 
  <div class="table_data homedata">   
	<table class="table table-hover table-bordered">
	
	<?php	if($role_id === '0' || $role_id === '2'){ ?>
		<thead class="home-table-header static-header">
	<?php 	}else{	?>
		<thead class="home-table-header">
	<?php 	}	 ?>
		<tr>			
			<th class="cmpny th_company_class">
            <?php 
 				$total =  $this->uri->segment(3)+1; 
				if($total<>1){
					$totaldss = $this->uri->segment(3)+$pagef;
				}else{
					$totaldss = $pagef; 
				}
				if($totaldss > $record_count){
					$totaldss = $record_count;
				}
 				if($this->pagination->create_links()){
			?>
					 Company(<?php echo $total.'-'.$totaldss; ?> of <?php echo $record_count;?>)
              <?php } else{ ?>
               		 Company(<?php echo $record_count;?>)
              <?php	 }  ?>
			</th>
			<?php $services =  $this->search->getServiceOptions();
				if($role_id === '1' || $role_id === '2'){
			?>		
			<th class="reseller-head th_reseller_class">
			<form action="<?php echo base_url();?>home/reseller_change" method="POST" name="reseller_form_change" id="reseller_form_change">
			<?php $resellers =  $this->search->resellers();?>
			  <select id="reseller_change" name="reseller_change">
			  <option>Reseller</option>
			  <option value="Show All">Show All</option>
			  <?php foreach($resellers as $reseller){?>
			    <option value="<?php echo $reseller->id;?>"><?php echo $reseller->company_name;?></option>
			   <?php }?>
			  </select>	
			    </form>
			</th>
			<?php } ?>	 
			<th class="th_state_class">
			<form action="<?php echo base_url();?>home/state_change1" method="GET" name="state_form_change" id="state_form_change">
			<input type="hidden" id="query_string_1" name="query_string_1" value="<?php echo $_GET['new_search_bar']?>">
 			<input type="hidden" id="query_string_2" name="query_string_2" value="<?php echo $_GET['search_new']?>">
			<?php $states =  $this->search->getStatusOptions();?>
			 <select id="state_change" name="state_change">
			    <option>Status</option>
				<option value="Show All">Show All</option>
			      <?php foreach($states as $key =>$state){	?>
			    <option value="<?php echo $key ;?>"><?php echo $state;?></option>
			   <?php }	?>
			  </select>
			  </form>
			</th>
			<th>
			     <form action="<?php echo base_url();?>home/access_list" method="POST" name="acess_list" id="acess_list">
						<?php $acess =  $this->order->getacessOptions();?>
						<select id="asess_form" name="asess_form" class="getSelect1 asessform">
							<option>Access</option>
							<?php  foreach($acess as $key=>$acess){?>
								<option value="<?php echo $key;?>"><?php echo $acess;?></option>
							<?php }?>
						</select>
						
				  </form>
			</th>
			<?php if($url_renew =="new_formation"){ ?>
			<th  class="th_renew_class">
			    Submitted
			</th>
			<?php }  ?>
			<th onClick="getDateOtions()" class="th_renew_class">
				Renew
			</th>
			<th class="th_location_class">
 					<form action="<?php echo base_url();?>home/filterSearch" method="POST" name="location" id="location_select">
						<?php $locations =  $this->search->getLocationOptions();?>
						<select id="location_select_option" name="location_select" class="getSelect1 locClick">
								<option>Office</option>
							<?php  foreach($locations as $location){?>
								<option value="<?php echo $location;?>"><?php echo $location;?></option>
							<?php }?>
						</select>
				  </form>
			</th>
			<?php if($url_renew == "renewable_new" || $url_renew == "renewalRecordsNew"){ ?>
			 <th class="th_ltd th_reg_class">CH (A)</th>
			 <th class="th_ltd th_reg_class">CH (S)</th>
			 <?php } else { ?>
			<th class="th_ltd th_reg_class">
				<form action="<?php echo base_url();?>home/ltdSearch" method="POST" name="comp_ltd" id="comp_ltd">
						<?php $ltds =  $this->order->getLtdOptions();?>
						<select id="comp_ltd_select_option" name="comp_ltd_select" class="getSelect1 ltdClick">
							<option>LTD</option>
							<?php  foreach($ltds as $key=>$ltd){?>
								<option value="<?php echo $key;?>"><?php echo $ltd;?></option>
			 				<?php }?>
						</select>
				  </form> 
			</th>
			<?php  } ?>
			<th class="th_reg_class">
			<form action="<?php echo base_url();?>home/pdf_form" method="POST" name="pdf_form_list" id="pdf_form_list">
						<?php $pdf =  $this->order->getpdfOptions();?>
						<select id="pdf_form_id" name="pdf_form_id" class="pdf_form">
							<option>PDF+</option>
							<?php  foreach($pdf as $key=>$pdf){?>
								<option value="<?php echo $key;?>"><?php echo $pdf;?></option>
							<?php }?>
						</select>
				  </form>
			</th>
			<th class="th_reg_class">
			<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="registered_office" id="registered_office_select">
				<select id="registered_office" name="registered_office" class="getSelect1 regClick">
						<option>ROA</option>
				   <?php  foreach($services as $service){?>
						<option value="<?php echo $service;?>"><?php echo $service;?></option>
					<?php }?>
			  </select>
			  </form>
			</th>
			<th class="th_director_class">
				<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="director_service_address" id="director_service_address_select">
					<select id="director_service_address"  name="director_service_address" class="getSelect1 dirClick">
					<option>DSA</option>
					<?php foreach($services as $service){?>
						<option value="<?php echo $service;?>"><?php echo $service;?></option>
					<?php }?>
					</select>
				</form>
			</th>
			<th class="th_business_class">
				<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="business_address" id="business_address_select">
				<select id="business_address" name="business_address" class="getSelect1 busClick">
			    <option>VBA</option>
				<?php foreach($services as $service){?>
					<option value="<?php echo $service;?>"><?php echo $service;?></option>
				<?php }?>
				</select>
				</form>
			</th>
			<th class="th_tele_class">
			  <form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="telephone_service" id="telephone_service_select">
				<select id="telephone_service" name="telephone_service" class="getSelect1 telClick">
						<option>TAS</option>
					<?php foreach($services as $service){?>
						<option value="<?php echo $service;?>"><?php echo $service;?></option>
					<?php }?>
			   
			  </select>
			  </form>
			</th>
			<?php if($role_id === '1' || $role_id === '2'){ ?>
			<th class="th_billing_class" width="80">
				<form action="<?php echo base_url();?>home/billing_change" method="POST" name="type_form_change" id="type_form_change">
				<?php $types =  $this->order->getTypeOptions();?>
					<select id="type_change" name="type_change">
						<option>Payment</option>
						<option value="Show All">Show All</option>
						<?php foreach($types as $key =>$type){?>
							<option value="<?php echo $key ;?>"><?php echo $type;?></option>
						<?php }?>
				  </select>
			  </form>
			</th>
			<?php }?>
			<?php if($url_renew == "renewable_new" || $url_renew == "new_formation" || $url_renew == "renewalRecordsNew"){ ?>
			
			<?php }else{?>
				<th width="80px" class="th_diposite_class">
					Deposit			<?php //echo $url_renew; ?>  		  
				</th>
			<?php } ?>
			<th class="th_price_class">
				Charge
			</th>
			<th class="th_upload_class" width="70px">
			<form action="<?php echo base_url();?>home/filterSearch" method="POST" name="upload_form_change" id="upload_form_change">
			<?php $uploads =  $this->search->getUploadOptions();?>
			  <select id="upload_change" name="upload_change" class="getSelect1 IdClick">
			  <option>ID</option>
			  <option value="Show All">Show All</option>
			  <?php foreach($uploads as $upload){?>
			    <option value="<?php echo $upload;?>"><?php echo $upload;?></option>
			   <?php }?>
			  </select>
			</form>
			</th>
			<?php if($state_change === '4' && $role_id === '1'){ ?>
				<th class="">
					Delete Company
				</th>
			<?php } ?>
			<th class="th_upload_class" width="70px">
			<form action="<?php echo base_url();?>home/filterSearch" method="POST" name="upload_form_change" id="upload_form_change">
			<?php $uploads =  $this->search->getUploadOptions();?>
			  <select id="upload_change" name="upload_change" class="getSelect1 IdClick">
			  <option>POA</option>
			  <option value="Show All">Show All</option>
			  <?php foreach($uploads as $upload){?>
			    <option value="<?php echo $upload;?>"><?php echo $upload;?></option>
			   <?php }?>
			  </select>
			</form>
			</th>
			<th>Email</th>
			<th>Note</th>
		</tr>
		</thead>
		
		<tbody id="OrderPackages">
			<?php if($user_orders){
				$count = 0;
				$count2 = 0;
				//var_dump($user_orders);die("FSD");
				foreach ($user_orders as $key=>$user_order){
				  // $company =  $this->search->filterSearch($user_order->company_id);
				  // $user =  $this->search->userSearch($user_order->create_user_id);
				  // $update_company_user = $this->search->userSearch($user_order->create_user_id);
				  // $orders =  $this->search->orders($user_order->company_id);
				  // $files_info =  $this->search->fileInfo($user_order->create_user_id);
				  // $files_info_attach =  $this->search->files_info_attach($user_order->create_user_id);
				  // $files_info_reseller =  $this->search->fileInfo1($user_order->reseller_id);
				  // $Get_company_data_formation =  $this->search_model->Get_company_data_formation($user_order->company_id);
			?>
			<?php $postcode = $user_order->post_town; ?>		
			<?php $company_status = $user_order->company_status;?>	
			<tr id="<?php echo 'row_'.$user_order->id;?>">
			<?php 
			if($user_order->limited =="1")
			{
				$limited= "Limited";
			}
			elseif($user_order->limited == "2")
			{
				$limited= "Ltd";
			}
			elseif($user_order->limited == "3")
			{
				$limited= "Lp";
			}
			elseif($user_order->limited == "4")
			{
				$limited= "Llp";
			}
			else{
				$limited= "";
			}
			?>
				<td class = "td_company_class dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">
				<?php
				$comp_name_length = strlen($user_order->company_name ." ".$limited);
					if($comp_name_length >=10){?>
					<span class="comp_full_name">
				<?php echo stripslashes($user_order->company_name ." ".$limited);?>
				
				<?php }
				else
					echo stripslashes($user_order->company_name ." ".$limited);
					?>
					</span>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo  base_url();?>dashboard/showCompanyResult?id=<?php echo $user_order->company_id ?> ">Company Overview</a></li>
					<li><a href="javascript:void(0)" onclick ="userDetails ('<?php echo $user_order->company_id;?>')">Client Details</a></li>
					<li><a href="javascript:void(0)" onclick ="billingDetails('<?php echo $user_order->company_id;?>')">Billing Information</a></li>
					<li><a href="javascript:void(0)" onclick ="orderDetails('<?php echo $user_order->company_id;?>','<?php echo $user_order->id;?>','<?php echo $user_order->company_name;?>')">Order Information</a></li>
					<li><a href="javascript:void(0)" onclick ="Othercompanies('<?php echo $user_order->company_id;?>','<?php echo $user_order->create_user_id;?>','<?php echo $user_order->company_name;?>')">Other Companies</a></li>
					<li><a href="javascript:void(0)" onclick ="invoice('<?php echo $user_order->company_id;?>','<?php echo $user_order->create_user_id;?>','<?php echo $user_order->company_name;?>','<?php echo $files_info_attach[$count]->email_alert;?>')">INVOICE UPLOAD</a></li>
					<?php 
					$myData = array('user_id'=>$user_order->create_user_id, 'company_id'=>$user_order->company_id);
					$arg = base64_encode( json_encode($myData) );
					?>
					<li><a href="<?php echo  base_url();?>dashboard/AddNewCompany?id=<?php echo $arg; ?>">Add New Company</a></li>
				</ul>
			</td>
			<?php if($role_id === '1' || $role_id === '2'){?>		
			<td data-cname="<?php echo $user_order->company_name;?>" class="reseller td_reseller_class"  onclick = "reseller('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo($user_order->company_id);?>','<?php echo $user_order->reseller;?>')"><a href="#"><?php echo $user_order->reseller;?></a></td>			
			<td class = "state_id td_status_class" data-id="<?php echo $user_order->id;?>" data-cname="<?php echo $user_order->company_name;?>" data-content="<?php echo $user_order->company_id;?>" data-email="<?php echo $user_order->email;?>" data-currentdata="<?php echo $this->search->getStatusOptions($user_order->state_id);?>">
				<a href="#">
				<?php 
					$url = $this->uri->segment(2);
					$url1 = base_url()."home/".$url;
				?>
				<?php if($user_order->state_id=="10"){
					echo '<div class="green">';
					echo "Active";
					echo '<div>';
				}elseif($url1 == "https://www.companymanager.online/home/new_formation"){
					echo '<div class="green">';
					echo "In Review";
					echo '<div>';
				}elseif($user_order->state_id=="0" || $user_order->state_id=="9" || $user_order->state_id=="6"){
					echo '<div class="green">';
					echo $this->search->getStatusOptions($user_order->state_id);
					echo '</div>';
				}elseif($user_order->state_id=="5"){
					echo '<div class="orange">';
					echo $this->search->getStatusOptions($user_order->state_id);
					echo '</div>';
				} elseif($_GET['state_change']=="14"){
					echo '<div class="orange">';
					echo "In Review";
					echo '<div>';
				} else{
					echo '<div class="red">';
					echo $this->search->getStatusOptions($user_order->state_id);
					echo '</div>';
				}?>
				</a> 
            </td>
             <td class="" onclick =" acess_event('<?php echo $user_order->create_user_id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $user_order->Access;?>')">
			 <?php $Access = $user_order->Access; ?>
			 <a href="#">
			 <?php if($Access==0)
			 {
				    echo '<div class="red">';
					echo "NO";
					echo '</div>';
			 }
			   elseif($Access==1){
				    echo '<div class="green">';
					echo "Yes";
					echo '</div>';
			 }
			 ?></a></td>
            
            <?php if($url_renew =="new_formation"){ ?>
			<td  class="th_renew_class">
				<?php  $date_update = date ("d-m-Y",strtotime($user_order->ltd_updated_date));
					if($date_update != "30-11--0001"){
						echo $date_update;  
					}
				?>
			</td>
			<?php } ?>			
			<td  data-cname="<?php echo $user_order->company_name;?>" class = "renew_id td_renew_class" id="<?php echo $user_order->id;?>" onclick = "displayDatepicker('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $date_renewal_cur = date ("d-m-Y",strtotime($user_order->renewable_date));?>')">
				<a href="#">	
					<?php  $date_current = date ("d-m-Y");
					       $date_renewal= date ("d-m-Y",strtotime($user_order->renewable_date));
						   //$user_order->renewable_date_next
						  if(strtotime($date_renewal) > strtotime($date_current)){
							  echo '<div class="green">';
							  echo $date_renewal;
							  echo '</div>';
						  }else{
							  echo '<div class="red">';
							  echo $date_renewal;
							  echo "</div>";
						  }
					?>
				</a>					
			</td>			
			<td onclick =" location_event('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $user_order->location;?>')" class="td_location_class">
				<a href="#">
					<?php echo $user_order->location;?>
				</a>
			</td>
			<?php if($url_renew == "renewable_new" || $url_renew == "renewalRecordsNew"){ ?>
				<td><?php echo $postcode;?></td>
				<td class="import_status"><?php echo $company_status;?></td>
			<?php } else { ?>
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" >
				<?php 
				$state_id = $user_order->state_id;
				$comp_ltd = $user_order->comp_ltd;
				$state_id_new = $user_order->state_id_new;
				$ltd = $this->order->getLtdOptions($user_order->comp_ltd);
				if($state_id ==3 && $comp_ltd=="1"){
					echo "<div class='red'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id =='6' && $comp_ltd=="1" && $state_id_new =='0'){
					echo "<div class='orange'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id_new =='1' && $comp_ltd=="1" && $state_id =='6'){
					echo "<div class='green'>";
					echo $ltd;
					echo "</div>";
				} else{
					echo "<div class='red'>";
					echo $ltd;
					echo "</div>";
				} 
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="changeLtd('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $this->order->getLtdOptions($user_order->comp_ltd);?>')">Change Status</a></li>
					<li><a href="javascript:void(0)" onClick="uploadData('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $files_info_attach[$count]->email_alert;?>','<?php echo $user_order->create_user_id;?>')">Upload Data</a></li>
					<li><a href="javascript:void(0)" onClick="company_num_house('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>')">Import Data</a></li>
					<li><a href="javascript:void(0)" onClick="edit_order_pop_ltd('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>','comp_ltd','<?php echo $user_order->comp_ltd;?>')">Edit Order</a></li>
				</ul>
			</td>
			<?php } ?>
			<td>
			<?php 
			$atach = $files_info_attach->email_alert;
			if($atach == '1')
			{?>
				<div class="green">
				<a href="javascript:void(0)" onClick="emailalerts('<?php echo $user_order->create_user_id?>','<?php echo $files_info_attach[$count]->email_alert;?>','<?php echo addslashes($user_order->company_name);?>')">YES</a></div>
			<?php }
			else
			{ ?>
				<div class="red"><a href="javascript:void(0)" onClick="emailalerts('<?php echo $user_order->create_user_id?>','<?php echo $files_info_attach[$count]->email_alert;?>','<?php echo addslashes($user_order->company_name);?>')">NO</a></div>
			<?php }
			?>
			</td>
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php $ros = strtoupper($user_order->registered_office);
					if($ros == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
					 	if($fetch_preferene->officail_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $ros;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $ros;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $ros;
							echo "</div>";
						}else{
							echo "<div class='green'>";
							echo $ros;
							echo "</div>";
						} 
					}else {
						echo $ros;
					} 
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->create_user_id;?>','<?php echo addslashes($user_order->company_name);?>','omail','<?php echo $files_info_attach->email_alert;?>')">Official Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmailo('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Official Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','rosbmail')">Official Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','ros')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
					<li><a href="javascript:void(0)" onClick="edit_order_pop('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>','registered_office','<?php echo $user_order->registered_office;?>')">Edit Order</a></li>
				</ul>
			</td>			
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php
					$dsa = strtoupper($user_order->director_service_address);
					if($dsa == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
						if($fetch_preferene->officail_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $dsa;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $dsa;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $dsa;
							echo "</div>";
						}else{
							echo "<div class='green'>";
							echo $dsa;
							echo "</div>"; 
						}
					}else {
						echo $dsa;
					}
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->create_user_id;?>','<?php echo addslashes($user_order->company_name);?>','omail','<?php echo $files_info_attach[$count]->email_alert;?>')">Official Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmailo('<?php echo $user_order->companyid;?>','<?php echo addslashes($user_order->company_name);?>')">Official Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','dsabmail')">Official Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','dsa')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
					<li><a href="javascript:void(0)" onClick="edit_order_pop('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>','director_service_address','<?php echo $user_order->director_service_address;?>')">Edit Order</a></li>					
			</td>
			<td class="dropdown set_dropdown_ltd vba_row">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php
					$vba = strtoupper($user_order->business_address);
					
					if($vba == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
						if($fetch_preferene->business_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $vba;
							echo "</div>";
						}else if($fetch_preferene->business_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $vba;
							echo "</div>";
						}else if($fetch_preferene->business_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $vba;
							echo "</div>";
						}else{
							echo "<div class='orange'>";
							echo $vba;
							echo "</div>";
						}
					}else {
						echo $vba;
					}
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->create_user_id;?>','<?php echo addslashes($user_order->company_name);?>','bmail','<?php echo $files_info_attach[$count]->email_alert;?>')">Business Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Business Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','vbabmail')">Business Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','vba')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref1('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
					<li><a href="javascript:void(0)" onClick="edit_order_pop('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>','business_address','<?php echo $user_order->business_address;?>')">Edit Order</a></li>					
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd text-center">
			<?php //$select_phone_number = $this->home_model->select_phone_number($company->id);?>
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" >
				<?php $tas = strtoupper($user_order->telephone_service);
				if($tas=="YES"){
				  echo '<div class="green">';
				  echo $tas;
				  echo '</div>';
				}else{
				  echo $tas;
				}
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="message_handel('<?php echo $user_order->id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $update_company_user[$count]->email;?>','<?php echo addslashes($user_order->tas_notes);?>',' <?php echo addslashes($company->comp_info);?>')">Add Message</a></li>
					<li><a href="javascript:void(0)" onClick="cancel_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Service Cancelled</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="add_number('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->create_user_id;?>','<?php echo $select_phone_number->phone_number;?>','<?php echo $select_phone_number->name ; ?>')">Add Number</a></li>
					<li><a href="javascript:void(0)" onClick="edit_order_pop('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>','telephone_service','<?php echo $user_order->telephone_service;?>')">Edit Order</a></li>
				</ul>
			</td>
			<?php }else{?>
				<td class = "td_status_class" data-id="<?php echo $user_order->id;?>"><?php echo $this->search->getStatusOptions($user_order->state_id);?></td>
				<td  class = "renew_id td_renew_class" id="<?php echo $user_order->id;?>"><?php echo date ("d-m-Y",strtotime($user_order->renewable_date));?></td>
				<td class="td_location_class"><?php echo $user_order->location;?></td>
				<td><?php echo strtoupper($this->order->getLtdOptions($user_order->comp_ltd));?></td>
				<td class="td_reg_class"><?php echo strtoupper($user_order->registered_office);?></td>
				<td class="td_director_class"><?php echo strtoupper($user_order->director_service_address);?></td>
				<td class="td_business_class"><?php echo strtoupper($user_order->business_address);?></td>
				<td class="td_tele_class"><?php echo strtoupper($user_order->telephone_service);?></td>
			<?php }	?>
			<?php if($role_id === '1'){?>
			<td class="td_bill_class" data-id="<?php echo $user_order->id;?>" data-billing_cname="<?php echo $user_order->company_name;?>" data-cid ="<?php echo $user_order->company_id;?>" ><a href="#">
			<?php
				if($user_order->type_id == "0")
				{
					echo "Annual";
				}
				else if($user_order->type_id == '1'){
					echo "Monthly";
				}
			?></a></td>
			<?php $dot = number_format((float)$user_order->deposit, 2, '.', '');?>
			
			<?php if($url_renew == "renewable_new" || $url_renew == "renewalRecordsNew" ||$url_renew =="new_formation"){ 
			} else { ?>
			<td class="td_deposit_class" onclick ="editDiposite('<?php echo $user_order->id?>','<?php echo $dot?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>')">
			<?php }}else{ ?>
				<td class="td_deposit_class">
			<?php }?>
			
			<?php if($url_renew == "renewable_new" || $url_renew == "renewalRecordsNew" || $url_renew =="new_formation"){ 
			} else {	$dot = number_format((float)$user_order->deposit, 2, '.', '');
				echo '<a href="#">&pound;'.$dot.'</a>';
			?></td>
			<?php } ?>
			<td class="td_price_class dropdown set_dropdown_ltd"><a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true" href="javascript:void(0)">&pound;<?php 
				
                $final_order_updated = 0;			
				$dot = strstr($user_order->price, '.');
				if($final_order_updated != 0)
				{
					echo $final_order_updated;
				}
				else{
				if($dot)
				{
					echo $user_order->price;
				}
				else
				{
					echo $user_order->price.'.00';
				}
				}
				?></a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onclick="edit_price_charge('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $total_price[$count];?>')">Edit Charge</a></li>
					<li><a href="javascript:void(0)" onclick="editPrice('<?php echo $user_order->id?>','<?php echo $user_order->deposit?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>')">Request Payment</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			$file_type_resseller="";
			foreach($files_info_reseller as $file_info){
				$file_type_resseller=$file_info->reseller_id;
			}
			foreach($files_info as $file_info){
				$file_type_id[]=$file_info->type_id;
			}
			$file_type_upload1 =  in_array("4",$file_type_id);
			$file_type_upload3 =  in_array("10",$file_type_id);
			$getYes = "";
			if($file_type_resseller!="")
			//if($file_type_resseller!="" && $file_type_upload1=== true)
			{
				echo "<div class='green' title='Id Recieved'>";
				echo "YES";
				echo "</div>";
			}elseif($file_type_upload1=== true || $file_type_upload3 === true){		    
			    echo "<div class='green' title='Id Recieved'>";
				echo "YES";
				echo "</div>";
			}else{
			    echo "<div class='red' title='Id Not Recieved'>";
				echo "NO";
				echo "</div>";
			}		
			?></a>
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadId('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $reseller->id; ?>')">Upload ID</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			$file_type_resseller="";
			foreach($files_info_reseller as $file_info){
				$file_type_resseller=$file_info->reseller_id;
			}
			foreach($files_info as $file_info){
				$file_type_id[]=$file_info->type_id;
			}
			$file_type_upload2 =  in_array("5",$file_type_id);
			$file_type_upload3 =  in_array("10",$file_type_id);
			$getYes = "";
			if($file_type_resseller!=""){
			//if($file_type_resseller!="" && $file_type_upload2=== true){
				echo "<div class='green' title='Proof Address Recieved'>";
				echo "YES";
				echo "</div>";
			} else if($file_type_upload2=== true || $file_type_upload3 === true){
			    echo "<div class='green' title='Proof Address Recieved'>";
				echo "YES";
				echo "</div>";
			}else{
			    echo "<div class='red' title='Proof Address  Not Recieved'>";
				echo "NO";
				echo "</div>";
			}
			?></a>
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadId('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $reseller->id; ?>')">Upload ID</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd">
			<a data-toggle="dropdown" class="dropdown-toggle envelope_icon" aria-expanded="false" href="javascript:void(0)"><i class="fa fa-envelope" aria-hidden="true"></i></a>
			<ul class="dropdown-menu envelope_menu">
			<li><a href="mailto:<?php echo $user_order->email?>"><?php echo $user_order->email;?></a></li>
			</ul>
			
			</td>
			<td class="dropdown set_dropdown_ltd">
			<a class="dropdown-toggle envelope_icon" href="javascript:void(0)" onClick="notes_handel('<?php echo $user_order->id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $user_order->email;?>','<?php echo addslashes($user_order->notes);?>',' <?php echo addslashes($user_order->comp_info);?>',' <?php echo addslashes($user_order->create_user_id);?>')">
				<?php if($user_order->notes){?><div class="chat_hover"><i class="fa fa-commenting-o active" aria-hidden="true"></i><span><?php echo $user_order->notes; ?></span></div>
				
				<?php } else{ ?>
					<i class="fa fa-commenting-o active1" aria-hidden="true"></i>
				<?php }?>
			</a>
			</td>
			<?php if($state_change === '4' && $role_id === '1'){?>
				<td><a href="<?php echo base_url().'home/deleteCompany/'.$user_order->company_id;?>" class="btn btn-danger">Delete</a></td>
			<?php } ?>
		</tr>		
		<?php $count++;}
	}?>
		</tbody>
	</table>
</div>
<?php 	if($this->pagination->create_links()){		
	$class="scroll_set";?>
<?php 	}else{
$class= "scroll_set";}		?>

<div class="<?php echo $class; ?>">	 
	 <div class="row pagination-btm" style="float:right;margin:0;">
       <?php if($this->pagination->create_links()){ ?>
     <div  style="float:left; color:#303030">
     <?php 
			$url =  $_SERVER['REQUEST_URI']; 
			$urlexp = explode('/',$url);
			$data_posted= urldecode($_SERVER['QUERY_STRING']);
			$pieces = explode("&", $data_posted);
			
			for($a=0;$a<count($pieces);$a++){
				$profile_key=strstr($pieces[$a],"=",true);
				$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
			}
 	 ?>
	 <?php //echo $this->uri->segment();?> 
    <form method="get" action="<?php echo $uri;?>" id="formElementId">
	<?php	foreach($profile as $key=>$val){	?>
		<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
	<?php }	?>
    <select name="page"  id="selectElementId"> 
        <option value="10"<?php if($pagef=='10'){ echo 'selected="selected"';} ?>>10 Items</option>
    	<option value="25"<?php if($pagef=='25'){ echo 'selected="selected"';} ?>>25 Items</option>
        <option value="50"<?php if($pagef=='50'){ echo 'selected="selected"';} ?>>50 Items</option>
        <option value="100"<?php if($pagef=='100'){ echo 'selected="selected"';} ?>>100 Items</option>
    </select> 
    </form> 
    </div>
<?php 	echo $this->pagination->create_links();		}	?>
  </div>
 </section>
	<?php if( $this->uri->segment(2) == "searchResult" ||$this->uri->segment(2) == "proof_upload" ||$this->uri->segment(2) == "do_upload" || $this->uri->segment(2) == "allusercompanies"){ 
		 $this->load->view('director_view');	
		$this->load->view('trading_view');	
		 $this->load->view('Company_House_Import');
	 } ?>
<script>
	$('#selectElementId').change(function(){ 
	//alert("hdc");
		$('#formElementId').submit();
    });
	
	function download_doc(file_name,userid){
		$.ajax({
			'url' : base_url+"home/download",
			'type' : 'POST',
			'data' :{
				'file_name' : file_name,
				'userid' :userid
			}, 
			'success' : function(data){
				alert(data);
			},
			'error' : function(request,error){
				alert("Request: "+JSON.stringify(request));
			}
		});
	}
</script> 

<?php $this->load->view('modal');?>
<?php $this->load->view('footer');?>




