<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Company Manager</title>
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon-16x16.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/email_css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome/css/font-awesome.min.css" />

</head>
<body>
	<!-- navbar  --->
	<div class="nav2">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="#"> <img src="assets/images/1.png" class="img-responsive"></img></a>
		</div>
	  </div>
	</nav>
	</div>
	<!-- end navbar  --->
	<!-- main section --->
	<div class="login__page custom_container">
		<div class="card card-container">
			<!--<img class="profile-img-card" src="<?php echo base_url();?>assets/images/no-user.jpg" />-->
			
			<h2 class="login__heading text-center text">Welcome Back !</h2>
			<div id="error"></div>
			<form class="form-signin" method="post" action="<?php echo base_url();?>dashboard" id="form_submit">
				<div class="outer__input">
					<input type="text" size="20" id="email" name="email" class="form-control" placeholder="Username"/>
				</div>
				<div class="outer__input">
					<input type="password" size="20" id="password_user" name="password" class="form-control" placeholder="Password"/>
				</div>
				<div class="outer_input">
				<input type="button" value="Sign in" class="btn btn-primary btn-signin"  onclick="checkvalidation();"/>
				</div><br/></br/>
				<div class="hr"></div>
			</form>
			<div class="ip">Login IP Address: <?php echo $ip;?></div>
		</div>
	</div>
	
	<!-- end footer --->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/classie.js"></script>
<script src="<?php echo base_url();?>assets/js/borderMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/login.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
<script type="text/javascript" >
//$("#show_password").text("show");
	function ShowHide(id){
	 //alert(id);
	if(id=="show_password"){
	var data = $('#show_password').val();
	 document.getElementById('password_user').type = 'text';
		$("#show_password").html('Hide');
		$('.icons').attr('id', 'hide');
	}
else if(id=="hide"){
	document.getElementById('password_user').type = 'password';
	$("#hide").html('Show');
	$('.icons').attr('id', 'show_password');
}
}	

</script> 
</body>
</html>










