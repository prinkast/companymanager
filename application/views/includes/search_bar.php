	<?php if($this->uri->segment(2) == "renewable_new" || $this->uri->segment(2) == "renewalRecordsNew" || $this->uri->segment(2) == "renewed"){ ?>
	<div class="search-form-custom" id="Renewal_search" >
	<!--div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
				<li id= "renewal_tab"><a href="<?php echo base_url(); ?>dashboard/renewed">Renewals</a></li>
				<li id="Renewed_tab"><a href="<?php echo base_url(); ?>dashboard/renewable_new"> Renewed</a></li>
			</ul>
		</div-->
		<form method="get" action="<?php echo base_url(); ?>home/renewalRecordsNew">
		<div class="search-custom search2_custom">
			<!--div class="sub_nav search_tabs new__box">
				<!--ul class="nav navbar-nav nav-pills">
					<li class=""><a href="#" class="user-profile">Company View</a></li>
					<li class="active"><a href="#" class="user-profile">Order View</a></li>
				</ul>
			</div-->
			<?php $reseller_lists = $this->search_model->getResellerList_new();		?>
			<select id="reseller_id1" name="reseller_id1">
			<option value="">Select Reseller</option>
			<option value="Show All">Show All</option>
				<?php foreach($reseller_lists as $reseller1){	?>
					<option value="<?php echo $reseller1->id ;?>"><?php echo $reseller1->company_name;?></option>
				<?php } ?> 
			</select>
		
			<input value="" name="renew_company_from1" id="renew_company_from1"type="search" placeholder="Renewal From" class="form-control custom_srch">
			<input value="" name="renew_company_to1" id="renew_company_to1"type="search" placeholder="Renewal To" class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new_all" value="show_all" <?php echo ($_GET['search_new_all']=='show_all')?'checked':'checked' ?>>Show All</label>
			<label><input type="radio" class="asds" name="search_new_all" value="1" <?php echo ($_GET['search_new_all']=='1')?'checked':'' ?>>Monthly Payment</label>
			<label><input type="radio" class="asds" name="search_new_all" value="0" <?php echo ($_GET['search_new_all']=='0')?'checked':'' ?>>Annual Payment</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	<?php } else if($this->uri->segment(2) == "NewOrders" || $this->uri->segment(2) == "OrdersRecordsNew"){ ?>
	<div class="search-form-custom days__list" id="">
		<div class="sub_nav search_tabs new__box">
			<ul class="nav navbar-nav nav-pills">
				<li class="<?php if($this->uri->segment(2)=='NewOrdersCompany')echo'active';else echo'';?>"><a href="<?php echo base_url();?>/home/NewOrdersCompany?orderDate=today" class="user-profile">Company View</a></li>
				
				<li class="<?php if($this->uri->segment(2)=='NewOrders')echo'active';else echo'';?>"><a href="<?php echo base_url();?>/home/NewOrders" class="user-profile">Order View</a></li>
			</ul>
		</div>
		<div class="sub_nav search_tabs">
			<ul class="nav navbar-nav nav-pills">
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrders">
				<input name="orderDate" type="hidden" class="form-control" value="today">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="Today" id="today">
				</form>
				</li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrders">
				<input name="orderDate" type="hidden" class="form-control" value="week">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="This Week" id="weekly">
				</form>
				<!--a onclick="duration_order('week');" class="user-profile">This Week</a--></li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrders">
				<input name="orderDate" type="hidden" class="form-control" value="month">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="This Month" id="monthly">
				</form>
				<!--a onclick="duration_order('week');" class="user-profile">This Week</a--></li>
				</li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrders">
				<input name="orderDate" type="hidden" class="form-control" value="all">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="Show All" id="showall">
				</form></li>
			</ul>
		</div> 
		<div class="search-custom">
		<?php $uri = $_SERVER['REQUEST_URI'];?>
			<form method="get" action="<?php echo base_url(); ?>home/OrdersRecordsNew">
			<input value=""  type="search" placeholder="New Orders from" class="form-control" name="new_order_from" id="new_order_from">
			<input value=""  type="search" placeholder="New Orders to" class="form-control" name="new_order_to" id="new_order_to">
			<input value="<?php echo $uri;?>"  type="hidden" class="form-control" name="uri_orders" id="uri_orders">
			<!--button class="btn btn-success">Search</button-->
			<input class="btn btn-success" type="submit" value="Search" id="order_search">
			
		</form>
		</div>
	</div>
	
<?php } else if($this->uri->segment(2) == "NewOrdersCompany" || $this->uri->segment(2) == "companyOrdersRecordsNew"){ ?>
	<div class="search-form-custom days__list" id="">
		<div class="sub_nav search_tabs new__box">
			<ul class="nav navbar-nav nav-pills">
				<li class="<?php if($this->uri->segment(2)=='NewOrdersCompany' || $this->uri->segment(2) == 'companyOrdersRecordsNew')echo'active';else echo'';?>"><a href="<?php echo base_url();?>/home/NewOrdersCompany" class="user-profile">Company View</a></li>
				
				<li class="<?php if($this->uri->segment(2)=='NewOrders')echo'active';else echo'';?>"><a href="<?php echo base_url();?>/home/NewOrders?orderDate=today" class="user-profile">Order View</a></li>
			</ul>
		</div>
		<div class="sub_nav search_tabs">
			<ul class="nav navbar-nav nav-pills">
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrdersCompany">
				<input name="orderDate" type="hidden" class="form-control" value="today">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="Today" id="today">
				</form>
				</li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrdersCompany">
				<input name="orderDate" type="hidden" class="form-control" value="week">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="This Week" id="weekly">
				</form>
				<!--a onclick="duration_order('week');" class="user-profile">This Week</a--></li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrdersCompany">
				<input name="orderDate" type="hidden" class="form-control" value="month">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="This Month" id="monthly">
				</form>
				<!--a onclick="duration_order('week');" class="user-profile">This Week</a--></li>
				</li>
				<li class="">
				<form method="get" action="<?php echo base_url(); ?>home/NewOrdersCompany">
				<input name="orderDate" type="hidden" class="form-control" value="all">
				<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
				<input class="btn" type="submit" value="Show All" id="showall">
				</form></li>
			</ul>
		</div> 
		<div class="search-custom">
		<?php $uri = $_SERVER['REQUEST_URI'];?>
			<form method="get" action="<?php echo base_url(); ?>home/companyOrdersRecordsNew">
			<input value=""  type="search" placeholder="New Orders from" class="form-control" name="new_order_from" id="new_order_from">
			<input value=""  type="search" placeholder="New Orders to" class="form-control" name="new_order_to" id="new_order_to">
			<input value="<?php echo $uri;?>"  type="hidden" class="form-control" name="uri_orders" id="uri_orders">
			<!--button class="btn btn-success">Search</button-->
			<input class="btn btn-success" type="submit" value="Search" id="order_search">
			
		</form>
		</div>
	</div>
	
<?php } else if ($this->uri->segment(2) == "comapnyRenewableNew" || $this->uri->segment(2) == "renewed_c_view"){?>
	<div class="search-form-custom" id="Renewal_search" >
		<form method="get" action="<?php echo base_url(); ?>home/companyRenewalRecordsNew">
		<div class="search-custom search2_custom">
			<?php $reseller_lists = $this->search_model->getResellerList_new();		?>
			<select id="reseller_id1" name="reseller_id1">
			<option value="">Select Reseller</option>
			<option value="Show All">Show All</option>
				<?php foreach($reseller_lists as $reseller1){	?>
					<option value="<?php echo $reseller1->id ;?>"><?php echo $reseller1->company_name;?></option>
				<?php } ?>
			</select>
		
			<input value="" name="renew_company_from1" id="renew_company_from1"type="search" placeholder="Renewal From" class="form-control custom_srch">
			<input value="" name="renew_company_to1" id="renew_company_to1"type="search" placeholder="Renewal To" class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new_all" value="show_all" <?php echo ($_GET['search_new_all']=='show_all')?'checked':'checked' ?>>Show All</label>
			<label><input type="radio" class="asds" name="search_new_all" value="1" <?php echo ($_GET['search_new_all']=='1')?'checked':'' ?>>Monthly Payment</label>
			<label><input type="radio" class="asds" name="search_new_all" value="0" <?php echo ($_GET['search_new_all']=='0')?'checked':'' ?>>Annual Payment</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	<?php }
	else if ($this->uri->segment(2) == "companyView" || $this->uri->segment(2) == "companynewsearch"){?>
	<div class="search-form-custom" id="company_new_search" >
	<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
				<li class="<?php if($this->uri->segment(2)=='companyView' || $this->uri->segment(2)=='companynewsearch')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Company View</a></li>
				<?php if($this->input->get('new_search_bar')!="" && $this->uri->segment(2)=='companynewsearch'){ ?>
				<li class="<?php if($this->uri->segment(2)=='dashboard_index'|| $this->uri->segment(2) == "searchResult")echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/searchResult?new_search_bar=<?php echo $this->input->get('new_search_bar'); ?>&search_new=search_all_new" class="user-profile">Order View</a></li>
				<?php } else {?>
				<li class="<?php if($this->uri->segment(2)=='dashboard_index'|| $this->uri->segment(2) == "searchResult")echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
				<?php }?>
				<!--li class="<?php if($this->uri->segment(2)=='dashboard_index')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/dashboard_index" class="user-profile">Order View</a></li-->
			</ul>
		</div> 
		<form method="get" action="<?php echo base_url(); ?>home/companynewsearch">
		<div class="search-custom search2_custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search…" class="form-control custom_srch">
			<input value="" name="accountreturnsfrom" id="accountreturnsfrom"type="search" placeholder="accounts/returns from" class="form-control custom_srch">
			<input value="" name="accountreturnsto" id="accountreturnsto"type="search" placeholder="accounts/returns to" class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new_all" value="show_all" <?php echo ($_GET['search_new_all']=='show_all')?'checked':'checked' ?>>Search All</label>
			<label><input type="radio" class="asds" name="search_new_all" value="1" <?php echo ($_GET['search_new_all']=='1')?'checked':'' ?>>Accounts Only</label>
			<label><input type="radio" class="asds" name="search_new_all" value="0" <?php echo ($_GET['search_new_all']=='0')?'checked':'' ?>>Returns Only</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	<?php } else if($this->uri->segment(2) == "companyStateChange1" || $this->uri->segment(2) == "companyStateChange" || $this->uri->segment(2) == "companyNewFormation" || $this->uri->segment(2) == "companyRenewalRecordsNew" || $this->uri->segment(2) == "NewOrdersCompany"){ ?>
	<div class="search-form-custom" id="search_all">
		<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
				<li class="<?php if($this->uri->segment(2)=='companyView' || $this->uri->segment(2) == "companySearchResult" || $this->uri->segment(2) == "companyStateChange" || $this->uri->segment(2) == "companyNewFormation"|| $this->uri->segment(2) == "additional_serveics" || $this->uri->segment(2) == "companyRenewalRecordsNew" || $this->uri->segment(2) == "companyStateChange1" || $this->uri->segment(2) == "NewOrdersCompany")echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/companyView" class="user-profile">Company View</a></li>
				<?php if($this->uri->segment(2) == "companyNewFormation" )
				{ ?>
				<li class="<?php if($this->uri->segment(2)=='index')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/new_formation" class="user-profile">Order View</a></li>
				<?php } 
				if($this->uri->segment(2) == "companyStateChange" )
				{?>
				<li class="<?php if($this->uri->segment(2)=='index')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/state_change?state_change=3" class="user-profile">Order View</a></li>
				<?php } if($this->uri->segment(2) == "companyStateChange1" )
				{?>
				<li class="<?php if($this->uri->segment(2)=='index')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/state_change_id?state_change=5,7" class="user-profile">Order View</a></li>
				<?php }if($this->uri->segment(2) == "state_change_id" )
				{?>
				<li class="<?php if($this->uri->segment(2)=='index')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/companyStateChange1?state_change=5,7" class="user-profile">Order View</a></li>
				<?php } ?>
			</ul>
		</div>  
		<?php if($this->uri->segment(2) == "companyNewFormation" ) {?>
		<form method="get" action="<?php echo base_url(); ?>home/companyNewFormation">
		<?php } ?>
		<?php if($this->uri->segment(2) == "companyStateChange1" ) {?>
		<form method="get" action="<?php echo base_url(); ?>home/companyStateChange1">
		<?php } else { ?>
		<form method="get" action="<?php echo base_url(); ?>home/companyStateChange">
		<?php } ?>
		<div class="search-custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search…" class="form-control custom_srch">
			<input type="hidden" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>>
			<!--label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label-->
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	
	<?php } 
	else if($this->uri->segment(2) == "new_formation"){ ?>
	<?php //echo "hello"; die("hoja")?>
	<div class="search-form-custom" id="search_all">
		<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
			<?php if($this->uri->segment(2)=='new_formation') ?>
				<li class="<?php if($this->uri->segment(2)=='companyView')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/companyNewFormation" class="user-profile">Company View</a></li>
				<li class="<?php if($this->uri->segment(2)=='index' || $this->uri->segment(2)=='new_formation')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
			</ul>
		</div> 
		<form method="get" action="<?php echo base_url(); ?>home/new_formation">
		<div class="search-custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>> Search all fields</label>
			<label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	
	<?php }
	else if($this->uri->segment(2) == "state_change"){ ?>
	<?php //echo "hello"; die("hoja")?>
	<div class="search-form-custom" id="search_all">
		<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
			<?php if($this->uri->segment(2)=='state_change')
			{?>
				<li class="<?php if($this->uri->segment(2)=='companyView')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/companyStateChange?state_change=3" class="user-profile">Company View</a></li>
			<?php } ?>
				<li class="<?php if($this->uri->segment(2)=='index' || $this->uri->segment(2)=='state_change')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
			</ul>
		</div> 
		<form method="get" action="<?php echo base_url(); ?>home/state_change">
		<div class="search-custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<input type="hidden" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>>
			<!--label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label!-->
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	
	<?php }
	else if($this->uri->segment(2) == "state_change_id"){ ?>
	<?php //echo "hello"; die("hoja")?>
	<div class="search-form-custom" id="search_all">
		<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
			<?php if($this->uri->segment(2)=='state_change_id')
			{?>
				<li class="<?php if($this->uri->segment(2)=='companyView')echo'active';else echo'';?>"><a href="<?php echo base_url();?>home/companyStateChange1?state_change=5,7" class="user-profile">Company View</a></li>
			<?php } ?>
				<li class="<?php if($this->uri->segment(2)=='index' || $this->uri->segment(2)=='state_change_id')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
			</ul>
		</div> 
		<form method="get" action="<?php echo base_url(); ?>home/state_change_id?state_change=5,7">
		<div class="search-custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<input type="hidden" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>>
			<!--label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label-->
			<button class="btn btn-success">Search</button>
		</div>
		</form>
	</div>
	
	<?php }

	else if($this->uri->segment(2) == "additional_serveics" || $this->uri->segment(2) =="document_service" || $this->uri->segment(2) == "company_register" || $this->uri->segment(2) == "Hosting"|| $this->uri->segment(2) == "company_register_status" || $this->uri->segment(2) == "document_service_status" || $this->uri->segment(2) == "hosting_status"){ ?>
	<?php //echo "hello"; die("hoja")?>
	<div class="search-form-custom" id="search_all">
	<?php if($this->uri->segment(2) == "additional_serveics")
	{?>
	<form method="get" action="<?php echo base_url(); ?>dashboard/additional_serveics">
	<?php }?>
	<?php if($this->uri->segment(2) == "company_register" || $this->uri->segment(2) == "company_register_status")
	{?>
	<form method="get" action="<?php echo base_url(); ?>dashboard/company_register">
	<?php }?>
	<?php if($this->uri->segment(2) == "document_service" || $this->uri->segment(2) == "document_service_status")
	{?>
	<form method="get" action="<?php echo base_url(); ?>dashboard/document_service">
	<?php }?>
	<?php
	
	if($this->uri->segment(2) == "Hosting" || $this->uri->segment(2) == "hosting_status" )
	{?>
	<form method="get" action="<?php echo base_url(); ?>dashboard/Hosting">
	<?php }?>
		
		<div class="search-custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<button class="btn btn-success">Search</button>
			<input type="hidden" class="asds" name="search_new" value="search_all_new">
		
		</div>
		</form>
	</div>
	
	<?php }
	else if($this->uri->segment(2) == "dashboard_index" || $this->uri->segment(2) == "searchResult")
	{?>
     <div class="search-form-custom" id="search_all">
	<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
				<?php if($this->input->get('new_search_bar')!=""){ ?>
				<li class="<?php if($this->uri->segment(2)=='companyView')echo'active';else echo'';?>"><a href="<?php echo base_url();?>/home/companynewsearch?new_search_bar=<?php echo $this->input->get('new_search_bar'); ?>&accountreturnsfrom=&accountreturnsto=&search_new_all=show_all" class="user-profile">Company View</a></li>
				<?php }else{ ?>
				<li class="<?php if($this->uri->segment(2)=='companyView')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/companyView" class="user-profile">Company View</a></li>
				<?php } ?>

				<li class="<?php if($this->uri->segment(2)=='dashboard_index'|| $this->uri->segment(2) == "searchResult")echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
				
			</ul>
		</div>
		<form method="get" action="<?php echo base_url(); ?>dashboard/searchResult">
		<div class="search-custom">
			<input value="" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>> Search all fields</label>
			<label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
		</div>
	<?php }
	else { ?>	
	<div class="search-form-custom" id="search_all">
		<div class="sub_nav search_tabs active">
			<ul class="nav navbar-nav nav-pills">
				<li class="<?php if($this->uri->segment(2)=='company_view')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/company_view" class="user-profile">Company View</a></li>
				<li class="<?php if($this->uri->segment(2)=='index' || $this->uri->segment(2)!='company_view')echo'active';else echo'';?>"><a href="<?php echo base_url();?>dashboard/index" class="user-profile">Order View</a></li>
			</ul>
		</div> 
		<?php if($this->uri->segment(2)=='company_view') {?>
		<form method="get" action="<?php echo base_url(); ?>home/companynewsearch">
		<div class="search-custom search2_custom">
			<input value="<?php echo $_GET['new_search_bar'] ; ?>" name="new_search_bar" type="search" placeholder="Company Search…" class="form-control custom_srch">
			<input value="" name="accountreturnsfrom" id="accountreturnsfrom"type="search" placeholder="accounts/returns from" class="form-control custom_srch">
			<input value="" name="accountreturnsto" id="accountreturnsto"type="search" placeholder="accounts/returns to" class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new_all" value="show_all" <?php echo ($_GET['search_new_all']=='show_all')?'checked':'checked' ?>>Search All</label>
			<label><input type="radio" class="asds" name="search_new_all" value="1" <?php echo ($_GET['search_new_all']=='1')?'checked':'' ?>>Accounts Only</label>
			<label><input type="radio" class="asds" name="search_new_all" value="0" <?php echo ($_GET['search_new_all']=='0')?'checked':'' ?>>Returns Only</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
		<?php } 
		else{?>
		<form method="get" action="<?php echo base_url(); ?>dashboard/searchResult">
		
		<div class="search-custom">
			<input value="" name="new_search_bar" type="search" placeholder="Company Search..." class="form-control custom_srch">
			<label><input type="radio" class="asds" name="search_new" value="search_all_new" <?php //echo ($_GET['search_new']=='search_all_new')?'checked':'checked' ?>> Search all fields</label>
			<label><input type="radio" class="asds" name="search_new" value="search_company_new"<?php //echo ($_GET['search_new']=='search_company_new')?'checked':'' ?>> Company Search Only</label>
			<label><input type="radio" class="asds" name="search_new" value="search_director_new" <?php //echo ($_GET['search_new']=='search_director_new')?'checked':'' ?>> Director Search Only</label>
			<button class="btn btn-success">Search</button>
		</div>
		</form>
		<?php }?>
	</div>
<?php } ?>