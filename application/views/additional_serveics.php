<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/left_nav');?>
<?php $uri_test = $this->uri->segment(2); ?>
<section id="content_info" <?php (($uri_test == "searchResult") ? "echo class='top_content_info'" : "echo class =''"); ?> class="add__service">
<?php $this->load->view ('includes/additonal_sidebar');?>
<center class="text-primary"><?php echo isset($message)?$message:'';?></center>
<div class="add_service__sidebar">
<?php $this->load->view ('includes/search_bar');?>
<div class="loader_background" id="loader_background" style="display:none;">
	<div class="loader" id="loader">
	</div>
</div>
  <div class="table_data">   
	<table class="table table-hover table-bordered">
	
	<?php	if($role_id === '0' || $role_id === '2'){ ?>
		<thead class="home-table-header static-header">
	<?php 	}else{	?>
		<thead class="home-table-header">
	<?php 	}	 ?>
		<tr>			
			<th class="cmpny th_company_class">
			<?php //var_dump($role_id);die();?>
            <?php 
 				$total =  $this->uri->segment(3)+1; 
 				if($total<>1){
				  $totaldss = $this->uri->segment(3)+$pagef;
				}else{
					$totaldss = $pagef;
				}
				if($totaldss > $record_count){
					$totaldss = $record_count;
				}
 				if($this->pagination->create_links()){
			?>
					 Company Name (<?php echo $total.'-'.$totaldss; ?> of <?php echo $record_count;?>)
              <?php } else{ ?>
               		 Company Name (<?php echo $record_count;?>)
              <?php	 }  ?>  
			</th>
			<?php $services =  $this->search->getServiceOptions();
			if($role_id === '1')
			{
			?>		
			<th class="reseller-head th_reseller_class">
			<form action="<?php echo base_url();?>home/reseller_change" method="POST" name="reseller_form_change" id="reseller_form_change">
			<?php $resellers =  $this->search->resellers();?>
			  <select id="reseller_change" name="reseller_change">
			  <option>Reseller</option>
			  <option value="Show All">Show All</option>
			  <?php foreach($resellers as $reseller){?>
			    <option value="<?php echo $reseller->id;?>"><?php echo $reseller->company_name;?></option>
			   <?php }?>
			  </select>
			    </form>	
			</th>
			<?php }?>	 
			<th class="th_state_class">
			<form action="<?php echo base_url();?>home/state_change1" method="GET" name="state_form_change" id="state_form_change">
			<input type="hidden" id="query_string_1" name="query_string_1" value="<?php echo $_GET['new_search_bar']?>">
			<input type="hidden" id="query_string_2" name="query_string_2" value="<?php echo $_GET['search_new']?>">
			<?php $states =  $this->search->getStatusOptions1();?>
			 <select id="state_change" name="state_change">
			    <option>Status</option>
				<option value="Show All">Show All</option>
			      <?php foreach($states as $key =>$state){	?>
			    <option value="<?php echo $key ;?>"><?php echo $state;?></option>
			   <?php }	?>			    
			  </select>			   
			  </form>			 
			</th>					
			<th onClick="getDateOtions()" class="th_renew_class">
			Order Date			
			</th>
			<th class="th_ltd th_reg_class">			
			<form action="<?php echo base_url();?>home/ltdSearch" method="POST" name="comp_ltd" id="comp_ltd">
						<?php $ltds =  $this->order->getLtdOptions();?>
						<select id="comp_ltd_select_option" name="comp_ltd_select" class="getSelect1 ltdClick">
								<option>Website</option>
							<?php /*	  <?php foreach($services as $service){?>
					<option value="<?php echo $service;?>"><?php echo $service;?></option>
			   <?php }?>
			    */?>			   
						</select>
				  </form>			
			</th>
			<th class="th_reg_class">
			<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="registered_office" id="registered_office_select">		
			  <select id="registered_office" name="registered_office" class="getSelect1 regClick">
						<option>Legal</option>
				   <?php /*	  <?php foreach($services as $service){?>
					<option value="<?php echo $service;?>"><?php echo $service;?></option>
			   <?php }?>
			    */?>			   
			  </select>
			  </form>			  
			</th>			
			<th class="th_director_class">
			<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="director_service_address" id="director_service_address_select">
			    <option>Register</option>
			  <select id="director_service_address"  name="director_service_address" class="getSelect1 dirClick">
			<?php /*	  <?php foreach($services as $service){?>
					<option value="<?php echo $service;?>"><?php echo $service;?></option>
			   <?php }?>
			    */?>
			  </select>
			  </form>			 
			</th>
			<th class="th_price_class">
			Charge 			
			</th>
		</tr>
		</thead>		
		<tbody id="OrderPackages">
			<?php 
			if($user_orders){
				//var_dump($user_orders);die();
				foreach ($user_orders as $key=>$user_order)
				{				 
					//var_dump($user_order);die;
				  // $company =  $this->search->filterSearch($user_order->company_id);
				  // //print_r($company);die;
				  // $user =  $this->search->userSearch($user_order->create_user_id);
				  // $update_company_user = $this->search->userSearch($company->create_user_id);
				  // //var_dump($user_order->create_user_id);				  
				  // //var_dump($fetch_preferene);
				  // $billing =  $this->search->billing($company->billing_adress_id);
				  // $mailling =  $this->search->mailling($company->mailing_adress_id);
				  // $orders =  $this->search->orders($company->id);
				  // $order_details =  $this->search->orderDetails($orders->id);
				  // $files_info =  $this->search->fileInfo($user_order->company_id);
				  // $messages =  $this->search->Message($user_order->id);
				  // $alt_emails =  $this->search->alterEmails($company->id);
				  // $comp_tele_service =  $this->search->comp_tele_service($company->id);
				  // $comp_secretaries =  $this->search->comp_secretary($company->id);
				  // $comp_activities =  $this->search->comp_activity($company->id);
				  // $document =  $this->ltd_model->GetDocument($company->id);
					$host = strtoupper($user_order->hosting);
					$legal = strtoupper($user_order->Legal);
					$comp_reg = strtoupper($user_order->company_register);
					if($host == "YES" || $legal=="YES" ||$comp_reg=="YES"){
				 //var_dump( $alt_emails );die;
					?>					
			<tr id="<?php echo 'row_'.$user_order->id;?>">
			<?php
			//va_dump($order_details->product);die();			
			?>			
			<td class = "td_company_class dropdown set_dropdown_ltd">
				<!--a onClick="sendCompany_id(<?php echo $user_order->id; ?>);"-->
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">
				<?php
				$comp_name_length = strlen($user_order->company_name);
					if($comp_name_length >=10){?>
					<span class="comp_full_name">
				<?php echo $user_order->company_name;?>
				</span>
				<?php }
				else
					echo $user_order->company_name;
					?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo  base_url();?>dashboard/showCompanyResult?id=<?php echo$user_order->company_id ?> ">Company Overview</a></li>
					<!--li><a href="javascript:void(0)">Contact Details</a></li-->
					
					<li><a href="javascript:void(0)" onclick ="userDetails ('<?php echo$user_order->company_id;?>')">Client Details</a></li>
					<li><a href="javascript:void(0)" onclick ="billingDetails('<?php echo$user_order->company_id;?>')">Billing Information</a></li>
					<li><a href="javascript:void(0)" onclick ="orderDetails('<?php echo$user_order->company_id;?>','<?php echo $orders->id;?>')">Order Information</a></li>
				</ul>
			</td>
			<!--td id="<?php echo 'data_'.$user_order->id;?>" class = "td_company_class">
			<a href="<?php echo '.packageDetails'.$user_order->id;?>" id="<?php echo 'package_'.$user_order->id;?>" 
			class="accordion-toggle package_click" data-toggle="collapse" data-parent="#OrderPackages">			
			<?php
			$comp_name_length = strlen($user_order->company_name);
			if($comp_name_length >=10){?>
			<span class="comp_full_name">
			<?php echo $user_order->company_name;?>
			</span>
			<?php }
			else
				echo $user_order->company_name;
				?>			
			</a>
			</td-->				
			<?php if($role_id === '1'){?>
			<td data-cname="<?php echo $user_order->company_name;?>" class="reseller td_reseller_class"  onclick = "reseller('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>')"><a href="#"><?php echo $user_order->reseller;?></a></td>            
            <!-- STATUS--->
            <td class = "state_id td_status_class" data-id="<?php echo $user_order->id;?>" data-cname="<?php echo $user_order->company_name;?>">
            <a href="#"><?php echo	$this->search->getStatusOptions1($user_order->state_id);?></a>
            </td>
			<td class = "state_id td_status_class" data-id="<?php echo $user_order->id;?>" data-cname="<?php echo $user_order->company_name;?>">
			<?php //echo $user_order->create_time;?>
            <a href="#"><?php echo date ("d-m-Y",strtotime($user_order->renewable_date));?></a>			
            </td>  
			<!--td class="dropdown set_dropdown_ltd">
				<!--a href="<?php echo base_url();?>home/ltd_company?company=<?php echo$user_order->company_id;?>"-->
				<!--a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" >
				< ?php echo $this->order->getLtdOptions($user_order->comp_ltd);?>
				</a>				
			< /td-->
			<!--td onclick ="mail_handel('<?php echo$user_order->company_id?>','<?php echo addslashes($user_order->company_name);?>')" class="td_reg_class"><a href="javascript:void(0)"><?php echo strtoupper($user_order->hosting);?></a></td-->
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php 
				/* $host = strtoupper($user_order->hosting); */				
				 if($host=="" || $host=="NO"){
				 echo "<div class='red'>"; 
					echo "NO";
					echo "</div>";
					}
					else{
					echo "<div class='green'>";
					echo $host;
					echo "</div>";
					}
				?>	
				</a>			
			</td>
			<!--td onclick ="mail_handel('<?php echo$user_order->company_id?>','<?php echo addslashes($user_order->company_name);?>')" class="td_director_class"><a href="javascript:void(0)"><?php echo strtoupper($user_order->director_service_address);?></a></td-->
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php
					if($legal =="" || $legal=="NO"){
					echo "<div class='red'>";
					echo "NO";
					echo "</div>";
					}
					else{
					echo "<div class='green'>";
					echo $legal; 
					echo "</div>";
					}
					?>
				</a>				
			</td>
			<!--td onclick ="mail_handel('<?php echo$user_order->company_id?>','<?php echo addslashes($user_order->company_name);?>')" class="td_business_class"><a href="javascript:void(0)"><?php echo strtoupper($user_order->business_address);?></a></td-->
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php					
					if($comp_reg == "" || $comp_reg=="NO"){
					echo "<div class='red'>";
					echo "NO";
					echo "</div>";
					}
					else{
					echo "<div class='green'>";
					echo $comp_reg;
					echo "</div>";
					}
					?>
				</a>
			</td> 
			<!-- td>COMPANY</td-->
			<td class="td_price_class" onclick="editPrice('<?php echo $user_order->id?>','<?php echo $user_order->deposit?>','<?php echo addslashes($user_order->company_name);?>','<?php echo$user_order->company_id;?>')">
			<?php 
			$h = $user_order->hosting;
			$c = $user_order->company_register;
			$l = $user_order->Legal;
			$price = 0.00; 
			if($h == "Yes") $price = $price +99.00;
			if($c == "Yes") $price = $price +29.00;
			if($l == "Yes") $price = $price +99.00;	
			echo '&pound; '.$price.'.00';
			?>
		<?php /*	<a href="#">&pound;<?php 
			$total_price = $this->order->orderPrice($user_order->id);		
			$dot = strstr($total_price, '.');
			if($dot)
				echo $total_price;
			else
				echo $total_price.'.00';
			?></a>
			*/?>
			</td>
			<!--td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			foreach($files_info as $file_info)
			{
				$file_type_id[]=$file_info->type_id;
			}
			$file_type_upload1 =  in_array("4",$file_type_id);
			$file_type_upload2 =  in_array("5",$file_type_id);
			$file_type_upload3 =  in_array("200",$file_type_id);
			$getYes = "";			
			if($file_type_upload1=== true || $file_type_upload2 === true) 
			{
			    echo "<div class='green'>";
				echo "YES";
				echo "</div>";
			}
			elseif($file_type_upload3 ==true)
			{
				echo "<div class='green'>";
				echo "YES";
				echo "</div>";
			}
			else
			{
			    echo "<div class='red'>";
				echo "NO";
				echo "</div>";
				//echo "Upload";
			}		
			?></a></td!-->
			<?php }?>
					</tr>
			<tr>	
			<td colspan="16" class="hiddenRow">			
			 <input type="hidden" data-del_id="<?php echo 'del_comp_id'.$user_order->id;?>" name="del_comp_id" id="<?php echo 'del_comp'.$user_order->id;?>" class="form-control" value="<?php echo $user_order->company_id;?>">
									
			      <div class="panel-body accordion-body collapse <?php echo 'packageDetails'.$user_order->id;?>" id="<?php echo 'accordion_data_'.$user_order->id;?>">
			         <div class="tabs-main">	
				      <!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="<?php echo "#tab_company".$user_order->id;?>" role="tab" data-toggle="tab">Company Details</a></li>
					    <li role="presentation"><a href="<?php echo "#tab_user".$user_order->id;?>" role="tab" data-toggle="tab">Your Details</a></li>
					    <li role="presentation"><a href="<?php echo "#tab_billing".$user_order->id;?>" role="tab" data-toggle="tab">Billing Information</a></li>
					    <li role="presentation"><a href="<?php echo "#tab_order".$user_order->id;?>" role="tab" data-toggle="tab">Your Order</a></li>
					    <li role="presentation"><a href="<?php echo "#tab_mail".$user_order->id;?>" role="tab" data-toggle="tab">Mail Centre</a></li>
					    <li role="presentation"><a href="<?php echo "#tab_message".$user_order->id;?>" role="tab" data-toggle="tab">Message Centre</a></li>
						<li role="presentation"><a href="<?php echo "#tab_document".$user_order->id;?>" role="tab" data-toggle="tab">Documents</a></li>
						<li role="presentation"><a href="<?php echo "#tab_emails".$user_order->id;?>" role="tab" data-toggle="tab">Email Alerts</a></li>
						<li role="presentation"><a href="<?php echo "#tab_activity".$user_order->id;?>" role="tab" data-toggle="tab">History</a></li>
					</ul>					
					  <!-- Tab panes -->
					  <div class="tab-content">					 
					    <div role="tabpanel" class="tab-pane active" id="<?php echo "tab_company".$user_order->id;?>">
					     <form  name="update_company" method="POST" action="<?php echo base_url();?>home/updateCompany" id="update_company">
					         <div class="form-group col-sm-6 col-xs-12 box">
					   		  <input type="hidden" name="company_id" id="company_id" class="form-control" value="<?php echo$user_order->company_id;?>">
								<label for="Title ">Company Name (if not yet registered this can be changed later):</label> <input type="text" name="user_company" id="user_company" class="form-control" value="<?php echo $user_order->company_name;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="First Name">Trading Name(if different):</label> <input type="text" name="trade_name" id="trade_name" class="form-control" value="<?php echo $user_order->trading;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Director1:</label>
								<input type="text" name="director1" id="director1" class="form-control" value="<?php echo $user_order->director1;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Director2:</label>
								<input type="text" name="director2" id="director2" class="form-control" value="<?php echo $user_order->director2;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Director3:</label>
								<input type="text" name="director3" id="director3" class="form-control" value="<?php echo $user_order->director3;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Director4:</label>
								<input type="text" name="director4" id="director4" class="form-control" value="<?php echo $user_order->director4;?>">
							</div>							
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Renewal Date:</label>
								<input onclick = "displayDatepicker('<?php echo $user_order->id?>','<?php echo $user_order->company_name;?>')" type="text" name="renewal" id="<?php echo 'renewal_'.$user_order->id;?>" class="form-control" value="<?php echo date ("d-m-Y ",strtotime($user_order->renewable_date));?>">
							</div>							
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Middle Name">Location:</label>
								<input  onclick =" location_event('<?php echo $user_order->id?>','<?php echo $user_order->company_name;?>')" type="text" name="location" id="<?php echo 'location'.$user_order->id;?>" class="form-control" value="<?php echo $user_order->location;?>">
							</div>
							  <div class="clearfix"></div>
					     <hr>
					    <div class="col-sm-12 text-center">
					   <input type="submit" value="Update" name="update" class="btn btn-success"/>
					      <a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
					    </div>
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
						   <i class="fa fa-close"></i>
						</a>
						</form>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="<?php echo "tab_user".$user_order->id;?>">
					    <form  name="update_user" method ="POST" action="<?php echo base_url();?>home/updateCompany" id="update_user">
					         <div class="form-group col-sm-6 col-xs-12 box">
									<label for="Title ">First Name:</label> 
									<input type="hidden" name="user_company_id" value="<?php echo$user_order->company_id;?>" id="user_company_id" class="form-control">
									<input type="text" name="user_name" value="<?php echo $update_company_user->first_name;?>" id="user_name" class="form-control">
							<input type="hidden" name="user_id" value="<?php echo $update_company_user->id;?>" id="user_id" class="form-control">
							<input type="hidden" name="mailing_id" value="<?php echo $mailling->id;?>" id="mailing_id" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="First Name">Last Name:</label> 
									<input type="text" name="user_last_name" value="<?php echo $update_company_user->last_name;?>" id="user_last_name" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Middle Name">Email:</label> 
									<input type="text" name="user_email" value="<?php echo $update_company_user->email;?>" id="user_email" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Last Name">Daytime Phone:</label>
									 <input type="text" value="<?php echo $update_company_user->ph_no;?>" name="user_daytime_contact" id="user_daytime_contact" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Mobile Number:</label> 
									<input type="text" value="<?php echo $update_company_user->mobile;?>" name="user_contact" id="user_contact" class="form-control">
							</div>							
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Skype Address:</label>
									 <input type="text" value="<?php echo $update_company_user->skpe_address;?>" name="user_skype" id="user_skype" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Street Address:</label>
									 <input type="text" value="<?php echo  $mailling->street;?>" name="user_street" id="user_street" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Town/City:</label> 
									<input type="text" value="<?php echo  $mailling->city;?>" name="user_city" id="user_city" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">County/Region:</label>
									<input type="text" name="user_country" value="<?php echo  $mailling->country;?>" id="user_country" class="form-control">
							</div>	
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Postcode:</label>
									 <input type="text" value="<?php echo  $mailling->postcode;?>" name="user_post_code" id="user_post_code" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Country:</label> 
									<input type="text" value="<?php echo  $mailling->county;?>" name="user_county" id="user_county" class="form-control">
							</div>
							<?php if($role_id !== '2'){?>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Nationality">Change Password:</label> 
									<input type="password" value="" name="user_password" id="user_password" class="form-control">
							</div>
							<?php }?>
							<div class="clearfix"></div>
					     <hr>
					    <div class="col-sm-12 text-center">
					   <input type="submit" value="Update" name="update" class="btn btn-success"/>
					    
					      <a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
					    </div>						
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
						   <i class="fa fa-close"></i>
						</a>
						</form>
					    </div>					    
					    <div role="tabpanel" class="tab-pane" id="<?php echo "tab_billing".$user_order->id;?>">
					    <form  name="update_billing" id="update_billing" method="Post" action="<?php echo base_url();?>home/updateCompany">
					         <div class="form-group col-sm-6 col-xs-12 box">
									<label for="Title ">Billing Name:</label>
									<input type="hidden" name="billing_company_id" value="<?php echo$user_order->company_id;?>" id="billing_company_id" class="form-control">
									<input type="hidden" value="<?php echo $billing->id;?>" name="billing_id" id="billing_id" class="form-control">
									 <input type="text" value="<?php echo $billing->billing_name;?>" name="billing_name" id="billing_name" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Nationality">Street Address:</label>
								 <input type="text" value="<?php echo $billing->street;?>" name="billing_street" id="billing_street" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Nationality">Town/City:</label> 
								<input type="text" value="<?php echo $billing->city;?>" name="billing_city" id="billing_city" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Nationality">County/Region:</label>
								<input type="text" name="billing_country" value="<?php echo $billing->country;?>" id="billing_country" class="form-control">
							</div>	
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Nationality">Postcode:</label>
								<input type="text" value="<?php echo $billing->postcode;?>" name="billing_post_code" id="billing_post_code" class="form-control">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
								<label for="Nationality">Country:</label>
								<input type="text" value="<?php echo $billing->county;?>" name="billing_county" id="billing_county" class="form-control">
							</div>
							<div class="clearfix"></div>
						<hr>
					    <div class="col-sm-12 text-center">
							<input type="submit" value="Update" name="update" class="btn btn-success"/>
							<a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
						</div>
						
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
						   <i class="fa fa-close"></i>
						</a>
						</form>
					    </div>					    
					    <div role="tabpanel" class="tab-pane" id="<?php echo "tab_order".$user_order->id;?>">
			               
						    <form id="<?php echo 'service_payment'.$user_order->id;?>" name="service_payment" action="https://secure.wp3.rbsworldpay.com/wcc/purchase" class="service_payment">
		                 <div class="form-group col-sm-3">
							  <label><?php if($role_id === '2')
									echo "New Order:";
								  else
								 	 echo "Order:";
								 ?></label>
		                       <select class="form-control" id="payment_options<?php echo $user_order->id;?>"  name="payment_options" onChange="getOptions('<?php echo $user_order->id;?>',this);">
		                           
		                            <option value="Registered Office Address - £29.00" id="<?php echo 'Registered Office Address'.$user_order->id?>" name="<?php echo 'Registered Office Address'.$user_order->id?>">Registered Office Address - &pound;29.00 Per Year</option>
									<option value="Director Service Address - £9.95" id="<?php echo 'Director Service Address'.$user_order->id?>" name="<?php echo 'Director Service Address'.$user_order->id?>">Director Service Address - &pound;9.95 Per Year</option>
									<option value="Registered Office and Director Address - £38.99" id="<?php echo 'Registered Office and Director Address'.$user_order->id?>" name="<?php echo 'Registered Office and Director Address'.$user_order->id?>">Registered Office + Director Address - &pound;38.99 Per Year</option>
									<option value="Business Address (1 Month) - £99.00" id="<?php echo 'Business Address'.$user_order->id?>" name="<?php echo 'Business Address'.$user_order->id?>">Virtual Business Address  - &pound;99.00 Per Year</option>
									<option value="Telephone Answer Service - £124.99" id="<?php echo 'Telephone Address'.$user_order->id?>" name="<?php echo 'Telephone Address'.$user_order->id?>">Telephone Answer Service  - &pound;124.99 Per Year</option>
									<option value="Website Hosting - £99.00" id="<?php echo 'Website Hosting'.$user_order->id?>" name="<?php echo 'Website Hosting'.$user_order->id?>">Website Hosting - &pound;99.00 Per Year</option>
									<option value="Business Plus - £124.99" id="<?php echo 'Complete Package'.$user_order->id?>" name="<?php echo 'Complete Package'.$user_order->id?>">Virtual Business Plus - &pound;124.99 Per Year</option>
									<option value="Partner Package - £50.00" id="<?php echo 'Partner Package'.$user_order->id?>" name="<?php echo 'Partner Package'.$user_order->id?>">Partner Package - £50.00</option>
									<option value="Postal Deposit (UK) - £20.00" id="<?php echo 'Postal Deposit'.$user_order->id?>" name="<?php echo 'Postal Deposit'.$user_order->id?>">Postal Deposit (UK) - &pound;20.00</option>
									<option value="Postal Deposit (Overseas) - £50.00" id="<?php echo 'Postal Deposit'.$user_order->id?>" name="<?php echo 'Postal Deposit'.$user_order->id?>">Postal Deposit (Overseas) - &pound;50.00</option>
		                       </select>							
		                 </div>		                 
		                 <div class="form-group col-sm-1">
		                     <label>Price</label>
							 <input type="hidden" class="form-control" id="<?php echo 'service_name'.$user_order->id;?>"  name="service_name" value="Registered Office Address">
							  <input type="hidden" class="form-control" id="<?php echo 'add_order_id'.$user_order->id;?>"  name="add_order_id" value="<?php echo $user_order->id;?>">
							   <input type="hidden" class="form-control" id="<?php echo 'add_order_id_price'.$user_order->id;?>"  name="add_order_id_price" value="29.00">
							    <input type="hidden" class="form-control" id="<?php echo 'add_order_company_id'.$company->id;?>"  name="add_order_company_id" value="<?php echo$user_order->company_id;?>">
							
							 <input type="text" class="form-control" id="<?php echo 'service_amount'.$user_order->id;?>"  name="service_amount "value="29.00" onkeyup="getAmountValue('<?php echo $user_order->id;?>');">						
		                 </div>		                 
						  <div class="form-group col-sm-1">
		                     <label>Quantity</label>
							 <input type="text" class="form-control" id="<?php echo 'service_quantity'.$user_order->id;?>"  name="service_quantity"value="1" onChange="calPrice('<?php echo $user_order->id;?>','service')">							
		                 </div>
		                      <input type="hidden" name="instId"  value="1068922"><!-- The "instId" value "211616" "1068922"should be replaced with the Merchant's own installation Id -->
					<input type="hidden" name="cartId" value="abc123"><!-- This is a unique identifier for merchants use. Example: PRODUCT123 -->
					<input type="hidden" name="currency" value="GBP"><!-- Choose appropriate currency that you would like to use -->
					<input type="hidden" name="amount" id="pay_amount<?php echo $user_order->id;?>" value="29.00">
					<input type="hidden" name="desc" value="">
					<input type="hidden" name="testMode" value="0">
					<input type="hidden" name="name" value="AUTHORISED">
		                 <div class="form-group col-sm-4 ">
                               <?php if($role_id === '2')
							  {?>	                 
		                     <input class="btn btn-success btn-mk-payment payment_submit" onClick="payment_form('service_payment<?php echo $user_order->id;?>','<?php echo $user_order->id;?>','pay_now')" 
							 type="button" value="Pay Now" id="<?php echo 'payment_submit'.$user_order->id;?>" />
							<input class="btn btn-success btn-mk-payment payment_submit" onClick="payment_form('service_payment<?php echo $user_order->id;?>',
							 '<?php echo $user_order->id;?>','invoice')"  type="button" value="Pay on Invoice" id="<?php echo 'payment_submit'.$user_order->id;?>" /> 
								 <?php  }else {?>
								   <label class="no-label">options</label>
						           <select class="form-control" id="order_options<?php echo $user_order->id;?>"  name="order_options" >
									<option value="0" id="<?php echo 'order_type'.$user_order->id?>" name="<?php echo 'order_type'.$user_order->id?>">Add To Existing order</option>
									<option value="1" id="<?php echo 'order_type'.$user_order->id?>" name="<?php echo 'order_type'.$user_order->id?>">Create New Order</option>
								</select>
		                     </div> 		  
								<?php	  }?>
                          <div class="form-group col-sm-2">
						      <input class="btn btn-primary btn-mk-payment payment_submit" onClick="payment_form('service_payment<?php echo $user_order->id;?>','<?php echo $user_order->id;?>','pay_now')" 
							 type="button" value="Make Payment Now" id="<?php echo 'payment_submit'.$user_order->id;?>" />
						  </div>
						 <div class="form-group col-sm-12">
							<!--label class="make-pay terms"> <input type="checkbox" name="terms" id="terms" class="required"> <span> I agree to <a href="#" data-target="#myModal_terms" data-toggle="modal">Terms and Conditions</a></span>
							</label-->
						</div>
		             </form>   
					<?php foreach ($order_details as $order_detail){?>
						<form id="<?php echo 'order_detail'.$user_order->id;?>" name="order_detail" action="<?php echo base_url();?>home/orderDetailUpdate" class="order_detail col-sm-12" method="POST">
							<div class="form-group col-sm-3 col-xs-10 box">
									<label for="Nationality">
									<?php 	if($role_id === '2'){
											echo " Current Order:";								  
											}	else	{ 	 
											echo "Order:";
											}
									?>
									</label> 
									<input type="hidden" value="<?php echo$user_order->company_id;?>" name="order_compaqny_id" id="order_compaqny_id<?php echo$user_order->company_id;?>" >
									<input type="hidden" value="<?php echo $order_detail->id;?>" name="product_id" id="product_id<?php echo $order_detail->id;?>" class="form-control text_disable" >
									<input type="text" value="<?php if($order_detail->product == "Complete Package")
									echo "Virtual Business Plus";
									else if($order_detail->product == "Business Address")
									echo "Virtual Business Address";
									else echo $order_detail->product; ?>" name="product_name" id="product_name<?php echo $order_detail->id;?>" class="form-control text_disable" >
							</div>
							<div class="form-group col-sm-2 col-xs-10 box">
							<label for="Nationality">Price:</label> 
							<input type="text" value="<?php
									$dec_price = strstr($order_detail->price, '.');
									if($dec_price)
										echo '£'.$order_detail->price;
									else
										echo '£'.$order_detail->price.'.00';?>" name="product_price" id="product_price<?php echo $order_detail->id;?>" class="form-control text_disable">
							</div>
							<div class="form-group col-sm-2 col-xs-10 box">
									<label for="Nationality">Quantity:</label>
									 <input type="text" value="<?php echo $order_detail->quantity;?>" onChange="calPrice('<?php echo $order_detail->id;?>','product','<?php echo $order_detail->price;?>')" name="product_quantity" id="product_quantity<?php echo $order_detail->id;?>" class="form-control text_disable">
							</div>
							<div class="form-group col-sm-2 col-xs-10 box">
								<label for="Nationality">Date:</label>
							    <input type="text" value="<?php echo $order_detail->create_time;?>" name="product_date" id="product_date<?php echo $order_detail->id;?>" class="form-control text_disable">
							</div>
							<?php if($role_id === '1'){?>
							<div class="form-group col-sm-3 col-xs-10 box action-btn">
								<input type="submit" value="Edit" name="update" class="btn btn-success" />
								<input type="button" value="Delete" name="update" class="btn btn-danger" onClick="deleteRecord('<?php echo $order_detail->id;?>','order_detail','<?php echo $order_detail->product;?>','<?php echo $user_order->id;?>','<?php echo$user_order->company_id;?>')"/>
							</div>
							<?php 	}	?>
						</form>
						<?php 	}	?>	
						<div class="clearfix"></div>
						<hr>
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
							<i class="fa fa-close"></i>
						</a>
						  	<!--form action="<?php //echo base_url();?>home/addOrder" method="POST" id="service_payment" name="service_payment"-->		              
							<div class="col-sm-12 text-center">
								<?php if($role_id !== '2'){?>
									<a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
							  <?php  }	?>
							</div>
					</div>
					      <div role="tabpanel" class="tab-pane" id="<?php echo "tab_mail".$user_order->id;?>">
					        <div class="col-sm-12">
						        <table class="table table-under-panel">
						          <thead>
						            <tr>
						               <th class="date_th">Date</th>
						               <th class="time_th">Time</th>
						               <th class="type_th">Type</th>
						               <th class="file_th">File Name</th>
						               <th class="download_th">Download</th>
						               <th class="original_th">Original Copy</th>
									   <?php if($role_id === '1'){?>
									    <th class="delete_th">Delete</th>
										<?php } ?>
						            </tr>
						          </thead>
						          <tbody class="get_mail_data" id="get_mail_data<?php echo $user_order->id?>">
						          <?php foreach ($files_info as $file_info){
								  if($file_info->type_id !=4 && $file_info->type_id !=5 && $file_info->type_id !=6 && $file_info->type_id !=7 && $file_info->type_id !=8)
								 { 
								  ?>
						            <tr>
						              <td name="mail_date" id="mail_date<?php echo $user_order->id?>"><?php echo date("Y-m-d",strtotime($file_info->create_time));?></td>
						              <td name="mail_time" id="mail_time<?php echo $user_order->id?>"><?php echo date("h:i A",strtotime($file_info->create_time));?></td>
						              <td name="mail_type" id="mail_type<?php echo $user_order->id?>"><?php echo $this->search->getTypeOptions($file_info->type_id);?></td>
						              <td name="mail_file_name" id="mail_file_name<?php echo $user_order->id?>"> <?php echo $file_info->file_name?></td>
						              <!--td>yes</td-->
									    <td name="mail_file_name" id="mail_file<?php echo $user_order->id?>"> <a href="<?php echo base_url().'home/download/'.$file_info->file_name?>">  Click Here </a></td>
									  
									  
									    <?php if($file_info->state_id != 1 ) {?>
									   <td onClick="get_mail_data('<?php echo$user_order->company_id;?>,<?php echo $file_info->create_time;?>,<?php echo $this->search->getTypeOptions($file_info->type_id);?>,<?php echo $file_info->file_name;?>,<?php echo $file_info->id;?>')">
									   	<a href="#">  Request Original </a>
									   </td>
									   <?php } else{?>
									   <td>
									   <a href="#">
									  <?php  echo "Requested -".date("d/m/Y",strtotime($file_info->req_time)); ?>
									  </a>
									  </td>
									<?php  }	?>
									  <?php if($role_id === '1'){?>
									  <td>
									  <a href="#" onClick="deleteRecord('<?php echo $file_info->id;?>','file_mail','null','null','<?php echo$user_order->company_id;?>')"/>Delete</a>
									  </td>
									  <?php }?>
						            </tr>
						            <?php } }?>
						          </tbody>
								</table>
						        <?php if($role_id === '1'){?>
						                <?php echo $error;?> <!-- Error Message will show up here -->
										<?php echo form_open_multipart('home/do_upload');?>
										       <input type="hidden" name="comp_id" id="<?php echo 'comp_id'.$company->id?>" class="form-control" value="<?php echo$user_order->company_id;?>">								
										<?php echo "<input type='file' name='userfile[]' size='20' multiple/>"; ?>
										
										<?php if($role_id === '1'){
											$options = $this->search->getTypeOptions();
										?>
										    <label>Select Type:</label>
										   <select id="file_type_id" class="form-control" name="file_type_id">
										   <?php foreach($options as $key=>$value){?>
							              <option value="<?php echo $key?>"><?php echo $value; ?></option>
											<?php }?>
							              </select>
							              <?php }?>
										<?php echo "<input type='submit' name='submit' value='+Add' class='btn btn-success' /> ";?>
										<?php echo "</form>";
										}?>
						      </div>   
						        <a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
								   <i class="fa fa-close"></i>
								</a>
					      </div>
					      <div role="tabpanel" class="tab-pane" id="<?php echo "tab_message".$user_order->id;?>">
					      <div class="col-sm-12">
						        <table class="table table-under-panel">
						          <thead>
						            <tr>	
						               <th class="date_th">Date</th>
						               <th class="time_th">Time</th>
						               <th class="to_th">To</th>
						               <th class="form_th">From</th>
						               <th class="tel_th">Tel No.</th>
									   <th class="tab_message_email">Email</th>
						               <th class="tab_message_message">Message</th>
									   
									   <?php if($role_id === '1'){?>
									   <th class="delete_th">Delete</th>
									   <?php }?>   
						            </tr>
						          </thead>
						           <tbody>
						           <?php foreach ($messages as $message){?>
						            <tr>
						              <td><?php echo date("Y-m-d",strtotime($message->create_time));?></td>
						              <td><?php echo date("h:i A",strtotime($message->create_time));?></td>
						              <td><?php echo $message->to_id;?></td>
						              <td><?php echo $message->from_id;?></td>
						              <td><?php echo $message->tel_no;?></td>
									    <td><?php echo $message->email_id;?></td>
									   
						              <td class="td_message_class">
									   <input name="message_msg" id="<?php echo 'message'.$message->id;?>" size="40" type="hidden"  class="to_id" value="<?php echo $message->message;?>"  />
									  
									  <?php $strlen =  strlen($message->message);
									  if($strlen<100 )
									  echo $result = substr($message->message, 0, 100);
									  else
									  { echo $result = substr($message->message, 0, 100);
									  
									  ?><a href="#" onClick="getMessage('message<?php echo $message->id;?>')"> Read More</a>
									  <?php } /**/ //echo $message->message;?></td>
									  
									  <?php if($role_id === '1'){?>
									  <td>
									  <a href="#"  onClick="deleteRecord('<?php echo $message->id;?>','message','null','null','<?php echo$user_order->company_id?>')">Delete </a>
									  </td>
									  <?php }?>
						            </tr>
						            <?php }?>
						          </tbody>
						        </table>
						    </div>   
								<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
								   <i class="fa fa-close"></i>
								</a>
					</div>
					    <div role="tabpanel" class="tab-pane" id="<?php echo "tab_document".$user_order->id;?>">
					        <div class="col-sm-12">
						        <table class="table table-under-panel">
						          <thead>
						            <tr>
						               <th class="date_th">Date</th>
						               <th class="time_th">Time</th>
						               <th class="type_th">Type</th>
						               <th class="file_th">File Name</th>
						               <th class="download_th">Download</th>
									   <?php if($role_id === '1'){?>
									   <th class="delete_th">Delete</th>
									   <?php }?>
						            </tr>
						          </thead>
						          <tbody>
						         <?php
								  foreach ($files_info as $file_info){
								  if($file_info->type_id !=0 && $file_info->type_id !=1 && $file_info->type_id !=2 && $file_info->type_id !=3)
								 { ?>
						            <tr>
						              <td><?php echo date("Y-m-d",strtotime($file_info->create_time));?></td>
						              <td><?php echo date("h:i A",strtotime($file_info->create_time));?></td>
						              <td><?php echo $this->search->docTypeOptions($file_info->type_id);?></td>
									  
						              <td><?php echo $file_info->file_name;?></td>
						              <td><a href="<?php echo base_url().'home/download/'.$file_info->file_name;?>" >Click Here</a></td>
									  <!--td><a onclick="download_doc('<?php echo $file_info->file_name; ?>','<?php echo $user_order->id;?>');" >Click Here</a></td-->
									  <?php if($role_id === '1'){?>
									   <td>
									  <a href="#" onClick="deleteRecord('<?php echo $file_info->id;?>','file_doc','null','null','<?php echo$user_order->company_id?>')"/>Delete</a>
									  </td>
									  <?php }?>
						            </tr>
						            <?php }}?>
						          </tbody>
						        </table>
										<?php if($role_id !== '0' && $role_id !== '2'){?>
						          <input type="button" value="Add" name="add_doc"  id = "add_doc<?php echo $user_order->id;?>" class="btn btn-success" onClick="addDoc('<?php echo$user_order->company_id;?>')"/>
										<?php }?>
							 </div> 
						        <a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
								   <i class="fa fa-close"></i>
								</a>
					      </div>
					  <div role="tabpanel" class="tab-pane" id="<?php echo "tab_emails".$user_order->id;?>">
					     <form  name="update_company" method="POST" action="<?php echo base_url();?>home/addAltEmail" id="altEmail"<?php echo$user_order->company_id;?>>
					         <div class="form-group col-sm-6 col-xs-12 box">
					   		  <input type="hidden" name="company_id" id="company_id<?php echo$user_order->company_id;?>" class="form-control" value="<?php echo$user_order->company_id;?>">
									<label for="Email1 ">Email Alert 1:</label> <input type="email" name="email_1" id="email_1<?php echo$user_order->company_id;?>" class="form-control" value="<?php echo $update_company_user->email;?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Email2">Email Alert 2:</label> <input type="email" name="email_2" id="email_2<?php echo$user_order->company_id;?>" class="form-control" value="<?php echo $alt_emails->email_2?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="Email3">Email Alert 3:</label>
									 <input type="email" name="email_3" id="email_3<?php echo$user_order->company_id;?>" class="form-control" value="<?php echo $alt_emails->email_3?>">
							</div>
							<div class="form-group col-sm-6 col-xs-12 box">
									<label for="4">Email Alert 4:</label>
									 <input type="email" name="email_4" id="email_4<?php echo$user_order->company_id;?>" class="form-control" value="<?php echo $alt_emails->email_4?>">
							</div>
							  <div class="clearfix"></div>
					     <hr>
					    <div class="col-sm-12 text-center">
					   <input type="submit" value="Update" name="update" class="btn btn-success"/>
					      <a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
					    </div>
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
						   <i class="fa fa-close"></i>
						</a>
						</form>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="<?php echo "tab_activity".$user_order->id;?>">
					     <form class="col-sm-12" name="activity_company" method="POST" action="" id="activity_company"<?php echo$user_order->company_id;?>>
							<table class="table">
								<thead>
									<tr>
										<th>Activity</th>
										<th>Date & Time</th>
									</tr>
								</thead>
								<tbody>
									<?php	foreach($comp_activities as $comp_activity)	{ 	?>
									<input type="hidden" name="company_id" id="company_id<?php echo $comp_activity->id;?>" class="form-control" value="<?php echo $comp_activity->id;?>">
									<tr>
										<td>
											<label for="activity_name "><?php echo $comp_activity->activity;?></label>
										</td>
										<td>
											<label for="activity_time "><?php echo $comp_activity->create_time;?></label>
										</td>
									</tr>
									<?php	}	?>					
								</tbody>
							</table>
								<div class="clearfix"></div>
								<hr>
								<div class="col-sm-12 text-center">
									<a class="btn btn-danger accordion-toggle" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">Close</a>
								</div>
						</form>
						<a class="btn-close" id="<?php echo 'packageDetails'.$user_order->id;?>" data-toggle="collapse" data-parent="#OrderPackages" data-target="<?php echo '.packageDetails'.$user_order->id;?>">
						   <i class="fa fa-close"></i>
						</a>
					    </div>
					</div>
			     </div>
			</tr>
				<?php 
					}
				}
			}?>
		</tbody>
	</table>
	</div>
<?php 	if($this->pagination->create_links()){			$class="scroll_set";		?>
<?php 	}	else	{				$class= "scroll_set";			}		?>

<div class="<?php echo $class; ?>">	 
<?php 	if($this->pagination->create_links()){	?> 
<?php 	}else{	}	?>
	 <div class="row pagination-btm" style="float:right;margin:0;">
       <?php if($this->pagination->create_links()){ ?>
     <div  style="float:left; color:#303030">
     <?php 
			$url =  $_SERVER['REQUEST_URI']; 
			
			$urlexp = explode('/',$url);
			//echo $this->uri->segment(2);
			//print_r($urlexp);
			$data_posted= urldecode($_SERVER['QUERY_STRING']);
			//print_r($data_posted);
			$pieces = explode("&", $data_posted);
			
			for($a=0;$a<count($pieces);$a++)
			{
				$profile_key=strstr($pieces[$a],"=",true);
				$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
			}	
			//print_r($profile);
 	 ?>
    <!--form method="get" action="<?php echo base_url().''.$urlexp['2'].'/'.$urlexp['3']; ?>" id="formElementId"-->
    <form method="get" action="<?php echo base_url(); ?>dashboard/<?php echo $this->uri->segment(2);//$urlexp['3'];?>" id="formElementId">
	<?php	foreach($profile as $key=>$val){	?>
		<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
	<?php }	?>
    <select name="page"  id="selectElementId">
        <option value="10"<?php if($pagef=='10'){ echo 'selected="selected"';} ?>>10 Items</option>
    	<option value="25"<?php if($pagef=='25'){ echo 'selected="selected"';} ?>>25 Items</option>
        <option value="50"<?php if($pagef=='50'){ echo 'selected="selected"';} ?>>50 Items</option>
        <option value="100"<?php if($pagef=='100'){ echo 'selected="selected"';} ?>>100 Items</option>
    </select>
    </form>
    </div>
<?php 	echo $this->pagination->create_links();		}	?>
  </div>
  </div>
 </section>
	<?php if( $this->uri->segment(2) == "searchResult" ||  $this->uri->segment(2) == "renewalRecordsNew"){ ?>
		<?php $this->load->view('director_view');	?>
	<?php } ?>
<script>
	$('#selectElementId').change(function(){
		$('#formElementId').submit();
    });
	
	function download_doc(file_name,userid){
		$.ajax({
			'url' : base_url+"home/download",
			'type' : 'POST',
			'data' :{
				'file_name' : file_name,
				'userid' :userid
			}, 
			'success' : function(data){
				alert(data);
			},
			'error' : function(request,error){
				alert("Request: "+JSON.stringify(request));
			}
		});
	}
</script> 
<?php $this->load->view('modal');?>
<?php $this->load->view('footer');?>


