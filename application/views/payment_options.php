<?php $this->load->view ('header');?>
<div class="col-xs-12 paymentoption">
<div class="overview-box1 payment_head">
		<h3>Address services</h3>
		<div class="payment_box_outer">
			<div class="payment_box">
				<div class="col-xs-3 pay_img_sec">
					<img class="img-responsive" src="<?php echo base_url()?>assets/images/pay_3.png">
				</div>
				<div class="col-xs-6 pay_cont_sec">
					<div class="row">
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3 pay_info_sec">
					<button type="submit" class="btn btn-success">Continue</button>
				</div>
			</div>
			<div class="payment_box">
				<div class="col-xs-3 pay_img_sec">
					<img class="img-responsive" src="<?php echo base_url()?>assets/images/pay_2.png">
				</div>
				<div class="col-xs-6 pay_cont_sec">
					<div class="row">
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3 pay_info_sec">
					<p><a href="#">Upgrade </a>to Bussiness us londonformation.com</p>
				</div>
			</div>
			<div class="payment_box">
				<div class="col-xs-3 pay_img_sec">
					<img class="img-responsive" src="<?php echo base_url()?>assets/images/pay_1.png">
				</div>
				<div class="col-xs-6 pay_cont_sec">
					<div class="row">
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
						<div class="col-xs-6">
							<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
							<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
							<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
							<p><i class="fa fa-university"></i> Standard dummy text ever</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3 pay_info_sec">
					<p><a href="#">Upgrade </a>to Bussiness us londonformation.com</p>
				</div>
			</div>
		</div>
		
	</div>
	</div>
<?php $this->load->view ('footer');?>
<?php $this->load->view ('modal');?>