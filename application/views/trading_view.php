<?php //$this->load->view ('header');?>
<?php //$this->load->view ('includes/left_nav');?>
<?php $uri_test = $this->uri->segment(2); 
$uri = $_SERVER['REQUEST_URI'];?>
<?php $session_data = $this->session->userdata('logged_in');
$role_id = $session_data['role_id'];
?>
<section id="content_info" <?php (($uri_test == "searchResult") ? "echo class='top_content_info'" : "echo class =''"); ?>>
<?php  

if($message)
{//echo $message;
	 echo '<script type="text/javascript">';
     echo 'setTimeout(function () { swal("Document Uploaded");';
     echo '}, 2000);</script>';
}
else
{
 
}
?>
<center class="text-primary"><?php echo isset($message)?$message:'';?></center>
<div class="loader_background" id="loader_background" style="display:none;">
	<div class="loader" id="loader">
	</div>
</div>
<div class="diector_view trading_view"><span>Trading Name Results<span></div>
 <div class="table_data newdirclass">   
	<table class="table table-hover table-bordered">
	
	<?php	if($role_id === '0' || $role_id === '2'){ ?>
		<thead class="home-table-header static-header">
	<?php 	}else{	?>
		<thead class="home-table-header">
	<?php 	}	 ?>
		<tr>	
		<?php $countDir = $this->order->counttrading($user_orders); ?>
			<th class="cmpny th_company_class">Trading Name (<?php echo $countDir; ?>)</th>
			<th class="cmpny th_company_class">
            <?php 
 				$total =  $this->uri->segment(3)+1; 
				//var_dump($total);die("dff");
 				if($total<>1){
				  $totaldss = $this->uri->segment(3)+$pagef;
				  //var_dump($totaldss);die("if");
				}else{
					$totaldss = $pagef; 
					//var_dump($pagef);die("else");
				}
				if($totaldss > $record_count){
					$totaldss = $record_count;
				}
 				if($this->pagination->create_links()){
			?>
					 Company(<?php echo $total.'-'.$totaldss; ?> of <?php echo $record_count;?>)
              <?php } else{ ?>
               		 Company(<?php echo $record_count;?>)
              <?php	 }  ?>
 			  
			  
			</th>
			<?php $services =  $this->search->getServiceOptions();
			?> 
			<th class="th_state_class">
			<form action="<?php echo base_url();?>home/state_change1" method="GET" name="state_form_change" id="state_form_change">
			<input type="hidden" id="query_string_1" name="query_string_1" value="<?php echo $_GET['new_search_bar']?>">
			<input type="hidden" id="query_string_2" name="query_string_2" value="<?php echo $_GET['search_new']?>">
			<?php $states =  $this->search->getStatusOptions();?>
			 <select id="state_change" name="state_change">
			    <option>Status</option>
				<option value="Show All">Show All</option>
			      <?php foreach($states as $key =>$state){	?>
			    <option value="<?php echo $key ;?>"><?php echo $state;?></option>
			   <?php }	?>
			    
			  </select>
			  
			  </form>
			 
			</th>
					
			
			<!--?php $url=$this->uri->segment(2);
			if($url=="renewable_new" || $url=="renewalRecordsNew")
			{
			?>
			<th onClick="getDateOtions()" class="th_renew_class">
			
			Previous
			</th>
			<th onClick="getDateOtions()" class="th_renew_class">
			
			Next
			
			
			</th>
			< ?php } else { ?-->
			<th onClick="getDateOtions()" class="th_renew_class">
			
			Renew
			
			
			</th>
			<!--?php } ?-->
			
 			<th class="th_location_class">
 					<form action="<?php echo base_url();?>home/filterSearch" method="POST" name="location" id="location_select">
						<?php $locations =  $this->search->getLocationOptions();?>
						<select id="location_select_option" name="location_select" class="getSelect1 locClick">
								<option>Office</option>
							<?php  foreach($locations as $location){?>
								<option value="<?php echo $location;?>"><?php echo $location;?></option>
							<?php }?>
			   
						</select>
				  </form>
				 
			</th>
			<th class="th_ltd th_reg_class">
			
			<form action="<?php echo base_url();?>home/ltdSearch" method="POST" name="comp_ltd" id="comp_ltd">
						<?php $ltds =  $this->order->getLtdOptions();?>
						<select id="comp_ltd_select_option" name="comp_ltd_select" class="getSelect1 ltdClick">
								<option>LTD</option>
							<?php  foreach($ltds as $key=>$ltd){?>
								<option value="<?php echo $key;?>"><?php echo $ltd;?></option>
							<?php }?>
			   
						</select>
				  </form>
			
			</th>
			<th class="th_reg_class">
			PDF+
			</th>
			<th class="th_reg_class">
			<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="registered_office" id="registered_office_select">
		
			  <select id="registered_office" name="registered_office" class="getSelect1 regClick">
						<option>ROA</option>
				   <?php  foreach($services as $service){?>
						<option value="<?php echo $service;?>"><?php echo $service;?></option>
					<?php }?>
			   
			  </select>
			  </form>
			  
			</th>
			
			<th class="th_director_class">
			<form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="director_service_address" id="director_service_address_select">
			  <select id="director_service_address"  name="director_service_address" class="getSelect1 dirClick">
			    <option>DSA</option>
				  <?php foreach($services as $service){?>
					<option value="<?php echo $service;?>"><?php echo $service;?></option>
			   <?php }?>
			    
			  </select>
			  </form>
			 
			</th>
			
			<th class="th_business_class">
			 <form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="business_address" id="business_address_select">
			  <select id="business_address" name="business_address" class="getSelect1 busClick">
			    <option>VBA</option>
				  <?php foreach($services as $service){?>
			    <option value="<?php echo $service;?>"><?php echo $service;?></option>
			   <?php }?>
			  
			  </select>
			  </form>
			  
			</th>
			<th class="th_tele_class">
			  <form action="<?php echo base_url();?>home/serviceSearch" method="POST" name="telephone_service" id="telephone_service_select">
				<select id="telephone_service" name="telephone_service" class="getSelect1 telClick">
						<option>TAS</option>
					<?php foreach($services as $service){?>
						<option value="<?php echo $service;?>"><?php echo $service;?></option>
					<?php }?>
			   
			  </select>
			  </form>
			 
			</th>
			<?php 
			if($role_id === '1' || $role_id === '2')
			{?>
				<th class="th_billing_class" width="80">
				
		 <form action="<?php echo base_url();?>home/billing_change" method="POST" name="type_form_change" id="type_form_change">
			<?php $types =  $this->order->getTypeOptions();?>
			 <select id="type_change" name="type_change">
			    <option>Payment</option>
				<option value="Show All">Show All</option>
			      <?php foreach($types as $key =>$type){?>
			    <option value="<?php echo $key ;?>"><?php echo $type;?></option>
			   <?php }?>
			    
			  </select>
			  
			  </form>		  		  
			 
			</th>
			<?php }?>
			<th width="80px" class="th_diposite_class">
				Deposit			  		  
				
			</th>
			<th class="th_price_class">
			Charge 
			
			</th>
			<th class="th_upload_class" width="70px">
			<form action="<?php echo base_url();?>home/filterSearch" method="POST" name="upload_form_change" id="upload_form_change">
			<?php $uploads =  $this->search->getUploadOptions();?>
			  <select id="upload_change" name="upload_change" class="getSelect1 IdClick">
			  <option>ID</option>
			  <option value="Show All">Show All</option>
			  <?php foreach($uploads as $upload){?>
			    <option value="<?php echo $upload;?>"><?php echo $upload;?></option>
			   <?php }?>
			  </select>
			</form>
			
			</th>
			<?php if($state_change === '4' && $role_id === '1'){?>
				<th class="">
					Delete Company
					
				</th>
			<?php }?>
			<th>POA</th>
			<th>Email</th>
			<th>Note</th>
			
		</tr>
		</thead>		
				 
			
		
		<tbody id="OrderPackages">
			<?php 
			if($user_orders){
				
				foreach ($user_orders as $key=>$user_order)
				{
				  // $company =  $this->search->filterSearch($user_order->company_id);
				  // $user =  $this->search->userSearch($user_order->create_user_id);
				  // $update_company_user = $this->search->userSearch($user_order->create_user_id);
				  // $billing =  $this->search->billing($company->billing_adress_id);
				  // $mailling =  $this->search->mailling($company->mailing_adress_id);
				  // $orders =  $this->search->orders($user_order->company_id);
				  // $order_details =  $this->search->orderDetails($orders->id);
				  // $files_info =  $this->search->fileInfo($user_order->create_user_id);
				  // $files_info_reseller =  $this->search->fileInfo1($user_order->reseller_id);
				  // $messages =  $this->search->Message($user_order->id);
				  // $alt_emails =  $this->search->alterEmails($user_order->company_id);
				  // $comp_tele_service =  $this->search->comp_tele_service($user_order->company_id);
				  // $comp_secretaries =  $this->search->comp_secretary($user_order->company_id);
				  // $comp_activities =  $this->search->comp_activity($user_order->company_id);
				  // $document =  $this->ltd_model->GetDocument($user_order->company_id);
				  // $files_info_attach =  $this->search->files_info_attach($user_order->create_user_id);
					?>
					
			<tr id="<?php echo 'row_'.$user_order->id;?>">
			<?php 
			if($user_order->comp_ltd =="1")
			{
				$limited= "Limited";
			}
			elseif($user_order->comp_ltd == "2")
			{
				$limited= "Ltd";
			}
			elseif($user_order->comp_ltd == "3")
			{
				$limited= "Lp";
			}
			elseif($user_order->comp_ltd == "4")
			{
				$limited= "Llp";
			}
			else{
				$limited= "";
			}
			?>
			<td class="reseller td_reseller_class">
			<?php 	
					echo $user_order->trading;
			?></td>
				<td class = "td_company_class dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">
				<?php
				$comp_name_length = strlen($user_order->company_name);
					if($comp_name_length >=10){?>
					<span class="comp_full_name">
				<?php echo stripslashes($user_order->company_name ." ".$limited);?>
				</span>
				<?php }
				else
					echo stripslashes($user_order->company_name ." ".$limited);
					?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo  base_url();?>dashboard/showCompanyResult?id=<?php echo $user_order->company_id ?> ">Company Overview</a></li>
					<li><a href="javascript:void(0)" onclick ="userDetails ('<?php echo $user_order->company_id;?>')">Client Details</a></li>
					<li><a href="javascript:void(0)" onclick ="billingDetails('<?php echo $user_order->company_id;?>')">Billing Information</a></li>
					<li><a href="javascript:void(0)" onclick ="orderDetails('<?php echo $user_order->company_id;?>','<?php echo $orders->id;?>','<?php echo $user_order->company_name;?>')">Order Information</a></li>
				</ul>
			</td>
				
			<?php if($role_id === '1' || $role_id === '2'){?>
			
			<!--td data-cname="<?php echo $user_order->company_name;?>" class="reseller td_reseller_class"  onclick = "reseller('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>',<?php echo($user_order->company_id);?>,'<?php echo $user_order->reseller;?>')"><a href="#"><?php echo $user_order->reseller;?></a>
			<!--div class="sub__menu">
				<span><a href="#">Company name</a></span>
				<span><a href="#">Company name</a></span>
				<span><a href="#">Company name</a></span>
				<span><a href="#">Company name</a></span>
			</div-->
			
			<!--/td-->
			
			<td class = "state_id td_status_class" data-id="<?php echo $user_order->id;?>" data-cname="<?php echo $user_order->company_name;?>" data-content="<?php echo $user_order->company_id;?>" data-email="<?php echo $user_order->email;?>" data-currentdata="<?php echo $this->search->getStatusOptions($user_order->state_id);?>">
			<?php //var_dump($_GET['state_change']);?>
            <a href="#">
			<?php 
				$url = $this->uri->segment(2);
				$url1 = base_url()."home/".$url;
			?>
			<?php if($user_order->state_id=="10"){
				echo '<div class="green">';
				echo "Active";
				echo '<div>';
			}elseif($url1 == "https://www.companymanager.online/home/new_formation"){
				echo '<div class="green">';
				echo "In Review";
				echo '<div>';
			}elseif($user_order->state_id=="0" || $user_order->state_id=="9" || $user_order->state_id=="6"){
				echo '<div class="green">';
				echo $this->search->getStatusOptions($user_order->state_id);
				echo '</div>';
			}elseif($user_order->state_id=="5"){
				echo '<div class="orange">';
				echo $this->search->getStatusOptions($user_order->state_id);
				echo '</div>';
			} elseif($_GET['state_change']=="14"){
				echo '<div class="orange">';
				echo "In Review";
				echo '<div>';
			} else{
				echo '<div class="red">';
				echo $this->search->getStatusOptions($user_order->state_id);
				echo '</div>';
			}?>
			</a> 
            </td>
            
            
            
			
			<!--?php $url=$this->uri->segment(2);
			if($url=="renewable_new" || $url=="renewalRecordsNew")
			{
			?>
			<td-->
			<td  data-cname="<?php echo $user_order->company_name;?>" class = "renew_id td_renew_class" id="<?php echo $user_order->id;?>" onclick = "displayDatepicker('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $date_renewal_cur = date ("d-m-Y",strtotime($user_order->renewable_date));?>')">
				<a href="#">	
					<?php  $date_current = date ("d-m-Y");
					       $date_renewal= date ("d-m-Y",strtotime($user_order->renewable_date));
						   //$user_order->renewable_date_next
						  if(strtotime($date_renewal) > strtotime($date_current)){
							  echo '<div class="green">';
							  echo $date_renewal;
							  echo '</div>';
							  
						  }else{
							  echo '<div class="red">';
							  echo $date_renewal;
							  echo "</div>";
						  }
						?>
				</a>					
			</td>
			
			<td onclick =" location_event('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $user_order->location;?>')" class="td_location_class">
				<a href="#">
					<?php echo $user_order->location;?>
				</a>
			</td>
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" >
				<?php 
				$state_id = $user_order->state_id;
				$comp_ltd = $user_order->comp_ltd;
				$state_id_new = $user_order->state_id_new;
				$ltd = $this->order->getLtdOptions($user_order->comp_ltd);
				/* if($state_id ==6 && $ltd=="YES"){
					echo "<div class='red'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id ==9 && $ltd=="YES"){
					echo "<div class='orange'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id ==0 && $ltd=="YES"){
					echo "<div class='green'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id ==3 && $ltd=="YES"){
					echo "<div class='green'>";
					echo $ltd;
					echo "</div>";
				}else{
					echo $ltd;
				} */
			     if($state_id ==3 && $comp_ltd=="1"){
				
					echo "<div class='red'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id =='6' && $comp_ltd=="1" && $state_id_new =='0'){
				
					echo "<div class='orange'>";
					echo $ltd;
					echo "</div>";
				}else if ($state_id_new =='1' && $comp_ltd=="1" && $state_id =='6'){
					echo "<div class='green'>";
					echo $ltd;
					echo "</div>";
				} else{
					echo "<div class='red'>";
					echo $ltd;
					echo "</div>";
				} 
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="changeLtd('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $this->order->getLtdOptions($user_order->comp_ltd);?>')">Change Status</a></li>
					<li><a href="javascript:void(0)" onClick="uploadData('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>')">Upload Data</a></li>
					<li><a href="javascript:void(0)" onClick="company_num_house('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>')">Import Data</a></li>
				</ul>
			</td>
			<td>
			<?php 
			$atach = $files_info_attach->email_alert;
			if($atach == '1')
			{?>
				<div class="green">YES</div>
			<?php }
			else
			{ ?>
				<div class="red">NO</div>
			<?php }
			?>
			</td>
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php $ros = strtoupper($user_order->registered_office);
					if($ros == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
					 	if($fetch_preferene->officail_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $ros;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $ros;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $ros;
							echo "</div>";
						}else{
							echo "<div class='green'>";
							echo $ros;
							echo "</div>";
						} 
					}else {
						echo $ros;
					} 
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','omail')">Official Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmailo('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Official Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','rosbmail')">Official Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','ros')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
				</ul>
			</td>
			
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php
					$dsa = strtoupper($user_order->director_service_address);
					if($dsa == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
						if($fetch_preferene->officail_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $dsa;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $dsa;
							echo "</div>";
						}else if($fetch_preferene->officail_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $dsa;
							echo "</div>";
						}else{
							echo "<div class='green'>";
							echo $dsa;
							echo "</div>"; 
						}
					}else {
						echo $dsa;
					}
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','omail')">Official Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmailo('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Official Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','dsabmail')">Official Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','dsa')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" class="img_set">
				<?php
					$vba = strtoupper($user_order->business_address);
					
					if($vba == 'YES'){
						$fetch_preferene = $this->order->selectPreferences($user_order->create_user_id);
						if($fetch_preferene->business_mail_option1=='yes'){
							echo "<div class='green'>";
							echo $vba;
							echo "</div>";
						}else if($fetch_preferene->business_mail_option2=='yes'){
							echo "<div class='orange'>";
							echo $vba;
							echo "</div>";
						}else if($fetch_preferene->business_mail_option3=='yes'){
							echo "<div class='red'>";
							echo $vba;
							echo "</div>";
						}else{
							echo "<div class='orange'>";
							echo $vba;
							echo "</div>";
						}
					}else {
						echo $vba;
					}
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="mail_handel('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','bmail')">Business Mail – Scan</a></li>
					<li><a href="javascript:void(0)" onClick="newEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Business Mail – Forward</a></li>
					<li><a href="javascript:void(0)" onClick="mail_business('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','vbabmail')">Business Mail – Collect</a></li>
					<li><a href="javascript:void(0)" onClick="upgradeEmail('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>','vba')">Upgrade Email</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="openPref1('<?php echo $user_order->create_user_id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo addslashes($user_order->company_id);?>')">Change Preference</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)" >
				<?php $tas = strtoupper($user_order->telephone_service);
				if($tas=="YES"){
				  echo '<div class="green">';
				  echo $tas;
				  echo '</div>';
				}else{
				  echo $tas;
				}
				?>

				</a>
				<ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onClick="message_handel('<?php echo $user_order->id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $update_company_user->email;?>','<?php echo addslashes($company->tas_notes);?>',' <?php echo addslashes($company->comp_info);?>')">Add Message</a></li>
					<li><a href="javascript:void(0)" onClick="cancel_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">Service Cancelled</a></li>
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
				</ul>
			</td>
			<?php }else{?>
			<td class = "td_status_class" data-id="<?php echo $user_order->id;?>"><?php echo $this->search->getStatusOptions($user_order->state_id);?></td>
			<td  class = "renew_id td_renew_class" id="<?php echo $user_order->id;?>"><?php echo date ("d-m-Y",strtotime($user_order->renewable_date));?></td>
			<td class="td_location_class"><?php echo $user_order->location;?></td>
			<td>
			<?php echo strtoupper($this->order->getLtdOptions($user_order->comp_ltd));?>
			</td>
			<td class="td_reg_class"><?php echo strtoupper($user_order->registered_office);?></td>
			<td class="td_director_class"><?php echo strtoupper($user_order->director_service_address);?></td>
			<td class="td_business_class"><?php echo strtoupper($user_order->business_address);?></td>
			<td class="td_tele_class"><?php echo strtoupper($user_order->telephone_service);?></td>
			<?php }			
			?>
			<?php if($role_id === '1'){?>
			<td class="td_bill_class" data-id="<?php echo $user_order->id;?>" data-billing_cname="<?php echo $user_order->company_name;?>" data-cid ="<?php echo $user_order->company_id;?>" ><a href="#">
			<?php
				if($user_order->type_id == "0")
				{
					echo "Annual";
				}
				else if($user_order->type_id == '1'){
					echo "Monthly";
				}
			?></a></td>
			<?php $dot = number_format((float)$user_order->deposit, 2, '.', '');?>
			
			
			<td class="td_deposit_class" onclick ="editDiposite('<?php echo $user_order->id?>','<?php echo $dot?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>')">
			<?php }else{?>
				<td class="td_deposit_class">
			<?php }?>
			
			<?php 			
			$dot = number_format((float)$user_order->deposit, 2, '.', '');
				echo '<a href="#">&pound;'.$dot.'</a>';
			?></td>
			<td class="td_price_class" onclick="editPrice('<?php echo $user_order->id?>','<?php echo $user_order->deposit?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>')"><a href="#">&pound;<?php 
			$total_price = $this->order->orderPrice($user_order->id);		
			$dot = strstr($total_price, '.');
			if($dot)
				echo $total_price;
			else
				echo $total_price.'.00';
			?></a></td>
			<td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			$file_type_resseller="";
			foreach($files_info_reseller as $file_info){
				$file_type_resseller=$file_info->reseller_id;
			}
			foreach($files_info as $file_info){
				$file_type_id[]=$file_info->type_id;
			}
			$file_type_upload1 =  in_array("4",$file_type_id);
			$file_type_upload3 =  in_array("10",$file_type_id);
			$getYes = "";
			if($file_type_resseller!="")
			//if($file_type_resseller!="" && $file_type_upload1=== true)
			{
				echo "<div class='green' title='Id Recieved'>";
				echo "YES";
				echo "</div>";
			}elseif($file_type_upload1=== true || $file_type_upload3 === true){		    
			    echo "<div class='green' title='Id Recieved'>";
				echo "YES";
				echo "</div>";
			}else{
			    echo "<div class='red' title='Id Not Recieved'>";
				echo "NO";
				echo "</div>";
			}
		
			?></a>
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadId('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $reseller->id; ?>')">Upload ID</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			$file_type_resseller="";
			foreach($files_info_reseller as $file_info){
				$file_type_resseller=$file_info->reseller_id;
			}
			foreach($files_info as $file_info){
				$file_type_id[]=$file_info->type_id;
			}
			$file_type_upload2 =  in_array("5",$file_type_id);
			$file_type_upload3 =  in_array("10",$file_type_id);
			$getYes = "";
			if($file_type_resseller!=""){
			//if($file_type_resseller!="" && $file_type_upload2=== true){
				echo "<div class='green' title='Proof Address Recieved'>";
				echo "YES";
				echo "</div>";
			} else if($file_type_upload2=== true || $file_type_upload3 === true){
			    echo "<div class='green' title='Proof Address Recieved'>";
				echo "YES";
				echo "</div>";
			}else{
			    echo "<div class='red' title='Proof Address  Not Recieved'>";
				echo "NO";
				echo "</div>";
			}
			?></a>
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $user_order->company_id;?>','<?php echo addslashes($user_order->company_name);?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadId('<?php echo $user_order->company_id;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $reseller->id; ?>')">Upload ID</a></li>
				</ul>
			</td>
			<td class="dropdown set_dropdown_ltd">
			<a data-toggle="dropdown" class="dropdown-toggle envelope_icon" aria-expanded="false" href="javascript:void(0)"><i class="fa fa-envelope" aria-hidden="true"></i></a>
			<ul class="dropdown-menu envelope_menu">
			<li><a href="mailto:<?php echo $user_order->email?>"><?php echo $user_order->email;?></a></li>
			</ul>
			
			</td>
			<td class="dropdown set_dropdown_ltd">
			<a class="dropdown-toggle envelope_icon" href="javascript:void(0)" onClick="notes_handel('<?php echo $user_order->id;?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_id;?>','<?php echo $update_company_user->email;?>','<?php echo addslashes($company->notes);?>',' <?php echo addslashes($company->comp_info);?>',' <?php echo addslashes($user_order->create_user_id);?>')">
			<?php if($company->notes){?><div class="chat_hover"><i class="fa fa-commenting-o active" aria-hidden="true"></i><span><?php echo $company->notes; ?></span></div><?php } 
		else{?>
			
			<i class="fa fa-commenting-o active1" aria-hidden="true"></i>
            
			<?php }?>
			</a>
			</td>
				<?php if($state_change === '4' && $role_id === '1'){?>
				<td><a href="<?php echo base_url().'home/deleteCompany/'.$user_order->company_id;?>" class="btn btn-danger">Delete</a></td>
			<?php }?>
					</tr>		
		<?php }
	}?>
		</tbody>
	</table>
    

	</div>
<?php 	if($this->pagination->create_links()){			$class="scroll_set";		?>
<?php 	}	else	{				$class= "scroll_set";			}		?>

<div class="<?php echo $class; ?>">	 

<?php 	if($this->pagination->create_links()){	?> 
<?php 	}else{	}	?>
	 <div class="row pagination-btm" style="float:right;margin:0;">
       <?php if($this->pagination->create_links()){ ?>
     <div  style="float:left; color:#303030">
     <?php 
			$url =  $_SERVER['REQUEST_URI']; 
			
			$urlexp = explode('/',$url);
			$data_posted= urldecode($_SERVER['QUERY_STRING']);
			
			$pieces = explode("&", $data_posted);
			
			for($a=0;$a<count($pieces);$a++)
			{
				$profile_key=strstr($pieces[$a],"=",true);
				$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
			}	
 	 ?>
    <form method="get" action="<?php echo base_url(); ?>dashboard/<?php echo $this->uri->segment(2);//$urlexp['3'];?>" id="formElementId">
	<?php	foreach($profile as $key=>$val){	?>
		<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
	<?php }	?>
    <select name="page"  id="selectElementId">
        <option value="10"<?php if($pagef=='10'){ echo 'selected="selected"';} ?>>10 Items</option>
    	<option value="25"<?php if($pagef=='25'){ echo 'selected="selected"';} ?>>25 Items</option>
        <option value="50"<?php if($pagef=='50'){ echo 'selected="selected"';} ?>>50 Items</option>
        <option value="100"<?php if($pagef=='100'){ echo 'selected="selected"';} ?>>100 Items</option>
    </select>
    </form>
    </div>
<?php 	echo $this->pagination->create_links();		}	?>
  </div>
 </section>
	
<script>
	$('#selectElementId').change(function(){
		$('#formElementId').submit();
    });
	
	function download_doc(file_name,userid){
		$.ajax({
			'url' : base_url+"home/download",
			'type' : 'POST',
			'data' :{
				'file_name' : file_name,
				'userid' :userid
			}, 
			'success' : function(data){
				alert(data);
			},
			'error' : function(request,error){
				alert("Request: "+JSON.stringify(request));
			}
		});
	}
</script>