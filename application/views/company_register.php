<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/left_nav');?>
<?php $uri_test = $this->uri->segment(2); 
//var_dump($user_orders);die();
?>
<section id="content_info" <?php (($uri_test == "searchResult") ? "echo class='top_content_info'" : "echo class =''"); ?> class="add__service">
<?php $this->load->view ('includes/additonal_sidebar');?>
<center class="text-primary"><?php echo isset($message)?$message:'';?></center>
<div class="add_service__sidebar">
<?php $this->load->view ('includes/search_bar');?>
<div class="loader_background" id="loader_background" style="display:none;">
	<div class="loader" id="loader">
	</div>
</div>
  <div class="table_data new_tables">   
	<table class="table table-hover table-bordered">
		<thead class="home-table-header">
		<tr>
		<tr>			
			<th class="cmpny th_company_class">
            <?php 
 				$total =  $this->uri->segment(3)+1; 
				if($total<>1){
					$totaldss = $this->uri->segment(3)+$pagef;
				}else{
					$totaldss = $pagef; 
				}
				if($totaldss > $record_count){
					$totaldss = $record_count;
				}
 				if($this->pagination->create_links()){
			?>
					 Company(<?php echo $total.'-'.$totaldss; ?> of <?php echo $record_count;?>)
              <?php } else{ ?>
               		 Company(<?php echo $record_count;?>)
              <?php	 }  ?>
			</th>
			<th class="th_state_class">
			<form action="<?php echo base_url();?>home/company_register_status" method="GET" name="company_register_status" id="company_register_status">
			<input type="hidden" id="query_string_1" name="query_string_1" value="<?php echo $_GET['new_search_bar']?>">
			<input type="hidden" id="query_string_2" name="query_string_2" value="<?php echo $_GET['search_new']?>">
			 <select id="company_register_state_change" name="company_register_state_change">
			    <option>Status</option>
				<option value="Show All">Show All</option>
			    <option value="1">Deliverd</option>
			    <option value="2">Cancelled</option>
			    <option value="0">Pending</option>
			  </select>
			  </form>
			</th>
			<th>Order Date</th>
			<th>Delivered</th>
			<th>Charge</th>
		</tr>
		</thead>
		<tbody id="OrderPackages">
			<?php if($user_orders){ 
				//var_dump($user_orders);
				foreach ($user_orders as $key=>$user_order){
				  // $company =  $this->search->filterSearch($user_order->company_id);
				  // $user =  $this->search->userSearch($user_order->create_user_id);
				  // $update_company_user = $this->search->userSearch($company->create_user_id);
				  // $orders =  $this->search->orders($user_order->company_id);
				  // $orders_details =  $this->search->Order_details_data($user_order->id);
				  // //var_dump($orders_details);die();
				  // $files_info =  $this->search->fileInfo($user_order->create_user_id);
				  // $files_info_reseller =  $this->search->fileInfo1($user_order->reseller_id);
				  // $Get_company_data_formation =  $this->search_model->Get_company_data_formation($user_order->company_id);
			?>
		 <tr>
		 <td class = "td_company_class dropdown set_dropdown_ltd">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">
				<?php
				$comp_name_length = strlen($user_order->company_name);
					if($comp_name_length >=10){?>
					<span class="comp_full_name">
				<?php echo stripslashes($user_order->company_name);?>
				</span>
				<?php }
				else
					echo stripslashes($user_order->company_name);
					?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo  base_url();?>dashboard/showCompanyResult?id=<?php echo $user_order->company_id ?> ">Company Overview</a></li>
					<li><a href="javascript:void(0)" onclick ="userDetails ('<?php echo $user_order->company_id;?>')">Client Details</a></li>
					<li><a href="javascript:void(0)" onclick ="billingDetails('<?php echo $user_order->company_id;?>')">Billing Information</a></li>
					<li><a href="javascript:void(0)" onclick ="orderDetails('<?php echo $user_order->company_id;?>','<?php echo $user_order->id;?>','<?php echo $user_order->company_name;?>')">Order Information</a></li>
				</ul>
			</td>
			<td class = "">
			<?php if($user_order->company_register_status=="2"){
					echo '<div class="red">';
					echo "Cancelled";
					echo '<div>';
				}elseif($user_order->company_register_status=="1"){
					echo '<div class="green">';
					echo "Deliverd";
					echo '<div>';
				}else{
					echo '<div class="orange">';
					echo "Pending";
					echo '</div>';
				}?>
            </td>
			<td class="text-center">
			<?php $register_date  = $user_order->create_time;
			$register_date_add = strtotime($register_date);
			$new_date = date('Y-m-d', $register_date_add);
			echo $new_date;?>
			</td>
			<td onclick = "displayDatepicker_by_deliver('<?php echo $user_order->id?>','<?php echo addslashes($user_order->company_name);?>','<?php echo $user_order->company_register_deliver_date?>')">
			<?php 
			 if($user_order->company_register_deliver_date == "0000-00-00 00:00:00" || $user_order->company_register_deliver_date == NULL) {
		      echo "Select date";
			 } 
			 else { 
			echo $user_order->company_register_deliver_date;
			}
			?>	
			</td>
			<td>
			£24.99
			</td>
		 </tr>
			<?php }
			}
			?>
		</tbody>
	</table>
	</div>
<?php 	if($this->pagination->create_links()){			$class="scroll_set";		?>
<?php 	}	else	{				$class= "scroll_set";			}		?>

<div class="<?php echo $class; ?>">	 

<?php 	if($this->pagination->create_links()){	?> 
<?php 	}else{	}	?>
	 <div class="row pagination-btm" style="float:right;margin:0;">
       <?php if($this->pagination->create_links()){ ?>
     <div  style="float:left; color:#303030">
     <?php 
			$url =  $_SERVER['REQUEST_URI']; 
			
			$urlexp = explode('/',$url);
			//echo $this->uri->segment(2);
			//print_r($urlexp);
			$data_posted= urldecode($_SERVER['QUERY_STRING']);
			//print_r($data_posted);
			$pieces = explode("&", $data_posted);
			
			for($a=0;$a<count($pieces);$a++)
			{
				$profile_key=strstr($pieces[$a],"=",true);
				$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
			}	
			//print_r($profile);
 	 ?>
    <!--form method="get" action="<?php echo base_url().''.$urlexp['2'].'/'.$urlexp['3']; ?>" id="formElementId"-->
    <form method="get" action="<?php echo base_url(); ?>dashboard/<?php echo $this->uri->segment(2);//$urlexp['3'];?>" id="formElementId">
	<?php	foreach($profile as $key=>$val){	?>
		<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
	<?php }	?>
    <select name="page"  id="selectElementId">
        <option value="10"<?php if($pagef=='10'){ echo 'selected="selected"';} ?>>10 Items</option>
    	<option value="25"<?php if($pagef=='25'){ echo 'selected="selected"';} ?>>25 Items</option>
        <option value="50"<?php if($pagef=='50'){ echo 'selected="selected"';} ?>>50 Items</option>
        <option value="100"<?php if($pagef=='100'){ echo 'selected="selected"';} ?>>100 Items</option>
    </select>
    </form>
    </div>
<?php 	echo $this->pagination->create_links();		}	?>
  </div>
  </div>
 </section>
	<?php if( $this->uri->segment(2) == "searchResult" ||  $this->uri->segment(2) == "renewalRecordsNew"){ ?>
		<?php $this->load->view('director_view');	?>
	<?php } ?>
<script>
	$('#selectElementId').change(function(){
		$('#formElementId').submit();
    });
	
	function download_doc(file_name,userid){
		$.ajax({
			'url' : base_url+"home/download",
			'type' : 'POST',
			'data' :{
				'file_name' : file_name,
				'userid' :userid
			}, 
			'success' : function(data){
				alert(data);
			},
			'error' : function(request,error){
				alert("Request: "+JSON.stringify(request));
			}
		});
	}
</script> 
<?php $this->load->view('modal');?>
<?php $this->load->view('footer');?>


