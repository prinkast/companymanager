

<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/left_nav');?><!----sub side bar new design 22-march-17---->
<section id="content_info">
<?php $this->load->view ('includes/search_bar');?>	
  <div class="table_data">
	<table class="table table-hover table-bordered">
	<?php	if($role_id === '0' || $role_id === '2'){ ?>
	<thead class="home-table-header static-header">
	<?php }else{?>
	<thead class="home-table-header">
	<?php }?>
	<tr>			
		<th class="cmpny th_company_class">
			<?php 	$total =  $this->uri->segment(3)+1; 
					if($total<>1){
					  $totaldss = $this->uri->segment(3)+$pagef;
					}else{
						$totaldss = $pagef;
					}
					if($totaldss > $record_count){
						$totaldss = $record_count;
					}
					if($this->pagination->create_links()){
			?>
			Company Name (<?php echo $total.'-'.$totaldss; ?> of <?php echo $record_count;?>)
			<?php } else{ ?>
			Company Name (<?php echo $record_count;?>)
			<?php	}	?>
		</th>		
		<th onClick="getDateOtions()" class="th_renew_class">Renewal</th>
		<th class="th_location_class roa_row_view">
		  ROA
			<!--form action="< ?php echo base_url();?>home/filterSearch" method="POST" name="location" id="location_select">
				< ?php $locations =  $this->search->getLocationOptions();?>
				<select id="location_select_option" name="location_select" class="getSelect1 locClick">
						<option>ROA</option>
					< ?php  foreach($locations as $location){?>
						<option value="< ?php echo $location;?>">< ?php echo $location;?></option>
					< ?php }?>
				</select>
			</form-->
		</th>
		<th>Company No</th>
		<th>Formation Date</th>
		<th>Accounts Due</th>
		<th>Return Due</th>
		<th class="th_state_class">
			<form action="<?php echo base_url();?>home/state_change" method="POST" name="state_form_change" id="state_form_change">
			<?php $states =  $this->search->getStatusOptions();?>
			<select id="state_change" name="state_change">
				<option>Status</option>
				<option value="Show All">Show All</option>
				<?php foreach($states as $key =>$state){?>
				<option value="<?php echo $key ;?>"><?php echo $state;?></option>
				<?php }?>
			</select>
			</form>
		</th>
	</tr>
	</thead>
		<tbody id="OrderPackages">
		<?php 
			if($user_orders){
				//var_dump($user_orders);die("FD");
				$count = 0; $count2 = 0;
				foreach ($user_orders as $key=>$user_order)
				{
				// $company =  $this->search->filterSearch($user_order->company_id);
				// $user =  $this->search->userSearch($user_order->create_user_id);
				// $update_company_user = $this->search->userSearch($user_order->create_user_id);
				// $orders =  $this->search->orders($user_order->company_id);
				// $files_info =  $this->search->fileInfo($user_order->company_id);
				// $Get_company_data_formation =  $this->search_model->Get_company_data_formation($user_order->company_id);
				//echo "<pre>"; print_r($Get_company_data_formation); die('testing');
		?>
			<?php $company_name = $user_order->company_name;?>		
			<?php $address_line1 = $user_order->location;?>		
		<tr id="<?php echo 'row_'.$user_order->company_id;?>">
			<td class = "td_company_class dropdown set_dropdown_ltd">
				<!--a href="< ?php echo  base_url();?>dashboard/showCompanyResult?id=< ?php echo $user_order->company_id ?> "-->
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">
				<?php	 $comp_name_length = strlen($user_order->company_name);
				         $comp_name_length1 = strlen($company_name);
				if($company_name ==""){
				if($comp_name_length >=10){?>
				<span class="comp_full_name">
				<?php echo $user_order->company_name;?>
				</span>
				<?php }
				else{
					echo $user_order->company_name;
					}
				}				
				else
				{
				if($comp_name_length1 >=10){?>
				<span class="comp_full_name">
				<?php echo $company_name;?>
				</span>
				<?php }
				else{
					echo $company_name;
					}
				} 
				?>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo  base_url();?>dashboard/showCompanyResult?id=<?php echo $user_order->company_id ?> ">Company Overview</a></li>
					<!--li><a href="javascript:void(0)">Contact Details</a></li-->
					
					<li><a href="javascript:void(0)" onclick ="userDetails ('<?php echo $user_order->company_id;?>')">Client Details</a></li>
					<li><a href="javascript:void(0)" onclick ="billingDetails('<?php echo $user_order->company_id;?>')">Billing Information</a></li>
					<li><a href="javascript:void(0)" onclick ="orderDetails('<?php echo $user_order->company_id;?>','<?php echo $user_order->id;?>')">Order Information</a></li>
				</ul>
			</td>
			<td  data-cname="<?php echo $user_order->company_name;?>" class = "renew_id td_renew_class" id="<?php echo $user_order->company_id;?>" onclick = "displayDatepicker('<?php echo $user_order->company_id?>','<?php echo addslashes($user_order->company_name);?>')">
				<a href="#">	
					<?php echo date ("d-m-Y",strtotime($user_order->renewable_date));?>
				</a>					
			</td>
			<?php $postcode = $user_order->post_town;?>		
			<td onclick =" location_event('<?php echo $user_order->company_id?>','<?php echo addslashes($user_order->company_name);?>')" class="td_location_class">
			<?php 
			if($postcode=="")
			{ ?>
			<a href="#">
					<?php echo $user_order->location;?>
			</a>
			<?php } else
			{ ?>
			<a href="#">
					<?php echo $postcode;?>
			</a>
			<?php } ?>	
			</td>
<?php 
// $formation = $Get_company_data_formation[$count]->incorporation_date;
// $next_due_date = $Get_company_data_formation[$count]->next_due_date;
// $next__due_return = $Get_company_data_formation[$count]->next__due_return_dated;
// $status = $Get_company_data_formation[$count]->company_status;
// $postcode = $Get_company_data_formation[$count]->post_town;
// //echo $formation;die(); 
?>
			<td><?php $comno = $user_order->company_number;
			if($comno==0)
			{?>
		<a href="#" onClick="company_num_house('<?php echo $user_order->id;?>','','','<?php echo $user_order->company_name;?>')">Import</a>  
			<?php } 
			else
			{
			echo $comno;
			}
			?></td>
			<td><?php echo $user_order->incorporation_date;?></td>
			<td><?php echo $user_order->next_due_date;?></td>
			<td><?php echo $user_order->next__due_return_dated;?></td>
			<td class = "state_id td_status_class active_box" data-id="<?php echo $user_order->id;?>" data-cname="<?php echo $user_order->company_name;?>">
			<?php if($status==""){?>
            <a href="#"><?php echo	$this->search->getStatusOptions($user_order->state_id);?></a>
			<?php } else { ?>
            <a href="#"><?php echo $status;?></a>
			<?php }?>
            </td>
		</tr>	
				<?php }
			}?>
		</tbody>
	</table>
	</div>
	       <?php if($this->pagination->create_links()){
		   $class="scroll_set";
		   //$class= "pagination-bottom";
		   //Commented to make a new design  22/03/2017
		    ?>
 	   <?php }else{
		  	 $class= "scroll_set";
		  }?>
<div class="<?php echo $class; ?>">	 
<?php if($this->pagination->create_links()){
   ?> 
   <!-- Commented to make a new design  22/03/2017-->
     <!--p class="pull-left copyright-text"><a href="http://thelondonoffice.com">Powered by The London Office</a></p--> <?php
}else{
	echo "fs";
} ?>
	 <div class="row pagination-btm" style="float:right;margin:0;">
       <?php if($this->pagination->create_links()){ ?>
     <div  style="float:left; color:#303030">
     <?php 
 	 	   $url =  $_SERVER[REQUEST_URI]; 
		   $urlexp = explode('/',$url);
		   $data_posted= urldecode($_SERVER['QUERY_STRING']);
			$pieces = explode("&", $data_posted);
			
			for($a=0;$a<count($pieces);$a++)
			{
			$profile_key=strstr($pieces[$a],"=",true);
			$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
			}	
			//print_r($profile);
 	 ?>
    <form method="get" action="<?php echo base_url(); ?>dashboard/<?php echo $this->uri->segment(2);//$urlexp['3'];?>" id="formElementId">
	<?php
		foreach($profile as $key=>$val){
	?>
	<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $val; ?>" />
	<?php }?>
    <select name="page"  id="selectElementId">
        <option value="10"<?php if($pagef=='10'){ echo 'selected="selected"';} ?>>10 Items</option>
    	<option value="25"<?php if($pagef=='25'){ echo 'selected="selected"';} ?>>25 Items</option>
        <option value="50"<?php if($pagef=='50'){ echo 'selected="selected"';} ?>>50 Items</option>
        <option value="100"<?php if($pagef=='100'){ echo 'selected="selected"';} ?>>100 Items</option>
    </select>
    </form>
    </div>
        <?php echo $this->pagination->create_links();
	   }?>
  </div>
 </section>
<script>
	$('#selectElementId').change(function(){
         //$(this).closest('form').trigger('submit');
         /* or:
         $('#formElementId').trigger('submit');
            or: */
         $('#formElementId').submit();
     });
	function download_doc(file_name,userid){
		$.ajax({
			'url' : base_url+"home/download",
			'type' : 'POST',
			'data' :{
				'file_name' : file_name,
				'userid' :userid
			}, 
			'success' : function(data){
				alert(data);
			},
			'error' : function(request,error){
				alert("Request: "+JSON.stringify(request));
			}
		});
	}
</script>
	<?php $this->load->view ('footer');?>
   <?php $this->load->view ('modal');?>

