<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
 <meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--title class="head_top">The Company Manager</title-->
		<?php if($title == "") {?> 
		<title class="head_top">Company Manager</title>
		<?php }
		else 
		{ ?>
		<title class="head_top"><?php echo $title;?></title>
		<?php } ?>
		<meta name="description" content="Responsive Animated Border Menus with CSS Transitions" />
		<meta name="keywords" content="navigation, menu, responsive, border, overlay, css transition" />
		<!--link rel="icon" type="image/x-icon" sizes="16x16" href="https://www.companymanager.online/assets/images/favicon-16x16.ico" /-->
		<link type="image/x-icon" rel="icon" sizes="16x16" href="../assets/images/favicon-16x16.ico" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/demo.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/icons.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style2.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/form-style.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" />
                    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
		
  <script>
   $(document).ready(function(){
   var url = window.location.href;
   //alert(url);
   if(url=="https://www.companymanager.online/dashboard/renewable_new"){
   $("#Renewed_tab").addClass("active");
   }
   else if(url=="https://www.companymanager.online/dashboard/renewed"){
   $("#renewal_tab").addClass("active");
   }
   else if(url=="https://www.companymanager.online/dashboard/renewed_c_view"){
   $("#renewal_tab").addClass("active");
   }
   else if(url=="https://www.companymanager.online/dashboard/comapnyRenewableNew"){
   $("#Renewed_tab").addClass("active");
   }
  var activeTab = localStorage.getItem('getorder');
  //alert(activeTab);
   if(activeTab == 'weekly'){
    $("#weekly").addClass("btn-success");
   //localStorage.clear();
  } 
  if(activeTab == 'monthly'){
    $("#monthly").addClass("btn-success");
   //localStorage.clear();
  } 
  if(activeTab == 'showall'){
    $("#showall").addClass("btn-success");
   //localStorage.clear();
  } 
  if(activeTab == 'today'){
    $("#today").addClass("btn-success");
   //localStorage.clear();
  } 
   
  //alert(activeTab);
  });
 </script>
   <script type="text/javascript">

/***********************************************
* Drop Down Date select script- by JavaScriptKit.com
* This notice MUST stay intact for use
* Visit JavaScript Kit at http://www.javascriptkit.com/ for this script and more
***********************************************/

var monthtext=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];

function populatedropdown(dayfield, monthfield, yearfield){
var today=new Date()
var dayfield=document.getElementById(dayfield)
var monthfield=document.getElementById(monthfield)
var yearfield=document.getElementById(yearfield)
for (var i=0; i<31; i++)
dayfield.options[i]=new Option(i, i+1)
dayfield.options[today.getDate()]=new Option(today.getDate(), today.getDate(), true, true) //select today's day
for (var m=0; m<12; m++)
monthfield.options[m]=new Option(monthtext[m], monthtext[m])
monthfield.options[today.getMonth()]=new Option(monthtext[today.getMonth()], monthtext[today.getMonth()], true, true) //select today's month
var thisyear=today.getFullYear()
for (var y=0; y<20; y++){
yearfield.options[y]=new Option(thisyear, thisyear)
thisyear+=1
}
yearfield.options[0]=new Option(today.getFullYear(), today.getFullYear(), true, true) //select today's year
}
 function fun_alert(){
	var className = document.getElementById("lg_col").className;
	if(className == "xs_col"){
		$("#lg_col").addClass("lg_col");
		$("#lg_col").removeClass("xs_col");
	}else if(className == "lg_col"){
		$("#lg_col").addClass("xs_col");
		$("#lg_col").removeClass("lg_col");
	} 
} 
$(window).scroll(function(){
	if ($(this).scrollTop() > 80) {
		$('.left_col').addClass('package_box_fixed');
	} else {
		$('.left_col').removeClass('package_box_fixed');
	}
});
</script>

 </head>
 <?php
 //Log out Back
	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.
 ?>
 <?php 
$role_id = 1;
?>
    <body id="lg_col" class="lg_col">
   
   <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>
					<?php $url=$this->uri->segment(2);
						if($url =="companyView" || $url =="companyStateChange" || $url =="companyStateChange1" || $url =="companyNewFormation" || $url =="comapnyRenewableNew" || $url == "companyRenewalRecordsNew" ||$url == "NewOrdersCompany" || $url == "dashboard_index" || $url == "state_change"|| $url == "additional_serveics" || $url =="searchResult" || $url =="state_change_id" || $url =="manageReseller" || $url =="new_formation" || $url =="renewable_new" || $url =="renewable_new" || $url =="Hosting" || $this->uri->segment(1) =="dashboard" || $url == "allusercompanies"){?>
                    <div class="left-nav-tab">
						<ul class="nav nav-tabs tabs-left">
							<!--li><a href="#" >Overview</a></li-->
							<li class="<?php echo ($url=='companyView')?'active':'' ?>" id="company_search"><a href="<?php echo base_url();?>dashboard/companyView">Company Manager</a></li>
							<li id="NewOrders" class="<?php echo ($url=='NewOrdersCompany')?'active':'' ?>" >
							<form method="get" action="<?php echo base_url(); ?>home/NewOrdersCompany">
							<input name="orderDate" type="hidden" class="form-control" value="today">
							<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
							<input class="user-profile" type="submit" value="New Orders" id="new_ord">
							</form>
							</li>
							<li id="pending_order" class="<?php echo ($url=='companyStateChange')?'active':'' ?>" ><a href="<?php echo base_url();?>home/companyStateChange?state_change=3"  >Pending Orders</a></li>
							<li id="new_formation" class="<?php echo ($url=='companyNewFormation')?'active':'' ?>"><a href="<?php echo base_url();?>home/companyNewFormation">New Formations</a></li>
							<li id="additional"class="<?php echo ($url=='additional_serveics')?'active':'' ?>"><a href="<?php echo base_url();?>dashboard/additional_serveics">Additional Services</a></li>
							<li id="Renewals" class="<?php echo ($url=='comapnyRenewableNew')?'active':'' ?>"><a href="<?php echo base_url();?>dashboard/comapnyRenewableNew">Renewals</a></li>
							<li id="pending_id" class="<?php echo ($url=='companyStateChange1')?'active':'' ?>" ><a href="<?php echo base_url();?>home/companyStateChange1?state_change=5,7" >Pending ID</a></li>
							<li id="partner" class="<?php echo ($url=='manageReseller')?'active':'' ?>"><a href="<?php echo base_url();?>home/manageReseller">Partner Accounts</a></li>
						</ul>
					</div>
					<?php }elseif($url = ""){?>
					<div class="left-nav-tab">
						<ul class="nav nav-tabs tabs-left">
							<!--li><a href="#" >Overview</a></li-->
							<?php //$url= $this->uri->segment(2);
								?>
							<li class="<?php echo ($url=='companyView')?'active':'' ?>" id="company_search"><a href="<?php echo base_url();?>dashboard/companyView" >Company Manager</a></li>
							<li id="NewOrders" class="<?php echo ($url=='NewOrders')?'active':'' ?>" >
							<form method="get" action="<?php echo base_url(); ?>home/NewOrders">
							<input name="orderDate" type="hidden" class="form-control" value="today">
							<!--a onclick="duration_order('today');" class="user-profile">Today</a-->
							<input class="user-profile" type="submit" value="New Orders" id="new_ord">
							</form>
							</li>
							<li id="pending_order" class="<?php echo ($url=='state_change')?'active':'' ?>" ><a href="<?php echo base_url();?>home/state_change?state_change=3"  >Pending Orders</a></li>
							<li id="new_formation" class="<?php echo ($url=='new_formation')?'active':'' ?>"><a href="<?php echo base_url();?>home/new_formation"  >New Formations</a></li>
							<li id="additional" class="<?php echo ($url=='additional_serveics')?'active':'' ?>"><a href="<?php echo base_url();?>dashboard/additional_serveics" >Additional Services</a></li>
							<li id="Renewals" class="<?php echo ($url=='renewable_new')?'active':'' ?>"><a href="<?php echo base_url();?>dashboard/renewable_new" >Renewals</a></li>  
							<li id="pending_id" class="<?php echo ($url=='state_change_id')?'active':'' ?>" ><a href="<?php echo base_url();?>home/state_change_id?state_change=5,7" >Pending ID</a></li>
							<li id="partner" class="<?php echo ($url=='manageReseller')?'active':'' ?>"><a href="<?php echo base_url();?>home/manageReseller">Partner Accounts</a></li>
						</ul>
					</div>
					<?php } ?>
                </div>
            </div>
			<?php $session_data = $this->session->userdata('logged_in');
				$role_id = $session_data['role_id'];
			?>
   <div class="main_section">
	<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
		<nav id="bt-menu" class="bt-menu">
		<?php  if($role_id === '2'){?>
		<i class="collapse_icon fa fa-caret-right" id="collapse_icon" onClick="fun_alert()"></i>
		<a class="navbar-brand logo" href="<?php echo base_url();?>dashboard">
		<?php 
			$reseller = $this->search->resellerByUserId();
			if($reseller->logo_text == ""){
				echo "The Registered Office";
			}else{
				echo $reseller->logo_text;
			}
		?>
		</a>
		<?php }
			elseif($role_id === '0'){
			$resellerCompany = $this->search->companyReseller();
		?>
			<i class="collapse_icon fa fa-caret-right" id="collapse_icon" onClick="fun_alert()"></i>	
			
			<a class="navbar-brand logo" href="<?php echo base_url();?>dashboard"><?php echo $resellerCompany->logo_text;?></a>
		<?php 
			}
		else {
			?>
				<i class="collapse_icon fa fa-caret-right" id="collapse_icon" onClick="fun_alert()"></i>
				<div class="search_view dropdown">
					<a class="fa fa-search dropdown-toggle" data-toggle="dropdown"></a>
					<form method="get" action="<?php echo base_url(); ?>dashboard/searchResult" class="input_field dropdown-menu">
						<span class="fa fa-caret-up"></span>
						<input value="" name="new_search_bar" type="search" placeholder="Search..." class="form-control">
						<input type="hidden" class="asds" name="search_new" value="search_all_new" <?php echo ($_GET['search_new']=='search_all_new');?>>
						<button class="fa fa-search dropdown-search"></button>
					</form>
				</div>
				<?php 	if($this->uri->segment(2) == "showCompanyResult"){
							$limited= "";
							if($getCompanyInfo_new->limited =="1")
							{
							$limited= "Limited";
							}
							elseif($getCompanyInfo_new->limited == "2")
							{
							$limited= "Ltd";
							}
							else{
							$limited= "";
							}
				if($getCompanyInfo_new->trading)
				{
				?>				
					<a class="navbar-brand logo" href="<?php echo base_url();?>dashboard"><?php echo $getCompanyInfo_new->company_name ." ".$limited ." <small>(t/as ".$getCompanyInfo_new->trading .")</small>" ;?></a>
				<?php } else
				{?>
				<a class="navbar-brand logo" href="<?php echo base_url();?>dashboard"><?php echo $getCompanyInfo_new->company_name .$limited;?></a>
				<?php }} else {?>
					<a class="navbar-brand logo" href="#">
					<?php 
					$url= $this->uri->segment(2);
					if($url=="state_change"){
					?>
                   <span id="file_exchange">PENDING ORDERS</span>					
				<?php }else if($url=="new_formation"){ ?>
					  <span id="file_exchange">NEW FORMATIONS</span>
                 <?php }else if($url=="renewable_new"){ ?>
				 <span id="file_exchange">RENEWALS</span>
				 <?php }else if($url=="additional_serveics"){ ?>
					<span id="file_exchange">ADDTIONAL SERVEICS</span>
					<?php }else if($url=="manageReseller"){ ?>
					<span id="file_exchange">PATNER ACCOUNTS</span>
					<?php }else if($url=="state_change_id"){ ?>
					<span id="file_exchange">PENDING ID</span>
					<?php }else if($url=="companyStateChange1"){ ?>
					<span id="file_exchange">PENDING ID</span>
					<?php }else if($url=="comapnyRenewableNew"){ ?>
					<span id="file_exchange">RENEWALS</span>
					<?php }else if($url=="companyStateChange"){ ?>
					<span id="file_exchange">PENDING ORDERS</span>
					<?php }else if($url=="companyNewFormation"){ ?>
					<span id="file_exchange">NEW FORMATIONS</span>
					<?php } else if($url=="renewalRecordsNew"){ ?>
					<span id="file_exchange">RENEWALS</span>
					<?php } else if($url=="NewOrdersCompany"){ ?>
					<span id="file_exchange">New Orders</span>
					<?php }
					else if($url=="NewOrders"){ ?>
					<span id="file_exchange">New Orders</span>
					<?php } else { ?>
					<span id="file_exchange">COMPANY MANAGER</span>
				 	<?php } ?>
					<span class="head_top"></span></a>
				<?php	} ?>
		<?php }?>
			<ul>
				<li><a href="<?php echo base_url();?>home" class="bt-icon icon-zoom">Home</a></li>
				<li><a href="#" class="bt-icon icon-refresh">Refresh</a></li>
				<li><a href="#" class="bt-icon icon-lock">Lock</a></li>
				<li><a href="#" class="bt-icon icon-speaker">Sound</a></li>
				<li><a href="#" class="bt-icon icon-star">Favorite</a></li>
			</ul>
		</nav>	
			 <ul class="nav navbar-nav navbar-right">
				<li class="setting_icon1" id="dash">
					<a href="<?php echo base_url()?>dashboard">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li class="add_company dropdown setting_icon"><!--modified on 23/03/2017-->
					<a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">+ Company</a>
					<ul class="dropdown-menu">
						<li><a href="#" data-toggle="modal" data-target="#company1"> Single Company</a></li>
						<li><a href="<?php echo base_url(); ?>dashboard/multipleCompany">Multiple Companies</a></li>
					</ul>
				</li>		
				<!--li><a href="< ?php echo base_url(); ?>dashboard/renewed">Renewals</a></li-->
				<!--li><a href="<?php echo base_url(); ?>dashboard/renewable_new"> Renewed</a></li-->
				<li><a href="#" data-toggle="modal" data-target="#mypartner"> + Partner</a></li>
				<?php	if($role_id === '1'){ ?>
				<?php }?>
				<li class="live-btn"><a href="#" onclick="phplive_launch_chat_0(0)" id="phplive_btn_1421304743"></a></li>
                        <script type="text/javascript">
                                (function() {
                                    var phplive_e_1421304743 = document.createElement("script");
                                    phplive_e_1421304743.type = "text/javascript";
                                    phplive_e_1421304743.async = true;
                                    phplive_e_1421304743.src = "//t2.phplivesupport.com/equinitee/js/phplive_v2.js.php?v=0|1421304743|0|";
                                    document.getElementById("phplive_btn_1421304743").appendChild(
                                            phplive_e_1421304743);
                                })();
                            </script>
			<?php $url =  $_SERVER['REQUEST_URI']; 
					//var_dump($url); die('test');
					$urlexp = explode('/',$url);
					//echo $this->uri->segment(0);
					$string =$_GET['orderDate'];
					$string1 =$_GET['alluser_id'];
					$state_change=$_GET['state_change'];
			?>			
			<li class="export">
			<?php if($string=="today")
			{ ?> <form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?orderDate=today" method="POST" name="export" id="export"><?php }
			if($string=="week") { ?>
			<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?orderDate=week" method="POST" name="export" id="export">		
			<?php } if($string=="month") { ?>
			<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?orderDate=month" method="POST" name="export" id="export">	
			<?php } if($string=="all") { ?>
			<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?orderDate=all" method="POST" name="export" id="export">		
			<?php } 
			if($string1!="")
			{?>
				<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?alluser_id=<?php echo $string1; ?>" method="POST" name="export" id="export">	
			<?php }
			if($state_change!="")
			{?>
				<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>?state_change=<?php echo $state_change; ?>" method="POST" name="export" id="export">	
			<?php }
			else{?>
			
			<form action="<?php echo base_url().$this->uri->segment(1).'/'.$this->uri->segment(2) ; ?>" method="POST" name="export" id="export" ><?php }?>
						<input type="hidden" name="search_field" value="<?php echo $_GET['new_search_bar'];?>">
						<input type="hidden" name="export1" value="export" /> 
						<input type="submit" value="Export" name="export"  id = "export" class="btn export-btn"/>
					</form>
				</li>
				<li class="<?php //if($this->uri->segment(2)==""){echo "active";}?>"><a href="#" title="Coming Soon"> Reports</a></li>
				<li>
					<input type="submit" value="Advance Search" name=""  id = "" class="btn export-btn" onClick="AdvanceSearch()"/>
				</li>
				<li class="dropdown setting_icon">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true"><i class="fa fa-ellipsis-v"></i></a>
					<ul class="dropdown-menu">
						<li><a href="https://thelondonoffice.com/" target="blank">London Office</a></li>
						<li><a href="https://theedinburghoffice.com/"target="blank">Edinburgh Office</a></li>
						<li><a href="https://thedublinoffice.com/" target="blank">Dublin Office</a></li>
						<li><a href="https://thegibraltaroffice.com/"target="blank">Gibraltar Office</a></li>
						<li><a href="https://theregisteredoffice.com/" target="blank">Registered Office</a></li>
						<li><a href="https://thecompanyregister.com/"target="blank">Company Register</a></li>
						<li><a href="https://londonformations.com/" target="blank">London Formations</a></li>
						<li><a href="https://apostillecompany.co.uk/"target="blank">Apostille Company</a></li>
						<li><a href="http://clickformations.co.uk/" target="blank">Click Formations</a></li>
						<li><a href="http://t2.phplivesupport.com/equinitee/"target="blank">Live Chat (Login)</a></li>
						<li><a href="https://securedwebapp.com/login.asp" target="blank">Kashflow (Login)</a></li>
						<li><a href="http://www.theoffice.support/" target="blank">Office Support</a></li>
					</ul>
				</li>	
				<li><a href="<?php echo base_url();?>home/logout">Logout</a> </li>				
            </ul>
		</nav>	 