<?php $this->load->view ('header');?>
           
                    <div class="companysection">
						<div class="col-sm-8 add_company_left">
							<div class="sub_nav">
								<ul class="nav navbar-nav nav-pills">
									<li class="active"><a data-toggle="tab" href="#individual_services" class="user-profile">Individual Services</a></li>
									<li class=""><a data-toggle="tab" href="#packages_services" class="user-profile">Packages</a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div id="individual_services" class="overview-box1 add_companies tab-pane fade active in">
									<h3>Address services</h3>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Registered Office Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Director Service Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Virtual Business Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<select class="form-control">
												<option value="0">--Select--</option>
												<option value="1">1 year- £99.00</option>
												<option value="2">6 Mths - £79.00</option>
												<option value="3">3 Mths - £49.00</option>
												<option value="4">1 Mth - £19.00</option>
											</select>
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									
									<h3>Additional Services</h3>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Telephone Answering Service</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Document Pack</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Certificate of Good Standing</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
								</div>
								<div id="packages_services" class="overview-box1 add_companies tab-pane fade">
									<h3>Address services</h3>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Registered Office Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Director Service Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Virtual Business Address</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<select class="form-control">
												<option value="0">--Select--</option>
												<option value="1">1 year- £99.00</option>
												<option value="2">6 Mths - £79.00</option>
												<option value="3">3 Mths - £49.00</option>
												<option value="4">1 Mth - £19.00</option>
											</select>
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									
									<h3>Additional Services</h3>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Telephone Answering Service</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Document Pack</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
									<div class="row margin_bott">
										<div class="col-xs-7 company_det">
											<h4><i class="fa fa-plus-circle"></i> Certificate of Good Standing</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
										</div>
										<div class="col-xs-3 free_sec">
											<i class="fa fa-info-circle">
												<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
											</i> Free
										</div>
										<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 add_company_right">
							<form class="right_package_box">
								<div class="box_header">
									<h3 class="pull-left">Shopping Cart</h3>
								</div>
								<div class="overview-box">
									<h3>Order Details</h3>
									<div class="form-group">
										<label>Company Name</label>
										<input id="Comp_name" type="text" class="form-control">
									</div>
									<div class="form-group">
										<label>Location (Select address required)</label>
										<select class="form-control">
											<option value="0">--Select--</option>
											<option value="1">Great Portland Street, W1</option>
											<option value="2">Bloomsbury Way, Wc1</option>
											<option value="3">Edinburgh, EH2</option>
											<option value="4">Other Address</option>
										</select>
									</div>
									<div class="form-group price_row price_border">
										<p><span>Address Services <i class="fa fa-times-circle"></i></span><span class="pull-right">$3250.00</span></p>
									</div>
									<div class="form-group price_row price_border">
										<p><span>Company Formation <i class="fa fa-times-circle"></i></span><span class="pull-right">$20.00</span></p>
									</div>
									<div class="form-group price_row price_border">
										<p><span>Additional Services <i class="fa fa-times-circle"></i></span><span class="pull-right">$25.00</span></p>
									</div>
									<div class="form-group price_row">
										<p><span>Total <i class="fa fa-times-circle"></i></span><span class="pull-right">$2250.00</span></p>
									</div>
									<div class="form-group price_btn">
										<button type="submit" class="btn btn-success">Add Company</button>
									</div>
									<div class="form-group price_btn">
										<p class="text-center">You will be sent an invoice</p>
									</div>
								</div>
							</form>
							
						</div>
                    </div>
            
   
    <?php $this->load->view ('footer');?>
<?php $this->load->view ('modal');?>
