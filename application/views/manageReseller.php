<!--?php $counttotal = $this->search->Countactive();?-->
<?php $this->load->view ('header');?>
<!--form end-->
<?php $this->load->view ('includes/left_nav');?>
<?php $uri_test = $this->uri->segment(2); ?>
<section id="content_info" <?php (($uri_test == "searchResult") ? "echo class='top_content_info'" : "echo class =''"); ?>>
<!--?php $this->load->view ('includes/search_bar');?-->
<section class="form-section partner_page">
  <div class="container-fluid">
     <div class="row">
      <div class="booking-form">
	   <table class="table manage-reseller table-bordered style_table"> 
	     <thead>
		   <tr>
		    <th>Reseller Name (<?php echo $resellersCount; ?>)</th>
			<!-- <th>Logo</th>-->
            <th>Active</th>
            <!--th>Active (< ?php echo $Countactive; ?>)</th-->
            <!--th>Not Active (< ?php echo $CountAll; ?>)</th-->
            <th>Not Active</th>
            <th width="200">Contact Name</th>
            <th>Website</th>
            <th>Email</th>
            <th>Country</th>	
            <th>ID</th>	
            <th>POA</th>			
            <th>Status</th>				 				 
            <th>Edit</th>
            <th>Download</th>
            <th>Delete</th>
		  </tr>
		 </thead>
		 <tbody>
		     <?php foreach($resellers as $reseller)
		{
			$count = $this->search->countResellerCompanies($reseller->id);	
			$count_active = $this->search->countResellerCompaniesActive($reseller->id);
			$files_info =  $this->search->fileInfo1($reseller->id);
 		?> 
 		   <tr>
			    <td class = "td_company_class dropdown set_dropdown_ltd">
				<!--a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)">< ?php echo $reseller->company_name;?></a-->
				<a onclick =" edit_reseller('<?php echo $reseller->company_name;?>','<?php echo $reseller->id;?>')">
				<?php echo $reseller->company_name;?></a>
			  <!--ul class="dropdown-menu">
					<li><a href="javascript:void(0)" onclick ="invoice_reseller('<?php echo $reseller->id;?>','<?php echo $reseller->user_id;?>','<?php echo $reseller->company_name;?>')">INVOICE UPLOAD</a></li>
			 </ul-->
			  </td>
<!--			  <td><a href="#"><?php //echo $reseller->logo_text;?></a></td>
-->			  <td class="active_view"><a href="<?php echo base_url().'home/active_exportshhet/'.$reseller->id;?>"><?php echo    			$count_active;?></a> </td>
              <td class="not-active_view"><a href="<?php echo base_url().'home/inactive_exportshhet/'.$reseller->id;?>"><?php echo $count;?></a> </td>
			  <td class="contact_view"><a href="#"><?php echo $reseller->contact_name;?></a></td>
			  <td class="web_view"><a href="https://<?php echo $reseller->web_address;?>" target="blank"><?php echo $reseller->web_address;?></a></td>
			  <!--td><a href="#">< ?php echo $reseller->mail_email;?></a></td-->
			  <td class="dropdown set_dropdown_ltd">
					<a data-toggle="dropdown" class="dropdown-toggle envelope_icon" aria-expanded="true" href="javascript:void(0)"><i class="fa fa-envelope" aria-hidden="true"></i></a>
					<ul class="dropdown-menu envelope_menu">
						<li><a href="mailto:<?php echo $reseller->mail_email?>"><?php echo $reseller->mail_email;?></a></li>
					</ul>
				</td>
			  <td class="country_view"><a href="#"><?php echo $reseller->country;?></a></td>
			  <td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			foreach($files_info as $file_info)
			{
				$file_type_id[]=$file_info->type_id;
				//var_dump($file_type_id);die();
			}
			$file_type_upload1 =  in_array("4",$file_type_id);
			//$file_type_upload2 =  in_array("5",$file_type_id);
			$getYes = "";
			
			if($file_type_upload1=== true) 
			{
				echo "<div class='green'>";
				echo "YES";
				echo "</div>";
				//echo "Recieved";
			}
			else
			{
				echo "<div class='red'>";
				echo "NO";
				echo "</div>";
			}
			?></a> 
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('<?php echo $reseller->company_name;?>','<?php echo $reseller->id;?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadResellerId('','','','<?php echo $reseller->company_name;?>','<?php echo $reseller->id;?>')">Upload ID</a></li>
				</ul>
			</td>
			   <td class="dropdown set_dropdown_ltd text-center">
				<a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false" href="javascript:void(0)"><?php 
			$file_type_id = array();
			foreach($files_info as $file_info)
			{
				$file_type_id[]=$file_info->type_id;	
			}
			$file_type_upload2 =  in_array("5",$file_type_id);
			$getYes = "";
			if($file_type_upload2 === true) 
			{
				echo "<div class='green'>";
				echo "YES";
				echo "</div>";
				//echo "Recieved";
			}
			else
			{
				echo "<div class='red'>";
				echo "NO";
				echo "</div>";
			}
			?></a> 
				<ul class="dropdown-menu set-alin">
					<li><a href="javascript:void(0)" onClick="send_Email('','<?php echo $reseller->company_name;?>','<?php echo $reseller->id;?>')">ID Required</a></li>
					<li><a href="javascript:void(0)" onClick="uploadResellerId('<?php echo $user_order->comp_ltd;?>','<?php echo $user_order->id?>','<?php echo $user_order->comp_ltd; ?>','<?php echo $reseller->company_name;?>','<?php echo $reseller->id;?>')">Upload ID</a></li>
				</ul>
			</td>
			  <td>
			   <?php echo "<div class='green'>";
				echo "Active";
				echo "</div>";?>
			</td>
			  <!--td><a href="#">< ?php echo $reseller->admin_email;?></a></td-->
			  <td class="active_view">
			 <?php  $passord_update = $this->search_model->Getuserbyuserid($reseller->user_id);	
			 //var_dump($passord_update)
			 ?>
			  <a href="#" onClick="ResellerUpdate('<?php echo $reseller->id;?>','<?php echo $reseller->company_name;?>','<?php echo $reseller->contact_name;?>','<?php echo $reseller->phone_number;?>','<?php echo $reseller->mail_email;?>','<?php echo $reseller->country;?>','<?php echo $reseller->address1;?>','<?php echo $reseller->address2;?>','<?php echo $reseller->web_address;?>','<?php echo $reseller->town;?>','<?php echo $reseller->region;?>','<?php echo $reseller->post_code;?>','<?php echo $reseller->admin_email;?>','<?php echo $reseller->logo_text;?>','<?php echo $reseller->county;?>','<?php echo $reseller->user_id;?>','<?php echo $passord_update["password"]?>')">Edit</a></td>
			  <td class="active_view"><a href="<?php echo base_url().'home/exportshhet/'.$reseller->id;?>">Download</td>
			<td class="active_view" onClick="deleteRecord('<?php echo $reseller->id;?>','reseller')"><a href="#">Delete</a></td>
			<!--td class="active_view"><a href="<?php echo base_url()?>home/invoice_reseller_get?id=<?php echo $reseller->id;?>">View invoices</a></td-->
			</tr> 	   
<?php }?>
		 </tbody>
	</table>	 
	   </div>	
      </div>
    </div> 
     </div>
  </section> 
  <?php $this->load->view ('modal');?>
	<?php $this->load->view ('footer');?>
  
