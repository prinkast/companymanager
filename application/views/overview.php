<?php $this->load->view ('header');?>
<?php $this->load->view ('includes/indiv_nav');?><!----sub side bar new design 22-march-17---->
<section class="content_info_section">
<?php $country =$this->ltd_model->getCountry();?>
<!--?php var_dump($comp_order_details); die();?-->

			<div class="tab-content right-tab-panes">
				<!---------Overview---------->
				<div class="tab-pane fade in active" id="home">
					<div class="overview_section">
						<div class="col-sm-6 outer_overview">
							<div class="overview-box">
							<?php 
												$limited= "";
												if($notes->limited =="1")
												{
												$limited= "Limited";
												}
												elseif($notes->limited == "2")
												{
												$limited= "Ltd";
												}
												else{
												$limited= "";
												}
												?>
							<?php if($comp_overview->company_name !="")
									{
									$company_name = $comp_overview->company_name;
									}
									else
									{
									$company_name =	$notes->company_name ." " .$limited;
									}
									?>
								<h3>Company Overview <a href="javascript:void(0)" onClick="company_num_house('<?php echo $notes->id;?>','','','<?php echo addslashes($company_name);?>')">Import Data</a>
								<a href="javascript:void(0)" onClick="associated('<?php echo $notes->create_user_id;?>')">Associated Companies</a></h3>
								
								<table class="table table-striped jambo_table bulk_action">
									<tbody>
										<tr class="even pointer">
											<td width="50%">Company Name</td>
											<td class="edit_cname">
											<?php if($comp_overview->company_name !="")
											{
											echo $comp_overview->company_name ;?>
											<i class="pull-right fa fa-pencil" aria-hidden="true" onclick = "editxmlcom_name('<?php echo $getCompanyInfo_new->id;?>','<?php echo $comp_overview->company_name;?>')"></i>
											<?php } else { 
											echo $notes->company_name ." " .$limited ; ?>
											<i class="pull-right fa fa-pencil" aria-hidden="true" onclick = "editcom_name('<?php echo $getCompanyInfo_new->id;?>','<?php echo $notes->company_name;?>')"></i>
											<?php } ?>
											
											</td>
											
										</tr>
										<tr>
										<td width="50%">Order Company Name</td>
										<td class=""><?php echo $notes->company_old_name ." " .$limited;?></td>
										</tr>
										<tr class="even pointer">
											<td width="50%">Trading Name</td>
											<td class="edit_cname"><?php echo $notes->trading;?>
											<i class="pull-right fa fa-pencil" aria-hidden="true" onclick = "editcom_name1('<?php echo $getCompanyInfo_new->id;?>','<?php echo $notes->trading;?>')"></i>
											</td>
											
										</tr>
										<tr class="even pointer">
											<td>Company Number</td>
											<td><?php echo $comp_overview->company_number; ?></td>
											
										</tr>
										<tr class="even pointer">
											<td>Registered In</td>
											<td><?php echo $comp_overview->creation_date; ?></td>
										</tr>
										<tr class="even pointer">
											<td>Status</td>
											<td><?php echo $comp_overview->company_status; ?></td>
										</tr>
										<tr class="even pointer">
											<td>Incorporation Date</td>
											<td><?php echo $comp_overview->incorporation_date; ?></td>	
												
										</tr>
										<tr class="even pointer">
											<td>Authentication Code</td>
											<td><?php echo $comp_overview->auth_code; ?></td>
											
											
										</tr>
										<tr class="even pointer">
											<td>Accounts Due</td>
											<td><?php echo $comp_overview->account_reference_date; ?></td>	
										</tr>
										<tr class="even pointer">
											<td>Accounts Made Up To</td>
											<?php if(date ("d-m-Y",strtotime($comp_overview->last_made_update))=='01-01-1970')
														{
															$last_made_update = "No previous filing";
														}
														else
														{
															$last_made_update = date ("d-m-Y",strtotime($comp_overview->last_made_update));
														}?>
											<td><?php echo $last_made_update;?></td>
											
											
										</tr>
										<tr class="even pointer">
											<td>Confirmation Statement Due</td>
											<?php if(date ("d-m-Y",strtotime($comp_overview->next_due_date))=='01-01-1970')
														{
															$next_due_date = "No previous filing";
														}
														else
														{
															$next_due_date = date ("d-m-Y",strtotime($comp_overview->next_due_date));
														}?>
											<td><?php echo $next_due_date ;?></td>
											
										</tr>
										<tr class="even pointer">
											<td>Confirmation Statement Up To</td>
											<?php if(date ("d-m-Y",strtotime($comp_overview->next__due_return_dated))=='01-01-1970')
														{
															$next__due_return_dated = "No previous filing";
														}
														else
														{
															$next__due_return_dated = date ("d-m-Y",strtotime($comp_overview->next__due_return_dated));
														}?>
											<td><?php echo $next__due_return_dated;?></td>
											
										</tr>
										<tr class="even pointer">
											<td>Nature of Business</td>
											<td><?php echo $comp_overview->	SIC_Codes; ?></td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div class="overview-box1">
								<h3>Order Overview</h3>
								
								<table class="table table-striped jambo_table bulk_action">
									<tbody>
										<?php $i=0;

										foreach($comp_order_details as $det)
										{
											
										if($i<3)
										{
										?>						
										
										<tr class="even pointer">
											<td class="" width="320"><?php echo $det['product'];?></td>
											<td class="" width="230"><?php echo $det['total'];?></td>
											<td class="" width="230"><?php echo date ("d-m-Y",strtotime($comp_order['renewable_date']));?></td>
										</tr>
										<?php $i++;}}?>
										
									</tbody>
								</table>
								
								
								
								
								
								
								
								
							</div>
						</div>
						<div class="col-sm-6 outer_overview">
							<div class="overview-box1">
								<h3>Mail Centre</h3>
								<table class="table table-striped jambo_table bulk_action">
									<tbody>
										<tr class="even pointer">
										<?php $i=0;?>
										<?php foreach($comp_mail as $mail)
										{
										if($i<3)
										 {
											$type=$mail->type_id;
											if($type==0)
											{
											$type_id ="HM Revenue & Customs";
											}
											else if($type==1)
											{
											$type_id="proof of address";
											}
											else if($type==2)
											{
											$type_id="Government Gateway";
											}
											else if($type==3)
											{
											$type_id="";
											}
											else if($type==11)
											{
											$type_id="Business Mail";
											}

											else if($type==12)
											{
											$type_id="Other Mail";
											}
											else if($type==13)
											{
											$type_id="Use Standard Memorandum & Articles";
											}
											else if($type==14)
											{
											$type_id="Upload Custom Memorandum & Articles";
											}
											else if($type==9)
											{
											$type_id="HM Courts";
											}
											else if($type==10)
											{
											$type_id="Intellectual Property Office";	
											}
										?>
											<td class="" width="230"><?php echo $type_id;?></td>
											<td class="text-center"><?php echo $mail->create_time;?></td>
											<!--td class="text-center">< ?php echo $mail->file_name;?></td-->
											<td class="text-center view_det"><a href="https://www.companymanager.online/uploads/<?php echo $mail->file_name;?>" target="blank">View</a></td>
										</tr>
										<?php $i++;}}?>
										
									</tbody>
								</table>
							</div>
							<div class="overview-box1">
								<h3>Message Centre</h3>
								<table class="table table-striped jambo_table bulk_action">
									<tbody>
							<?php $i=0;
							foreach($comp_message as $message)
							{
								if($i<3)
										 {
							?>						
										
										
										<tr class="even pointer">
											<td class="" width="230"><?php echo $message->message;?></td>
											<td class="text-center"><?php echo $message->to_id;?></td>
											<td class="text-center"><?php echo $message->from_id;?></td>
											<td class="text-center"><?php echo $message->create_time;?></td>
											
										</tr>
										<?php $i++;}}?>
										
									</tbody>
								</table>
							</div>
							<div class="overview-box1">
								<h3>Documents</h3>
								<table class="table table-striped jambo_table bulk_action">
									<tbody>
									<?php $i=0;?>
									<?php foreach($comp_doc as $document)
										{
										 if($i<3)
										 {
										//var_dump($document);die("done");
										$type=$document->type_id;
										//echo $type;die("jas");
											if($type==4)
											{
											$type_id ="passport";
											}
											else if($type==5)
											{
											$type_id="proof of address";
											}
											else if($type==6)
											{
											$type_id="Invoice";
											}
											else if($type==7)
											{
											$type_id="Terms and Conditions";
											}
											else if($type==8)
											{
											$type_id="";
											}

											else if($type==15)
											{
											$type_id="Certificate of Incorporation";
											}
											else if($type==16)
											{
											$type_id="Memorandum";
											}
											else if($type==17)
											{
											$type_id="Articles of Association";
											}
											else if($type==18)
											{
											$type_id="First Minutes";
											}
											else if($type==19)
											{
											$type_id="Share Certificates";	
											}
										?>
										<tr class="even pointer">
											<td class="" width="230"><?php echo $type_id;?></td>
											<td class="text-center"><?php echo $document->create_time;?></td>
											<!--td class="text-center">< ?php echo $document->file_name;?></td-->	
											<td class="text-center view_det"><a href="https://www.companymanager.online/uploads/<?php echo $document->file_name;?>" target="blank">View</a></td>
										</tr>
										
										<?php $i++;}}?>
										
									</tbody>
								</table>
							</div>
							<div class="overview-box1">
								<h3>Notes</h3>
								<p><?php echo $notes->notes;?></p>
							</div>
						</div>
					</div>
				</div>
				<!---------/Overview---------->
				<!--------Order History------->
				<div class="tab-pane fade" id="opportunities">
					<div class="overview_section">
						<div class="col-sm-12">
							
							<table class="table table-striped jambo_table bulk_action tbl-bg-email table-bordered">
								<thead>
									<tr class="headings">
									
										<th class="column-title">Date </th>
										<th class="column-title">Time </th>
										<th width="220" class="column-title">Activity </th>
										<th class="column-title">Description</th>
										<th width="200" class="column-title">User</th>
										
									</tr>
								</thead>
								<tbody class="History_print">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!------/Order History------->
				<!--------Add Service-------->
				<div class="tab-pane fade" id="add_service">
					<div class="overview_section">
						<div class="">
							<div class="col-sm-8 add_company_left">
								<div class="sub_nav">
									<ul class="nav navbar-nav nav-pills">
										<li class="active"><a data-toggle="tab" href="#individual_services" class="user-profile">Individual Services</a></li>
										<li class=""><a data-toggle="tab" href="#packages_services" class="user-profile">Packages</a></li>
									</ul>
								</div>
								<div class="tab-content">
									<div id="individual_services" class="My Company add_companies tab-pane fade active in">
										<h3>Address services</h3>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Registered Office Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Director Service Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Virtual Business Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<select class="form-control">
													<option value="0">--Select--</option>
													<option value="1">1 year- �99.00</option>
													<option value="2">6 Mths - �79.00</option>
													<option value="3">3 Mths - �49.00</option>
													<option value="4">1 Mth - �19.00</option>
												</select>
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										
										<h3>Additional Services</h3>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Telephone Answering Service</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Document Pack</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Certificate of Good Standing</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
									</div>
									<div id="packages_services" class="overview-box1 add_companies tab-pane fade">
										<h3>Address services</h3>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Registered Office Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Director Service Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Virtual Business Address</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<select class="form-control">
													<option value="0">--Select--</option>
													<option value="1">1 year- �99.00</option>
													<option value="2">6 Mths - �79.00</option>
													<option value="3">3 Mths - �49.00</option>
													<option value="4">1 Mth - �19.00</option>
												</select>
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										
										<h3>Additional Services</h3>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Telephone Answering Service</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Document Pack</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
										<div class="row margin_bott">
											<div class="col-xs-7 company_det">
												<h4><i class="fa fa-plus-circle"></i> Certificate of Good Standing</h4>
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
											</div>
											<div class="col-xs-3 free_sec">
												<i class="fa fa-info-circle">
													<div class="help"><i class="fa fa-caret-down"></i>Everything that the essential package offers, plus professional photography and a floor plan.</div>
												</i> Free
											</div>
											<div class="col-xs-2 company_btn"><button type="submit" class="btn btn-success">Add</button></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 add_company_right">
								<form class="right_package_box">
									<div class="box_header">
										<h3 class="pull-left">Shopping Cart</h3>
									</div>
									<div class="overview-box">
										<h3>Order Details</h3>
										
										<div class="form-group price_row price_border">
											<p><span>Address Services <i class="fa fa-times-circle"></i></span><span class="pull-right">$3250.00</span></p>
										</div>
										<div class="form-group price_row price_border">
											<p><span>Company Formation <i class="fa fa-times-circle"></i></span><span class="pull-right">$20.00</span></p>
										</div>
										<div class="form-group price_row price_border">
											<p><span>Additional Services <i class="fa fa-times-circle"></i></span><span class="pull-right">$25.00</span></p>
										</div>
										<div class="form-group price_row">
											<p><span>Total <i class="fa fa-times-circle"></i></span><span class="pull-right">$2250.00</span></p>
										</div>
										<div class="form-group price_btn">
											<button type="submit" class="btn btn-success">Add Company</button>
										</div>
										<div class="form-group price_btn">
											<p class="text-center">You will be sent an invoice</p>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--------/Add Service------->
				<!-----------Mail------------>
				<div class="tab-pane fade" id="ad_groups">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
							</div>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										<th class="column-title">Date <b class="caret caret-custom"></b></th>
										<th class="column-title">Time <b class="caret caret-custom"></b></th>
										<th class="column-title">Type <b class="caret caret-custom"></b></th>
										<th width="50%"; class="column-title file__types">File Name <b class="caret caret-custom"></b></th>
										<th class="column-title text-center">Download <b class="caret caret-custom"></b></th>
										<th class="column-title text-center">Original Copy <b class="caret caret-custom"></b></th>
										<th class="column-title text-center">Delete <b class="caret caret-custom"></b></th>
									</tr>
								</thead>
								<tbody class="mail-print">
									
								</tbody>
							</table>
						</div>
					</div>
				</div> 
				<div class="tab-pane fade" id="invoice_data">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
							</div>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										<th class="column-title">Date <b class="caret caret-custom"></b></th>
										<th class="column-title">Time <b class="caret caret-custom"></b></th>
										<th class="column-title">View <b class="caret caret-custom"></b></th>
									</tr>
								</thead>
								<tbody class="invoice-print">
									
								</tbody>
							</table>
						</div>
					</div>
				</div> 
				<div class="tab-pane fade" id="notify">
					<div class="col-md-12">
						<div class="my__payment__page">
							<form method="POST" id="alt_email" name="alt_email" action="<?php echo base_url();?>home/addAltEmail">
								<div class="services_list">
									<div class="email-row even">
										<i class="fa fa-envelope"></i><?php echo $email;?><a href="#">Primary</a>
									</div>
									<div class="email-row">
										<?php if(!$getAltEmail->email_2 == ""){	echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_2;	echo "<a onclick=change_email('$getAltEmail->email_2','email_2','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
										</div>
										<div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_3 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_3;	echo "<a onclick=change_email('$getAltEmail->email_3','email_3','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_4 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_4; echo "<a onclick=change_email('$getAltEmail->email_4','email_4','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_5 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_5; echo "<a onclick=change_email('$getAltEmail->email_5','email_5','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_6 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_6; echo "<a onclick=change_email('$getAltEmail->email_6','email_6','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_7 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_7; echo "<a onclick=change_email('$getAltEmail->email_7','email_7','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_8 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_8; echo "<a onclick=change_email('$getAltEmail->email_8','email_8','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_9 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_9; echo "<a onclick=change_email('$getAltEmail->email_9','email_9','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
							  <div class="email-row">
							  <?php echo ""; if(!$getAltEmail->email_10 == ""){echo "<i class='fa fa-envelope'></i>";echo $getAltEmail->email_10; echo "<a onclick=change_email('$getAltEmail->email_10','email_10','$getAltEmail->company_id','$notification');> Make Primary</a>";} ?>
							  </div>
								<div class="email-row">
								<i class="fa fa-plus-circle"></i>
								<input type="email" class="form-control" placeholder="Alternate Email Address" id="alternate_email" name="alternate_email" required>
								</div>
							  
								</div>
								<input type="hidden" id="alt_id" name="alt_id" value="<?php echo $notification;?>">
								<input type="hidden" id="alt_comm_id" name="alt_comm_id" value="<?php echo $getCompanyInfo_new->id;?>">
								
								<div class="row">
									<div class="col-xs-12 btns__row">
										<input class="btn btn-success" type="submit" value="Add Email Address" id="add_alter_email" name="Add Email Address">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div> 
				<!-----------/Mail------------>
				<!----------Messages---------->
				<div class="tab-pane fade" id="messages">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
								<ul class="nav navbar-nav">
									
							</div>
							<style>
							.message-print tr td {
								max-width: 400px;
								min-width: 30px;
								border-right: 1px solid #dddddd;
							}
							.table-striped.jambo_table.bulk_action.tbl-bg-email, tbody.mail-print tr td {
								border: 1px solid #ddd;
							}
							</style>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										
										<th class="column-title">Date<b class="caret caret-custom"></b></th>
										<th class="column-title">Time <b class="caret caret-custom"></b></th>
										<th class="column-title" width="135px">To<b class="caret caret-custom"></b></th>
										<th class="column-title">From <b class="caret caret-custom"></b></th>
										<th class="column-title">Tel No.<b class="caret caret-custom"></b></th>
										<th width="400" class="column-title">Message <b class="caret caret-custom"></b></th>
										<th  width="100" class="column-title Delivery_Status text-center">Delete <b class="caret caret-custom"></b></th>
									</tr>
								</thead>
								<tbody class="message-print">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!---------/Messages---------->
				<!--------My Company---------->
				<div class="tab-pane fade" id="my_company">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav"> 
								<ul class="nav navbar-nav nav-pills no_margin">
									<li class="active"><a data-toggle="tab" href="#over_view" class="user-profile">Overview</a></li>
									<li class=""><a data-toggle="tab" href="#reg_office" class="user-profile">Registered Office</a></li>
									<li><a data-toggle="tab" href="#comp_directors" class="user-profile dirtab">Directors</a></li>
									<li class=""><a data-toggle="tab" href="#comp_shareholders" class="user-profile sharetab">Shareholders</a></li>
									<li class=""><a data-toggle="tab" href="#comp_secretary" class="user-profile sectrtab">Secretary</a></li>
									<li class=""><a data-toggle="tab" href="#comp_PSC" class="user-profile psctab">PSC</a></li>
									<li class=""><a data-toggle="tab" href="#Documents_company" class="user-profile doctab" onclick = "Document_ajax12()">Documents</a></li>
								</ul>
							</div>

							<div class="overview-box22 tab-content">
								<!----Overview----->
								<div id="over_view" class="address_section tab-pane fade in active">
									<div class="row row_edit">
										<div class="col-sm-6">
											<div class="overview-box1">
												<h3><?php// echo $getCompanyInfo_new->company_name;?> Registered Office Address</h3>
													<span style="color:black;"></span>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Street</th>
														<th class="">Address1</th>
														<th class="">Town</th>
														<th class="">Country</th>
														<th class="">Postcode</th>
													</tr>
												</thead>
												<tbody class="">
													<tr class="">
														<td class=""><?php echo $comp_RegisterOffice->name_number;?><?php echo " ".$comp_RegisterOffice->street;?></td>
														<td class=""><?php echo $comp_RegisterOffice->address1;?></td>
														<td class=""><?php echo $comp_RegisterOffice->town;?></td>
														<td class=""><?php echo $comp_RegisterOffice->country;?></td>
														<td class=""><?php echo $comp_RegisterOffice->postcode;?></td>
													</tr>
													<?php //}?>
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>Director Summary</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														<th class="" width="40px">Edit</th>
														<th class="" width="40px">P / C</th>
														
													</tr>
												</thead>
												<tbody class="">
												<?php if($getdirector_corp){?>
												
												<?php foreach($getdirector_corp as $crop){?>
												
													<tr class="crop_<?php echo $crop->id;?>">
														<td class=""><?php echo $crop->first_name;?></td>
														<td class=""><?php echo $crop->last_name;?></td>
														
														<td class=""><input type="button" name="edit" value="edit" onclick="MyDirector('<?php echo $crop->id;?>','corp_dir','<?php echo $getCompanyInfo_new->company_name;?>');" ></td>
														<td class="">c</td>
													</tr>
													<?php }?>
													<?php }?>
												<?php if($getcompany_director){?>
												<?php foreach($getcompany_director as $dir){
												if($dir->api==0)
												{
												?>
												 
													<tr class="dir_<?php echo $dir->id;?>">
														<td class=""><?php echo $dir->first_name;?></td>
												 		<td class=""><?php echo $dir->last_name;?></td>
														<td class=""><input type="button" name="edit" value="edit" onclick="MyDirector('<?php echo $dir->id;?>','dir','<?php echo $getCompanyInfo_new->company_name;?>');"></td>
														<td class="">p</td>
													</tr>
													<?php }}?>
													<?php }?>
													
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>Shareholder Summary</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First name</th>
														<th class="">Last Name</th>
														<th class="">Shares</th>
														<th class="" width="40px">Edit</th>
														<th class="" width="40px">P / C</th>
													</tr>
												</thead>
												<tbody class="">
												<?php if($getShareholder){?>
												<?php foreach($getShareholder as $share){
												if($share->api!=1){
												?>

													<tr class="">
														<td class=""><?php echo $share->name;?></td>
														<td class=""><?php echo $share->last_name;?></td>
														<td class=""><?php echo $share->shares;?></td>
														<td class=""><input type="button" name="edit" value="edit" onclick="MyShare('<?php echo $share->id;?>','share' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
														<td class="">p</td>
													</tr>
													<?php }} }?>
													<?php if($GetcropShareholder){?>
													<?php foreach($GetcropShareholder as $sharecop){?>
													<tr class="">
														<td class=""><?php echo $sharecop->first_name;?></td>
														<td class=""><?php echo $sharecop->last_name;?></td>
														<td class=""><?php echo $sharecop->shares;?></td>
														<td class=""><input type="button" name="edit" value="edit" onclick="MyShare('<?php echo $sharecop->id;?>','corp_share' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
														<td class="">c</td>
													</tr>
													<?php }} ?>
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>Persons of Control (PSC) Summary</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														 <th class="" width="40px">Edit</th>
														 <th class="" width="40px">P / C</th>
													</tr>
												</thead>
												<tbody class="">
													<?php if($company_psc){?>
													<?php foreach($company_psc as $psc){
												     if($dir->api==0)
											         {
													?>
														<tr class="psc_<?php echo $psc->id;?>">
															<td class=""><?php echo $psc->first_name;?></td>
															<td class=""><?php echo $psc->last_name;?></td>
															<td class=""><input type="button" name="edit" value="edit" onclick="Mypsc('<?php echo $psc->id;?>','psc' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
															<td class="">P</td>
														</tr>
													<?php }}?>
													<?php }?>
													<?php if($company_psc_corp){?>
													<?php foreach($company_psc_corp as $psc_corp){?>
														<tr class="psc_corp_<?php echo $psc_corp->id;?>">
															<td class=""><?php echo $psc_corp->first_name;?></td>
															<td class=""><?php echo $psc_corp->last_name;?></td>
															<td class=""><input type="button" name="edit" value="edit" onclick="Mypsc('<?php echo $psc_corp->id;?>','corp_psc' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
															<td class="">C</td>
														</tr>
													<?php }?>
													<?php }?>
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>Secretary Summary</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														<th class="" width="40px">Edit</th>
														<th class="" width="40px">P / C</th>
														
													</tr>
												</thead>
												<tbody class="">
													<?php if($getcompany_secretaries){?>
													<?php foreach($getcompany_secretaries as $sectr){
													if($sectr->api !=1){
													?>
													<tr class="sectr_<?php echo $sectr->id;?>">
													<td class=""><?php echo $sectr->name;?></td>
													<td class=""><?php echo $sectr->last_name;?></td>
													<td class=""><input type="button" name="edit" value="edit" onclick="MySecterory('<?php echo $sectr->id;?>','sectr','<?php echo $getCompanyInfo_new->company_name;?>')"></td>
                                                    <td class="">p</td>
													</tr>
													<?php }}?>
													<?php }?>
												
												   <?php if($getcompany_secratory_corp){?>
													<?php foreach($getcompany_secratory_corp as $sectrcrop){?>
													<tr class="sectr_<?php echo $sectrcrop->id;?>">
													<td class=""><?php echo $sectrcrop->first_name;?></td>
													<td class=""><?php echo $sectrcrop->last_name;?></td>
													<td class=""><input type="button" name="edit" value="edit" onclick="MySecterory('<?php echo $sectrcrop->id;?>','corp_sectr' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
													<td class="">c</td>
													</tr>
													<?php }?>
													<?php }?>
												</tbody>
											</table>
											</div>
										</div>
										
										
										
										
										<div class="col-sm-6">
											<div class="overview-box1">
												<h3>Share Capital</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Issues</th>
														<th class="">Currency</th>
														<th class="">Type</th>
													</tr>
												</thead>
												<tbody class="">
												<?php foreach($getShareholder as $share){
												if($share->api==0)
												{
												?>
												
												
													<tr class="">
														<td class=""><?php echo $share->per_person_shares;?></td>
														<td class=""><?php echo $share->currency;?></td>
														<td class=""><?php echo $share->share_class;?></td>
														
													
													</tr>
													<?php }} ?>
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>SIC CODE</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">sic code </th>
														<th class="">sic code1 </th>
														<th class="">sic code2 </th>
														<th class="">sic code3 </th>
														<!--th class="">Next_due_date to Next Accounts Due</th-->
														
														
													</tr>
												</thead>
												<tbody class="">
													
												
												
													<tr class="imp_<?php echo $sic_code->id;?>">
														<td class="last_<?php echo $sic_code->id;?>"><?php echo $sic_code->sic_code; ?>
													    </td>
														<td class="last_<?php echo $sic_code->id;?>"><?php echo $sic_code->sic_code1; ?>
														<td class="last_<?php echo $sic_code->id;?>"><?php echo $sic_code->sic_code2; ?>
													    </td>
														<td class="last_<?php echo $sic_code->id;?>"><?php echo $sic_code->sic_code3; ?>
													    </td>
													</tr>
													
												
														
												</tbody>
											</table>
											</div>
											<div class="overview-box1">
												<h3>Company Documents</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Date</th>
														<th class="">Document Type</th>
														<th class="">View</th>
														
													</tr>
												</thead>
												<tbody class="">
												<?php foreach($comp_doc_ltd as $document){
												$type=$document->type_id;
												if($type==15)
											{ 
											$type_id="Certificate of Incorporation";
											}
											else if($type==16)
											{
											$type_id="Memorandum";
											}
											else if($type==17)
											{
											$type_id="Articles of Association";
											}
											else if($type==18)
											{
											$type_id="First Minutes";
											}
											else if($type==19)
											{
											$type_id="Share Certificates";	
											}
												?>
												
												
												
											      <tr class="">
														<td class=""><?php echo $document->create_time;?></td>
														<td class=""><?php echo $type_id;?></td>
														<td class=""><a href="https://www.companymanager.online/uploads/<?php echo $document->file_name;?>" target="blank">View</a></td>
														<!--td class="">< ?php echo $admin->currency;?></td!-->
													
													</tr>
													<?php } ?>
													
					 							</tbody>
											</table>
											</div>
										</div>
									</div>
								</div>
								<!----/Overview----->
								<!----Registered Office----->
								<div id="reg_office" class="address_secs address_section tab-pane fade">
									<div class="director_header">
										<h3 class="pull-left"><?php echo $notes->company_name;?></h3>
									</div>
									
									<div class="address_edit">
										<form>
											<div class="row">
												<div class="col-xs-12 edit_sec_head">
													<h2>Registered Office Address</h2>
												</div>
												<!--div class="col-sm-6 col-xs-12 form-group">
													<label>Select Address</label>
													<select class="form-control" name="ldt_reg_form[register][reg_add_prefix]" id="get_address_select">
														<option value="0">--Select--</option>
														<option value="1">Great Portland Street, W1</option>
														<option value="2">Bloomsbury Way, Wc1</option>
														<option value="3">Edinburgh, EH2</option>
														<option value="4">--</option>
													</select>
												</div-->
												<div class="col-sm-6 col-xs-12 form-group">
													<label>House No. or Name</label>
													<input class="form-control" type="hidden" name="ldt_reg_form[register][selected_address]" id="selected_address" value="<?php echo $_SESSION ['ldt_reg_form']["selected_address"];?>">
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_building_name]" id="reg_building_name" value="<?php echo $comp_RegisterOffice->name_number;?>">
													<input class="form-control" type="hidden" value="<?php echo $comp_RegisterOffice->id;?>" id="roc_id">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>Street*</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_street]" id="reg_street" value="<?php echo $comp_RegisterOffice->street;?>">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>Address</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_address1]" id="reg_address1" value="<?php echo $comp_RegisterOffice->address1;?>">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>Town*</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_town]" id="reg_town" value="<?php echo $comp_RegisterOffice->town;?>">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>County</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_county]" id="reg_county" value="<?php echo $comp_RegisterOffice->county;?>">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>Postcode*</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_postcode]" id="reg_postcode" value="<?php echo $comp_RegisterOffice->postcode;?>">
												</div>
												<div class="col-sm-6 col-xs-12 form-group">
													<label>Country*</label>
													<input class="form-control" type="text" name="ldt_reg_form[register][reg_country]" id="reg_country" value="<?php echo $comp_RegisterOffice->country;?>">
												</div>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success" id="regupdateform">Submit Changes</a>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!----/Registered Office----->
								<!----Director----->
								<div id="comp_directors" class="address_secs address_section tab-pane fade">
									<div class="director_header">
										<h3 class="pull-left"><?php echo $notes->company_name;?></h3>
										<div class="director_btn pull-right">
											<a class="btn btn-success edit_pencil" id="add_director">Add Person Director</a>
											<a class="btn btn-success edit_pencil2" id="add_corp_director">Add Corporate Director</a>
										</div>
									</div>
									
									<div class="address_edit">
									<div class="col-xs-12 edit_sec_head">
													<!--h2>Company Name</h2-->
												</div>
												<div class="row">
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Person Directors</label> 
													<select id="person_select_dir" onchange="MyDirector('','dir','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($getcompany_director as $dir){?>
													<option value="<?php echo $dir->id?>">

													<?php echo $dir->first_name . " " .$dir->last_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Corporate Director</label> 
													<select id="person_select_crop" onchange="MyDirector('','corp_dir','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($getdirector_corp as $crop){?>
													<option value="<?php echo $crop->id?>">

													<?php echo $crop->first_name . " " .$dir->last_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												</div>
										<form class="directer" style="display:none;">
											<div class="row">
												<div class="col-xs-12 edit_sec_head">
													<h2>Person Director</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Title:</label>
													<select class="form-control" id="d_title">
														<option value="Mr">Mr</option>
														<option value="Mrs">Mrs</option>
														<option value="Miss">Miss</option>
														<option value="Ms">Ms</option>
														<option value="Dr">Dr</option>
														<option value="Prof">Prof</option>
														<option value="Master">Master</option>
														<option value="Sir">Sir</option>
														<option value="Lord">Lord</option>
														<option value="Rev">Rev</option>
													</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">First name:</label> 
													<input class="form-control" type="text" value="" id="sec_fname">
													<input class="form-control" type="hidden" value="" id="d_id">
													<input class="form-control" type="hidden" value="<?php echo $getCompanyInfo_new->id ;?>" id="d_comp_id">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Middle name:</label> 
													<input class="form-control" type="text" id="d_Middle">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Last name:</label> 
													<input class="form-control" type="text" id="d_last">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box dob">
													<label for="Title ">Date of birth:</label>
													<div class="row">
													<?php
																$dates = $this->ltd_model->getDays();
																$months = $this->ltd_model->getMonths();
																$year =$this->ltd_model->getYear();?>
														<div class="col-sm-4">
														<select class="form-control" name="ltd_dir[directors][dir_date]" id="dir_date" required>
															<?php foreach($dates as $value){	?>
																	<option value="<?php echo $value;?>"><?php echo $value; ?></option>
																<?php }?>
														</select>
														</div>
														<div class="col-sm-4">
															<select class="form-control" name="ltd_dir[directors][dir_month]" id="dir_month" required>
																<?php 
																
																
																foreach($months as $value){?>
																		<option value="<?php echo $value;?>"><?php echo $value; ?></option>
																
																<?php }?>
															<!--option><?php //echo $_SESSION ['ltd_dir']['directors']["dir_month"];?></option-->
															 </select>
														</div>
														<div class="col-sm-4">
															<select class="form-control" name="ltd_dir[directors][dir_year]" id="dir_year" required>
																<!--option><?php //echo $_SESSION ['ltd_dir']['directors']["dir_year"];?></option-->
																	<?php foreach($year as $value){?>
																		<option value="<?php echo $value;?>"><?php echo $value; ?></option>
																
																	<?php }?>
															   	</select>
														</div>
													</div>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Nationality:</label> 
													<?php //$country =$this->ltd_model->getCountry();?>
													<select class="form-control" id="dir_nationality">
														<?php foreach($country as $value){?>
																		<option value="<?php echo $value;?>"><?php echo $value; ?></option>
																	<?php }?>
													</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title">Occupation:</label> 
													<input class="form-control" type="text" id="dc_occu">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Country of Residence:</label> 
													
													<select class="form-control" name="ltd_dir[directors][dir_residence]" id="dir_residence" required>
																	<?php foreach($country as $value){?>
																		<option value="<?php echo $value;?>" ><?php echo $value; ?></option>
																	<?php }?>
															</select>
												</div>
												<!-----/Person Director------>
												<!-----Select a Director Service Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Select a Director Service Address</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
															<label for="Title ">Select an Address:</label> 
															<select class="form-control" name="ltd_dir[directors][dir_prefill]" id="director_select_pre_adrress" required="">
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option>  
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>
													</select>
												</div>
												<!-----/Select a Director Service Address------>
												<!-----Service Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Service Address</h2>
												</div>
												
													<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="hidden" name="ltd_dir[directors][edit_person_id]" id="edit_person_id" value="0">
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_name]" id="dir_build_name" value="" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_street]" id="dir_build_street" value="" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_address]" id="dir_build_address" value="" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_town]" id="dir_build_town" value="" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_county]" id="dir_build_county" value="" > 
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_postcode]" id="dir_build_postcode" value="" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_dir[directors][dir_build_country]" id="dir_build_country" value="" required>
														</div>
														<div class="form-group col-sm-12 col-xs-12 box new_checkbox">
														<label for="Title ">
																<input class="form-control" type="checkbox" id="same_reg_address">
																<span>My Residential address is the same as my service address.</span>
														 </label> 
															
														</div>
		
												<!-----/Service Address------>
												<!-----Residential Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Residential Address</h2>
												</div>
												<div class="col-xs-12 sec_head">
													<p class="text-note">The residential address must be the address where you live.</p>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Building name/number:</label>>
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_name]" id="dir_reg_name" value="" required>
												</div>
												
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Street:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_street]" id="dir_reg_street" value="" required>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Address 3:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_address]" id="selected_dir_reg_address" value="" >
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Town:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_town]" id="dir_reg_town" value="" required>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>County:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_county]" id="dir_reg_county" value="" >
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Postcode:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_postcode]" id="dir_reg_postcode" value="" required>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Country:</label> 
													<input class="form-control" type="text" name="ltd_dir[directors][dir_reg_country]" id="dir_reg_country" value="" required>
												</div>
												<!-----/Residential Address------>
												<!-----/Consent to act------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Residential Address</h2>
												</div>
												<div class="form-group col-sm-12 box">
													<label class=""> <input type="checkbox">
													 <span>The subscribers (shareholders) confirm that the person named has consented to
													 act as director</span>
													</label>
												</div>
												<!-----/Consent to act------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success dirinsert" id="dir_button">Save changes</a>
														<a class="btn btn-success dirinsert" id="btn2" style="display:none;">Save changes</a>
													</div>
												</div>
											</div>
										</form> 
									</div>
									<!--Bob Hoskins-->
									<!--Company Director-->
									<div class="address_edit2">
										<form class="directer_crop" style="display:none;">
											<div class="row">
												<!-----Company Name------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Company Name</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Company Name:</label> 
													<input class="form-control" type="text" id="crop_company">
												</div>
												<!-----/Company Name------>
												<!-----Corporate------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Corporate</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Company Name:</label> 
													<input class="form-control" type="text" id="dcrop_cname">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">First name:</label> 
													<input class="form-control" type="text" id="dcop_fname">
													<input class="form-control" type="hidden" value="" id="dc_id">
													<input class="form-control" type="hidden" value="<?php echo $getCompanyInfo_new->id ;?>" id="dc_comp_id">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Last name:</label> 
													<input class="form-control" type="text" id="dcrop_lname">
												</div>
												<!-----/Corporate------>
												<!-----Prefill------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
													<label for="Title ">Select an Address:</label> 
													<select class="form-control" name="ltd_dir[corp_directors][corp_directors_pre_address]" id="dir_corpo_select_preeAddress">
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>															
															</select>
												</div>
												
												<!-----/Prefill------>
												<!-----Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Building name/number:</label>
													<input class="form-control" type="hidden" name="ltd_dir[corp_directors][edit_corp_id]" id="edit_corp_id" value="0">
													<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_name]" id="corp_dir_build_name" required>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_street]" id="corp_dir_build_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_address]" id="corp_pree_adrress">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_town]" id="corp_dir_build_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_county]" id="corp_dir_build_county" > 
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_postcode]" id="corp_dir_build_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_build_country]" id="corp_dir_build_country" required>
														</div>
												<!-----/Address------>
												<!-----EEA/Non EEA------>
												<div class="col-xs-12 edit_sec_head">
													<h2>EEA/Non EEA</h2>
												</div>
												
												<div class="form-group col-sm-12 col-xs-12 box">
															<label for="Title ">Company Type:</label> 
															<div>
																<input type="radio" name="ltd_dir[corp_directors][dir_type]" id="dir_type1" value="0"> EEA
																<input type="radio" name="ltd_dir[corp_directors][dir_type]" id="dir_type2" value="1"> Non EEA
															</div>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country Registered:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_corp_country]" id="dir_corp_country" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Registration Number:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_corp_reg_numbr]" id="dir_corp_reg_numbr" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Governing Law:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_corp_low]" id="dir_corp_low" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Legal Form:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_directors][dir_corp_legal]" id="dir_corp_legal" required> 
														</div>
												
												<!-----/EEA/Non EEA------>
												<!-----/Consent to act------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Consent to act</h2>
												</div>
												<div class="form-group col-sm-12 box">
													<label class="checkbox"> 
															<input type="checkbox" class="required" name="ltd_dir[corp_directors][dir_corp_act]" id="dir_corp_act" required>
															 <span>The subscribers (shareholders) confirm that the corporate body named has consented to act as a director</span>
															</label>
												</div>
												<!-----/Consent to act------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														
														<a class="btn btn-success cropinsert" id="dir_button1" >Save changes</a>
														<a class="btn btn-success  cropinsert" id="btn3" style="display:none;">Save changes</a>
													</div>
												</div> 
											</div>
										</form>
									</div>
									<!--Company Director-->
								</div>
								<!----/Director----->
								<!----Shareholders----->
								<div id="comp_shareholders" class="address_secs address_section tab-pane fade">
									<div class="director_header">
										<h3 class="pull-left"><?php echo $notes->company_name;?></h3>
										<div class="director_btn pull-right">
											<a class="btn btn-success edit_pencil" id="addshare">Add a Person Shareholder</a>
											<a class="btn btn-success edit_pencil2" id="addsharecrop">Add a Corporate Shareholder</a>
										</div>
									</div>
									<!-- Add a Person Shareholder-->
									<div class="address_edit">
									<div class="row">
									<div class="form-group col-sm-6 col-xs-12 box">
													<label>Person Shareholder</label> 
													<select id="person_select_share" onchange="MyShare('','share','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($getShareholder as $share){
													//var_dump($share);die("kjki");
													?>
													
													<option value="<?php echo $share->id?>">

													<?php echo $share->name ." ".$share->last_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Corporate Shareholder</label> 
													<select id="person_select_sharecrop" onchange="MyShare('','corp_share','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($GetcropShareholder as $sharecop){?>
													<option value="<?php echo $sharecop->id?>">

													<?php echo $sharecop->first_name?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												</div>
										<form class="shareform" style="display:none;">
											<div class="row">
												
												<div class="col-xs-12 edit_sec_head">
													<h2>Person Shareholder</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Title:</label>
													<select class="form-control" id="shareholder_title">
														<option value="Mr">Mr</option>
														<option value="Mrs">Mrs</option>
														<option value="Miss">Miss</option>
														<option value="Ms">Ms</option>
														<option value="Dr">Dr</option>
														<option value="Prof">Prof</option>
														<option value="Master">Master</option>
														<option value="Sir">Sir</option>
														<option value="Lord">Lord</option>
														<option value="Rev">Rev</option>
													</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="hidden" name="ltd_shareholder[shareholder][shareholder_record_id]" id="shareholder_record_id" value="0">
															<input class="form-control" type="hidden" name="ltd_shareholder[shareholder][shareholder_edit_id]" id="shareholder_edit_id" value="0">
															<input class="form-control" type="text"  name="ltd_shareholder[shareholder][shareholder_first_name]" id="shareholder_first_name" required>
															<input class="form-control" type="hidden" value="" id="sh_id">
													         <input class="form-control" type="hidden" value="<?php echo $getCompanyInfo_new->id ;?>" id="sh_comp_id">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Middle name:</label> 
															<input class="form-control" type="text"  name="ltd_shareholder[shareholder][shareholder_middle_name]" id="shareholder_middle_name" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_last_name]" id="shareholder_last_name" required>
														</div>
												<!-----/Person Shareholder------>
												<!-----Shareholder Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Shareholder Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_building_name]" id="shareholder_building_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_building_street]" id="shareholder_building_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_building_address3]" id="shareholder_building_address3">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_building_town]" id="shareholder_building_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_postcode]" id="shareholder_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<select class="form-control" name="ltd_shareholder[shareholder][shareholder_country]" id="shareholder_country" required>
																
																<?php foreach($country as $value){?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php }?>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Nationality:</label> 
															<select class="form-control" name="ltd_shareholder[shareholder][shareholder_nationality]" id="shareholder_nationality" required>
																
																<?php foreach($country as $value){?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php }?>
															</select>
														</div>
												
												<!-----/Shareholder Address------>
												<!-----Allotment of Shares------>
												
												<div class="form-group col-sm-6 col-xs-12 box">
														<label for="Title ">Share Currency:</label> 
														<select class="form-control" name="ltd_shareholder[shareholder][shareholder_currency]" id="shareholder_share_Currency" required>
														   <option value="GBP">GBP &pound;</option>
															<option value="USD">USD $</option>
															<option value="EURO">EURO �</option>
														</select>
													</div>
													<div class="form-group col-sm-6 col-xs-12 box">
														<label for="Title ">Share Class:</label> 
														<input class="form-control" type="text" value="ORDINARY" name="ltd_shareholder[shareholder][shareholder_share_class]" id="shareholder_share_class" required>
													</div>
													<div class="form-group col-sm-6 col-xs-12 box">
														<label for="Title ">Number of shares:</label> 
														<input class="form-control" type="text" value="1" name="ltd_shareholder[shareholder][shareholder_of_shares]" id="shareholder_of_shares" required>
														<p>By default we recommend 1 share per shareholder. You can easily allocate extra shares after incorporation.</p>
													</div>
													<div class="form-group col-sm-6 col-xs-12 box">
														<label for="Title ">Value per share:</label> 
														<input class="form-control" type="text" value="1" name="ltd_shareholder[shareholder][shareholder_per_share]" id="shareholder_per_share" required>
														<p>By default we recommend a share value of 1. The number of shares and value per share limits your company's liability.</p>
													</div>
												<!-----/Allotment of Shares------>
												<!-----/Security------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Security</h2>
												</div>
												<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">First three letters of Town of  birth*:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_1]" id="shareholder_security_1" required>
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">Last three digits of Telehone number*:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_2]" id="shareholder_security_2" required>
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">First three letters of your fathers name</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_3]" id="shareholder_security_3" required>
														</div>
												<!-----/Security------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success" id="share">Submit</a>
														<a class="btn btn-success" id="share_inc" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!-- /Add a Person Shareholder-->
									<!-- Add a Corporate Shareholder-->
									<div class="address_edit2">
										<form class="sharecorpform" style="display:none;">
											<div class="row">

												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Company Name:</label> 
													<input class="form-control" type="text">
												</div>
												<!-----/Company Name------>
												<!-----Corporate------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Corporate</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Company name:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp_comp_name]" id="corp_comp_name" required>
															<input class="form-control" type="hidden" name="ltd_shareholder[shareholder_corp][shareholder_corp_record_id]" id="shareholder_corp_record_id" value="0">
															
															<input class="form-control" type="hidden" value="" id="shcorp_id">
													<input class="form-control" type="hidden" value="<?php echo $getCompanyInfo_new->id ;?>" id="shcorp_comp_id">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp_first_name]" id="corp_first_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp_last_name]" id="corp_last_name" required>
														</div>
												<!-----/Corporate------>
												<!-----Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__build_name]" id="corp__build_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__street]" id="corp__street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__address]" id="share_corp_pre_address" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__town]" id="corp__town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__county]" id="corp__county" required> 
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder_corp][corp__postcode]" id="corp__postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<?php// $country =$this->ltd_model->getCountry();?>
															<select class="form-control" name="ltd_shareholder[shareholder_corp][corp_build_country]" id="corp_build_country" required>
																	<?php foreach($country as $value){?>
																		<option value="<?php echo $value;?>" ><?php echo $value; ?></option>
																	<?php }?>
															</select>
														</div>
												<!-----/Address------>
												<!-----Allotment of Shares------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Allotment of Shares</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Share Currency:</label> 
															<select class="form-control" name="ltd_shareholder[shareholder_corp][shareholder_currency]"  id ="shareholder_currency1" required>
																<option value="GBP">GBP &pound;</option>
																<option value="USD">USD $</option>
																<option value="EURO">EURO �</option>
															</select>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Share Class:</label> 
															<input class="form-control" type="text" value="ORDINARY" name="ltd_shareholder[shareholder_corp][shareholder_share_class]" id="shareholder_share_class1" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Number of shares:</label> 
															<input class="form-control" type="text" value="1" name="ltd_shareholder[shareholder_corp][shareholder_of_shares]" id="shareholder_of_shares1" required>
															<p>By default we recommend 1 share per shareholder. You can easily allocate extra shares after incorporation.</p>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Value per share:</label> 
															<input class="form-control" type="text" value="1" name="ltd_shareholder[shareholder_corp][shareholder_per_share]" id="shareholder_per_share1" required>
															<p>By default we recommend a share value of 1. The number of shares and value per share limits your company's liability.</p>
														</div>
												<!-----/Allotment of Shares------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Security</h2>
												</div>
												<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">First three letters of Town of  birth*:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_1]" id="shareholder_security_1s" required>
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">Last three digits of Telehone number*:</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_2]" id="shareholder_security_2s" required>
														</div>
														<div class="form-group col-sm-4 col-xs-12 box">
															<label for="Title ">First three letters of your fathers name</label> 
															<input class="form-control" type="text" name="ltd_shareholder[shareholder][shareholder_security_3]" id="shareholder_security_3s" required>
														</div>
												<!-------------------------------->
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success" id="sharecorp">Submit</a>
														<a class="btn btn-success" id="sharecorp_inc" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!-- /Add a Corporate Shareholder-->
								</div>
								<!----/Shareholders----->
								<!----secretary----->
								<div id="comp_secretary" class="address_secs address_section tab-pane fade">
									<div class="director_header">
										<h3 class="pull-left"><?php echo $notes->company_name;?></h3>
										<div class="director_btn pull-right">
											<a class="btn btn-success edit_pencil" id="add_Secretary">Add a Person Secretary</a>
											<a class="btn btn-success edit_pencil2" id="add_Secretarycorp">Add a Corporate Secretary</a>
										</div>
									</div>
									<!--Add a Person Secretary-->
									<div class="address_edit">
									<div class="row">
									<div class="form-group col-sm-6 col-xs-12 box">
													<label>Person PSC</label> 
													<select id="person_select_sect" onchange="MySecterory('','sectr','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($getcompany_secretaries as $sectr){?>
													<option value="<?php echo $sectr->id?>">

													<?php echo $sectr->name ." ".$sectr->last_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Corporate PSC</label> 
													<select id="person_select_sectcrop" onchange="MySecterory('','corp_sectr','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($getcompany_secratory_corp as $sectrcrop){?>
													<option value="<?php echo $sectrcrop->id?>">

													<?php echo $sectrcrop->first_name ." ".$sectrcrop->last_name; ?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												</div>
										<form class="Secretary" style="display:none;">
											<div class="row">
												<!-----Company Name------>
												
												<!-----/Company Name------>
												<!-----Person------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Person</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Title:</label>
													<select class="form-control" id="s_title">
														<option value="Mr">Mr</option>
														<option value="Mrs">Mrs</option>
														<option value="Miss">Miss</option>
														<option value="Ms">Ms</option>
														<option value="Dr">Dr</option>
														<option value="Prof">Prof</option>
														<option value="Master">Master</option>
														<option value="Sir">Sir</option>
														<option value="Lord">Lord</option>
														<option value="Rev">Rev</option>
													</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">First name:</label> 
													<input class="form-control" type="text" id="s_fname">
													<input class="form-control" type="hidden" id="s_id">
													<input class="form-control" type="hidden" id="scom_id" value="<?php echo $getCompanyInfo_new->id;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Middle name:</label> 
													<input class="form-control" type="text" id="s_mname">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Last name:</label> 
													<input class="form-control" type="text" id="s_lname">
												</div>
												<!-----/Person------>
												<!-----Prefill------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
													<select class="form-control" name="ltd_secretary[Secretary][secretary_pre_address]" id="select_secretary_pre_adrress">
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option>  
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>
															</select>
												</div>
												<!-----/Prefill------>
												<!-----Service Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Service Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_name]" id="secretary_building_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_street]" id="secretary_building_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_address3]" id="secretary_pre_address" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_town]" id="secretary_building_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_county]" id="secretary_building_county" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_postcode]" id="secretary_building_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_secretary[Secretary][secretary_building_country]" id="secretary_building_country" required>
															

														</div>
												<div class="form-group col-sm-12 col-xs-12 box new_checkbox">
												<label for="Title ">
														<input class="form-check" type="checkbox">
														<span>My Residential address is the same as my service address.</span>
												 </label> 
													
												</div>
												<!-----/Service Address------>
												<!-----/Consent to act------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Residential Address</h2>
												</div>
												<div class="form-group col-sm-12 box">
													<label class=""> <input type="checkbox">
													 <span>The subscribers (shareholders) confirm that the person named has consented to act as secretary</span>
													</label>
												</div>
												<!-----/Consent to act------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success sectbtn" id="sec_button">Submit</a>
														<a class="btn btn-success sectbtn" id="sectorbtn2" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!--/Add a Person Secretary-->
									<!--Add a Corporate Secretary-->
									<div class="address_edit2">
										<form class="Secretarycorp" style="display:none;">
											<div class="row">
												<!-----Company Name------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Company Name</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Company Name:</label> 
													<input class="form-control" type="text">
												</div>
												<!-----/Company Name------>
												<!-----Corporate------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Corporate</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Company Name:</label> 
													<input class="form-control" type="text">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">First name:</label> 
													<input class="form-control" type="text" id="sc_fname">
													<input class="form-control" type="hidden" id="sc_id">
													<input class="form-control" type="hidden" id="sccom_id" value="<?php echo $getCompanyInfo_new->id;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Last name:</label> 
													<input class="form-control" type="text" id="sc_lname">
												</div>
												<!-----/Corporate------>
												<!-----Prefill------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
													<label for="Title ">Select an Address:</label> 
													<select class="form-control" name="ltd_dir[corp_secratory][corp_secratory_pre_address]" id="corp_secratory_pre_address">
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>															
															</select>
												</div>
												<!-----/Prefill------>
												<!-----Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="hidden" name="ltd_dir[corp_secratory][secratory_edit_corp_id]" id="secratory_edit_corp_id" value="0">
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_build_name]" id="secratory_build_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][corp_secratory_build_street]" id="corp_secratory_build_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_corp_pree_adrress]" id="secratory_corp_pree_adrress">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][corp_secratory_build_town]" id="corp_secratory_build_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][corp_secratory_build_county]" id="corp_secratory_build_county" > 
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][corp_secratory_build_postcode]" id="corp_secratory_build_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<select class="form-control" name="ltd_dir[corp_secratory][corp_secratory_build_country]" id="corp_secratory_build_country" required>
																<?php 
																//$country = $this->ltd_model->getCountry();
																foreach($country as $value){?>
																	<option value="<?php echo $value;?>" ><?php echo $value; ?></option>
																<?php }?>
															</select>
															
														</div>
												<!-----/Address------>
												<!-----EEA/Non EEA------>
												<div class="col-xs-12 edit_sec_head">
													<h2>EEA/Non EEA</h2>
												</div>
												
												<div class="form-group col-sm-12 col-xs-12 box">
															<label for="Title ">Company Type:</label> 
															<div>
																<input type="radio" name="ltd_dir[corp_secratory][secratory_type]" id="secratory_type1" value="0"> EEA
																<input type="radio" name="ltd_dir[corp_secratory][secratory_type]" id="secratory_type2" value="1"> Non EEA
															</div>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country Registered:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_corp_country]" id="secratory_corp_country" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Registration Number:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_corp_reg_numbr]" id="secratory_corp_reg_numbr" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Governing Law:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_corp_low]" id="secratory_corp_low" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Legal Form:</label> 
															<input class="form-control" type="text" name="ltd_dir[corp_secratory][secratory_corp_legal]" id="secratory_corp_legal" required> 
														</div>
												
												<!-----/EEA/Non EEA------>
												<!-----/Consent to act------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Consent to act</h2>
												</div>
												<div class="form-group col-sm-12 box">
													<label class=""> <input type="checkbox">
													 <span><span>The subscribers (shareholders) confirm that the corporate body named has consented to act as a director</span></span>
													</label>
												</div>
												<!-----/Consent to act------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success sectcorpbtn" id="sec_button1">Submit</a>
														<a class="btn btn-success sectcorpbtn" id="sectorropbtn2" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!--/Add a Corporate Secretary-->
								</div>
								<!----/secretary----->
								
								<!----PSC----->
								<div id="comp_PSC" class="address_secs address_section tab-pane fade">
									<div class="director_header">
										<h3 class="pull-left"><?php echo $notes->company_name;?></h3>
										<div class="director_btn pull-right">
											<a class="btn btn-success edit_pencil" id="person_psc"> Add a Person PSC</a>
											<a class="btn btn-success edit_pencil2" id="person_pscrop"> Add a Corporate PSC</a>
										</div>
									</div>
									<!-- Add a Person PSC-->
									<div class="address_edit">
									<div class="row">
									<div class="form-group col-sm-6 col-xs-12 box">
													<label>Person PSC</label> 
													<select id="person_select_psc" onchange="Mypsc('','psc','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">Select</option>
													<?php foreach($company_psc as $psc){?>
													<option value="<?php echo $psc->id?>">

													<?php echo $psc->first_name ." ".$psc->last_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
										 		<div class="form-group col-sm-6 col-xs-12 box">
													<label>Corporate PSC</label> 
													<select id="person_select_psccrop" onchange="Mypsc('','corp_psc','<?php echo $getCompanyInfo_new->company_name;?>');">
													<option value="0">select</option>
													<?php foreach($company_psc_corp as $psc_corp){?>
													
									  				<option value="<?php echo $psc_corp->id?>">

													<?php echo $psc_corp->first_name;?></option>
													<?php }?>
													
													</select>
													<!--input class="form-control" type="text" id="dc_name" value=""-->
												</div>
												</div>
										<form id="psc_form" style="display:none";>
											<div class="row">
												<!-----Company Name------>
												
												<!-----/Company Name------>
												<!-----Prefill Menu------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill Menu</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Person Director:</label> 
													<select class="form-control" name="psc_dir_change" id="psc_dir_change" required>
															
															<option>------Select------</option>
															<?php
															$prefillAddressDir = $this->ltd_model->directorData($getCompanyInfo_new->id);
															 foreach ($prefillAddressDir as $values){
															 
															?>
																		<option value="<?php echo $values['id'];?>"><?php echo $values['first_name']." ".$values['middle_name']." ".$values['last_name'];?></option> 
															<?php	}	?>
															</select>
												</div>
												<!-----/Prefill Menu------>
												<!-----Person------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Person</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Title:</label>
													<select class="form-control" id="p_title">
														<option value="Mr">Mr</option>
														<option value="Mrs">Mrs</option>
														<option value="Miss">Miss</option>
														<option value="Ms">Ms</option>
														<option value="Dr">Dr</option>
														<option value="Prof">Prof</option>
														<option value="Master">Master</option>
														<option value="Sir">Sir</option>
														<option value="Lord">Lord</option>
														<option value="Rev">Rev</option>
													</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">First name:</label> 
													<input class="form-control" type="text" id="p_fname">
													<input class="form-control" type="hidden" id="pc_id">
													<input class="form-control" type="hidden" id="pccom_id" value="<?php echo $getCompanyInfo_new->id;?>">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Middle name:</label> 
													<input class="form-control" type="text" id="p_mname">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Last name:</label> 
													<input class="form-control" type="text" id="p_lname">
												</div>
												<div class="form-group col-sm-6 col-xs-12 box dob">
													<label for="Title ">Date of birth:</label>
													<div class="row">
														<div class="col-sm-3">
															<select class="form-control" name="ltd_psc[psc][psc_dob_day]" value ="" id ="psc_date_select" required >
															 	<?php 
																
																foreach($dates as $value){	?>
																	<option value="<?php echo $value;?>" <?php if($per_date == $value){ echo 'selected="selected"';} ?>><?php echo $value; ?></option>
																<?php }?>
															</select>
														   </div>
														   <div class="col-sm-3">
															<select class="form-control"  name="ltd_psc[psc][psc_dob_month]" value ="" id ="psc_month_select" required>
															  <?php foreach($months as $value){?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php }?>
															 </select>
															</div>
															<div class="col-sm-5">
																<select class="form-control" name="ltd_psc[psc][psc_dob_year]" value ="" id ="psc_year_select" required>
																  <?php foreach($year as $value){?>
																		<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																	<?php }?>
															   	</select>
															   </div>
													</div>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Nationality:</label> 
													<select class="form-control" name="ltd_psc[psc][psc_nationality]"  value="" id="psc_nationality" required>
																
																<?php foreach($country as $value){?>
																		<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																	<?php }?>
															</select>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title">Occupation:</label> 
													<input class="form-control" type="text" name="ltd_psc[psc][psc_occupation]" id="psc_occupation" required>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label for="Title ">Country of Residence:</label> 
													<select class="form-control" name="ltd_psc[psc][psc_country_res]" value="" id="psc_country_res" required>
															
																<?php foreach($country as $value){?>
																	<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
																<?php }?>
															</select>
												</div>
												<!-----/Person Director------>
												<!-----Prefill------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
													
													<div class="form-group col-sm-8 col-xs-12 box">
															 
															<select class="form-control" name="ltd_psc[psc][psc_pre_address]" id="select_psc_pre_address" required>
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option>  
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>
															</select>
														</div> 
												</div>
												<!-----/Prefill------>
												<!-----Service Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Service Address</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text"  name="ltd_psc[psc][psc_building_name]" id="psc_building_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_street]" id="psc_building_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_address3]" id="psc_pre_address" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_town]" id="psc_building_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_aounty]" id="psc_building_aounty" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_postcode]" id="psc_building_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_country]" id="psc_building_country" required>
															

														</div>
												<!-----/Service Address------>
												<!-----Residential Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Residential Address</h2>
												</div>
												<div class="col-xs-12 sec_head">
													<p class="text-note">The residential address must be the address where you live.</p>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_name]" id="psc_building_res_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_street]" id="psc_building_res_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_address3]" id="psc_building_res_address3" >
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_town]" id="psc_building_res_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_county]" id="psc_building_res_county" >
														</div> 
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_postcode]" id="psc_building_res_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_psc[psc][psc_building_res_country]" id="psc_building_res_country" required>
														</div>
												<!-----/Residential Address------>
												<!-----/Nature of Control------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Nature of Control</h2>
												</div>
												<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
												     <p class="text-note">What is the nature of the person's control over this company?</p>
													    <div class="col-sm-12 box list-content">
															  <ul>
															    <li><a onClick="showHideFinal('own_share11','own_share12','own_share13','own_share14')">Ownership of shares</a>
																<ul>
																<div id="own_share11" style="display:none">
																	<li><a id="" onClick="showHide('show_id','show_id1','show_id2')">The person holds shares</a>
																		<ul id="show_id" style="display:none">
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 25% but not more than 50% of shares"> More than 25% but not more than 50% of shares</li>
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 50% but not more than 75% of shares"> More than 50% but not more than 75% of shares</li>
																		<li><input type="radio" name="ltd_psc[psc][psc_ownership]" value="More than 75% of shares"> More than 75% of shares</li>
																		</ul>
																	</li>
																	
																	<li><a onClick="showHide('show_id1','show_id','show_id2')">The members of the firm hold shares</a>
																		<div id="show_id1" style="display:none">
																		<p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
																		<ul>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_members]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		</div>
																	</li>
															       
																	<li><a onClick="showHide('show_id2','show_id','show_id1')">The trustees of a trust hold shares</a>
																		<div id="show_id2" style="display:none">
																		<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
																		<ul>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_psc[psc][psc_trustees]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		</div>
																	</li>
																	
																	</div>
																</ul>															      
																</li>
																
															    <li><a onClick="showHideFinal('own_share12','own_share11','own_share13','own_share14')">Ownership of voting rights</a>
															      <ul>
																  <div style="display:none" id="own_share12"> 
															        <li><a onClick="showHide('show_id111','show_id112','show_id113')">The person holds voting rights</a>
															         <ul id="show_id111" style="display:none">
															           <li><input type="radio"  name="ltd_psc[psc][psc_Ownership_voting]" value="More than 25% but not more than 50% of voting rights"> More than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_Ownership_voting]" value="More than 50% but not more than 75% of voting rights"> More than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_Ownership_voting]" value="More than 75% of voting rights"> More than 75% of voting rights</li>
															         </ul>
															       </li>
															       
															       <li><a onClick="showHide('show_id112','show_id111','show_id113')">The members of a firm hold voting rights</a>
																     <div id="show_id112" style="display:none">
																	 <p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
															         <ul>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_members_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															       
															       <li><a onClick="showHide('show_id113','show_id111','show_id112')">The trustees of a trust hold voting rights</a>
															        <div id="show_id113" style="display:none">
																	<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
															         <ul>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_psc[psc][psc_trustees_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															     </div>
																 </ul>															      
															    </li>
															    
															    <li><a onClick="showHideFinal('own_share13','own_share11','own_share12','own_share14')">Right to appoint or remove the majority of the board of directors</a>
															      <div style="display:none" id="own_share13">
																  <ul>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has the right to appoint or remove the majority of the board of directors of the company"> The person has the right to appoint or remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company"> The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_appoint]" value="The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company"> The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company</li>
															     </ul>
																	</div>																 
															    </li>
															    
															    <li><a onClick="showHideFinal('own_share14','own_share11','own_share12','own_share13')">Has significant influence or control</a>
															      <div style="display:none" id="own_share14">
																  <ul>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has significant influence or control over the company"> The person has significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company"> The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_psc[psc][psc_significant]" value="The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company"> The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company</li>
															     </ul>
																</div>
															    </li>
															</ul> 
														</div>
													</div>
												</span>
												</div>
												<!-----/Nature of Control------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success"id="psc_btn">Submit</a>
														<a class="btn btn-success"id = "psc_inc" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!-- Add a Person PSC-->
									<!-- Add a Corporate PSC-->
									<div class="address_edit2">
										<form id ="pscrop_form" style="display:none";>
											<div class="row">
												<!-----Company Name------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Company Name</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
													<label>Company Name:</label> 
													<input class="form-control" type="text">
												</div>
												<!-----/Company Name------>
												<!-----Corporate------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Corporate</h2>
												</div>
												<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Company name:</label> 
															<input class="form-control" type="hidden" name="ltd_dir[psc_corp_directors][psc_corp_select]" id="psc_corp_select" value="0">
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_corp_dir_comp_name]" id="psc_corp_dir_comp_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">First name:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_corp_dir_first_name]" id="psc_corp_dir_first_name" required>
															<input class="form-control" type="hidden" value="" id="pcr_id">
													         <input class="form-control" type="hidden" value="<?php echo $getCompanyInfo_new->id ;?>" id="pcr_comp_id">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Last name:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_corp_dir_last_name]" id="psc_corp_dir_last_name" required>
														</div>
												<!-----/Corporate------>
												<!-----Prefill------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Prefill</h2>
												</div>
												<div class="form-group col-sm-8 col-xs-12 box">
													<label for="Title ">Select an Address:</label> 
													<select class="form-control" name="ltd_dir[psc_corp_directors][psc_corp_directors_pre_address]" id="psc_corp_directors_pre_address">
															<option value="0">----------------------Select-----------------------------</option>
															<option value="1">London - 85 Great Portland Street, First Floor,  London, W1W 7LT</option>
															<option value="2">Edinburgh - 101 Rose Street South Lane, Edinburgh, EH2 JG</option> 
															<option value="3">London - 40 Bloomsbury Way, Lower Ground Floor, London, WC1A 2SE</option>															
															</select>
												</div>
												<!-----/Prefill------>
												<!-----Address------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Address</h2>
												</div>
													<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Building name/number:</label> 
															<input class="form-control" type="hidden" name="ltd_dir[psc_corp_directors][psc_edit_corp_id]" id="psc_edit_corp_id" value="0">
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_name]" id="psc_dir_build_name" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Street:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_street]" id="psc_dir_build_street" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Address 3:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_address]" id="psc_dir_build_address">
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Town:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_town]" id="psc_dir_build_town" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">County:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_county]" id="psc_dir_build_county" > 
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Postcode:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_postcode]" id="psc_dir_build_postcode" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_build_country]" id="psc_dir_build_country" required>
														</div>
												<!-----/Address------>
												<!-----EEA/Non EEA------>
												<div class="col-xs-12 edit_sec_head">
													<h2>EEA/Non EEA</h2>
												</div>
												
												<div class="form-group col-sm-12 col-xs-12 box">
															<label for="Title ">Company Type:</label> 
															<div>
																<input type="radio" name="ltd_dir[psc_corp_directors][psc_dir_type]" id="psc_corp_type1" value="0"> EEA
																<input type="radio" name="ltd_dir[psc_corp_directors][psc_dir_type]" id="psc_corp_type2" value="1"> Non EEA
															</div>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Country Registered:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_corp_country]" id="psc_dir_corp_country" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Registration Number:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_corp_reg_numbr]" id="psc_dir_corp_reg_numbr" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Governing Law:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_corp_low]" id="psc_dir_corp_low" required>
														</div>
														<div class="form-group col-sm-6 col-xs-12 box">
															<label for="Title ">Legal Form:</label> 
															<input class="form-control" type="text" name="ltd_dir[psc_corp_directors][psc_dir_corp_legal]" id="psc_dir_corp_legal" required> 
														</div>
												
												<!-----/EEA/Non EEA------>
												<!-----/Nature of Control------>
												<div class="col-xs-12 edit_sec_head">
													<h2>Nature of Control</h2>
												</div>
												<div class="panel-collapse collapse in" id="collapseOne" aria-expanded="false">
												<span class="form-inline" role="form">
												  <div class="panel-body">
												     <p class="text-note">What is the nature of the person's control over this company?</p>
													    <div class="col-sm-12 box list-content">
															  <ul>
															    <li><a onClick="showHideFinal('own_share','own_share1','own_share2','own_share3')">Ownership of shares</a>
																<ul>
																<div id="own_share" style="display:none">
																	<li><a id="" onClick="showHide('show_id11','show_id12','show_id13')">The person holds shares</a>
																		<ul id="show_id11" style="display:none">
																		<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_ownership]" value="More than 25% but not more than 50% of shares"> More than 25% but not more than 50% of shares</li>
																		<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_ownership]" value="More than 50% but not more than 75% of shares"> More than 50% but not more than 75% of shares</li>
																		<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_ownership]" value="More than 75% of shares"> More than 75% of shares</li>
																		</ul>
																	</li>
																	
																	<li><a onClick="showHide('show_id12','show_id11','show_id13')">The members of the firm hold shares</a>
																		<div id="show_id12" style="display:none">
																		<p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
																		<ul>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		</div>
																	</li>
															       
																	<li><a onClick="showHide('show_id13','show_id11','show_id12')">The trustees of a trust hold shares</a>
																		<div id="show_id13" style="display:none">
																		<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
																		<ul>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees]" value="Hold more than 25% but not more than 50% of shares"> Hold more than 25% but not more than 50% of shares</li>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees]" value="Hold more than 50% but not more than 75% of shares"> Hold more than 50% but not more than 75% of shares</li>
																			<li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees]" value="Hold more than 75% of shares"> Hold more than 75% of shares</li>
																		</ul>
																		</div>
																	</li>
																	
																	</div>
																</ul>															      
																</li>
																
															    <li><a onClick="showHideFinal('own_share1','own_share','own_share2','own_share3')">Ownership of voting rights</a>
															      <ul>
																  <div style="display:none" id="own_share1"> 
															        <li><a onClick="showHide('show_id3','show_id4','show_id5')">The person holds voting rights</a>
															         <ul id="show_id3" style="display:none">
															           <li><input type="radio"  name="ltd_dir[psc_corp_directors][psc_Ownership_voting]" value="More than 25% but not more than 50% of voting rights"> More than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_Ownership_voting]" value="More than 50% but not more than 75% of voting rights"> More than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_Ownership_voting]" value="More than 75% of voting rights"> More than 75% of voting rights</li>
															         </ul>
															       </li>
															       
															       <li><a onClick="showHide('show_id4','show_id3','show_id5')">The members of a firm hold voting rights</a>
																     <div id="show_id4" style="display:none">
																	 <p>The person has right to exercise, or actually exercises, significant influence or control over the activities of a firm that, under that law by which it is governed, is not a legal person, and the members of the firm</p>
															         <ul>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_members_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															       
															       <li><a onClick="showHide('show_id5','show_id3','show_id3')">The trustees of a trust hold voting rights</a>
															        <div id="show_id5" style="display:none">
																	<p>The person has the right to exercise, or actually exercises, significant influence or control over the activities of a trust, and the trustees of that trust</p>
															         <ul>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees_voting]" value="Hold more than 25% but not more than 50% of voting rights"> Hold more than 25% but not more than 50% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees_voting]" value="Hold more than 50% but not more than 75% of voting rights"> Hold more than 50% but not more than 75% of voting rights</li>
															           <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_trustees_voting]" value="Hold more than 75% of voting rights"> Hold more than 75% of voting rights</li>
															         </ul>
																	 </div>
															       </li>
															     </div>
																 </ul>															      
															    </li>
															    
															    <li><a onClick="showHideFinal('own_share2','own_share3','own_share1','own_share')">Right to appoint or remove the majority of the board of directors</a>
															      <div style="display:none" id="own_share2">
																  <ul>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_appoint]" value="The person has the right to appoint or remove the majority of the board of directors of the company"> The person has the right to appoint or remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_appoint]" value="The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company"> The person has control over the firm and members of that firm (in their capacity as such) hold the right to appoint of remove the majority of the board of directors of the company</li>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_appoint]" value="The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company"> The person has control over the truste and the trustees of that trust (in their capacity as such) hold the right to appoint or remove the majority of the board of directors of the company</li>
															     </ul>
																	</div>																 
															    </li>
															    
															    <li><a onClick="showHideFinal('own_share3','own_share2','own_share1','own_share')">Has significant influence or control</a>
															      <div style="display:none" id="own_share3">
																  <ul>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_significant]" value="The person has significant influence or control over the company"> The person has significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_significant]" value="The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company"> The person has control over the firm and members of that firm (in their capacity as such) have significant influence or control over the company</li>
															        <li><input type="radio" name="ltd_dir[psc_corp_directors][psc_significant]" value="The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company"> The person has control over the trust and the trustees of that trust (in their capacity as such) have significant influence or control over the company</li>
															     </ul>
																</div>
															    </li>
															</ul> 
														</div>
													</div>
												</span>
												</div>
												
												<!-----/Nature of Control------>
												<div class="col-xs-12 text-right form-actn-btn">
													<div class="text-right">
														<a class="btn btn-danger">Cancel</a>
														<a class="btn btn-success" id="pscrop_btn">Submit</a>
														<a class="btn btn-success" id="pscrop_inc" style="display:none;">Submit</a>
													</div>
												</div>
											</div>
										</form>
									</div>
									<!-- Add a Corporate PSC-->
								</div>
								<!----/PSC----->
								<div class="tab-pane fade" id="Documents_company">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
								<ul class="nav navbar-nav">
									<!--li class="active"><a href="#" class="user-profile">All 3 Selected</a></li-->
									<!--li class=""><a href="#" class="user-profile">Mark as Read</a></li>
									<li class=""><a href="#" class="user-profile">Delete</a></li-->
								</ul>
								<!--span class="exit"><a href="#"><i class="fa fa-times"></i></a></span-->
							</div>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										<!--th class="column-title check-box-tab">
											<form class="">
												<div class="">
													<input type="checkbox" class="check_box" value="">
												</div>
											</form>
										</th-->
										<th class="column-title">Date<b class="caret caret-custom"></b></th>
										<th class="column-title">Time <b class="caret caret-custom"></b></th>
										<th class="column-title">Type <b class="caret caret-custom"></b></th>
										<th class="column-title">File Name <b class="caret caret-custom"></b></th>
										<th class="column-title">Download <b class="caret caret-custom"></b></th>
										<th width="100" class="column-title Delivery_Status text-center">Delete <b class="caret caret-custom"></b></th>
									</tr> 
								</thead>
								<tbody class="document_print1">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!---------/Documents--------->
							</div>
						</div>
					</div>
				</div>
				<!--------/My Company--------->
				<!-----Address_Services------->
				<div class="tab-pane fade" id="Address_Services">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="overview-box">
								<h3>Address Services</h3>
								<div class="address_section">
									<h4>Registered Office <span><a href="#"><i class="fa fa-pencil"></i></a></span></h4>
									<p>85 Great Portland Street, First Floor, London, W1W 7LT</p>
									<div class="gap"></div>
									<h4>Director Service Address <span><a href="#"><i class="fa fa-pencil"></i></a></span></h4>
									<p>85 Great Portland Street, First Floor, London, W1W 7LT</p>
									<div class="gap"></div>
									<h4>Virtual Business Address <span><a href="#"><i class="fa fa-pencil"></i></a></span></h4>
									<p>You do not have this service. Add it to your account - <a href="#" class="click_here">click here</a></p>
									<div class="gap"></div>
									<p>To update your address services select the edit icon next to the address requiring to be changed.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-----/Address_Services------>
				<!--------Documents----------->
				<div class="tab-pane fade" id="Documents">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
								<ul class="nav navbar-nav">
									
								</ul>
							</div>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										<th class="column-title">Date<b class="caret caret-custom"></b></th>
										<th class="column-title">Time <b class="caret caret-custom"></b></th>
										<th class="column-title">Type <b class="caret caret-custom"></b></th>
										<th class="column-title">File Name <b class="caret caret-custom"></b></th>
										<th class="column-title text-center">Download <b class="caret caret-custom"></b></th>
										<th width="100" class="column-title Delivery_Status text-center">Delete <b class="caret caret-custom"></b></th>
									</tr>
								</thead>
								<tbody class="document_print">
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!---------/Documents--------->
				<!----------History----------->
				<div class="tab-pane fade" id="history">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="sub_nav">
								<ul class="nav navbar-nav">
									
								</ul>
							</div>
							<table class="table table-striped jambo_table bulk_action tbl-bg-email">
								<thead>
									<tr class="headings">
										<th class="column-title check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box" value="">
												</div>
											</form>
										</th>
										<th class="column-title">Description <b class="caret caret-custom"></b></th>
										<th class="column-title">Action <b class="caret caret-custom"></b></th>
										<th class="column-title">Date Added <b class="caret caret-custom"></b></th>
										<th class="column-title">Time Added <b class="caret caret-custom"></b></th>
										<th class="column-title Delivery_Status">View <b class="caret caret-custom"></b></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
									<tr>
										<td class="check-box-tab">
											<form class="">
												<div class="checkbox">
													<input type="checkbox" class="check_box1" value="">
												</div>
											</form>
										</td>
										<td class="">The Registered Office</td>
										<td class="">-</td>
										<td class="">15-02-2018</td>
										<td class="">00:00 AM</td>
										<td class="Delivery_Status">-</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!----------/History---------->
				<!------Partner Services------>
				<div class="tab-pane fade" id="partner_serv">
					<div class="col-xs-12">
						<div class="overview-box1 payment_head">
							<h3>Partner Services</h3>
							<div class="payment_box_outer">
								<div class="payment_box">
									<div class="col-xs-3 pay_img_sec">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/pay_3.png">
									</div>
									<div class="col-xs-6 pay_cont_sec">
										<div class="row">
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
										</div>
									</div>
									<div class="col-xs-3 pay_info_sec">
										<button type="submit" class="btn btn-success">Continue</button>
									</div>
								</div>
								<div class="payment_box">
									<div class="col-xs-3 pay_img_sec">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/pay_2.png">
									</div>
									<div class="col-xs-6 pay_cont_sec">
										<div class="row">
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
										</div>
									</div>
									<div class="col-xs-3 pay_info_sec">
										<p><a href="#">Upgrade </a>to Bussiness us londonformation.com</p>
									</div>
								</div>
								<div class="payment_box">
									<div class="col-xs-3 pay_img_sec">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/pay_1.png">
									</div>
									<div class="col-xs-6 pay_cont_sec">
										<div class="row">
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
											<div class="col-xs-6">
												<p><i class="fa fa-plus-circle"></i> Lorem Ipsum is simply dummy text</p>
												<p><i class="fa fa-usd"></i> Lorem Ipsum</p>
												<p><i class="fa fa-pie-chart"></i> Printing and typesetting industry</p>
												<p><i class="fa fa-university"></i> Standard dummy text ever</p>
											</div>
										</div>
									</div>
									<div class="col-xs-3 pay_info_sec">
										<p><a href="#">Upgrade </a>to Bussiness us londonformation.com</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
						<!------/Partner Services------>
						
						<!--------Company data---------->
				<div class="tab-pane fade" id="companydata">
					<div class="overview_section">
						<div class="col-sm-12">
							<div class="overview-box22 tab-content">
								<!----Overview----->
								<div id="" class="address_section tab-pane fade in active">
									<div class="row row_edit">
									<div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3><?php echo $getCompanyInfo_new->company_name;?></h3>
													<span style="color:black;"></span>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Street</th>
														<th class="">Address1</th>
														<th class="">Town</th>
														<th class="">Country</th>
														<th class="">Postcode</th>
													</tr>
												</thead>
												<tbody class="">
												<!--?php foreach($comp_RegisterOffice as $resg){
												//var_dump($comp_RegisterOffice);
												?-->
												
													<tr class="">
														<td class=""><?php echo $comp_overview->address_line1; ?></td>
														<td class=""><?php echo $comp_overview->address_line2; ?></td>
														<td class=""><?php echo $comp_overview->locality;?></td>
														<td class=""><?php echo $comp_overview->country; ?></td>
														<td class=""><?php echo $comp_overview->post_town; ?></td>
													</tr>
													<?php //}?>
												</tbody>
											</table>
											</div>
											
										</div>
										
										<div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3>Directors</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														<th class="" width="60px">View</th>
													</tr>
												</thead>
												<tbody class="">
												<?php if($getcompany_director){?>
												<?php foreach($getcompany_director as $dir){
												if($dir->api==1)
												{
												?>
													<tr class="dir_<?php echo $dir->id;?>">
														<td class=""><?php echo $dir->first_name;?></td>
														<td class=""><?php echo $dir->last_name;?></td>
														<td class=""><a class="btn btn-success" href="#" onclick="Myxmldirector('<?php echo $dir->first_name;?>','<?php echo $dir->last_name;?>','<?php echo $dir->nationality;?>','<?php echo $dir->occupation;?>','<?php echo $dir->res_building_country;?>','<?php echo $dir->res_building_postcode;?>','<?php echo $dir->res_building_county;?>','<?php echo $dir->appoint_date;?>','<?php echo $dir->building_street;?>','<?php echo $dir->building_address;?>');">View</a></td>
														
													</tr>
													<?php }}?>
													<?php }?>
													</tbody>
											</table>
											</div>
										</div>
										
										<!--div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3>Confirmation Statement</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Last Confirmation Statement </th>
														<th class="">Next Confirmation Statement </th>
													</tr>
												</thead>
												<tbody class="">
													<tr class="">
													<?php 
													if(date ("d-m-Y",strtotime($comp_overview->next_due_date))=='01-01-1970')
														{
															$next_due_date = "No previous filing";
														}
														else
														{
															$next_due_date = date ("d-m-Y",strtotime($comp_overview->next_due_date));
														}
														if(date ("d-m-Y",strtotime($comp_overview->next__due_return_dated))=='01-01-1970')
														{
															$next__due_return_dated = "No Next filing";
														}
														else
														{
															$next__due_return_dated = date ("d-m-Y",strtotime($comp_overview->next__due_return_dated));
														}
														?>
													<td><?php echo $next_due_date;?></td>
													<td><?php echo $next__due_return_dated;?></td>
													</tr>
												</tbody>
											</table>
											</div>
										</div-->
										
										
										<!--div class="col-sm-6">
											<div class="overview-box1">
												<h3>Share Capital</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Issues</th>
														<th class="">Currency</th>
														<th class="">Type</th>
													</tr>
												</thead>
												<tbody class="">
												<?php foreach($getShareholder as $share){?>
												<?php if($share->api==1){?>
												
													<tr class="">
														<td class=""><?php echo $share->per_person_shares;?></td>
														<td class=""><?php echo $share->currency;?></td>
														<td class=""><?php echo $share->share_class;?></td>
														
													
													</tr>
													<?php }} ?>
												</tbody>
											</table>
											</div>
										</div-->
										<div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3>Secretaries</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														<th class="" width="60px">View</th>
														
													</tr>
												</thead>
												<tbody class="">
													<?php if($getcompany_secretaries){?>
													<?php foreach($getcompany_secretaries as $sectr){
													if($sectr->api !=0){
													?>
													
													<tr class="sectr_<?php echo $sectr->id;?>">
													<td class=""><?php echo $sectr->first_name;?></td>
													<td class=""><?php echo $sectr->last_name;?></td>
													<td class=""><a class="btn btn-success" href="#" onclick="Myxmlsec('<?php echo $sectr->first_name;?>','<?php echo $sectr->last_name;?>','<?php echo $sectr->nationality;?>','<?php echo $sectr->occupation;?>','<?php echo $sectr->res_building_country;?>','<?php echo $sectr->res_building_postcode;?>','<?php echo $sectr->res_building_county;?>','<?php echo $sectr->appoint_date;?>','<?php echo $sectr->building_street;?>','<?php echo $sectr->building_address;?>');">View</a></td>

													</tr>
													<?php }}?>
													<?php }?>
												</tbody>
											</table>
											</div>
										</div>
										
										<!--div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3>Accounts</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">Accounts Made Up Date</th>
														<th class="">Next Accounts Due</th>
													</tr>
												</thead>
												<tbody class="">
													
												
												
													<tr class="imp">
													<tr class="imp">
														<?php 
														if(date ("d-m-Y",strtotime($comp_overview->last_made_update))=='01-01-1970')
														{
															$last_made_update = "No previous filing";
														}
														else
														{
															$last_made_update = date ("d-m-Y",strtotime($comp_overview->last_made_update));
														}
														if(date ("d-m-Y",strtotime($comp_overview->account_next_due))=='01-01-1970')
														{
															$account_next_due = "No Next filing";
														}
														else
														{
															$account_next_due = date ("d-m-Y",strtotime($comp_overview->account_next_due));
														}
														?>
														<td class="next_<?php echo $imp->id;?>"><?php echo $last_made_update;?></td>
														<td class="next_<?php echo $imp->id;?>"><?php echo $account_next_due;?></td>
													
													</tr>
													
												</tbody>
											</table>
											</div>
										</div-->
										
										<div class="col-sm-6 custom_col">
											<div class="overview-box1">
												<h3>Persons of Control (PSC)</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First Name</th>
														<th class="">Last Name</th>
														<th class="" width="60px">View</th>
													</tr>
												</thead>
												<tbody class="">
													<?php if($company_psc){?>
													<?php foreach($company_psc as $psc){
												     if($psc->api==1)
											         {
													?>
														<tr class="psc_<?php echo $psc->id;?>">
															<td class=""><?php echo $psc->first_name;?></td>
															<td class=""><?php echo $psc->last_name;?></td>
															<td class=""><a class="btn btn-success" href="#" onclick="Myxmlpsc('<?php echo $psc->first_name;?>','<?php echo $psc->last_name;?>','<?php echo $psc->nationality;?>','<?php echo $psc->occupation;?>','<?php echo $psc->res_building_country;?>','<?php echo $psc->res_building_postcode;?>','<?php echo $psc->res_building_county;?>','<?php echo $psc->appoint_date;?>','<?php echo $psc->building_street;?>','<?php echo $psc->building_address;?>','<?php echo $psc->legal_authority;?>','<?php echo $psc->legal_form;?>');">View</a></td>
														</tr>
													<?php }}?>
													<?php }?>
												</tbody>
											</table>
											</div>
										</div>
										
										
										
											<!--div class="col-sm-6">
											<div class="overview-box1">
												<h3>Shareholders</h3>
												<table class="table table-striped jambo_table">
												<thead>
													<tr class="">
														<th class="">First name</th>
														<th class="">Last Name</th>
														<th class="">Shares</th>
													</tr>
												</thead>
												<tbody class="">
												<?php if($getShareholder){?>
												<?php foreach($getShareholder as $share){
												if($share->api!=1){
												?>

													<tr class="">
														<td class=""><?php echo $share->name;?></td>
														<td class=""><?php echo $share->last_name;?></td>
														<td class=""><?php echo $share->shares;?></td>
														
													</tr>
													<?php }} }?>
													<?php if($GetcropShareholder){?>
													<?php foreach($GetcropShareholder as $sharecop){?>
													<tr class="">
														<td class=""><?php echo $sharecop->first_name;?></td>
														<td class=""><?php echo $sharecop->last_name;?></td>
														<td class=""><?php echo $sharecop->shares;?></td>
														<td class=""><input type="button" name="edit" value="edit" onclick="MyShare('<?php echo $sharecop->id;?>','corp_share' ,'<?php echo $getCompanyInfo_new->company_name;?>');"></td>
													</tr>
													<?php }} ?>
												</tbody>
											</table>
											</div>
											
										</div!-->
								</div>
							</div>
						</div>
				</div>
		</div>
</div>
						
				<!--------end Company data---------->		
						
			</div>
			</section>
<?php $this->load->view ('footer');?>
<?php $this->load->view ('modal');?>