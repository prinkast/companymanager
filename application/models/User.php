<?php
Class User extends CI_Model
{
 /*function login($username, $password)
 {
   $this -> db -> select('*');
   $this -> db -> from('tbl_user');
   $this -> db -> where('email', $username);
   $this -> db -> where('password', MD5($password));
   $this -> db -> limit(1);
 
   $query = $this->db->get();
 //echo $this->db->last_query();die;
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }*/
 
	public function login($username, $password){
		$this -> db -> select('*');
		$this -> db -> from('tbl_user');
		$this -> db -> where('email', $username);
		$this -> db -> where('password', MD5($password));
		//$this -> db -> where('Access','1');
		$query = $this->db->get();
		return $query->row_array();
	}
 /*  function login($username, $password)
 { 
   $this -> db -> select('*');
   $this -> db -> from('tbl_user');
   $this -> db -> where('email', $username);
   $this -> db -> where('password', MD5($password));
   $this -> db -> limit(1);
 
   $query = $this->db->get();
 //echo $this->db->last_query();die;
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   elseif($query -> num_rows() != 1)
   {
	   $this -> db -> select('*');
	   $this -> db -> from('tbl_user');
	   $this -> db -> where('email', $username);
	   $this -> db -> where('master_password', MD5($password));
	   $this -> db -> limit(1);
	 
	   $query = $this->db->get();
	     if($query -> num_rows() == 1)
		   {
			 return $query->result();
		   }
		   else
		   {
			return false;
		   }
   }
   else
   {
     return false;
   }
 } */
 
 
function getStatusOptions($id = null)
 {
 	$list = array("Active","Suspend","Cancelled");
 	if ($id == null )	return $list;
 
 	if ( is_numeric( $id )) return $list [ $id ];
 	return $id;
 }
  function getCompanyinfo($comp_id)
 {
 	$this->db->select ( '*' );
		$this->db->from ( 'tbl_company' );
		$this->db->where ( 'tbl_company.id', $comp_id );
		$query_company = $this->db->get ();
		$detail_company = $query_company->row ();
		return $detail_company;
 }
 function getCompany($comp_id,$data)
 {
 
 /*------------company info--------------*/
 		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company' );
		$this->db->where ( 'tbl_company.id', $comp_id );
		$query_company = $this->db->get ();
		$detail_company = $query_company->row ();
 
 	$list = array();
	if($detail_company->company_name != $data['user_company'])
		array_push($list, "Company name updated");
		
	if($detail_company->trading != $data['trade_name'])
		array_push($list, "Trade name updated");
		
	if($detail_company->director1 != $data['director1'])
		array_push($list, "Director1 updated");
		
	if($detail_company->director2 != $data['director2'])
		array_push($list, "Director2 updated");
		
	if($detail_company->director3 != $data['director3'])
		array_push($list, "Director3 updated");
		
	if($detail_company->director4 != $data['director4'])
		array_push($list, "Director4 updated");
	if($detail_company->location != $data['location'])
		array_push($list, "Location updated");
		
	/*if($data['renewal'])
	{
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_order' );
		$this->db->where ( 'tbl_order.company_id', $comp_id );
		$query_company2 = $this->db->get ();
		$detail_company2 = $query_company2->row ();
		if($detail_company2->renewable_date != $data['renewal'])
		array_push($list, "Renewal date updated");
	}*/

 	return $list;
 }
 
 function getUser($user_id,$data,$mailling_id){

 /*=================mailling address======================*/ 
  
  $mailling =  $this->search->mailling($mailling_id);

 /*=================mailling address======================*/
 
		$this->db->select ( '*' );
		$this->db->from('tbl_user');
		$this->db->where('tbl_user.id', $user_id );
		$query_company = $this->db->get();
		$detail_user = $query_company->row();
		
		
		$list1 = array();
		if($detail_user->first_name != $data['user_name'])
			array_push($list1, "First Name updated");
		if($detail_user->last_name != $data['user_last_name'])
			array_push($list1, "Last Name updated");
		if($detail_user->email != $data['user_email'])
			array_push($list1, "Email updated");
		if($detail_user->ph_no != $data['user_daytime_contact'])
			array_push($list1, "Daytime Phone updated");
		if($detail_user->mobile != $data['user_contact'])
			array_push($list1, "Mobile Number updated");
		if($detail_user->skpe_address != $data['user_skype'])
			array_push($list1, "Skype Address updated");
		if($mailling->street != $data['user_street'])
			array_push($list1, "Street updated");
		if($mailling->city != $data['user_city'])
			array_push($list1, "City updated");
		if($mailling->postcode != $data['user_post_code'])
			array_push($list1, "Postcode updated");
					
		if($mailling->county != $data['user_county'])
			array_push($list1, "Country updated");
		if($mailling->country != $data['user_country'])
			array_push($list1, "County/Region updated");
		
	return $list1;
 }
 function getBilling($billing_id,$data){

/*=================billing_id address======================*/ 
   $billing =  $this->search->billing($billing_id);

 /*=================billing_id address======================*/

		$list1 = array();
		if($billing->billing_name != $data['billing_name'])
			array_push($list1, "Billing Name updated");
		if($billing->street != $data['billing_street'])
			array_push($list1, "Billing Street updated");
		if($billing->city != $data['billing_city'])
			array_push($list1, "Billing City updated");
		if($billing->country != $data['billing_country'])
			array_push($list1, "Billing Country updated");
		if($billing->postcode != $data['billing_post_code'])
			array_push($list1, "Billing Postcode updated");
		if($billing->county != $data['billing_county'])
			array_push($list1, "Billing County updated");	
		
	return $list1;
 }
  function getUserbyyear(){
	     $this->db->select ( '*' );
		$this->db->from('tbl_user');
		$query = $this->db->get();
		return $query->result();
  }
  function getCompaniesbyyear(){
	     $this->db->select ( '*' );
		$this->db->from('tbl_company');
		$query = $this->db->get();
		return $query->result();
  }
}