<?php
class home_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
	}


public $select = "tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name,tbl_company.trading as trading,tbl_company.comp_info,tbl_company_formation_details.company_name as form,tbl_company_formation_details.incorporation_date,tbl_company_formation_details.next_due_date,tbl_company_formation_details.next__due_return_dated,tbl_company_formation_details.company_status,tbl_company_formation_details.post_town,tbl_company_formation_details.company_number,tbl_user.email,tbl_user.Access";
	#admin countCompany
	public function countCompany($count=null,$limit= null,$offset=null,$exportcondition=null){
		$this->db->cache_on();
		$this->db->select ($this->select);
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
	}

	/* Count New Ordersdata By Month,day,Week*/

/* admin..............*/
public function count_NewOdersCompanies_new($count=null,$order,$limit= null,$offset=null,$exportcondition=null) {
	$this->db->cache_on();
		$this->db->select ('tbl_order.*,tbl_order.renewable_date as renewable_date,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.create_time", "DESC" );
		$this->db->where ( 'tbl_reseller.state_id !=1');
        if($order=='month')
		 {
			$this->db->where( 'MONTH(tbl_company.create_time) = MONTH(CURRENT_DATE()) AND Year(tbl_company.create_time) = year(CURRENT_DATE())');
		 }
		 else if($order=='today')
		 {
		
			$currenttime= date('Y-m-d');;
			$this->db->where('tbl_company.create_time > ',$currenttime." 00:00:00");
			$this->db->where('tbl_company.create_time <= ',$currenttime." 23:59:59");
		 }
		 else if($order=='all')
		 {
			
		 }
		 else if($order=='week')
		 {
			$current_date = date('Y-m-d');
				$week = date('W', strtotime($current_date));
				$year = date('Y', strtotime($current_date));
				$dto = new DateTime();
				$result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
				$result['1'] = $dto->setISODate($year, $week, 2)->format('Y-m-d');
				$result['2'] = $dto->setISODate($year, $week, 3)->format('Y-m-d');
				$result['3'] = $dto->setISODate($year, $week, 4)->format('Y-m-d');
				$result['4'] = $dto->setISODate($year, $week, 5)->format('Y-m-d');
				$result['5'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
				$result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
				$this->db->where('tbl_company.create_time > ',$result['start']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['start']." 23:59:59");
				$this->db->or_where('tbl_company.create_time > ',$result['1']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['1']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['2']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['2']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['3']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['3']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['4']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['4']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['5']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['5']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['end']." 00:00:00");
			    $this->db->where('tbl_company.create_time <= ',$result['end']." 23:59:59");
		 }	
		 if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
		
}

 public function count_state_change_new($count=null,$statechange=null,$limit= null,$offset=null,$exportcondition=null) { 
 	//print_r($statechange); die;
 	$this->db->select ($this->select);
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );

 	if ($statechange != '' ||	$statechange == 'Show All') {
		
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where_in ( 'tbl_order.state_id', $statechange );
	}  else {
	
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	} 
	if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
 }
/* admin */
 public function countNewFormStatus_new($count=null,$limit= null,$offset=null,$exportcondition=null) { 
		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ('tbl_order.comp_ltd = 1');
		$this->db->where ( 'tbl_order.state_id = 6 OR tbl_order.state_id = 9');
		if($count =='count'){
			$this->db->cache_off();

			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
 }

 public function count_companies($arra) {
	$this->db->cache_on();
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2){
			$resellers = $this->search->resellerUser($arra['user_id']);
			foreach($resellers as $reseller){
					$reseller_ids[] = $reseller->id;
			}
			$resellers1 = $this->search->resellerUser_child($reseller_ids[0]);
			foreach($resellers1 as $reseller12){
				$reseller_ids1[] = $reseller12->parent_reseller;					
			}
		}
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0){
			$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company_formation_details.company_name as form');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1');
				if ($arra['role_id'] == 0){
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
				}elseif ($arra['role_id'] == 2 && $reseller_ids1[0]!= NULL){
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
					$this->db->or_where_in('tbl_reseller.parent_reseller', $reseller_ids1);
				}elseif ($arra['role_id'] == 2 && $reseller_ids1[0]== NULL){
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				}
				$this->db->cache_off();
			return $query = $this->db->get('tbl_order')->num_rows();;
		}else{
			return  0;
		}
}


 public function count_companies_newformation($count=null,$limit= null,$offset=null,$exportcondition=null) {
		$this->db->select ($this->select);
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'MONTH(tbl_order.renewable_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renewable_date) = year(CURRENT_DATE())');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
		}
}

public function company_register_count_companies_new($count=null,$limit= null,$offset=null,$exportcondition=null) {
		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->where ( 'tbl_order.company_register = "Yes" ');
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
}
public function hosting_count_companies_new($count=null,$limit= null,$offset=null,$exportcondition=null) {
		
		$this->db->cache_on();
		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}		
}

public function count_document_service_companies_new($count=null,$limit= null,$offset=null,$exportcondition=null) {
	    $this->db->cache_on();
		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
}


public function show_companysearch_records($limit,$start,$arra,$exportcondition=null)
{
		//var_dump($arra['new_search_bar']);die();
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_company_formation_details.company_name as form' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["accountreturnsfrom"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["accountreturnsto"]  ) );
 		if($arra['new_search_bar']!= "" && $arra['accountreturnsto']=="" && $arra["accountreturnsto"]=="" && $arra['search_new_all']=="show_all")
		{
			$this->db->like('tbl_company.company_name', $arra['new_search_bar']);
		}
		
		/* if($arra['search_new_all'] != "show_all"){
			$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		} */
		/* echo $this->db->last_query();die("Bgfd"); */
		else if($arra['search_new_all'] == "1")
		{
			if($arra['new_search_bar']=="")
			{ 
			//var_dump($arra);die("if1");
			$this->db->where ( 'tbl_company_formation_details.next_due_date >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next_due_date <=', $to );
			}
			else
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next_due_date <=', $to );
		    $this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		}
		else if($arra['search_new_all'] == "0")
		{
		if($arra['new_search_bar']=="")
			{
			//var_dump($arra);die("if1");
			$this->db->where ( 'tbl_company_formation_details.next_due_date >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next_due_date <=', $to );
			}
			else
			{
			$this->db->where ( 'tbl_company_formation_details.next__due_return_dated >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next__due_return_dated <=', $to );
		    $this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		}
		else if($arra['search_new_all'] == 'show_all')
		{
			if($arra['new_search_bar']=="")
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >="'.$from.'" AND tbl_company_formation_details.next_due_date <= "'.$to.'"');
		    $this->db->or_where ( 'tbl_company_formation_details.next__due_return_dated >="'.$from.'" AND tbl_company_formation_details.next__due_return_dated <= "'.$to.'"');
			}
			else
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >="'.$from.'" AND tbl_company_formation_details.next_due_date <= "'.$to.'"');
		    $this->db->or_where ( 'tbl_company_formation_details.next__due_return_dated >="'.$from.'" AND tbl_company_formation_details.next__due_return_dated <= "'.$to.'"');
		    $this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		
 		}
		if ($arra['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		} elseif ($arra['role_id'] == 2) {
			if ($resellers){
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
			}
		}
		$query = $this->db->get ();
	    //echo $this->db->last_query();die();
		return $result = $query->result();
}
public function count_companysearch_records($arra) {

		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_company_formation_details.company_name as form' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->where ( 'tbl_order.state_id !=4' ); 
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["accountreturnsfrom"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["accountreturnsto"]  ) );
		//var_dump($from);die();
		if($arra['new_search_bar']!= "" && $arra['accountreturnsto']=="" && $arra["accountreturnsto"]=="" && $arra['search_new_all']=="show_all")
		{
			$this->db->like('tbl_company.company_name', $arra['new_search_bar']);
		}
		else if($arra['search_new_all'] == "1")
		{
			if($arra['new_search_bar']== "")
			{
			//var_dump($arra);die("if1");
			$this->db->where ( 'tbl_company_formation_details.next_due_date >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next_due_date <=', $to );
			}
			else
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next_due_date <=', $to );
		    $this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		}
		else if($arra['search_new_all'] == "0")
		{
		if($arra['new_search_bar']== "")
			{
			//var_dump($arra);die("if1");
			$this->db->where ( 'tbl_company_formation_details.next__due_return_dated >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next__due_return_dated <=', $to );
			}
			else
			{
			$this->db->where ( 'tbl_company_formation_details.next__due_return_dated >=', $from );
		    $this->db->where ( 'tbl_company_formation_details.next__due_return_dated <=', $to );
		    $this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		}
		else if($arra['search_new_all'] == 'show_all' )
		{
			if($arra['new_search_bar']== "")
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >="'.$from.'" AND tbl_company_formation_details.next_due_date <= "'.$to.'"');
			$this->db->or_where ( 'tbl_company_formation_details.next__due_return_dated >="'.$from.'" AND tbl_company_formation_details.next__due_return_dated <= "'.$to.'"');
			}

			else
			{
			$this->db->where ( 'tbl_company_formation_details.next_due_date >="'.$from.'" AND tbl_company_formation_details.next_due_date <= "'.$to.'"');
			$this->db->or_where ( 'tbl_company_formation_details.next__due_return_dated >="'.$from.'" AND tbl_company_formation_details.next__due_return_dated <= "'.$to.'"');
			$this->db->like ( 'tbl_company.company_name',$arra['new_search_bar']);
			}
		}
		if ($arra['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		} elseif ($arra['role_id'] == 2) {
			if ($resellers){
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
			}
		}
		$query = $this->db->get ();
 		$num = $query->num_rows();
		return  $num ;
}



/*---show all state_change--*/
public function show_state_change_company_register($limit=null,$start=null,$arr,$exportcondition=null) { 
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
     
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.company_register', 'Yes');
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where_in ( 'tbl_order.company_register_status', $arr['state_change'] );
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_order.company_register', 'Yes');
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		
		
		
		
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); die("HGF");
	return $user_orders = $query->result ();

  }
  public function count_state_change_company_register($arr) {
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
    //var_dump($arr['state_change']);die(0);
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.company_register', 'Yes');
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where_in ( 'tbl_order.company_register_status', $arr['state_change'] );
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_order.company_register', 'Yes');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	$query = $this->db->get ();
 		$num = $query->num_rows();
		return  $num ;

  }
  
  
  /*---show all state_change--*/
public function show_state_change_document_service($limit=null,$start=null,$arr,$exportcondition=null) { 
//var_dump($arr);die();
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
     //var_dump($arr['state_change']);die();
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		$this->db->where_in ( 'tbl_order.document_service_status', $arr['state_change'] );
		
		
		
		
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); die("HGF");
	return $user_orders = $query->result ();

  }
  public function count_state_change_document_service($arr) {
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
    //var_dump($arr['state_change']);die(0);
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		
		$this->db->where_in ( 'tbl_order.document_service_status', $arr['state_change'] );
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	$query = $this->db->get ();
 		$num = $query->num_rows();
		return  $num ;

  }
  
    /*---show all state_change--*/
public function show_state_change_hosting($limit=null,$start=null,$arr,$exportcondition=null) { 
//var_dump($arr);die();
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
     //var_dump($arr['state_change']);die();
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		$this->db->where_in ( 'tbl_order.state_id', $arr['state_change'] );
		
		
		
		
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
	    $this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); die("HGF");
	return $user_orders = $query->result ();

  }
  public function count_state_change_hosting($arr) {
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['state_change']<> "" && $arr['state_change'] <> "Show All") {
    //var_dump($arr['state_change']);die(0);
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		
		$this->db->where_in ( 'tbl_order.state_id', $arr['state_change'] );
		//$this->db->where ( 'tbl_company.company_name', $string );
	}else{
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		//$this->db->where ( 'tbl_order.state_id', $arr['state_change'] );
	} /**/
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	$query = $this->db->get ();
 		$num = $query->num_rows();
		return  $num ;

  }
  function allusercompanies_model($count=null,$userid,$limit= null,$offset=null,$exportcondition=null)
  {	
		$this->db->cache_on();
	    $this->db->select ( $this->select );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where('tbl_order.create_user_id',$userid);
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows(); 
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
  }
  
  function countallusercompanies_model($arr)
  {	
	    $this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from("tbl_order");
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where('tbl_order.create_user_id',$arr['userid']);
		$query=$this->db->get();
		$num = $query->num_rows();
		return $num;
  }
  /* CustomEmail function used for user email , alternative email*/
  public function CustomEmails($table,$arr,$fun)
  {
  	
  	$query = $this->db->get_where($table,$arr);
			return $query->$fun();
		
  }
  // function altemails($notification)
  // {
		// $this->db->select ( '*' );
		// $this->db->from ( 'tbl_alt_email' );
		// $this->db->where ( "tbl_alt_email.create_user_id", $notification);
		// $query = $this->db->get ();
		// $notification = $query->row ();
		// return $notification;
  // }
  // function user_emails($email) 
  // {
	 //  //var_dump($email);
	 //    $this->db->select ( '*' );
		// $this->db->from ( 'tbl_user' );
		// $this->db->where ( "tbl_user.id", $email);
		// $query = $this->db->get ();
		// $email = $query->row ();
		// return $email;
  // }
  public function user_mail_check($alt_email){
	$this->db->select ( '*' );
	$this->db->from ( 'tbl_user');
	$search_string = "(tbl_user.email LIKE '".$alt_email."')";
	$this->db->where ( $search_string );
	$query_email = $this->db->get ();
	return $emails = $query_email->row();
	}
	public function get_useremail_data($user_id){
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_alt_email');
		$this->db->where('create_user_id', $user_id);
		$query_order = $this->db->get();
		return $emails = $query_order->row();
	}
	public function update_altemail($user_id,$data_email){
		$this->db->where ( 'create_user_id', $user_id);
		return $this->db->update ( 'tbl_alt_email', $data_email );	
	}
	public function add_altemail($data_email){
		return $this->db->insert('tbl_alt_email', $data_email);
	}
	public function remove_email($arr){
		//var_dump($arr);die();
		$this->db->where('create_user_id',$arr['create_user_id']);
		unset($arr['email']);
		unset($arr['field']);
		unset($arr['id']);
		$this->db->update('tbl_alt_email',$arr);
		echo $this->db->last_query();die();
	}
	public function getuserdata($id){
		$this->db->select('first_name,last_name,email');
		$this->db->from('tbl_user');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}
	public function updateEmail($array){
			if($array['number'] == "email_2"){
				$array['email_2'] = $array['email_primary'];
			}else if($array['number'] == "email_3"){
				$array['email_3'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_4'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_5'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_6'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_7'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_8'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_9'] = $array['email_primary'];
			}else if($array['number'] == "email_4"){
				$array['email_`0'] = $array['email_primary'];
			}
		unset($array['email_primary']);
		unset($array['email']);
		unset($array['number']);
		unset($array['company_id']);
	
		$this->db->where('create_user_id',$array['create_user_id']);
		
		return $this->db->update('tbl_alt_email',$array);
	}
	public function invoice_model($arr)
	{
		//var_dump($arr);die();
		return $this->db->insert('tbl_invoice', $arr);
		
	}
	public function get_invoice_model($company_id)
	{
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_invoice' );
		$this->db->where ( "tbl_invoice.company_id", $company_id);
		$this->db->where ( "tbl_invoice.type_id", "51");
		$query_tele = $this->db->get();
		//echo $this->db->last_query(); die("HGF");
	    return $query_tele->result();	
	}
	public function invoice_reseller_model($arr)
	{
		//var_dump($arr);die();
		return $this->db->insert('tbl_invoice', $arr);
		
	}
	public function get_invoice_reseller_model($reseller_id)
	{
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_invoice' );
		$this->db->where ( "tbl_invoice.reseller_id", $reseller_id);
		$this->db->where ( "tbl_invoice.type_id", "51");
		$query_tele = $this->db->get();
		//echo $this->db->last_query(); die("HGF");
	    return $query_tele->result();	
	}
	public function acess_model($user_id,$acessradio)
	{
		$arr['Access'] = $acessradio;
		$this->db->where('id',$user_id);
		return $this->db->update('tbl_user',$arr);
	}
	public function alldocs($user_id)
	{
		//$this->db->select(DISTINCT('file_name'));
		$this->db->group_by('file_name');
		$this->db->from ( 'tbl_file_info' );
		$this->db->where ( "tbl_file_info.create_user_id", $user_id);
		$query_tele = $this->db->get();
		//echo $this->db->last_query(); die("HGF");
	    return $query_tele->result();
	}
	public function user_email($user_id)
	{
		//$this->db->select(DISTINCT('file_name'));
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_user' );
		$this->db->where ( "tbl_user.id", $user_id);
		$query_tele = $this->db->get();
		//echo $this->db->last_query(); die("HGF");
	    return $query_tele->row();
	}
	public function status_update($alerts_user_id,$email_alert_with)
	{
		$arr['email_alert'] = $email_alert_with;
		$this->db->where("tbl_file_info.create_user_id", $alerts_user_id);
		return $this->db->update('tbl_file_info',$arr);
	}
	function ediumnurgh()
	{
		$this->db->select ( '
		tbl_order.create_user_id,
		tbl_order.reseller_id,
		tbl_user.email,
		');
		$this->db->join ( 'tbl_user', 'tbl_user.id = tbl_order.create_user_id' );
		$this->db->group_by('tbl_order.create_user_id');
		$this->db->where ( 'tbl_order.reseller_id != 36');
		$this->db->get('tbl_order')->result_array();
		echo $this->db->last_query();die();
	}
	function charge_update_price($charge_order_id,$charge_company_price)
	{
		$arr['updated_charge'] = $charge_company_price;
		$this->db->where("tbl_order.id", $charge_order_id);
		return $this->db->update('tbl_order',$arr);
	}
	function reseller_update_name($company_name_reseller,$resellerr_update_id)
	{
		$arr['company_name'] = $company_name_reseller;
		$this->db->where("tbl_reseller.id", $resellerr_update_id);
		return $this->db->update('tbl_reseller',$arr);
	}
	public function insert_phone_number($arr)
	{
		//var_dump($arr);die();
		return $this->db->insert('tbl_tas_phone_number', $arr);
		
	}
	public function select_phone_number($arr)
	{
		//var_dump($arr);die();
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_tas_phone_number' );
		$this->db->where ( "tbl_tas_phone_number.company_id", $arr);
		$query_tele = $this->db->get();
		//echo $this->db->last_query(); die("HGF");
	    return $query_tele->row();
		
	}
	public function update_phone_number($arr)
	{
		$arr['company_id'] = $company_name_reseller;
		$this->db->where("tbl_tas_phone_number.company_id", $arr ["company_id"]);
		return $this->db->update('tbl_tas_phone_number',$arr);
		
	}
	public function update_order_table_model($order_company_id,$data_file)
	{
		$this->db->where("tbl_order.company_id",$order_company_id);
		return $this->db->update('tbl_order',$data_file);
	}
	public function companydetail($company_id){
			$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
			tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
			tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
			$this->db->from ( 'tbl_order' );
			$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
			$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
			$this->db->where ( 'tbl_order.company_id', $company_id);
			$query = $this->db->get ();
		   return $user_orders = $query->row();
	}
 

public function UpdateByCompanyRegisterdelivermodel($id,$deliver_date)
{
	    $arr['company_register_deliver_date'] = $deliver_date;
		$this->db->where('id',$id);
	    $query = $this->db->update('tbl_order',$arr);
		//echo $this->db->last_query();die();
		return $query;
}
public function UpdateByCompanydocumentdelivermodel($id,$deliver_date,$document_status=Null)
{
	    $arr['document_register_deliver_date'] = $deliver_date;
	    $arr['document_service_status'] = $document_status;
		$this->db->where('id',$id);
	    $query = $this->db->update('tbl_order',$arr);
		//echo $this->db->last_query();die();
		return $query;
} 
public function Updatedomainname($id,$domain_name)
{
      
	    $arr['Domain_name'] = $domain_name;
		$this->db->where('id',$id);
	    $query = $this->db->update('tbl_order',$arr);
		//echo $this->db->last_query();die();
		return $query;
}
	 public function document_service_companies($limit,$start,$arra,$exportcondition=null) {
	   $reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		$this->db->where ( 'tbl_reseller.state_id !=1');

			if ($arra['role_id'] == 0)
			{
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
			elseif ($arra['role_id'] == 2)
			{
				if(!empty($reseller_ids))
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				else
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
		
			if($exportcondition==''){
				$this->db->limit($limit, $start);
			}
			$query = $this->db->get ();
			//echo $this->db->last_query(); 
			$result = $query->result();
 		return  $result ;
		}
}
public function count_document_service_companies($arra) {
	//print_r($arra);die;
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);
			//var_dump($resellers);die;
				foreach($resellers as $reseller){
					$reseller_ids [] = $reseller->id;
				}
		}
		//var_dump($arra,$reseller_ids);die;
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.good_standing = "Yes" OR tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		$this->db->where ( 'tbl_reseller.state_id !=1');

		if ($arra['role_id'] == 0)
		{
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		}
		elseif ($arra['role_id'] == 2)
		{
			//var_dump($reseller_ids);die;
			if(!empty($reseller_ids))
			$this->db->where_in('tbl_reseller.id', $reseller_ids);
			else
	
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
		
		$num = $query->num_rows();
		return  $num ;
		}
		else
		{
		$num=0;
		return  $num ;
		}
}
public function show_search($limit=null,$start=null,$arr,$exportcondition=null) { 

	$reseller ='';
	$reseller_ids = array();
 	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	$this->db->select('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, 
	tbl_company.company_name as company_name ,tbl_company.trading as trading,
	tbl_company.create_user_id as create_user_id , tbl_reseller.company_name as reseller');
	$this->db->from ( 'tbl_order' );
	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	$this->db->order_by ( "tbl_company.company_name", "asc" );
	$this->db->where ( 'tbl_reseller.state_id !=1' );
	
	$query_string = $arr ['search_field'];
	if ($arr["search_field"] !='') {	
	
	//$search_string = "(tbl_company.company_name LIKE '%".$query_string."%' OR tbl_company.trading  LIKE  '%".$query_string."%')";
	$search_string = "(tbl_company.company_name LIKE '%".$query_string."%' OR tbl_company.trading  LIKE  '%".$query_string."%' OR tbl_company.director1  LIKE  '%".$query_string."%')";
	$this->db->where ( $search_string );
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );

	} elseif ($arr['role_id'] == 2) {
		if ($resellers)
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	
	}
	elseif($arr ["select_id"])
	{
		$select_id = $arr ["select_id"];
			$select_option = $arr ["select_option"];
			$this->db->where ( 'tbl_order.' . $select_id, $select_option );
			if ($arr['role_id'] == 0)
				$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
			if ($arr['role_id'] == 2)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	elseif(empty ($arr["search_field"]))
	{
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			if ($resellers)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
	}
	else
	{
		
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			if ($resellers)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
	}
	$query1 = $this->db->get();
	$user_order = $query1->result ();
if ($arr["search_field"] !='') {	
	/*====================================================================*/
		$this->db->select('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, 
	tbl_company.company_name as company_name ,tbl_company.trading as trading,
	tbl_company.create_user_id as create_user_id , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_user', 'tbl_user.id=tbl_order.create_user_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );		
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->like( "tbl_user.last_name ", $query_string );
		
		if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );

	} elseif ($arr['role_id'] == 2) {
		if ($resellers)
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
		$query2 = $this->db->get ();
		$result2 = $query2->result();
		
		/*foreach($user_order as $user_orde)
		{
		var_dump($user_orde->id);die;
			$data_insss = array('state_id'=>2);
				$this->db->where ( 'id', $user_orde->id );
			$this->db->update ( 'tbl_order', $data_insss );
			
		
		}*/
	
	$user_orders = array_merge($result2 , $user_order);
	//echo $this->db->last_query(); die;
	/*====================================================================*/
}
else
{

$user_orders=$user_orders;
}
	
	return  $user_orders ;
  }
  
  /*---Count state change--*/
public function count_ltd_change($arr) { 
 	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
 		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	 	if ($arr['comp_ltd_select'] != 0) {
		$state_ltd = $arr['comp_ltd_select'];;
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.comp_ltd', $state_ltd );
	} else {
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	}
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	$query = $this->db->get ();
	//echo $this->db->last_query(); 
	$num = $query->num_rows();
	return  $num ;
 }
/*---show all comp_ltd_select--*/
public function show_ltd_change($limit=null,$start=null,$arr,$exportcondition=null) { 
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	if ($arr['comp_ltd_select'] != 0) {
		
		$state_ltd = $arr['comp_ltd_select'];;
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.comp_ltd', $state_ltd );
	} else {
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	}
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); 
	$user_orders = $query->result ();
	return  $user_orders ;
  }
  public function show_acess_change($count=null,$arr,$limit= null,$offset=null,$exportcondition=null) { 

	if ($arr == 0 || $arr == 1 ||  $arr == '1') {
		//if ($arr['asess_form'] == 0 || $arr['asess_form'] == 1 || $arr['asess_form'] == '1') {
		$this->db->select ( '*,tbl_user.Access,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_user', 'tbl_user.id = tbl_company.create_user_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_user.Access', $arr );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	} else {
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	}
	if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
  } 
 public function show_pdf_change($count=null,$arr,$limit= null,$offset=null,$exportcondition=null) 
{ 
        if ($arr == 0 || $arr == 1) {
		$this->db->select ( '
		tbl_order.*,
		tbl_file_info.email_alert,
		tbl_company.company_name,
		tbl_company.location,
		tbl_reseller.company_name as reseller
		');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_file_info', 'tbl_file_info.company_id = tbl_order.company_id' );
		
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->group_by('tbl_file_info.company_id');
		$this->db->where ( 'tbl_file_info.email_alert', $arr );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		}  else {
		$this->db->select ( '
		tbl_order.*,
		tbl_company.company_name,
		tbl_company.location,
		tbl_reseller.company_name as reseller
		');
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	    } 
	   if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
			//echo $this->db->last_query();die("dfgdfg");
		}
		if($exportcondition=='export')
		{
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}	
  }  
  
  
/*---Count  type_change (for billing)--*/
public function count_type_change($arr) { 
 	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
 		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
 	if ($arr['type_change'] != 'Show All') {
		
		$state = $arr['type_change'];;
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		if($arr['state_change'] =10)
		$arr['state_change']=0;
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.type_id', $state );
	} else {
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	}
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	$query = $this->db->get ();
	//echo $this->db->last_query(); 
	$num = $query->num_rows();
	return  $num ;
 }


/*---show all type_change (for billing)--*/
public function show_type_change($limit=null,$start=null,$arr,$exportcondition=null) { 
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	
	if ($arr['type_change']!= 'Show All') {
		
		$state = $arr['type_change'];;
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		// $this->db->where ( 'tbl_order.state_id !=2');
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ( 'tbl_order.type_id', $state );
	} else {
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	}
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); 
	$user_orders = $query->result ();
	return  $user_orders ;
  }
/* prinka model*/
public function add_deposit_details($arr) {

	if($arr['type_id']=='1'){
		$value=0.20;
		$arr['deposit_admin_fee'] = ($arr['deposit_no_items'])*($value);
	}else if($arr['type_id']=='2'){
		$value=3.00;
		$arr['deposit_admin_fee'] = ($arr['deposit_no_items'])*($value);
	}else if($arr['type_id']=='3'){
		$value=0.00;
		$arr['deposit_admin_fee'] = $value;
	}
	
	$arr['total_amount']=$arr['deposit_post_charge'] +$arr['deposit_admin_fee'];
	$arr['new_balance'] = $arr['order_price']-$arr['total_amount'];
	/* $this->db->where('deposit_company_id',$arr['deposit_company_id']);
	$this->db->limit('1','0');
	$this->db->order_by('deposit_id','desc'); 
	$res=$this->db->get('tbl_deposit')->row_array();
	$arr['last_id'] = isset($res['deposit_id'])?$res['deposit_id']:'';  */
/* 	$orderDeposit = $this->order_deposit_details($arr);
	$id = $orderDeposit->id;
	if($arr['last_id']=="")
	{ 
		$deposit=$orderDeposit->deposit;
		
		$arr['deposit_price'] = $deposit-$arr['total_amount'];
	}else{ 
		 $fetchDeposit = $this->fetch_balance($arr['last_id']);
		$balance= $fetchDeposit->deposit_price;
		$balance1=$arr['total_amount'];
		$arr['deposit_price']= ($balance) - ($balance1);
	} */
	/* deposit_mail_type
	deposit_admin_fee
	deposit_no_items
	total_amount
	deposit_post_charge
	status
	created_date */
	$arr['status'] = '1';
	//$arr['deposit_refund'] = '0';
	$arr['created_date'] =date('Y-m-d');
	$this->UpdateOrderDep($arr['new_balance'],$arr['order_id']);
	unset($arr['order_id']);
	unset($arr['order_price']);
	//var_dump($arr['deposit_price'],$id);die();
	
	return  $this->db->insert('tbl_deposit', $arr);
}


public function add_deposit_details1($arr) {

	 /* $this->db->limit('1','0');
	$this->db->order_by('deposit_id','desc'); 
	$res=$this->db->get('tbl_deposit')->row_array();
	$arr['last_id'] = isset($res['deposit_id'])?$res['deposit_id']:''; 
	$price=$res['deposit_price']; */
/* 	if($arr['deposit_refund']=='4')
	{
		$arr['deposit_price']=$price-$arr['amount'];
	}
	else{
	$arr['deposit_price']=$price+$arr['amount'];
	}
	$arr['new_balance']=$price;
	$arr['status'] = '1';
	//$arr['deposit_refund'] = '0';
	$arr['created_date'] =date('Y-m-d');
	$orderDeposit = $this->order_deposit_details($arr);
	$id = $orderDeposit->id;
	$this->UpdateOrderDep($arr['deposit_price'],$id); */
	if($arr['type_id']=='4'){
		$arr['new_balance'] = ($arr['amount'])+($arr['deposit_post_charge']);
	}else if($arr['type_id']=='5'){
		$arr['new_balance'] =($arr['amount'])-($arr['deposit_post_charge']);
	}
	$arr['status'] = '1';
	$arr['created_date'] =date('Y-m-d');
	$this->UpdateOrderDep($arr['new_balance'],$arr['order_id']);
	unset($arr['order_id']);
	unset($arr['order_price']);
	return  $this->db->insert('tbl_deposit', $arr);

}
	public function update_deposit_details($arr) { 
		$this->db->where("deposit_id", $arr['deposit_id']);
		return $this->db->update("tbl_deposit", $arr);
	}
	
	public function order_deposit_details($arr) {
		$this->db->select("*");
		$this->db->from("tbl_order");
		$this->db->where('company_id',$arr['company_id']);
		$query_tele = $this->db->get();
		return $query_tele->row();
	}
	
	public function fetch_balance($id) {
	$this->db->select('deposit_price');
	$this->db->from("tbl_deposit");
	$this->db->where('deposit_id',$id);
	$query_tele = $this->db->get();
	 return $query_tele->row();
	 //echo $this->db->last_query();die();
	}
	public function fetch_new_balance($arr) {
	$this->db->select('deposit_price');
	$this->db->from("tbl_deposit");
	$this->db->where('last_id',$arr);
	$query_tele = $this->db->get();
	 return $query_tele->row();
	 //echo $this->db->last_query();die();
	} 
	public function getDepositById($id) {
		$this->db->select('*');
		$this->db->from("tbl_deposit");
		$this->db->where('deposit_id',$id);
		$query_tele = $this->db->get();
		return $query_tele->row();
	} 
	public function update_new_deposit_details($arr) {
	$this->db->where("last_id", $arr['last_id']);
		return $this->db->update("tbl_deposit", $arr);
	} 
	public function UpdateUser($arr) {	
	$this->db->where('id',$arr['id']);
	unset($arr['id']);
	//echo '<pre>';
	//print_r($arr);die();
	//var_dump($arr);die("prinka");
      return $this->db->update("tbl_user", $arr);
	} 
	public function UpdateBilling($arr) {	
	$this->db->where('id',$arr['id']);
	unset($arr['id']);
		   return $this->db->update("tbl_billing_address", $arr);
		//$this->db->last_query();die();
	} 
	public function UpdateOrderinfo($arr) {	
	//var_dump($arr);die();
	$this->db->where('id',$arr['id']);
	unset($arr['id']);
		   return $this->db->update("tbl_order_detail", $arr);
		  //$this->db->last_query();die();
	} 
	/*delete order*/ 
	public function deleteOrder($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete('tbl_order_detail'); 
	}
	
	public function InsertOrder($arr){
		$arr['create_time'] =date('Y-m-d h:i:s');
		return $this->db->insert('tbl_order_detail', $arr);
	}
		
	/*order deposit field change*/
	public function UpdateOrderDep($price,$id) {
	//var_dump($price,$id);die("HGF");
		$arr['deposit']=$price; 
		$this->db->where('id',$id);
		return $this->db->update("tbl_order", $arr);
	} 
	
	public function GetOrderById($id) {
		$this->db->select('*');
		$this->db->from("tbl_order_detail");
		$this->db->where('id',$id);
		$query_tele = $this->db->get();
		return $query_tele->row();
	} 
	  public function GetOrderByIdduplicate($order_compaqny_id)
	  {
	    $this->db->select ( '*' );
		$this->db->from ( 'tbl_company' );
		$this->db->where ( 'tbl_company.id ',$order_compaqny_id );
		$query_company = $this->db->get();
		return $query_company->row();
	  }
	public function Updatemessage_notes($id,$notes) {
	$arr['tas_notes']=$notes;
	$this->db->where('id',$id);
	//unset($arr['id']);
   $notes_update = $this->db->update("tbl_company", $arr);
   if($notes_update)
   {
	//var_dump("hjjg"); die("jih");
	$notes= $this->filterSearch_new_notes($id);
	//var_dump($notes); die("jih");
	//echo $this->db->last_query();die();
	return $notes; 
   }
  }
   public function Updatemessage_notes_userbyid($id,$notes,$create_user_id) {
   //var_dump($create_user_id);die();
	$arr['notes']=$notes;
	//$this->db->where('id',$id);
	$this->db->where('create_user_id',$create_user_id);
	//unset($arr['id']);
   $notes_update = $this->db->update("tbl_company", $arr);
   if($notes_update)
   {
	//var_dump("hjjg"); die("jih");
	$notes= $this->filterSearch_new_notes_byid($create_user_id);
	//var_dump($notes); die("jih");
	//echo $this->db->last_query();die();
	return $notes;
   }
   //echo $this->db->last_query();die();
	} 
	function filterSearch_new_notes($id){
		$this -> db -> select('*'); 
		$this -> db -> from('tbl_company');
		$this -> db -> where('tbl_company.id',$id); 	
		$query = $this -> db -> get();
		$user_company=  $query->row();
		$data['user_company'] = $user_company;
		return $user_company;
	}
	function filterSearch_new_notes_byid($id){
		$this -> db -> select('*');
		$this -> db -> from('tbl_company');
		$this -> db -> where('tbl_company.id',$id); 	
		$query = $this -> db -> get();
		$user_company=  $query->row();
		$data['user_company'] = $user_company;
		return $user_company;
	}
	public function Updatemessage_comp_info($id,$comp_info) {
	$arr['comp_info']=$comp_info;
	$this->db->where('id',$id);
	//unset($arr['id']);
    return $this->db->update("tbl_company", $arr);
    //echo $this->db->last_query();die();
	} 
	
	/*delete deposit*/
	public function deleteDeposit($id)
	{
		$this->db->where('deposit_id', $id); 
		return $this->db->delete('tbl_deposit'); 
	}
	public function company_data($arr1){
	//var_dump($arr1);die();
	return $this->db->insert('tbl_company_formation_details', $arr1);
	}
	/*insert dierector xml data*/
	public function company_director_data($arr1){
	
	//var_dump($arr1);die();
	return $this->db->insert('tbl_company_director', $arr1);
	
    }
	/*insert person Director xml data*/
	public function company_psc_data($arr1){
	//var_dump($arr1);die();
	return $this->db->insert('tbl_company_psc', $arr1);
	//echo $this->db->last_query();die();
    }
	public function company_sec_data($arr1){
	//var_dump($arr1);die();
	return $this->db->insert('tbl_company_secretaries', $arr1);
	//echo $this->db->last_query();die();
    }
	public function company_shareholder_data($arr1){
	//var_dump($arr1);die();
	return $this->db->insert('tbl_company_shareholders', $arr1);
	//echo $this->db->last_query();die();
    }	
	public function update_company_data($arr1){
		$this->db->where('company_id',$arr1['company_id']);
		return $this->db->update('tbl_company_formation_details',$arr1);
	}
	public function secretaries($company_id,$param=null){
		$this->db->where('company_id',$company_id);
		if($param=='1'){
			$this->db->where('api','1');
		}else if($param=='0'){
			$this->db->where('api','0');
		}
		return $this->db->get('tbl_company_secretaries')->result_array();		
	}
	/* public function update_company_data_name_import($id,$company_name){
	 $arr['company_name'] = $company_name;
		$this->db->where('id',$id);
	    $this->db->update('tbl_company',$arr['company_name']);
		echo $this->db->last_query();die();
	} */
	public function update_company_data_name_import($arr1){
    $this->db->where('id',$arr1['id']);
    return $this->db->update('tbl_company',$arr1);
    //echo $this->db->last_query();die();
 }
	public function Get_company_data($arr){
	//var_dump($arr);die();
		$this->db->select("*");
		$this->db->from("tbl_company_formation_details");
		$this->db->where('company_id',$arr);
		$query=$this->db->get();
		return $query->num_rows();
	} 
	/*get data from psc*/
	public function check_psc_company_data($arr){
		$this->db->select("*");
		$this->db->from("tbl_company_psc");
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_psc.api !=0' );
		$query=$this->db->get();
		return $query->row();
	}
	
	public function psc($company_id,$param=null){	
		$this->db->where('company_id',$company_id);
		if($param=='1'){
			$this->db->where('api','1');
		}else if($param=='0'){
			$this->db->where('api','0');
		}
		return $this->db->get('tbl_company_psc')->result_array();	
	}
	public function deletePscData($arr)
	{
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_psc.api !=0' );
		return $this->db->delete('tbl_company_psc'); 
	}
	public function insert_psc_address($arr){
		//var_dump($arr);
		return $this->db->insert('tbl_company_psc',$arr);
		//var_dump($data);die("hdfh");
		
	}
	public function check_director_company_data($arr){
		$this->db->select("*");
		$this->db->from("tbl_company_director");
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_director.api !=0' );
		$query=$this->db->get();
		return $query->row();
	}
	public function deletedirectorData($arr)
	{
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_director.api !=0' );
		return $this->db->delete('tbl_company_director'); 
	}
	public function directorData($company_id,$param=null){
		$this->db->where('company_id',$company_id);
		if($param=='1'){
			$this->db->where('api','1');
		}else if($param=='0'){
			$this->db->where('api','0');
		}
		return $this->db->get('tbl_company_director')->result_array();	
	}
	public function insert_secretaries_address($arr){	
		return $this->db->insert('tbl_company_secretaries',$arr);	
	}
	public function check_sectory_company_data($arr){
		$this->db->select("*");
		$this->db->from("tbl_company_secretaries");
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_secretaries.api !=0' );
		$query=$this->db->get();
		return $query->row();
	}
	public function insert_directer_address($arr){
		//var_dump($arr);die("fdsfs");
		return	$this->db->insert('tbl_company_director',$arr);
	}
	public function insert_shareholder_address($arr){
		//var_dump($arr);die("fdsfs");
		return	$this->db->insert('tbl_company_shareholders',$arr);
	}
	public function deletesectoryData($arr)
	{
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_secretaries.api !=0' );
		return $this->db->delete('tbl_company_secretaries'); 
	}
	public function check_shareholder_company_data($arr){
		$this->db->select("*");
		$this->db->from("tbl_company_shareholders");
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_shareholders.api !=0' );
		$query=$this->db->get();
		return $query->row();
	}
	
	public function deleteshareholderData($arr)
	{
		$this->db->where('company_id',$arr);
		$this->db->where ( 'tbl_company_shareholders.api !=0' );
		return $this->db->delete('tbl_company_shareholders'); 
	}
	 public function updateCompanyAddress($arr){
		$this->db->where('id',$arr['id']);
		return $this->db->update('tbl_company',$arr);
	 }
	 public function adtional_companies($limit,$start,$arra,$exportcondition=null) {
	   $reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name,tbl_company.comp_info,tbl_company_formation_details.company_name as form,tbl_company_formation_details.incorporation_date,tbl_company_formation_details.next_due_date,tbl_company_formation_details.next__due_return_dated,tbl_company_formation_details.company_status,tbl_company_formation_details.post_town,tbl_company_formation_details.company_number,tbl_user.email,tbl_user.Access');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
		$this->db->or_where ( 'tbl_order.Legal = "Yes" ');
		$this->db->where ( 'tbl_reseller.state_id !=1');

			if ($arra['role_id'] == 0)
			{
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
			elseif ($arra['role_id'] == 2)
			{
				if(!empty($reseller_ids))
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				else
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
		
			if($exportcondition==''){
				$this->db->limit($limit, $start);
			}
			$query = $this->db->get ();
			//echo $this->db->last_query(); 
			$result = $query->result();
 		return  $result ;
		}
}
	 public function hosting_model($limit,$start,$arra,$exportcondition=null) {
	   $reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		$this->db->where ( 'tbl_reseller.state_id !=1');

			if ($arra['role_id'] == 0)
			{
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
			elseif ($arra['role_id'] == 2)
			{
				if(!empty($reseller_ids))
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				else
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
		
			if($exportcondition==''){
				$this->db->limit($limit, $start);
			}
			$query = $this->db->get ();
			//echo $this->db->last_query(); 
			$result = $query->result();
 		return  $result ;
		}
}
   public function GetOrderByIdmailing($mailing_adress_id)
	  {
     $this->db->select ( '*' );
	$this->db->from ( 'tbl_mailling_address' );
	$this->db->where ( 'tbl_mailling_address.id ',$mailing_adress_id );
	$query_mailling = $this->db->get ();
	return $query_mailling->row();
  }
	public function GetOrderByIdbiling($billing_adress_id){
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_billing_address' );
		$this->db->where ( 'tbl_billing_address.id ',$billing_adress_id);
		$query_billing = $this->db->get ();
		return $query_billing->row();
	}
	
	public function GetOrderByIdcomdup($company_data){
		  $this->db->insert('tbl_company', $company_data);
		  //echo $this->db->last_query();die();
		 return  $this->db->insert_id();
    }
	public function GetOrderByIdoldorderid($oldOrderInfo_Id){
		$this->db->from ( 'tbl_order' );
		$this->db->where ( 'tbl_order.id', $oldOrderInfo_Id );
		$query = $this->db->get ();
		$user_order = $query->row ();
		return $user_order;
	}
		public function GetOrderByIdoldorderplace($data_order){
		 $this->db->insert('tbl_order', $data_order);
		return  $this->db->insert_id();
		}
  /*----------------------------search------------------------------*/
public function count_search($arr) { 
//print_r($arr);die;
	$reseller ='';
	$reseller_ids = array();
 	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
	$this->db->select('*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,
	tbl_order.state_id as state_id,tbl_company.trading as trading,
	tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
	$this->db->from ( 'tbl_order' );
	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	$this->db->order_by ( "tbl_company.company_name", "asc" );
	$this->db->where ( 'tbl_reseller.state_id !=1' );
		$query_string = $arr ['search_field'];
	if ($arr["search_field"] !='') {
		$search_string = "(tbl_company.company_name LIKE '%".$query_string."%' OR tbl_company.trading  LIKE  '%".$query_string."%')";
	$this->db->where ( $search_string );
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		if ($resellers)
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	
	}
	elseif($arr ["select_id"])
	{
		$select_id = $arr ["select_id"];
			$select_option = $arr ["select_option"];
			$this->db->where ( 'tbl_order.' . $select_id, $select_option );
			if ($arr['role_id'] == 0)
				$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
			if ($arr['role_id'] == 2)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	elseif(empty ($arr["search_field"]))
	{
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			if ($resellers)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
	}
		else
	{
		if ($arr['role_id'] == 0) {
				$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	$this->db->where ( 'tbl_company.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			if ($resellers)
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
	}
	$query1 = $this->db->get();
	$user_order = $query1->result ();
if ($arr["search_field"] !='') {	
	/*====================================================================*/
		$this->db->select('*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, 
	tbl_company.company_name as company_name ,tbl_company.trading as trading,
	tbl_company.create_user_id as create_user_id , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_user', 'tbl_user.id=tbl_order.create_user_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );		
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->like( "tbl_user.last_name ", $query_string );
		
		if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );

	} elseif ($arr['role_id'] == 2) {
		if ($resellers)
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
		$query2 = $this->db->get ();
		$result2 = $query2->result();
	
	$user_orders = array_merge($result2 , $user_order);
	//echo $this->db->last_query(); die;
	/*====================================================================*/
}
else
{

$user_orders=$user_orders;
}
	
	$num = count($user_orders);
	return  $num ;
 }

public function show_renewal_records($limit,$start,$arra,$exportcondition=null) {
//var_dump($arra['search_new_all']);die();
	  	$reseller = '';
		$reseller_ids = array ();
		$resellers = $this->search->resellerUser($arra['user_id']);
 		if ($arra['role_id'] == 2) {
			if ($resellers) {
				
				foreach ( $resellers as $reseller ) {
					$reseller_ids [] = $reseller->id;
				}
			}
		}
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["renew_company_from"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["renew_company_to"]  ) );
 		$this->db->where ( 'tbl_order.renewable_date >=', $from. " 00:00:00" );
		$this->db->where ( 'tbl_order.renewable_date <=', $to. " 23:59:59" );
		/* if($arra['search_new_all'] != "show_all"){
			$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		} */
		/* echo $this->db->last_query();die("Bgfd"); */
		if($arra['search_new_all'] == "1")
		{
			$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		}
		else if($arra['search_new_all'] == "0")
		{
		$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		}
		if($arra['reseller_id'] !='Show All' && $arra['reseller_id'] !='')
		$this->db->where ( 'tbl_order.reseller_id', $arra['reseller_id'] );
 		
		if ($arra['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		} elseif ($arra['role_id'] == 2) {
			if ($resellers){
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
			}
		}
  		if($exportcondition==''){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get ();
		
		return $result = $query->result();
	
		//var_dump($this->db->last_query());die;
 		//return  $result ;


 
	}
	
	/*---show all companies--*/
	
	/*---show all companies--*/
public function show_renewal_records1($limit,$start,$arra,$exportcondition=null) {

//var_dump($arra['search_new_all']);die();
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["new_order_from"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["new_order_to"]  ) );
 		$this->db->where ( 'tbl_order.create_time >=', $from. " 00:00:00" );
		$this->db->where ( 'tbl_order.create_time <=', $to. " 23:59:59" );
		/* if($arra['search_new_all'] != "show_all"){
			$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		} */
		/* echo $this->db->last_query();die("Bgfd"); */
 		
		if ($arra['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		} elseif ($arra['role_id'] == 2) {
			if ($resellers){
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
			}
		}
  		if($exportcondition==''){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get ();
		
		return $result = $query->result();
	
		//var_dump($this->db->last_query());die;
 		//return  $result ;


 
	}
	public function GetCompanyName($Comid) {
		$this->db->select( '*');
		$this->db->from( 'tbl_company' );
		$this->db->where(array('id'=> $Comid ));
		$query = $this->db->get();
		return $result = $query->row_array();
	}
	public function count_advance_search_companies($arra) {
		// $reseller ='';
		// $reseller_ids = array();
		// $comp_ids = array ();
 	// 	$this->db->select ( '*' );
		// $this->db->from ( 'tbl_file_info' );
		// $this->db->where ( 'tbl_file_info.type_id !=0' );
		// $this->db->where ( 'tbl_file_info.type_id !=1' );
		// $this->db->where ( 'tbl_file_info.type_id !=2' );
		// $this->db->where ( 'tbl_file_info.type_id !=3' );
		// $this->db->where ( 'tbl_file_info.type_id !=6' );
		// $this->db->where ( 'tbl_file_info.type_id !=7' );
		// $this->db->where ( 'tbl_file_info.type_id !=8' );
		// // $this->db->group_by("tbl_file_info.company_id");
		// $query_file_info = $this->db->get ();
		// $file_info_results = $query_file_info->result ();
		
		// foreach ( $file_info_results as $file_info_result ) {
		// 	$comp_ids [] = $file_info_result->company_id;
		// }
	
		// if($arra['role_id'] == 2)
		// { 
		// 	$resellers = $this->search->resellerUser($arra['user_id']);

		// 		foreach($resellers as $reseller)
		// 		{
		// 			$reseller_ids [] = $reseller->id;
		// 		}
		// }
			$this->db->select($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );		
		$this->db->where ( 'tbl_reseller.state_id !=1');		
		if($arra['state_id'] != "Show All")
			$this->db->where ( 'tbl_order.state_id',$arra['state_id']);
		if($arra['type_id'] != "Show All")
			$this->db->where ( 'tbl_order.type_id',$arra['type_id']);
		if($arra['reseller_id'] != "Show All")
			$this->db->where ( 'tbl_reseller.id',$arra['reseller_id']);	
		if($arra['location_id'] != "Show All")		
			$this->db->where ( 'tbl_order.location',$arra['location_id']);
		if($arra['register_id'] != "Show All")
			$this->db->where ( 'tbl_order.registered_office',$arra['register_id']);	
		if($arra['director_id'] != "Show All")
			$this->db->where ( 'tbl_order.director_service_address',$arra['director_id']);
		if($arra['business_id'] != "Show All")
			$this->db->where ( 'tbl_order.business_address',$arra['business_id']);
		if($arra['telephone_id'] != "Show All")
			$this->db->where ( 'tbl_order.telephone_service',$arra['telephone_id']);
		// if($arra['host_id'] != "Show All")
		// 	$this->db->where ( 'tbl_order.hosting',$arra['host_id']);
		if($arra['load_id'] != "Show All")
			{
				if ($arra['load_id'] == '0') {				
					if ($comp_ids)
						$this->db->where_not_in ( 'tbl_company.id', $comp_ids );						
				} else {
					if ($comp_ids)
						$this->db->where_in ( 'tbl_company.id', $comp_ids );						
				}
			}
			if($arra['company_name'] != "")
		{		
			$query_string = $arra['company_name'];
			$search_string = "(tbl_company.company_name LIKE '%".$query_string."%' OR tbl_company.trading  LIKE  '%".$query_string."%')";
	$this->db->where ( $search_string );
	}
	
		if($arra["renewal"] !="")
		{		
			$from = date ( 'Y-m-d', strtotime ( $arra["renewal"]  ) );
			$to = date ( 'Y-m-d', strtotime ( $arra["renewal_to"]  ) );
			$this->db->where ( 'tbl_order.renewable_date >=', $from. " 00:00:00" );
			$this->db->where ( 'tbl_order.renewable_date <=', $to. " 23:59:59" );
		}
		
	
		$query2 = $this->db->get ();
		$result1 = $query2->result();
		//echo "<pre>"; print_r($result1); die('testng');
		//echo $this->db->last_query(); 
	// 	if($arra['company_name'] != "")
	// 	{
	// 	/*=======================================================================*/
	// 		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
	// 	tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
	// 	tbl_company.company_name as company_name , tbl_reseller.company_name as reseller,tbl_user.last_name as last_name');
	// 	$this->db->from ( 'tbl_order' );
	// 	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	// 	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	// 	$this->db->join ( 'tbl_user', 'tbl_user.id=tbl_order.create_user_id' );
	// 	$this->db->order_by ( "tbl_company.company_name", "asc" );		
	// 	$this->db->where ( 'tbl_reseller.state_id !=1');
	// 	$this->db->like( "tbl_user.last_name ", $arra['company_name'] );	
	// 	$query2 = $this->db->get ();
	// 	$result2 = $query2->result();
		
	// 	$result = array_unique(array_merge($result2 , $result1));
	// 	$num = count($result);
	// /*===============================================================================*/
	// }
	// else
	// {
	$num = count($result1);
	//}
		
		return  $num ;
 }
/*---show all companies--*/
public function show_advance_search_companies($limit,$start,$arra,$exportcondition=null)
 {
 //var_dump($arra['website_id']);die("hghv");
		// $reseller ='';
		// $reseller_ids = array();
		// $comp_ids = array ();
 	// 	$this->db->select ( '*' );
		// $this->db->from ( 'tbl_file_info' );
		// $this->db->where ( 'tbl_file_info.type_id !=0' );
		// $this->db->where ( 'tbl_file_info.type_id !=1' );
		// $this->db->where ( 'tbl_file_info.type_id !=2' );
		// $this->db->where ( 'tbl_file_info.type_id !=3' );
		// $this->db->where ( 'tbl_file_info.type_id !=6' );
		// $this->db->where ( 'tbl_file_info.type_id !=7' );
		// $this->db->where ( 'tbl_file_info.type_id !=8' );
		// // $this->db->group_by("tbl_file_info.company_id");
		// $query_file_info = $this->db->get ();
		// $file_info_results = $query_file_info->result ();
		
		// foreach ( $file_info_results as $file_info_result ) {
		// 	$comp_ids [] = $file_info_result->company_id;
		// }
		
		// if($arra['role_id'] == 2)
		// {
		// 	$resellers = $this->search->resellerUser($arra['user_id']);

		// 		foreach($resellers as $reseller)
		// 		{
		// 			$reseller_ids [] = $reseller->id;
		// 		}
		// }
			$this->db->select($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );		
		$this->db->where ( 'tbl_reseller.state_id !=1');		
		if($arra['state_id'] != "Show All")
			$this->db->where ( 'tbl_order.state_id',$arra['state_id']);
		if($arra['type_id'] != "Show All")
			$this->db->where ( 'tbl_order.type_id',$arra['type_id']);
		if($arra['reseller_id'] != "Show All")
			$this->db->where ( 'tbl_reseller.id',$arra['reseller_id']);	
		if($arra['location_id'] != "Show All")		
			$this->db->where ( 'tbl_order.location',$arra['location_id']);
		if($arra['register_id'] != "Show All")
			$this->db->where ( 'tbl_order.registered_office',$arra['register_id']);	
		if($arra['director_id'] != "Show All")
			$this->db->where ( 'tbl_order.director_service_address',$arra['director_id']);
		if($arra['business_id'] != "Show All")
			$this->db->where ( 'tbl_order.business_address',$arra['business_id']);
		if($arra['telephone_id'] != "Show All")
			$this->db->where ( 'tbl_order.telephone_service',$arra['telephone_id']);
		/* if($arra['host_id'] != "Show All")
			$this->db->where ( 'tbl_order.hosting',$arra['host_id']); */
		if($arra['website_id'] != "Show All")
		//var_dump("jhuhg");die("hosting");
			$this->db->where ( 'tbl_order.hosting',$arra['website_id']);
		if($arra['legal_id'] != "Show All")
			$this->db->where ( 'tbl_order.Legal',$arra['legal_id']);
		if($arra['register__id'] != "Show All")
			$this->db->where ( 'tbl_order.company_register',$arra['register__id']);
		if($arra['load_id'] != "Show All")
			{
				if ($arra['load_id'] == '0') {				
					if ($comp_ids)
						$this->db->where_not_in ( 'tbl_company.id', $comp_ids );						
				} else {
					if ($comp_ids)
						$this->db->where_in ( 'tbl_company.id', $comp_ids );						
				}
			}
		if($arra['company_name'] != "")
		{		
			$query_string = $arra['company_name'];
			$search_string = "(tbl_company.company_name LIKE '%".$query_string."%' OR tbl_company.trading  LIKE  '%".$query_string."%')";
			$this->db->where ( $search_string );
		}
	
		if($arra["renewal"] !="")
		{		
			$from = date ( 'Y-m-d', strtotime ( $arra["renewal"]  ) );
			$to = date ( 'Y-m-d', strtotime ( $arra["renewal_to"]  ) );
			$this->db->where ( 'tbl_order.renewable_date >=', $from. " 00:00:00" );
			$this->db->where ( 'tbl_order.renewable_date <=', $to. " 23:59:59" );
		}
		
		if($exportcondition==''){
			$this->db->limit($limit, $start);
		}
		$query2 = $this->db->get ();
		$result1 = $query2->result();
		//var_dump($result1);die;
		//echo $this->db->last_query(); 
		//var_dump( $this->db->last_query());die;
	// 	if($arra['company_name'] != "")
	// 	{
	// 	/*=======================================================================*/
	// 		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
	// 	tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
	// 	tbl_company.company_name as company_name , tbl_reseller.company_name as reseller,tbl_user.last_name as last_name,tbl_user.email as email');
	// 	$this->db->from ( 'tbl_order' );
	// 	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	// 	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	// 	$this->db->join ( 'tbl_user', 'tbl_user.id=tbl_order.create_user_id' );
	// 	$this->db->order_by ( "tbl_company.company_name", "asc" );		
	// 	$this->db->where ( 'tbl_reseller.state_id !=1');
	// 	$this->db->like( "tbl_user.last_name ", $arra['company_name'] );	
	// 	$query2 = $this->db->get ();
	// 	$result2 = $query2->result();

	// 	$result = $result2;//array_unique(array_merge($result2 , $result1));
	// /*===============================================================================*/
	// 	}
	// 	else
	// 	{
		$result = $result1;
		
		//}
 		return  $result ;
}
public function count_companies_total_reseller($arra) {
//var_dump("ufhurfd");die();
	//print_r($arra);die;
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2)
		{
			//var_dump("jhjb");die("jhjhjbj");
			$resellers = $this->search->resellerUser($arra['user_id']);
			/* foreach($resellers as $reseller123)
				{
					$reseller_ids12  = $reseller123->id;					
				} */
            //var_dump($resellers);die("khkjh");
				foreach($resellers as $reseller)
				{
					$reseller_ids[] = $reseller->id;
				   // var_dump($reseller_ids[0]);
				}
				
			   $resellers1 = $this->search->resellerUser_child($reseller_ids[0]);
				foreach($resellers1 as $reseller12)
				{
					$reseller_ids1[] = $reseller12->parent_reseller;					
				} 
				//var_dump($reseller_ids1[0]);die("khkjh"); 
		}	
		//var_dump($arra,$reseller_ids);die;
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_reseller.state_id !=1');

		if ($arra['role_id'] == 0)
		{
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		}
		elseif ($arra['role_id'] == 2 && $reseller_ids1[0]!= NULL)
			{
			        //var_dump();die("jkhnjh");
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
					$this->db->or_where_in('tbl_reseller.parent_reseller', $reseller_ids1);
			}
			elseif ($arra['role_id'] == 2 && $reseller_ids1[0]== NULL)
			{
			        //var_dump($reseller_ids);die("jkhnjh");
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
					
			}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		//$user_orders = $query->result();
		//var_dump($user_orders);die();
		$num = $query->num_rows();
		return  $num ;
		}
		else
		{
		$num=0;
		return  $num ;
		}
}
public function count_companies_add($arra) {
	//print_r($arra);die;
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);
			//var_dump($resellers);die;
				foreach($resellers as $reseller){
					$reseller_ids [] = $reseller->id;
				}
		}
		//var_dump($arra,$reseller_ids);die;
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->where ( 'tbl_order.company_register = "Yes" ');
		$this->db->or_where ( 'tbl_order.Legal = "Yes" ');
		$this->db->or_where ( 'tbl_order.hosting = "Yes" ');
		$this->db->where ( 'tbl_reseller.state_id !=1');

		if ($arra['role_id'] == 0)
		{
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		}
		elseif ($arra['role_id'] == 2)
		{
			//var_dump($reseller_ids);die;
			if(!empty($reseller_ids))
			$this->db->where_in('tbl_reseller.id', $reseller_ids);
			else
	
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
		
		$num = $query->num_rows();
		return  $num ;
		}
		else
		{
		$num=0;
		return  $num ;
		}
}
public function count_companies1($arra) {
	//print_r($arra);die;
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);
			//var_dump($resellers);die;
				foreach($resellers as $reseller){
					$reseller_ids [] = $reseller->id;
				}
		}
		//var_dump($arra,$reseller_ids);die;
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.renewable_date as renewable_date,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller, tbl_company_formation_details.company_name as form');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'MONTH(tbl_order.renewable_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renewable_date) = year(CURRENT_DATE())');
		$this->db->where ( 'tbl_reseller.state_id !=1');

		if ($arra['role_id'] == 0)
		{
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		}
		elseif ($arra['role_id'] == 2)
		{
			//var_dump($reseller_ids);die;
			if(!empty($reseller_ids))
			$this->db->where_in('tbl_reseller.id', $reseller_ids);
			else
	
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
		
		$num = $query->num_rows();
		//echo $this->db->last_query();die("date");
		return  $num ;
		}
		else
		{
		$num=0;
		return  $num ;
		}
}
// public function count_companies_newformation($count=null,$limit= null,$offset=null,$exportcondition=null) {
// 		$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name,tbl_company_formation_details.company_name as form');
// 		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
// 		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
// 		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
// 		$this->db->order_by ( "tbl_company.company_name", "asc" );
// 		$this->db->where ( 'tbl_order.state_id NOT IN ("2","4")');
// 		$this->db->where ( 'MONTH(tbl_order.renewable_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renewable_date) = year(CURRENT_DATE())');
// 		$this->db->where ( 'tbl_reseller.state_id !=1');
// 		if($count =='count'){
// 			$this->db->cache_off();
// 			return $this->db->get('tbl_order')->num_rows();
// 		}else if($count =='result'){
// 			$this->db->cache_off();
// 			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
// 		}
// 		if($exportcondition=='export')
// 		{
// 			$this->db->cache_off();
// 			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
// 		}
// }
/* Count New Ordersdata By Month,day,Week*/

/* admin..............*/
// public function count_NewOdersCompanies_new($count=null,$order,$limit= null,$offset=null,$exportcondition=null) {
// 	$this->db->cache_on();
// 		$this->db->select ('tbl_order.*,tbl_order.renewable_date as renewable_date,
// 		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
// 		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
// 		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
// 		$this->db->order_by ( "tbl_company.create_time", "DESC" );
// 		$this->db->where ( 'tbl_reseller.state_id !=1');
//         if($order=='month')
// 		 {
// 			$this->db->where( 'MONTH(tbl_company.create_time) = MONTH(CURRENT_DATE()) AND Year(tbl_company.create_time) = year(CURRENT_DATE())');
// 		 }
// 		 else if($order=='today')
// 		 {
		
// 			$currenttime= date('Y-m-d');;
// 			$this->db->where('tbl_company.create_time > ',$currenttime." 00:00:00");
// 			$this->db->where('tbl_company.create_time <= ',$currenttime." 23:59:59");
// 		 }
// 		 else if($order=='all')
// 		 {
			
// 		 }
// 		 else if($order=='week')
// 		 {
// 			$current_date = date('Y-m-d');
// 				$week = date('W', strtotime($current_date));
// 				$year = date('Y', strtotime($current_date));
// 				$dto = new DateTime();
// 				$result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
// 				$result['1'] = $dto->setISODate($year, $week, 2)->format('Y-m-d');
// 				$result['2'] = $dto->setISODate($year, $week, 3)->format('Y-m-d');
// 				$result['3'] = $dto->setISODate($year, $week, 4)->format('Y-m-d');
// 				$result['4'] = $dto->setISODate($year, $week, 5)->format('Y-m-d');
// 				$result['5'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
// 				$result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
// 				$this->db->where('tbl_company.create_time > ',$result['start']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['start']." 23:59:59");
// 				$this->db->or_where('tbl_company.create_time > ',$result['1']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['1']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['2']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['2']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['3']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['3']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['4']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['4']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['5']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['5']." 23:59:59");$this->db->or_where('tbl_company.create_time > ',$result['end']." 00:00:00");
// 			    $this->db->where('tbl_company.create_time <= ',$result['end']." 23:59:59");
// 		 }	
// 		 if($count =='count'){
// 			$this->db->cache_off();
// 			return $this->db->get('tbl_order')->num_rows();
// 		}else if($count =='result'){
// 			$this->db->cache_off();
// 			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
// 		}
// 		if($exportcondition=='export')
// 		{
// 			$this->db->cache_off();
// 			return $this->db->limit($limit,$offset)->get('tbl_order')->result();
// 		}
		
// }
/* Current month data*/
public function count_companies_currentmonth($arra) {
	//print_r($arra);die;
		$reseller ='';
		$reseller_ids = array();
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);
			//var_dump($resellers);die;
				foreach($resellers as $reseller){
					$reseller_ids [] = $reseller->id;
				}
		}
		//var_dump($arra,$reseller_ids);die;
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		//var_dump("jkjn");die();
		$this->db->select ('*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.renewable_date as renewable_date,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_order.update_time as update_time,tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id = 2');
		$this->db->where ( 'MONTH(tbl_order.update_time) = MONTH(CURRENT_DATE()) AND Year(tbl_order.update_time) = year(CURRENT_DATE())');
		//$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
		$this->db->or_where( 'MONTH(tbl_order.renew_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renew_date) = year(CURRENT_DATE())');
		
		/* $this->db->where ( 'MONTH(tbl_order.renewable_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renewable_date) = year(CURRENT_DATE())'); */
		$this->db->where ( 'tbl_reseller.state_id !=1');

		if ($arra['role_id'] == 0)
		{
			$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		}
		elseif ($arra['role_id'] == 2)
		{
			//var_dump($reseller_ids);die;
			if(!empty($reseller_ids))
			$this->db->where_in('tbl_reseller.id', $reseller_ids);
			else
	
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
		
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
		
		$num = $query->num_rows();
		//echo $this->db->last_query();die("date");
		return  $num ;
		}
		else
		{
		$num=0;
		return  $num ;
		}
}
/* Current month data end*/
/*---show all companies--*/


public function show_companies1($limit,$start,$arra,$exportcondition=null) {
	   $reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.renewable_date as renewable_date,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller, tbl_company_formation_details.company_name as form,tbl_company_formation_details.incorporation_date,tbl_company_formation_details.next_due_date,tbl_company_formation_details.next__due_return_dated,tbl_company_formation_details.company_status,tbl_company_formation_details.post_town');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'MONTH(tbl_order.renewable_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renewable_date) = year(CURRENT_DATE())');
		$this->db->where ( 'tbl_reseller.state_id !=1');

			if ($arra['role_id'] == 0)
			{
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
			elseif ($arra['role_id'] == 2)
			{
				if(!empty($reseller_ids))
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				else
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
		
			if($exportcondition==''){
				$this->db->limit($limit, $start);
			}
			$query = $this->db->get ();
			//echo $this->db->last_query(); die("gvj");
			$result = $query->result();
 		return  $result ;
		}
}
public function show_companies_current_month($limit,$start,$arra,$exportcondition=null) {
	$this->db->cache_on();
	   $reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ('*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.renewable_date as renewable_date,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id = 2');
		$this->db->where ( 'MONTH(tbl_order.update_time) = MONTH(CURRENT_DATE()) AND Year(tbl_order.update_time) = year(CURRENT_DATE())');
		//$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
		$this->db->or_where( 'MONTH(tbl_order.renew_date) = MONTH(CURRENT_DATE()) AND Year(tbl_order.renew_date) = year(CURRENT_DATE())');
		$this->db->where ( 'tbl_reseller.state_id !=1');

			if ($arra['role_id'] == 0)
			{
				$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
			elseif ($arra['role_id'] == 2)
			{
				if(!empty($reseller_ids))
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				else
					$this->db->where ( 'tbl_order.create_user_id', $arra['user_id'] );
			}
		
			if($exportcondition==''){
				$this->db->limit($limit, $start);
			}
			$query = $this->db->get ();
			//echo $this->db->last_query(); die("gvj");
			$result = $query->result();
			$this->db->cache_off();
 		return  $result ;
		}
}
/*New orders tab model according to day,week,Month*/

/*---get the count of all reseller--*/
 public function count_reseller($reseller_change) { 
	if ($reseller_change != 'Show All') {
		$reseller_id = $reseller_change;
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		$this->db->where ( 'tbl_order.reseller_id', $reseller_id );
	} else {
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
	}
	$query = $this->db->get ();
	$num = $query->num_rows();
	return  $num ;
}


/*---show all reseller--*/
public function show_reseller($limit=null,$start=null,$reseller_change,$exportcondition=null) { 
	if ($reseller_change != 'Show All') {
		$reseller_id = $reseller_change;
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.reseller_id', $reseller_id );
	} else {
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
	}
	if($exportcondition==''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	//echo $this->db->last_query(); 
	$user_orders = $query->result();
	return  $user_orders ;
}

public function countNewFormStatus($arr) { 
 	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
 		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
 	
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where ('tbl_order.comp_ltd = 1');
		$this->db->where ( 'tbl_order.state_id = 6 OR tbl_order.state_id = 9');
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
		$query = $this->db->get ();
		return $num = $query->num_rows();
 }
 /* admin */
 // public function countNewFormStatus_new($count=null,$limit= null,$offset=null,$exportcondition=null) { 
	// 	$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
	// 	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	// 	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	// 	$this->db->order_by ( "tbl_company.company_name", "asc" );
 // 		$this->db->where ( 'tbl_reseller.state_id !=1' );
	// 	$this->db->where ('tbl_order.comp_ltd = 1');
	// 	$this->db->where ( 'tbl_order.state_id = 6 || tbl_order.state_id = 9');
	// 	if($count =='count'){
	// 		$this->db->cache_off();
	// 		return $this->db->get('tbl_order')->num_rows();
	// 	}else if($count =='result'){
	// 		$this->db->cache_off();
	// 		return $this->db->limit($limit,0)->get('tbl_order')->result();
	// 	}
	// 	if($exportcondition=='export')
	// 	{
	// 		$this->db->cache_off();
	// 		return $this->db->limit($limit,0)->get('tbl_order')->result();
	// 	}
 // }
/*---Count state change--*/
public function count_state_change($arr) { 
//var_dump($arr);die();
 	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
 		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
 	if ($arr['state_change'] != '' ||	$arr['state_change'] == 'Show All') {
		
		//$state = $arr['state_change'];
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
 		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->where_in ( 'tbl_order.state_id', $arr['state_change'] );
	}  else {
		$this->db->select ( 'tbl_order.*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
	} /**/
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}
	$query = $this->db->get ();
	//echo $this->db->last_query(); 
	return $num = $query->num_rows();
 }
 // public function count_state_change_new($count=null,$statechange=null,$limit= null,$offset=null,$exportcondition=null) { 
 // 	if ($statechange != '' ||	$statechange == 'Show All') {
	// 	$this->db->select ('tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name');
	// 	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	// 	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	// 	$this->db->order_by ( "tbl_company.company_name", "asc" );
 // 		$this->db->where ( 'tbl_reseller.state_id !=1' );
	// 	$this->db->where_in ( 'tbl_order.state_id', $statechange );
	// }  else {
	// 	$this->db->select ( '*,tbl_order.id as id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
	// 	$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
	// 	$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
	// 	$this->db->order_by ( "tbl_company.company_name", "asc" );
	// 	$this->db->where ( 'tbl_reseller.state_id !=1' );
	// } 
	// if($arr['query_string_1'] != ""){
	// 	$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	// }
	// if($count =='count'){
	// 		$this->db->cache_off();
	// 		return $this->db->get('tbl_order')->num_rows();
	// 	}else if($count =='result'){
	// 		$this->db->cache_off();
	// 		return $this->db->limit($limit,$offset)->get('tbl_order')->result();
	// 	}
	// 	if($exportcondition=='export')
	// 	{
	// 		$this->db->cache_off();
	// 		return $this->db->limit($limit,$offset)->get('tbl_order')->result();
	// 	}
 // }


/*---show all state_change--*/
public function show_state_change($limit=null,$start=null,$arr,$exportcondition=null) { 
//var_dump($arr);die("gfd");
	$reseller = '';
	$reseller_ids = array ();
	if ($arr['role_id'] == 2) {
		$resellers = $this->search->resellerUser ( $arr['user_id'] );
		foreach ( $resellers as $reseller ) {
			$reseller_ids [] = $reseller->id;
		}
	}
		$this->db->select ($this->select );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		if ($arr['state_change']!= "" || $arr['state_change'] == "Show All") {
		// $this->db->where ( 'tbl_order.state_id !=2');
			$this->db->where ( 'tbl_reseller.state_id !=1' );
			if($arr['state_change'] ==10)
			{
			$arr['state_change']=0;
			}
			else if($arr['state_change'] ==14)
			{
			$arr['state_change']=6;
			}
			$this->db->where_in ( 'tbl_order.state_id', $arr['state_change'] );
			
		} 
		else {
				$this->db->where ( 'tbl_reseller.state_id !=1' );
			} 
	
	if($arr['query_string_1'] != ""){
		$this->db->or_like('tbl_company.company_name', $arr['query_string_1']);
	}
	
	if ($arr['role_id'] == 0) {
		$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
	} elseif ($arr['role_id'] == 2) {
		$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
	}

	if($exportcondition == ''){
			$this->db->limit($limit, $start);
	}
	$query = $this->db->get();
	 return $user_orders = $query->result ();
	//echo $this->db->last_query(); die("HGF");
  }
	public function newFormationData($limit,$start,$arr,$exportcondition=null){
		$reseller = '';
		$reseller_ids = array ();
		if ($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser ( $arr['user_id'] );
			
			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
		
		$this->db->select ($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','LEFT');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		//$this->db->where ( 'tbl_order.state_id = 6 || tbl_order.state_id = 9 || tbl_order.state_id = 5' );
		$this->db->where ( 'tbl_order.state_id = 6 OR tbl_order.state_id = 9' );
		$this->db->where ('tbl_order.comp_ltd = 1');
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
		
		if($exportcondition == ''){
				$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
		return	$user_orders = $query->result ();
	}
/*---Count all filter search--*/
public function count_filter_search($arr) { 
  		$reseller_ids = array ();
		$comp_ids = array ();
 		$this->db->select ( '*' );
		$this->db->from ( 'tbl_file_info' );
		$this->db->where ( 'tbl_file_info.type_id !=0' );
		$this->db->where ( 'tbl_file_info.type_id !=1' );
		$this->db->where ( 'tbl_file_info.type_id !=2' );
		$this->db->where ( 'tbl_file_info.type_id !=3' );
		$this->db->where ( 'tbl_file_info.type_id !=6' );
		$this->db->where ( 'tbl_file_info.type_id !=7' );
		$this->db->where ( 'tbl_file_info.type_id !=8' );
		$this->db->limit(50);
 		$query_file_info = $this->db->get ();
		$file_info_results = $query_file_info->result ();
 		foreach($file_info_results as $file_info_result){
			$comp_ids[] = $file_info_result->company_id;
		}
 		if($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser($arr['user_id'] );
 			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
 		$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
 		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
 		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
 		if($arr['location_select']<>''){
 			$location = $arr['location_select'];
			if($location != 'Show All')
				$this->db->where('tbl_order.location', $location );
			// /die($_POST['location_select']);
		}elseif($arr["upload_change"]<>""){
 			  $Id_upload = $arr["upload_change"];
			if($Id_upload != 'Show All') {
 				if($Id_upload == "Upload") {
					if($comp_ids)
						$this->db->where_not_in('tbl_company.id', $comp_ids);
				}else{
					if($comp_ids)
						$this->db->where_in('tbl_company.id', $comp_ids);
				}
			}
		}
  		$query = $this->db->get();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
 		$num = $query->num_rows();
		return  $num ;
   }

/*---show all filter search--*/
public function show_filter_search($limit=null,$start=null,$arr,$exportcondition=null) {
 		$reseller = '';
 		$reseller_ids = array ();
		$comp_ids = array ();
 		$this->db->select ( '*' );
		$this->db->from ( 'tbl_file_info' );
		$this->db->where ( 'tbl_file_info.type_id !=0' );
		$this->db->where ( 'tbl_file_info.type_id !=1' );
		$this->db->where ( 'tbl_file_info.type_id !=2' );
		$this->db->where ( 'tbl_file_info.type_id !=3' );
		$this->db->where ( 'tbl_file_info.type_id !=6' );
		$this->db->where ( 'tbl_file_info.type_id !=7' );
		$this->db->where ( 'tbl_file_info.type_id !=8' );
		//$this->db->where ( 'tbl_file_info.reseller_id = 0' );
		$this->db->limit(50);
		// $this->db->group_by("tbl_file_info.company_id");
		$query_file_info = $this->db->get ();
		$file_info_results = $query_file_info->result ();
		//echo $this->db->last_query(); 
		foreach ( $file_info_results as $file_info_result ) {
			$comp_ids [] = $file_info_result->company_id;
		}
		
		// var_dump($file_info_results);die("857857");
		if ($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser ( $arr['user_id'] );
 			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
		// var_dump($reseller_ids);die;
		$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		$this->db->limit("10"); 
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbl_order.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
		}
 		if($arr['location_select']<>"") {
			
			$location = $arr['location_select'];
			if ($location != 'Show All')
				$this->db->where ( 'tbl_order.location', $location );
			// /die($_POST['location_select']);
		} elseif($arr["upload_change"]<>"") {
			
			$Id_upload = $arr["upload_change"];
			//var_dump($Id_upload);die("hdhd");
			if ($Id_upload != 'Show All') {
				
				if ($Id_upload =="NO") {
					if ($comp_ids)
						$this->db->where_not_in ( 'tbl_company.id', $comp_ids );
						
				} else {
					if ($comp_ids)
						$this->db->where_in ( 'tbl_company.id', $comp_ids );
						
				}
			}
		}
 		if($exportcondition == ''){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		$user_orders = $query->result ();
		//var_dump($user_orders);die("yes");
		return  $user_orders ;
		//var_dump($user_orders);die("yes");
   }
   public function show_filter_searchbyid($limit=null,$start=null,$arr,$exportcondition=null) { 
 		$reseller = '';
 		$reseller_ids = array ();
		$comp_ids = array ();
 		$this->db->select ( '*' );
		$this->db->from ( 'tbl_file_info' );
		$this->db->where ( 'tbl_file_info.type_id !=0' );
		$this->db->where ( 'tbl_file_info.type_id !=1' );
		$this->db->where ( 'tbl_file_info.type_id !=2' );
		$this->db->where ( 'tbl_file_info.type_id !=3' );
		$this->db->where ( 'tbl_file_info.type_id !=6' );
		$this->db->where ( 'tbl_file_info.type_id !=7' );
		$this->db->where ( 'tbl_file_info.type_id !=8' );
		// $this->db->group_by("tbl_file_info.company_id");
		$query_file_info = $this->db->get ();
		$file_info_results = $query_file_info->result ();
		
		foreach ( $file_info_results as $file_info_result ) {
			$comp_ids [] = $file_info_result->company_id;
		}
		
		// var_dump($user_id,$role_id);die();
		if ($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser ( $arr['user_id'] );
 			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
		// var_dump($reseller_ids);die;
		$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->where ( 'tbl_reseller.state_id !=1' );
		
		 if($arr["upload_change"]<>"") {
			
			$Id_upload = $arr["upload_change"];
			if ($Id_upload != 'Show All') {
				
				if ($Id_upload == "Upload") {
					if ($comp_ids)
						$this->db->where_not_in ( 'tbl_company.id', $comp_ids );
						
				} else {
					if ($comp_ids)
						$this->db->where_in ( 'tbl_company.id', $comp_ids );
						
				}
			}
		}
 		if($exportcondition == ''){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		$user_orders = $query->result ();
		return  $user_orders ;
   }



/*---Count all filter search--*/
public function count_service_search($arr) { 
 		$key_name = $arr['key_name'] ;
 		$reseller = '';
		$reseller_ids = array ();
		// var_dump($user_id,$role_id);die();
		if ($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser ( $arr['user_id'] );
			
			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
		
 		$this->db->select ( 'tbo.*,
 		tbo.id as id,
		tbo.company_id as company_id,
		tbo.create_user_id as create_user_id,
		tbo.state_id as state_id,

		tbc.company_name as company_name,
		tbr.company_name as reseller' );
		
		$this->db->from ( 'tbl_order as tbo' );
		$this->db->join ( 'tbl_company as tbc', 'tbc.id = tbo.company_id' );
		$this->db->join ( 'tbl_reseller as tbr', 'tbr.id = tbo.reseller_id' );
		$this->db->order_by ( "tbc.company_name", "asc" );
		$this->db->where ( 'tbo.state_id !=4' );
		
		if ($arr['value_name'] != "Show All")
			$this->db->where ( "tbo.$key_name", $arr['value_name'] );
		
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbo.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in ( 'tbr.id', $reseller_ids );
		}
		$query = $this->db->get ();
		//echo $this->db->last_query();die;
		$user_orders = $query->result ();
 		$num = $query->num_rows();
		return  $num ;
  }

/*---show all show_service_search  --*/
public function show_service_search($limit=null,$start=null,$arr,$exportcondition=null) {  
  		$key_name = $arr['key_name'] ;
 		$reseller = '';
		$reseller_ids = array ();
 		if ($arr['role_id'] == 2) {
			$resellers = $this->search->resellerUser ( $arr['user_id'] );
			
			foreach ( $resellers as $reseller ) {
				$reseller_ids [] = $reseller->id;
			}
		}
  		$this->db->select ( 'tbo.*,
 		tbo.id as id,
		tbo.company_id as company_id,
		tbo.create_user_id as create_user_id,
		tbo.state_id as state_id,

		tbc.company_name as company_name,
		tbr.company_name as reseller' );
		
		$this->db->from( 'tbl_order as tbo' );
		$this->db->join( 'tbl_company as tbc', 'tbc.id = tbo.company_id' );
		$this->db->join( 'tbl_reseller as tbr', 'tbr.id = tbo.reseller_id' );
		$this->db->order_by( "tbc.company_name", "asc" );
		$this->db->where('tbo.state_id !=4' );
		
		if ($arr['value_name'] != "Show All")
			$this->db->where("tbo.$key_name", $arr['value_name'] );
		
		if ($arr['role_id'] == 0) {
			$this->db->where ( 'tbo.create_user_id', $arr['user_id'] );
		} elseif ($arr['role_id'] == 2) {
			$this->db->where_in('tbr.id', $reseller_ids);
		}
 		if($exportcondition == ''){
			$this->db->limit($limit, $start);
		}
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		$user_orders = $query->result ();
		return  $user_orders ;
    }
 // get the count of all count_categories
public function count_renewal_records($arra) {
	  	$reseller = '';
		$reseller_ids = array ();
		$resellers = $this->search->resellerUser($arra['user_id']);
 		if ($arra['role_id'] == 2) {
			if ($resellers) {
 				foreach ( $resellers as $reseller ) {
					$reseller_ids [] = $reseller->id;
				}
			}
		}
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["renew_company_from"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["renew_company_to"]  ) );
 		$this->db->where ( 'tbl_order.renewable_date >=', $from. " 00:00:00" );
		$this->db->where ( 'tbl_order.renewable_date <=', $to. " 23:59:59" );
		if($arra['reseller_id'] !='Show All' && $arra['reseller_id'] !='')
		$this->db->where ( 'tbl_order.reseller_id', $arra['reseller_id'] );
 		if($arra['search_new_all'] == "1")
		{
			$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		}
		else if($arra['search_new_all'] == "0")
		{
		$this->db->where ( 'tbl_order.type_id', $arra['search_new_all'] );
		}
		if($arra['role_id'] == 0) {
			$this->db->where('tbl_order.create_user_id', $arra['user_id'] );
		} elseif($arra['role_id'] == 2) {
			if($resellers){
				$this->db->where_in('tbl_reseller.id', $reseller_ids);
			}
 		}
 		$query =  $this->db->get();
 		//echo $this->db->last_query();die;
 		$num = $query->num_rows();
		return  $num ;
}
 // get the count of all count_categories
public function count_renewal_records1($arra) {

		$this->db->select ( '*,tbl_order.id as id,tbl_order.create_time as create_time,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->where ( 'tbl_order.state_id !=4' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$from = date ( 'Y-m-d', strtotime ( $arra["new_order_from"]  ) );
		$to = date ( 'Y-m-d', strtotime ( $arra["new_order_to"]  ) );
 		$this->db->where ( 'tbl_order.create_time >=', $from. " 00:00:00" );
		$this->db->where ( 'tbl_order.create_time <=', $to. " 23:59:59" );
		
		if($arra['role_id'] == 0) {
			$this->db->where('tbl_order.create_user_id', $arra['user_id'] );
		} elseif($arra['role_id'] == 2) {
			if($resellers){
				$this->db->where_in('tbl_reseller.id', $reseller_ids);
			}
 		}
 		$query =  $this->db->get();
 		//echo $this->db->last_query();die;
 		$num = $query->num_rows();
		return  $num ;
}
}
?>