<?php
Class Ltd_company extends CI_Model
{


 function ltd_company_register($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_registred_office' );
		$this->db->where ( "tbl_company_registred_office.company_id", $comp_id);		
		$query = $this->db->get ();
		$comp_reg = $query->row();		
		return $comp_reg;
 
 
 }
  function ltd_company_director($comp_id)
 {
 $comp_id = 2770;
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_director' );
		$this->db->where ( "tbl_company_director.company_id", $comp_id);
		
		$query = $this->db->get ();
		$comp_director = $query->result ();
	
		return $comp_director;
 
 
 }
   function ltd_company_director_corp($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_director_corp' );
		$this->db->where ( "tbl_company_director_corp.company_id", $comp_id);
		
		$query = $this->db->get ();
		$comp_director_corp = $query->result ();
		
		return $comp_director_corp;
 
 
 }

  function ltd_company_shareholder($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_shareholders' );
		$this->db->where ( "tbl_company_shareholders.company_id", $comp_id);
		
		$query = $this->db->get ();
		$company_shareholders = $query->result ();
		
		return $company_shareholders;
 
 
 }
   function ltd_company_shareholder_corp($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_corp_shareholders' );
		$this->db->where ( "tbl_company_corp_shareholders.company_id", $comp_id);
		
		$query = $this->db->get ();
		$comp_shareholder_corp = $query->result ();
		
		return $comp_shareholder_corp;
 
 
 }
function ltd_company_psc($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_psc' );
		$this->db->where ( "tbl_company_psc.company_id", $comp_id);
		
		$query = $this->db->get ();
		$company_psc = $query->result ();
		
		return $company_psc;
 
 
 }
   function ltd_company_psc_corp($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_psc_corp' );
		$this->db->where ( "tbl_company_psc_corp.company_id", $comp_id);
		
		$query = $this->db->get ();
		$comp_psc_corp = $query->result ();
		
		return $comp_psc_corp;
 
 
 }
 function ltd_company_secratory($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_secretaries' );
		$this->db->where ( "tbl_company_secretaries.company_id", $comp_id);
		
		$query = $this->db->get ();
		$secretaries = $query->result();
		
		return $secretaries;
 
 
 }
   function ltd_company_secratory_corp($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_secratory_corp' );
		$this->db->where ( "tbl_company_secratory_corp.company_id", $comp_id);
		
		$query = $this->db->get ();
		$secratory_corp = $query->result ();
		
		return $secratory_corp;
 
 
 }
   function ltd_company_mna($comp_id)
 {
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_company_memorandum_articles' );
		$this->db->where ( "tbl_company_memorandum_articles.company_id", $comp_id);
		
		$query = $this->db->get ();
		$comp_mna = $query->result ();
		
		return $comp_mna;
 
 
 }

}