<?php
Class Search extends CI_Model
{
	/* Custom seach is used for getting info about orders, company details , company notes*/
		function CustomSearch($table,$arr,$fun)
		{	

			$query = $this->db->get_where($table,$arr);
			$data['result'] =  $query->$fun();
			
			return $data['result'];
		}

	// function CustomGet($table,$where)
	// {
	// 	for($i =0; $i<count($table);$i++) {
	// 			$query = $this->db->get_where($table[$i],$where);
	// 			//echo $this->db->last_query(); die();
	// 			$data[$table] =  $query->result();
	// 		}
	// 		echo "<pre>";
	// 			 print_r($data); die('testing');
	// 		return $data['result'];
		
	// }

	  function fileInfo1($id)
 	{
	 	$this -> db -> select('*');
	 	$this -> db -> from('tbl_file_info');
	 	$this -> db -> where('tbl_file_info.reseller_id',$id);
	 	$this->db->order_by("tbl_file_info.id", "desc"); 
	 	$query = $this -> db -> get();
	 	$files_company=  $query->result();
	 	$data['files'] = $files_company;
	 	return $files_company;
 	}



	// function filterSearch($id){
	// 	$this -> db -> select('*');
	// 	$this -> db -> from('tbl_company');
	// 	$this -> db -> where('tbl_company.id',$id); 	
	// 	$query = $this -> db -> get();
	// 	$user_company=  $query->row();
	// 	$data['user_company'] = $user_company;
	// 	return $user_company;
		
	// }
	
	// function fileInfo($id)
 // 	{
	//  	$this -> db -> select('*');
	//  	$this -> db -> from('tbl_file_info');
	//  	$this -> db -> where('tbl_file_info.create_user_id',$id);
	//  	$this->db->order_by("tbl_file_info.id", "desc"); 
	//  	$query = $this -> db -> get();
	//  	$files_company=  $query->result();
	//  	$data['files'] = $files_company;
	//  	return $files_company; 
 // 	}
 
 // function orders($id)
 // {

 // 	$this -> db -> select('*');
 // 	$this -> db -> from('tbl_order');
 // 	$this -> db -> where('tbl_order.company_id',$id); 
 // 	$query = $this->db->get();
 // 	$order=  $query->row();
 // 	//$data['order'] = $order;
 // 	//var_dump($order);die();
 // 	return $order;
 		
 // }

 // function CustomSearch2($table,$arr)
 // {
 // 	$query = $this->db->get_where($table,$arr);
 // 	$data['result'] = $query->row();
 // 	return $data['result'];
 // }
 // function userSearch($id)
 // {
 // 	$this ->db->where('id',$id); 
 // 	$query = $this->db->get('tbl_user');
 // 	$user_details=  $query->row();
 // 	return $user_details;
 	 
 // } 
 // 	function files_info_attach($id){
	// $this -> db -> select('*');
 // 	$this -> db -> from('tbl_file_info');
 // 	$this -> db -> where('tbl_file_info.create_user_id',$id);
 // 	$query = $this -> db -> get();
 // 	$files_company_attach=  $query->row();
 // 	$data['files'] = $files_company_attach;
 // 	return $files_company_attach;
	// }


 function Order_details_data($id)
 {
	//var_dump($id);die();
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_order_detail');
 	$this -> db -> where('tbl_order_detail.order_summery_id',$id); 
 	$query = $this -> db -> get();
 	$order_details=  $query->result();
	//echo $this->db->last_query();die();
 	//$data['user_details'] = $user_details;
 	return $order_details;
 	 
 }
 
 function billing($id)
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_billing_address');
 	$this -> db -> where('tbl_billing_address.id',$id); 
 	$query = $this -> db -> get();
 	$billing_details=  $query->row();
 	$data['billing_details'] = $billing_details;
 	return $billing_details;
 		
 }
 function mailling($id)
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_mailling_address');
 	$this -> db -> where('tbl_mailling_address.id',$id); 
 	$query = $this -> db -> get();
 	$mailling_details=  $query->row();
 	$data['mailling_details'] = $mailling_details;
 	return $mailling_details;
 		
 }
 

 function orderDetails($id)
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_order_detail');
 	$this -> db -> where('tbl_order_detail.order_summery_id',$id); 
 	$query = $this -> db -> get();
 	$order=  $query->result();
 	$data['order'] = $order;
 	return $order;
 		
 }

	function getUploadOptions($id = null){ 
		//$list = array("Upload","Received");
		$list = array("NO","YES");
		if ($id == null )	
			return $list;
		else
			return $list [ $id ];
	}
 
 
 

 function getTypeOptions($id = null)
 {
 	$list = array('0'=>"Companies House",'1'=>"HM Revenue & Customs",'2'=>"Government Gateway",'9'=>"HM Courts",'10'=>"Intellectual Property Office",'11'=>"Business Mail",'12'=>"Other Mail",'15'=>"Certificate of Incorporation",'16'=>"Memorandum",'17'=>"Articles of Association",'18'=>"First Minutes",'19'=>"Share Certificates");
 	if ($id == null )	return $list; 
 	if ( is_numeric( $id )) return $list [ $id ];
 	return $id;
 }
 function Message($id)
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_message');
 	$this -> db -> where('tbl_message.order_id',$id);
 	$this->db->order_by("tbl_message.id", "desc"); 
 	$query = $this -> db -> get();
 	$message_company=  $query->result();
 	$data['files'] = $message_company;
 	return $message_company;
 		
 }
 function resellersCount()
 {
 	$this->db->select('*');
 	$this->db->from('tbl_reseller');
	$this->db->where('tbl_reseller.state_id !=1');
 	$this->db->order_by("tbl_reseller.company_name", "asc" );	
 	$query = $this->db->get();
 	$resellers=  $query->num_rows();
  	return $resellers;
 	 
 }

function resellers()
{
	$this->db->select(' tbl_reseller.*,count(tbl_file_info.file_name)');
	$this->db->join ('tbl_file_info', 'tbl_reseller.id=tbl_file_info.reseller_id','LEFT');
	$this->db->where ( 'tbl_reseller.state_id !=1');
	$this->db->order_by ( "tbl_reseller.company_name", "asc" );	
	$this->db->limit(100);
	$query = $this->db->get('tbl_reseller');
 	$resellers=  $query->result();
 	echo "<pre>"; print_r($resellers); die('testing');
 	return $resellers;
}
// function resellers()
//  {
//  	$this -> db -> select('*');
//  	$this -> db -> from('tbl_reseller');
// 	$this->db->where ( 'tbl_reseller.state_id !=1');
// 	//$this->db->where ( 'tbl_reseller.state_id !=1');
//  	$this->db->order_by ( "tbl_reseller.company_name", "asc" );	
//  	$this->db->limit ( 100 );	
//  	$query = $this -> db -> get();
//  	$resellers=  $query->result();
//  	$data['resellers'] = $resellers;
//  	return $resellers;
 	 
//  }
 function slectparent_resellers()
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this->db->where ( 'tbl_reseller.state_id !=1');
	//$this->db->where ( 'tbl_reseller.parent_reseller == "0"');
	$this->db->where ( 'tbl_reseller.parent_reseller = " "');
 	$this->db->order_by ( "tbl_reseller.company_name", "asc" );	
 	$query = $this -> db -> get();
 	$resellers=  $query->result();
 	$data['resellers'] = $resellers;
 	return $resellers;
 	 
 }
 function resellersbyid($save_reseller_id)
 {
 	$this -> db -> select('company_name');
 	$this -> db -> from('tbl_reseller');
	$this->db->where ('id',$save_reseller_id);
 	$this->db->order_by ( "tbl_reseller.company_name", "asc" );	
 	$query = $this -> db -> get();
 	$resellers=  $query->row_array();
 	return $resellers;
 	 
 }
  function getServiceOptions($id = null)
 {
 	$list = array("Show All","Yes","No"); 
		
	if ($id == null )	return $list; 
 	if ( is_numeric( $id )) return $list [ $id ];
 	return $id;
 }
 function getLocationOptions($id = null)
 { 
 	$list = array("Show All","SE1","WC1","EH2","W1","IP4","EH3","GIB","DUB","EC4");	
 	if ($id == null )	return $list; 
 	if ( is_numeric( $id )) return $list [ $id ];
 	return $id;
	
 }
  function alterEmails($id)
 {
    	$this -> db -> select('*');
		$this -> db -> from('tbl_alt_email');
		$this -> db -> where('tbl_alt_email.company_id',$id);
		$query = $this -> db -> get();
		$emails=  $query->row();
		$data['emails'] = $emails;
 	return $emails;
 }
 function docTypeOptions($id = null)
 {
 	$list = array("4"=>"Identification","5"=>"Proof of Address","6"=>"Invoice","7"=>"Terms and Conditions","8"=>"Other");
 	if ($id == null )	return $list; 
 	if ( is_numeric( $id )) return $list [ $id ];
 	return $id;
 }
 function resellerUser( $id)
 {
 //var_dump($id);
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.user_id',$id); 
 	$query = $this -> db -> get();
 	$resellers =  $query->result();
	//var_dump($id);die;
 	$data['resellers'] = $resellers;
 	return $resellers;
 }
 function resellerUser_child( $id)
 {
    //var_dump($id);
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.parent_reseller',$id); 
 	$query = $this -> db -> get();
 	$resellers =  $query->result();
	//var_dump($id);die;
	//echo $this->db->last_query();die();
 	$data['resellers'] = $resellers;
 	return $resellers;
 }
  function getCompany( $id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_company');
	$this -> db -> where('tbl_company.id',$id); 
 	$query = $this -> db -> get();
 	$company=  $query->row();
 	$data['company'] = $company;
 	return $company;
 }
 #get data to send email for Id proof
 function getUser($id)
 {
	$this->db->select('create_user_id');
 	$this->db->from('tbl_company');
	$this->db->where('tbl_company.id',$id); 
 	$query=$this->db->get();
 	$company = $query->row_array();
	
	$this->db->select('id,email');
 	$this->db->from('tbl_user');
	$this->db->where('tbl_user.id',$company['create_user_id']); 
 	$query1=$this->db->get();
 	return $company1 = $query1->row_array();
 }
   function resellerEmail( $email)
 {
 //var_dump($email);die;
	$this -> db -> select('*');
 	$this -> db -> from('tbl_user');
	$this -> db -> where('tbl_user.email',$email); 
 	$query = $this -> db -> get();
 	$user =  $query->row();
 	$data['user'] = $user;
 	return $user;
 } 
    function userEmail( $email)
 {
 //var_dump($email);die;
	$this -> db -> select('*');
 	$this -> db -> from('tbl_user');
	$this -> db -> where('tbl_user.email',$email);	
 	$query = $this -> db -> get();
 	$user =  $query->row();
 	$data['user'] = $user;
 	return $user;
 } 
	function resellerByEmail( $email)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.mail_email',$email);
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$reseller_email =  $query->row();
 	$data['reseller_email'] = $reseller_email;
 	return $reseller_email;
 }
	function getMaillingAdd( $create_user_id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_mailling_address');
	$this -> db -> where('tbl_mailling_address.create_user_id',$create_user_id);
	$this->db->order_by("id", "desc");
	$this->db->limit(1); 
 	$query = $this -> db -> get();
 	$mailling =  $query->row();
 	$data['mailling'] = $mailling;
 	return $mailling;
 }
	function getResellerAdd( $create_user_id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.user_id',$create_user_id);	
 	$query = $this -> db -> get();
 	$mailling =  $query->row();
 	$data['mailling'] = $mailling;
 	return $mailling;
 }
   function getbillingAdd( $create_user_id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_billing_address');
	$this -> db -> where('tbl_billing_address.create_user_id',$create_user_id);
	$this->db->order_by("id", "desc");
	$this->db->limit(1); 
 	$query = $this -> db -> get();
 	$billing =  $query->row();
 	$data['billing'] = $billing;
 	return $billing;
 }
 
  function getBillingByTime( $create_time)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_billing_address');
	$this -> db -> where('tbl_billing_address.create_time',$create_time);
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$billing_id =  $query->row();
 	$data['billing_id'] = $billing_id;
 	return $billing_id;
 }
   function getMaillingByTime( $create_time)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_mailling_address');
	$this -> db -> where('tbl_mailling_address.create_time',$create_time);	
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$milling_id =  $query->row();
 	$data['milling_id'] = $milling_id;
 	return $milling_id;
 }
    function countResellerCompanies($id)
 {
 
 		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id','LEFT');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id','LEFT');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		//$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->where ( 'tbl_order.state_id != 0' );
		$this->db->where('tbl_reseller.id', $id);
		
	$query = $this->db->get();
	$user_orders = $query->num_rows();			
	return $user_orders;
			
 }  function countResellerCompaniesActive($id)
 {
	//$where = "name='Joe' AND status='boss' OR status='active'";
 // 	$this->db->select ( '*' );
	// $this->db->from ( 'tbl_order' );
 //    //$where = "(tbl_order.state_id = '0' OR tbl_order.state_id = '3') ";	
	// //$where = "name='Joe' AND status='boss' OR status='active'"
	// $this->db->where ( 'tbl_order.reseller_id',$id );
	// $this->db->where ( 'tbl_order.state_id = 0' );
	// //$this->db->where ($where);
	// $query = $this->db->get();
	$query = $this->db->get_where('tbl_order',['tbl_order.reseller_id'=>$id,'tbl_order.state_id'=> 0 ]);
	
	$user_orders = $query->num_rows();	
	return $user_orders;
			
 }
 
  function CountAll()
 {
 	$this->db->select ( '*' );
	$this->db->from ( 'tbl_order' );			
	//$this->db->where ( 'tbl_order.reseller_id',$id );
	$query = $this->db->get();
	$user_orders = $query->num_rows();			
	return $user_orders;
			
 }
function Countactive()
 {
 	$this->db->select ( '*' );
	$this->db->from ( 'tbl_order' );
    $this->db->where ( 'tbl_order.state_id =0');	
	//$this->db->where ( 'tbl_order.reseller_id',$id );
	$query = $this->db->get();
	$user_orders = $query->num_rows();			
	return $user_orders;
			
 }


	function getUserCreateTime($create_time)
 {
 	$this->db->select ( '*' );
 	$this->db->from ( 'tbl_user' ); 		
 	$this->db->where ( 'tbl_user.create_time',$create_time );
	$this->db->order_by("id", "desc");
 	$query = $this->db->get();
 	$user = $query->row(); 		
 	return $user;
 		
 }

	function getCompanyByTime($create_time)
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_company');
 	$this -> db -> where('tbl_company.create_time',$create_time);
	$this->db->order_by("id", "desc"); 
 	$query = $this->db->get();
 	$company=  $query->row();
 	$data['company'] = $company;
 	return $company;
 }
 
	function getOrderByTime( $create_time )
 {
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_order');
 	$this -> db -> where('tbl_order.create_time',$create_time);
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$order=  $query->row();
 	$data['order'] = $order;
 	return $order;
 }
	function resellerByUserId()
 {
 
	$session_data = $this->session->userdata ( 'logged_in' );
	$user_id = $session_data ['id'];
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.user_id',$user_id);
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$reseller_user =  $query->row();
 	$data['reseller_user'] = $reseller_user;
 	return $reseller_user;
 }
	function getResellerId( $reseller_name)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this->db->like ( "tbl_reseller.company_name ", $reseller_name );
	$query = $this -> db -> get();
 	$reseller_user =  $query->row();
	return $reseller_user->id;
 
 }
 	function companyReseller()
 {
 
	$session_data = $this->session->userdata ( 'logged_in' );
	$user_id = $session_data ['id'];	
	$this -> db -> select('*');
 	$this -> db -> from('tbl_order');
	$this->db->like ( "tbl_order.create_user_id ", $user_id );
	$query_order = $this -> db -> get();
 	$order_reseller =  $query_order->row();		
 	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.id',$order_reseller->reseller_id);
	$this->db->order_by("id", "desc"); 
 	$query = $this -> db -> get();
 	$reseller_user =  $query->row();
 	$data['reseller_user'] = $reseller_user;
 	return $reseller_user;
 }
 	function getResellerById($id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this->db->like ( "tbl_reseller.id ", $id );
	$query = $this -> db -> get();
 	$reseller_user =  $query->row();
	return $reseller_user;
 
 }
  function getStatusOptions($id =null){
	//  var_dump($id);die("id");
	//$list = array("Active","Suspend","Cancelled","Pending","Deleted","Pending ID","Submitted","Hold - ID","Hold - Deposit"); 
	$list = array("Active","Suspend","Cancelled","Pending","Deleted","Pending ID","In Review","Hold - ID","Hold - Deposit","Submitted","Active – No ID Required","Hold ROA","Hold DSA","Hold VBA","Pause"); 
		if ($id == null )	return $list;
		else
		return $list[$id];
	}
 function getStatusOptions1($id =null){
	//$list = array("Active","Suspend","Cancelled","Pending","Deleted","Pending ID","Submitted","Hold - ID","Hold - Deposit"); 
	$list = array("Pending","Competed","Cancelled","active"); 
		if ($id == null )	return $list;
		else
		return $list[$id];
	}
  function comp_tele_service($company_id)
 {
 
 	$this->db->select ('*');
 	$this->db->from ('tbl_company_tele_service');
 	$this->db->where('tbl_company_tele_service.company_id',$company_id);
 	$query_tele = $this->db->get();
 	$tele_service = $query_tele->row();
 	return $tele_service;
 
 }
  function comp_secretary($company_id)
 {
 
 	$this->db->select ('*');
 	$this->db->from ('tbl_company_secretaries');
 	$this->db->where('tbl_company_secretaries.company_id',$company_id);
	//$this->db->where('tbl_company_secretaries.state_id =1');
 	$query_tele = $this->db->get();
 	$tele_service = $query_tele->result();
 	return $tele_service;
 
 }
 
   function getUserByEmail($email)
 {
 
 	$this->db->select ('*');
 	$this->db->from ('tbl_user');
 	$this->db->where('tbl_user.email',$email);
 	$query = $this->db->get();
 	$user = $query->row();
 	return $user;
	 //echo $this->db->last_query();die();
 
 }	
 function getMaillingAdd_reseller( $create_user_id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.id',$create_user_id);
 	$query = $this -> db -> get();
 	$mailling =  $query->row();
 	$data['mailling'] = $mailling;
 	return $mailling;
 }
    function getbillingAdd_reseller( $create_user_id)
 {
	$this -> db -> select('*');
 	$this -> db -> from('tbl_reseller');
	$this -> db -> where('tbl_reseller.id',$create_user_id);	
 	$query = $this -> db -> get();
 	$billing =  $query->row();
 	$data['billing'] = $billing;
 	return $billing;
 }
 
   function comp_activity($company_id)
 {
 
 	$this->db->select ('*');
 	$this->db->from ('tbl_user_activity');
 	$this->db->where('tbl_user_activity.company_id',$company_id);
	$this->db->order_by("id", "desc");
	$this->db->limit(10); 
 	$query_tele = $this->db->get();
 	$activity = $query_tele->result(); 
 	return $activity;
 
 }
	public function fetch_deposit_details($comp_id){
		$this->db->select("*");
		$this->db->from("tbl_deposit");
		$this->db->where('company_id',$comp_id);
		$this->db->where ( 'tbl_deposit.type_id !=4 AND tbl_deposit.type_id !=5');
		/* $this->db->where('type_id','1');
		$this->db->where('type_id','2');
		$this->db->where('type_id','3'); */
		$this->db->order_by("deposit_id", "desc");
		$query_tele = $this->db->get();
		return $query_tele->result();
	}
	
	public function new_fetch_deposit_details($comp_id){
		$this->db->select("*");
		$this->db->from("tbl_deposit");
		$this->db->where('company_id',$comp_id);
		$this->db->where ( 'tbl_deposit.type_id !=1 AND tbl_deposit.type_id !=2 AND tbl_deposit.type_id !=3');
		//$this->db->order_by("deposit_id", "desc");
		$query_tele = $this->db->get();
		return $query_tele->result();
	}
	
	public function getUserCompnay($array)
    {
	$this->db->select('*');
 	$this->db->from('tbl_company');
	$this->db->where('id',$array['id']); 
 	$query=$this->db->get();
 	//$company = $query->row_array();
 	return $query->row_array();
 }
 public function getUserDetails($array)
    {
	//var_dump($array['create_user_id']);die();
	$this->db->select('*,tbl_user.id as id,tbl_billing_address.create_user_id as create_user_id');
 	$this->db->from('tbl_user');
	$this->db->join ( 'tbl_billing_address', 'tbl_billing_address.create_user_id=tbl_user.id' );
	$this->db->where('tbl_user.id',$array['create_user_id']); 
 	$query=$this->db->get();
 	//$company = $query->row_array();
 	 return $query->row_array(); 
	 //echo $this->db->last_query();die();
 }
  public function billingInfo($array)
    {
	$this->db->select('*');
 	$this->db->from('tbl_billing_address');
	$this->db->where('id',$array['billing_adress_id']); 
 	$query=$this->db->get();
 	//$company = $query->row_array();
 	return $query->row_array();
 }
 public function CustomOrderDetails($table,$arr,$fun,$order = null)
 {
 	//echo "dlksf"; die('dhl');
 	if($order != null){
 		$this->db->order_by($order);
 	}
 	$query = $this->db->get_where($table,$arr);
 	return $query->$fun();

 }
  public function orderInfo($array)
    {
	//var_dump($array);die();
	$this->db->select('*');
 	$this->db->from('tbl_order');
	$this->db->where('company_id',$array); 
 	$query=$this->db->get();
 	//$company = $query->row_array();
 	 return $query->row_array();
	 //echo $this->db->last_query();
 }
	public function orderInfoDetails($array){
		$this->db->select('*');
		$this->db->from('tbl_order_detail');
		$this->db->where('order_summery_id',$array);
        $this->db->order_by("id", "desc");		
		$query=$this->db->get();
		return $query->result_array();
	}
function order_compnay_id($id)
 {
 	$this->db->select ('company_id' );
	$this->db->from ( 'tbl_order' );			
	$this->db->where ( 'tbl_order.reseller_id',$id );
	//$this->db->where ( 'tbl_order.state_id =0');
	$query = $this->db->get();
	//$user_orders = $query->num_rows();			
	return $query->result_array();
			
 }
 function company_list($id){
        // var_dump($id);
		 $temp_id = array();
		foreach ($id as $comp_id ){
		 //var_dump($id);die();
		 //$temp_id[]= $comp_id;
		/* var_dump($comp_id);*/
		 	$this -> db -> select('company_name');
			$this -> db -> from('tbl_company');
			$this -> db -> where('tbl_company.id',$comp_id ["company_id"]); 	
			$query = $this -> db -> get();
			$temp_id[]=  $query->result(); 
		}
		
		return $temp_id;
	}

}
