<?php
Class Order extends CI_Model
{

 
	function getOrders($package_name,$field_id = null){
		$value = "";
		if($package_name === "Business Address".$field_id)
			$value = "business_address";
		elseif($package_name === "Complete Package".$field_id)
			$value = "complete_package";
		elseif($package_name === "Website Hosting".$field_id)
			$value = "hosting";
		elseif($package_name === "Director Service Address".$field_id)
			$value = "director_service_address";
		elseif($package_name === "Registered Office Address")
			$value = "registered_office";
		elseif($package_name === "Registered Office and Director Address".$field_id)
			$value = "both";
		elseif($package_name === "Postal Deposit".$field_id)
			$value = "deposit";
		elseif($package_name === "Partner Package".$field_id)
			$value = "partner";
		elseif($package_name === "Telephone Address".$field_id)
			$value = "telephone_service";
		elseif($package_name === "Telephone Answering Service".$field_id)
			$value = "telephone_service";
		elseif($package_name === "Virtual Business Address".$field_id)
			$value = "business_address";
		elseif($package_name === "Virtual Business Plus".$field_id)
			$value = "complete_package";
		elseif($package_name === "Partner Package".$field_id)
			$value = "partner_package";
		return $value;
	}
	
	function getOrderById($id){
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_order' );
		$this->db->where ( "tbl_order.id", $id);
		$query = $this->db->get ();
		$order_reseller = $query->row ();
		return $order_reseller->reseller_id;
	}
	function orderPrice($id){
		$totalPrice = 0;
		$this->db->select ( '*,tbl_order_detail.price' );
		$this->db->from ( 'tbl_order_detail' );
		$this->db->where ( "tbl_order_detail.order_summery_id", $id);
		$query = $this->db->get ();
		$user_orders = $query->result ();
		foreach($user_orders as $user_order)
		{
			$totalPrice += $user_order->price;
		}
		return number_format($totalPrice, 2);
	}
	function orderPrice_updated($id){
		$this->db->select ( 'updated_charge' );
		$this->db->from ( 'tbl_order' );
		$this->db->where ( "tbl_order.id", $id);
		$query = $this->db->get ();
		$order_updated = $query->row ();
		return $order_updated;
	}
	function getReseller($id){
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_reseller' );
		$this->db->where ( "tbl_reseller.id", $id);
		$query = $this->db->get ();
		$order_reseller = $query->row();
		return $order_reseller->company_name;
	}
	function getLtdOptions($id = null){
		$list = array("Show All","YES","NO"); 
		if ($id == null )	return $list; 
		if ( is_numeric( $id )) return $list [ $id ];
		return $id;
	}
	function getacessOptions($id = null){
		$list = array('2'=>"Show All",'1'=>"YES",'0'=>"NO"); 
		
		return $list;
	}
	function getpdfOptions($id = null){
		$list = array('1'=>"YES",'0'=>"NO",'2'=>"Show All"); 
		
		return $list;
	}
	function getTypeOptions($id = null){
		//echo $id; die;
		$list = array("Annual","Monthly"); 		
		if ($id == null )	return $list; 
		if (is_string( $id )) return $list [ $id ];
		return $id;
	}
	
	
	function countDir($user_orders){
		$c= 0;
		foreach ($user_orders as $key=>$user_order){
			if($user_order->director1 != ''){
				$c = $c+1;
			}
			if($user_order->director2 != ''){
				$c = $c+1;
			}
			if($user_order->director3 != ''){
				$c = $c+1;
			}
			if($user_order->director4 != ''){
				$c = $c+1;
			}
			if($user_order->first_name != '' || $user_order->last_name != ''){
				$c = $c+1;
			}
		}
		return $c; 
	}
	function counttrading($user_orders){
	
		$c= 0;
		foreach ($user_orders as $key=>$user_order){
			if($user_order->trading != ''){
				$c = $c+1;
			}
		}
		return $c; 
	}
	function countimporting($user_orders){
	 //var_dump($user_orders);die("jkhj");
		$c= 0;
		foreach ($user_orders as $key=>$user_order){
			if($user_order->form != ''){
				$c = $c+1;
			}
		}
		return $c; 
	}
	/*prefrence*/
	public function insertMailPreferences($arr){
		return $this->db->insert('tbl_handlings',$arr);
	}
	public function selectPreferences($id){
		$this->db->select("*");
		$this->db->from('tbl_handlings'); 
		$this->db->where('create_user_id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	public function UpdatePreferences($arr) {	
	//var_dump($arr);die();
	$this->db->where('create_user_id',$arr['create_user_id']);
	//unset($arr['create_user_id']);
		   return $this->db->update("tbl_handlings", $arr);
		  //$this->db->last_query();die();
	}
	/* update order table new state id*/
	public function orderstate_id_new($comp_id,$orderstate_id_new) {
    	$arr['state_id_new']=$orderstate_id_new;
	       $this->db->where('company_id',$comp_id);
		   return $this->db->update("tbl_order", $arr);
	}
	public function authcode($comp_id,$authcode) {
	//var_dump($comp_id);die("jkjknh");
    	   $arr['auth_code']= $authcode;
		   //var_dump($comp_id);die("update");
	       $this->db->where('company_id',$comp_id);
		    return $this->db->update("tbl_company_formation_details", $arr);
			//$this->db->last_query();die();
	}
	public function data_file_info($arr) {
    	return $this->db->insert('tbl_file_info',$arr);
	}
	public function insert_auth_code($id,$authcode) {
			$data = array(
			'company_id'=>$id,
			'auth_code'=>$authcode 
			);
			//var_dump($data);die("insert");
    	return $this->db->insert('tbl_company_formation_details',$data);
	}
}