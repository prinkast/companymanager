<?php
Class Search_model extends CI_Model
{
	#get the company info as per id  
	#fetch single row of company data

public $select = "tbl_order.*, tbl_reseller.company_name as reseller, tbl_company.company_name as company_name,tbl_company.comp_info,tbl_company_formation_details.company_name as form,tbl_company_formation_details.incorporation_date,tbl_company_formation_details.next_due_date,tbl_company_formation_details.next__due_return_dated,tbl_company_formation_details.company_status,tbl_company_formation_details.post_town,tbl_company_formation_details.company_number,tbl_user.email,tbl_user.Access";

/* CustomOrderInfo function is used for Get company data formation , get new OrderData , get new company info */
	public function CustomOrderInfo($table,$arr,$fun)
	{
		return $this->db->get_where($table,$arr)->$fun();
	}

	// public function Get_company_data_formation($company_id){
	// //var_dump($company_id);die("hjgjbjb");
	// 	$this->db->select("*");
	// 	$this->db->from("tbl_company_formation_details");
	// 	$this->db->where('company_id',$company_id);
	// 	$query=$this->db->get();
	// 	return $query->row();
	// }
	
	#show the list of all the reseller
	public function getResellerList_new(){
		return $this->db->get('tbl_reseller')->result();
	}
	
	#search fields as per 3 mail fields required
	public function getSearchAll_new_my($count=null,$limit=null,$search_query=null,$type=null)
	{
		
       $this->db->cache_on();		
		$fun =  $this->router->fetch_method();
		$this->db->select ($this->select);
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id','LEFT');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id','LEFT');
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','LEFT');
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($fun=='new_formation' || $fun=='companyNewFormation'){ 
			$this->db->where ( '(tbl_order.state_id = 6 OR tbl_order.state_id = 9) AND tbl_order.comp_ltd = 1' );
		}  
		if($fun=='state_change' || $fun=='companyStateChange' ){
			$this->db->where( 'tbl_order.state_id', '3' );
		}  
		if($fun=='state_change_id'|| $fun=='companyStateChange1'){
			$this->db->where('tbl_company.company_name' ,$search_query);  
			$this->db->where ( '(tbl_order.state_id = 7 OR tbl_order.state_id = 5)');
		}
		if($fun=='additional_serveics'){
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
			$this->db->where ( 'tbl_order.hosting = "Yes" ');
			$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
			$this->db->or_where ( 'tbl_order.Legal = "Yes" ');
		}
		if($type=="search_all_new"){ 
			$this->db->where('( tbl_company.company_name LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.trading LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.director1 LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.director2 LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.director3 LIKE "'.'%'.$search_query.'%'.'" OR tbl_company_formation_details.company_name LIKE "'.'%'.$search_query.'%'.'")');	
		}else if($type=="search_company_new"){
			$this->db->like('tbl_company.company_name', $search_query);
		}else if($type=="search_director_new"){
			$this->db->like('tbl_company.director1', $search_query);
			$this->db->or_like('tbl_company.director2', $search_query);
			$this->db->or_like('tbl_company.director3', $search_query);
		}
		if($count =='count'){
			$this->db->cache_off();
			return $this->db->get('tbl_order')->num_rows();
		}else if($count =='result'){
			$this->db->cache_off();
			return $this->db->limit($limit,0)->get('tbl_order')->result();
		}
	}
	public function getSearchAll_new($limit,$start,$search_query,$type){
        $this->db->cache_on();		
		$fun =  $this->router->fetch_method();
		$this->db->select ($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id','LEFT');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id','LEFT');
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','LEFT');
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->where ( 'tbl_reseller.state_id !=1');
		$this->db->limit(50);
		 if($fun=='new_formation' || $fun=='companyNewFormation'){ 
			$this->db->where ('tbl_order.state_id = 6 OR tbl_order.state_id = 9) AND tbl_order.comp_ltd = 1' );
		}  
		if($fun=='state_change' || $fun=='companyStateChange' ){ 
			$this->db->where( 'tbl_order.state_id', '3' );
		}  
		if($fun=='state_change_id'|| $fun=='companyStateChange1'){
		$this->db->where('tbl_company.company_name' ,$search_query);  
		$this->db->where ( '(tbl_order.state_id = 7 OR tbl_order.state_id = 5)');
		}
		if($fun=='additional_serveics'){
		$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4');
		$this->db->where ( 'tbl_order.hosting = "Yes" ');
		$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
		$this->db->or_where ( 'tbl_order.Legal = "Yes" ');
		}
		if($type=="search_all_new"){ 
			$this->db->where('( tbl_company.company_name LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.trading LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.director1 LIKE "'.$search_query.'" OR tbl_company.director2 LIKE "'.'%'.$search_query.'%'.'" OR tbl_company.director3 LIKE "'.'%'.$search_query.'%'.'" OR tbl_company_formation_details.company_name LIKE "'.'%'.$search_query.'%'.'")');	
		}else if($type=="search_company_new"){
			$this->db->like('tbl_company.company_name', $search_query);
		}else if($type=="search_director_new"){
			$this->db->like('tbl_company.director1', $search_query);
			$this->db->or_like('tbl_company.director2', $search_query);
			$this->db->or_like('tbl_company.director3', $search_query);
		} 
		$this->db->limit($limit,$start);
		$this->db->cache_off();
		return $this->db->get()->result();	
	}
	#count search fields as per 3 mail fields required
	public function countSearchAll_new($limit,$start,$search_query,$type){
		$this->db->cache_on(); 
		$this->db->select ('tbl_order.*,tbl_order.id as id,tbl_order.company_id as company_id,
		tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
		tbl_company.company_name as company_name , tbl_reseller.company_name as reseller,tbl_company_formation_details.company_name as form');
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id','LEFT');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id','LEFT');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','LEFT');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($type=="search_all_new"){   
			$this->db->like('tbl_company.company_name', $search_query);
			$this->db->or_like('tbl_company.trading', $search_query);
			$this->db->or_like('tbl_company.director1', $search_query);
			$this->db->or_like('tbl_company.director2', $search_query);
			$this->db->or_like('tbl_company.director3', $search_query);
		}else if($type=="search_company_new"){
			$this->db->like('tbl_company.company_name', $search_query);
		}else if($type=="search_director_new"){
			$this->db->like('tbl_company.director1', $search_query);
			$this->db->or_like('tbl_company.director2', $search_query);
			$this->db->or_like('tbl_company.director3', $search_query);
		}
		
		$query = $this->db->get ();
		$num = $query->num_rows();
        $this->db->cache_off();
		return $num;	  	 
	}
	
	#update the limited company status
	public function updateLimited($data){
		$this->db->where('id', $data['id']);
		return $this->db->update('tbl_order' ,$data);
	}
	
	#update the order status as per company id
	public function updateOrderStatus($data){
		$this->db->where('id',$data['company_id']);
		unset($data['company_id']);
		return $this->db->update('tbl_order',$data);
		//echo $this->db->last_query();die("gf");
	}
	
	#get the file images name 
	public function getIdData($company_id){
		return $this->db->where('company_id',$company_id)->get('tbl_file_info')->result();
	}
	public function getIdresslerData($resller_id){
		return $this->db->where('reseller_id',$resller_id)->get('tbl_file_info')->result();
	}
	
	
	public function Get_tbl_file_info_detail($company_id){
		//var_dump($company_id);die();
		$this->db->select("*");
		$this->db->from("tbl_file_info");
		$this->db->where('company_id',$company_id["company_id"]);
		$this->db->where('type_id',$company_id["state_id"]);
		$query=$this->db->get();
		$data = $query->num_rows();
		 return $data;
		//echo $this->db->last_query();die();
		//var_dump($data);die("df");
	}
	public function update_balnk_file($data){
	//var_dump($data);
	   $arr['type_id']= $data['state_id'];
		
		$this->db->where('company_id',$data['company_id']);
		unset($data['company_id']);
		 return $this->db->update('tbl_file_info',$arr);
		 
	}
	 public function insert_balnk_file($data){
	 //var_dump($data);die("jb");
		return $this->db->insert('tbl_file_info',$data);
	}
	function ressler_data($id)
	{
		//var_dump($id);die();
		$this->db->select("*"); 
		$this->db->from("tbl_company");
		$this->db->join("tbl_order","tbl_company.id = tbl_order.company_id","LEFT");
		$this->db->where('tbl_order.reseller_id',$id);
		$query=$this->db->get();
		return $query->result();
	}
	function ressler_data_active($id)
	{
		//var_dump($id);die();
		$this->db->select("*"); 
		$this->db->from("tbl_company");
		$this->db->join("tbl_order","tbl_company.id = tbl_order.company_id","LEFT");
		$this->db->where('tbl_order.reseller_id',$id);
		$this->db->where ( 'tbl_order.state_id =0');
		$query=$this->db->get();
		return $query->result();
	}
	function ressler_data_inactive($id)
	{
		//var_dump($id);die();
		$this->db->select("*"); 
		$this->db->from("tbl_company");
		$this->db->join("tbl_order","tbl_company.id = tbl_order.company_id","LEFT");
		$this->db->where('tbl_order.reseller_id',$id);
		$this->db->where ( 'tbl_order.state_id != 0');
		$query=$this->db->get();
		return $query->result();
		//echo $this->db->last_query();die();
	}
	
	function resellerUpdateData($user_id,$data_reseller1)
	{
	 $this->db->where('id', $user_id);
	 return $this->db->update('tbl_user' ,$data_reseller1);
	}
	function Getuserbyuserid($user_id)
	{
	//var_dump($user_id);
	$this->db->select("password");
	$this->db->from("tbl_user");
	 //$this->db->select("tbl_user.password");
	 $this->db->where('id',$user_id);
	 $query=$this->db->get();
	 return $query->row_array();
	}
	/* fetch resseeler name*/
	function ressler_Name($id)
	{ 
	//var_dump($id);die(); 
	$this->db->select("company_name");
	$this->db->from("tbl_reseller");
	 $this->db->where('id',$id);
	 $query=$this->db->get();
	 return $query->row_array();
	 
	}
	public function getCompanyDirector($companyId){
		//var_dump($companyId);die("Fds");
		//return 
		$this->db->select("*");
		$this->db->from("tbl_company_director");
		$this->db->where('company_id',$companyId);
		$query=$this->db->get();
		return $query->result_array();
		/* $this->db->where('company_id',$company_id)->get('tbl_company_director')->row_array();
		echo $this->db->last_query();die("Fs"); */
	}
	
	
	
	public function addtionalgetSearchAll_new($limit,$start,$search_query){
		//var_dump($start);die();
		$fun =  $this->router->fetch_method();
		$reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		$this->db->like('tbl_company.company_name',$search_query);
		//$this->db->where( 'tbl_order.hosting = "Yes" OR tbl_order.Legal = "Yes" OR tbl_order.company_register = "Yes"');
		//$this->db->or_where ( 'tbl_order.company_register = "Yes" ');
		//$this->db->or_where ( 'tbl_order.Legal = "Yes" ');
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($fun=='Hosting'){ 
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4 AND tbl_order.hosting = "Yes"');
		}  
		if($fun=='document_service'){ 
			$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		}  
		
		if($fun=='company_register'){ 
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4 AND  tbl_order.company_register = "Yes"');
		}  
		if($fun=='additional_serveics'){ 
			$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.hosting = "Yes" OR tbl_order.Legal = "Yes" OR tbl_order.company_register = "Yes")');
		}  
		$this->db->limit($limit, $start);
			$query = $this->db->get ();
			//echo $this->db->last_query();die(); 
			$result = $query->result();
 		return  $result ;
		}	
	}	
	public function countSearchAll_new_addtional($limit,$start,$search_query){
		//var_dump($start);die();
		$fun =  $this->router->fetch_method();
		$reseller ='';
		$reseller_ids = array();
	//var_dump($arra['role_id']);die('role_id');
		if($arra['role_id'] == 2)
		{
			$resellers = $this->search->resellerUser($arra['user_id']);

				foreach($resellers as $reseller)
				{
					$reseller_ids [] = $reseller->id;
				}
		}	
		if($arra['role_id'] == 1 || $reseller_ids !=''  || $arra['role_id'] == 0)
		{
		$this->db->select ($this->select);
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_company_formation_details', 'tbl_company_formation_details.company_id=tbl_order.company_id','left');
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
			$this->db->join('tbl_user','tbl_user.id=tbl_order.create_user_id');
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		
		$this->db->like('tbl_company.company_name',$search_query);
		$this->db->where ( 'tbl_reseller.state_id !=1');
		if($fun=='Hosting'){ 
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4 AND tbl_order.hosting = "Yes"');
		}  
		if($fun=='document_service'){ 
			$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.document_pack = "Yes" OR tbl_order.company_register = "Yes" OR tbl_order.good_standing = "Yes" OR tbl_order.good_standing_apostille = "Yes")');
		}  
		
		if($fun=='company_register'){ 
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4 AND  tbl_order.company_register = "Yes"');
		}  
		if($fun=='additional_serveics'){ 
			$this->db->where ( '(tbl_order.state_id !=2 AND tbl_order.state_id !=4) AND ( tbl_order.hosting = "Yes" OR tbl_order.Legal = "Yes" OR tbl_order.company_register = "Yes")');
		}  
		$query = $this->db->get ();
 		$num = $query->num_rows();
		return  $num ;
		}	
	}
	
}
