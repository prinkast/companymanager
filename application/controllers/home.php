﻿<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	// session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model('home_model');
		$this->load->model('search_model');
		$this->load->model('search');
		$this->load->model('order');
		$this->load->model('ltd_company');
		$this->load->model("ltd_model");
		$this->load->model('user');
		$this->load->library('calendar');
		$this->load->helper('download');
		$this->load->library('session');
		$this->load->library('pagination');
		//$this->output->enable_profiler(TRUE);
		//$this->load->library('PHPReport');
		
		//$this->session->set_flashdata('message', 'Sucessfully uploaded.');
	}
	function index() {
		if ($this->session->userdata ( 'logged_in' )) {
			$session_data = $this->session->userdata ( 'logged_in' );
			$user_id = $session_data ['id'];
			$role_id = $session_data ['role_id'];
			$reseller = '';
			$reseller_ids = array ();
			// var_dump($user_id,$role_id);die();
			if ($role_id === 2) {
				$resellers = $this->search->resellerUser ( $user_id );
				foreach ( $resellers as $reseller ) {
					$reseller_ids [] = $reseller->id;
				}
			}
			$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,
			tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
			tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
			$this->db->from ( 'tbl_order' );
			$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
			$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
			$this->db->order_by ( "tbl_company.company_name", "asc" );
			$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4' );
			$this->db->where ( 'tbl_reseller.state_id !=1' );
			if ($role_id === 0) {
				$this->db->where ( 'tbl_order.create_user_id', $user_id );
			} elseif ($role_id === 2) {
				$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
			}
			$query = $this->db->get ();
			$user_orders = $query->result ();
			$data ['username'] = $session_data ['first_name'];
			$data ['user_orders'] = $user_orders;
			$data ['role_id'] = $role_id;
			$this->load->view ( 'home_view', $data );
		} else {
			// If no session, redirect to login page
			redirect ( 'login', 'refresh' );
		}
	}
	
	function logout() {
		// var_dump($this->session->userdata ( 'logged_in' ));die;
		$this->session->unset_userdata ( 'logged_in' );
		$this->session->unset_userdata ( 'search_field' );
		$this->session->sess_destroy();
		redirect ( 'https://www.companymanager.online/', 'refresh' );
	}
	
	Public function search() {
		if ($this->session->userdata ( 'logged_in' )) {
			$data="";
			$page = $this->input->get_post('page');
			if($page==''){
				$data['pagef']= '50';
			}else{
				$data['pagef']= $page;
			}
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		
		$search_field = $this->input->post('search_field');
		if($search_field !=''){
			$search_field = $this->session->set_userdata('search_field',$search_field);
			$arr['search_field'] = $this->session->userdata('search_field');
		}else{
	 		$arr['search_field'] = $this->session->userdata('search_field');
		}
		/*----pagination start----*/
		$config["base_url"] = base_url() . "home/search";
		$config["total_rows"]  = $this->home_model->count_search($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$show_search= $this->home_model->show_search($config["per_page"],$page,$arr);
		$export ='1';
		$show_search2= $this->home_model->show_search($config["per_page"],$page,$arr,$export);
			if($_POST['export1']=='export'){
				$data1['user_orders'] = $show_search2;
				$this->load->view('spreadsheet_view', $data1);
			}else{
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_search;
				$data['user_orders1'] = $show_search2;
				$data['username'] = $session_data['first_name'];
				$data['record_count'] = $config["total_rows"];
				$this->load->view('home_view',$data);
			}
		}
	}
	public function search1() {
		if ($this->session->userdata ( 'logged_in' )) {
			$session_data = $this->session->userdata ( 'logged_in' );
			$user_id = $session_data ['id'];
			$role_id = $session_data ['role_id'];
			$reseller = '';
			$reseller_ids = array ();
			if ($role_id == 2) {
				$resellers = $this->search->resellerUser ( $user_id );
				foreach ( $resellers as $reseller ) {
					$reseller_ids [] = $reseller->id;
				}
			}
			$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id, 
			tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
			$this->db->from ( 'tbl_order' );
			$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
			$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
			$this->db->order_by ( "tbl_company.company_name", "asc" );
			// $this->db->where ( 'tbl_order.state_id !=4');
			$this->db->where ( 'tbl_reseller.state_id !=1' );
			if (isset ( $_POST["search_field"] )) {
			$setstate = $this->session->set_userdata('search_field',$_POST["search_field"]);
			
				$query = $_POST["search_field"];
				$this->db->like( "tbl_company.company_name ", $query );
				$this->db->or_like( "tbl_company.director1 ", $query );
				$this->db->or_like( "tbl_company.director2 ", $query );
				$this->db->or_like( "tbl_company.director3 ", $query );
				$this->db->or_like( "tbl_company.director4 ", $query );
				if ($role_id == 0) {
					$this->db->where ( 'tbl_order.create_user_id', $user_id );
				} elseif ($role_id == 2) {
					if ($resellers)
						$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
				}
				$query = $this->db->get ();
				$user_orders = $query->result ();
				// var_dump($this->db->last_query(),$user_orders,$role_id);die;
				$data ['username'] = $session_data ['email'];
				$data ['user_orders'] = $user_orders;
				$data ['role_id'] = $role_id;
				$data ['record_count'] = count($user_orders);
				$this->load->view ( 'home_view', $data );
			} elseif($_POST ["select_id"]) {
				$select_id = $_POST ["select_id"];
				$select_option = $_POST ["select_option"];
				$this->db->where ( 'tbl_order.' . $select_id, $select_option );
				if ($role_id == 0)
					$this->db->where ( 'tbl_order.create_user_id', $user_id );
				if ($role_id == 2)
					$this->db->where_in ( 'tbl_reseller.id', $reseller_ids );
					// $this->db->where ( 'tbl_reseller.id', $reseller->id );
				$query = $this->db->get ();
				$user_orders = $query->result ();
				$data ['username'] = $session_data ['email'];
				$data ['user_id'] = $user_id;
				$data ['user_orders'] = $user_orders;
				$data ['role_id'] = $role_id;
				$data ['record_count'] = count($user_orders);
			}
			else{
				 $page =$_POST['page']; //$this->input->post('page');
				 if($page==''){
						$data['pagef']= '50';
				 }else{
						$data['pagef']= $page;
				 }
				$session_data = $this->session->userdata ( 'logged_in' );
				$arr['user_id'] = $session_data['id'];
				$arr['role_id'] = $session_data['role_id'];		
				$arr['search_field'] = $this->session->userdata('search_field');		
				
				$config["base_url"] = base_url() . "home/search";
				$config["total_rows"]  = $this->home_model->count_search($arr);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$user_orders = $this->home_model->show_search($config["per_page"],$page,$arr);
				$export='1';
				$user_orders1 = $this->home_model->show_search($config["per_page"],$page,$arr,$export);
			
			}
		} else {
			// If no session, redirect to login page
			redirect ( 'login', 'refresh' );
		}
	}
	
	public function updateCompanyStatus(){
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data['id'];
		$email = $session_data['email'];
		$page =$_GET['page'];
		if($page==''){
			$data['pagef']= '50';
		}else{
			$data['pagef']= $page;
		}
		
		$da['company_id'] = $this->input->get('status_company_id');	
		$uri = $this->input->get('uri');
		$uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		//var_dump($com_uri);die();
		$da['state_id'] = $this->input->get('state_id');
		$da['update_time'] = date( 'Y-m-d h:i:s');
		//$update_time = date ( 'Y-m-d h:i:s');
		$order  = $this->search_model->getOrderData_new($da['company_id']);
		
		$da1['company_id'] = $this->input->get('file_company_id');
		$company_data  = $this->search_model->getCompanyInfo_new($da1['company_id']);
		$da1['state_id'] = $this->input->get('state_id');
		$user_email = $this->input->get('new_status_email');
		$currentdata = $this->input->get('currentdata');
		$yes = $this->input->get('yes');
		$update_stat = $this->search->getStatusOptions($da['state_id']);
		$session_data = $this->session->userdata('logged_in');
		$arr['query_string_1'] = $this->input->get('query_string_1');
		$arr['query_string_2'] = $this->input->get('query_string_2');
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		
		if($da['state_id']=='10'){
			$file_new =$this->search_model->Get_tbl_file_info_detail($da1); 
			if($file_new ==0){
				$da1['type_id'] = "10";
				$file_new1 =$this->search_model->insert_balnk_file($da1);
			}else{
				$file_new2 =$this->search_model->update_balnk_file($da);
			}
		}
		$updateOrderStatus = $this->search_model->updateOrderStatus($da);
		
		if($updateOrderStatus ){
			//var_dump($updateOrderStatus);die();
			if($yes=="yes"){
				$to = $this->input->get('new_status_email');
				if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
					$header = 'From: The London Office<post@theoffice support>'. "\r\n";	
				}elseif($order->location=='EH2' || $order->location=='EH3' ){
					$header = 'From: The Edinburgh Office<post@theoffice support>'. "\r\n";	
				}else{
					$header = 'From: The Office Support<post@theoffice support>'. "\r\n";	
				}
				
				$header .= 'MIME-Version: 1.0'."\r\n";
				$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$subject = 'The Office Support >'. $company_data->company_name .'> You Have Mail!';
				$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
				//$new_msg .= '<a style="margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
				//$new_msg .= "<br>";
				$new_msg .='<h3>You have a Request Original!</h3>'."<br><br>";
				$new_msg .='Hello ,'."<br><br>";		
				$new_msg .= 'We`re writing to let you know that you have a request original.'."<br><br>";
				$new_msg .= 'The message details of the request are:'."<br><br>";
				$new_msg .= 'Company Name : '. $company_data->company_name ."<br><br>";
				$new_msg .= 'Status Changed to : '. $update_stat ."<br><br>";
				$new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red; ">The Office Support Team</p>
		<p style="font-size: 8.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 8.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
				if (mail ($to,$subject,$new_msg,$header )) {
					//var_dump("suceess");die("gfdh");
				}
			}
			/* else{
				var_dump("else");die();
			} */
			$desc= "Status Changed from: ".$currentdata." to ".$update_stat."";
			$data_activity = array (
			'company_id' => $da1['company_id'],
			'activity' => "Account Status Change",
			'Description' => $desc,
			'create_time' => date ( 'Y-m-d h:i:s' ),
			'create_user_id' => $user_id,
			'email' => $email
			);
			if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				//var_dump("insert");die("hfh");
			}
			if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				redirect ( $com_uri, 'refresh' );
		}else{
			echo "error";
		}
	} 
	 
	public function updateCompany() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data['id'];
		$email = $session_data['email'];
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$company_id = $this->input->post('pc_id');
		$order  = $this->search_model->getOrderData_new($company_id);
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		//var_dump($order->location);die("Gd");
		$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
		
		$data['search_new'] = $this->input->get_post('query_string_2');
		if (isset ( $_POST ["company_id"] )) {
			// var_dump($order->location);die("1");
			$renewal = date ( "Y-m-d h:i:s", strtotime ( $_POST ['renewal'] ) );
			$company_updated_data['updated_log'] = $this->user->getCompany($_POST ["company_id"],$_POST);
			
			/* $order  = $this->search_model->getOrderData_new($company_id); */
			
			if($company_updated_data['updated_log']){
				$to = "update@thelondonoffice.com";
				$header .= 'MIME-Version: 1.0' . "\r\n";
				$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$subject ="Update > ".$_POST ["user_company"];
				$new_msg ="Company name: ".$_POST ["user_company"]."<br>";
				$new_msg .="Time of update: ".date ( "h:i:s")."<br>";
				$new_msg .="Date of update:".date ( "Y-m-d")."<br>";
				$new_msg .="What was updated:"."<br>";
				foreach($company_updated_data['updated_log'] as $company_updated){
					$new_msg .=$company_updated."<br>";
				}
				mail ( $to, $subject, $new_msg, $header );		
			}

			$data = array (
					'company_name' => addslashes($_POST ["user_company"]),
					'trading' => addslashes($_POST ["trade_name"]),
					'director1' => addslashes($_POST ["director1"]),
					'director2' => addslashes($_POST ["director2"]),
					'director3' => addslashes($_POST ["director3"]),
					'director4' => addslashes($_POST ["director4"]),
					'location' => $_POST ["location"] 
			);
			
			$this->db->where ( 'id', $_POST ["company_id"] );
			$this->db->update ( 'tbl_company', $data );
			$company_renew = array (
					'renewable_date' => $renewal 
			);
			$this->db->where ( 'company_id', $_POST ["company_id"] );
			
			if($this->db->update ( 'tbl_order', $company_renew )){
				$data_activity = array (
					'company_id' => $_POST ["company_id"],
					'activity' => "Company Details Updated",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id , 
					'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					
				}else{
						var_dump ( $this->db->_error_message () );
						die ();
				}
			}
		} else if (isset ( $_POST ["user_name"] )) {
			// var_dump($order->location);die("2");
			$renewal = date ( "Y-m-d h:i:s", strtotime ( $_POST ['renewal'] ) );
			$user_updated_data['updated_user'] = $this->user->getUser($_POST ["user_id"],$_POST,$_POST ["mailing_id"]);	
			$comp_info = $this->user->getCompanyinfo($_POST ["user_company_id"]);
						
			if($user_updated_data['updated_user']){
				$to = "update@thelondonoffice.com";
				$header .= 'MIME-Version: 1.0' . "\r\n";
				$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$subject ="Update > ".$comp_info->company_name;
				$new_msg ="Company name: ".$comp_info->company_name."<br>";
				$new_msg .="Time of update: ".date ( "h:i:s")."<br>";
				$new_msg .="Date of update:".date ( "Y-m-d")."<br>";
				$new_msg .="What was updated:"."<br>";
				foreach($user_updated_data['updated_user'] as $user_updated){
					$new_msg .=$user_updated."<br>";
				}
				mail ( $to, $subject, $new_msg, $header );		
			}
			
			$data_reseller = array (
					'contact_name' => $_POST ["user_name"],
					'admin_email' => $_POST ["user_email"],
					'phone_number' => $_POST ["user_contact"] 
			);
			$this->db->where ( 'user_id', $_POST ["user_id"] );
			$this->db->update ( 'tbl_reseller', $data_reseller );
			$data = array (
					'first_name' => $_POST ["user_name"],
					'last_name' => $_POST ["user_last_name"],
					'email' => $_POST ["user_email"],
					'ph_no' => $_POST ["user_daytime_contact"],
					'mobile' => $_POST ["user_contact"],
					'skpe_address' => $_POST ["user_skype"],
					'counrty' => $_POST ["user_county"],
					'county' => $_POST ["user_country"] 
			);
			$data_mailing = array (
					'street' => $_POST ["user_street"],
					'city' => $_POST ["user_city"],
					'county' => $_POST ["user_country"],
					'country' => $_POST ["user_county"],
					'postcode' => $_POST ["user_post_code"] 
			);
			$data_reseller = array(
				'mail_email' => $_POST ["user_email"],
			);
			
			$this->db->where ( 'user_id', $_POST ["user_id"] );
			$this->db->update ( 'tbl_reseller', $data_reseller );
			if (! empty ( $_POST ["user_password"] )) {
				$data_password = array (
						'password' => md5 ( $_POST ["user_password"] ) 
				);
				$this->db->where ( 'id', $_POST ["user_id"] );
				$this->db->update ( 'tbl_user', $data_password );
			}
			$this->db->where ( 'id', $_POST ["user_id"] );
			$this->db->update ( 'tbl_user', $data );
			$this->db->where ( 'id', $_POST ["mailing_id"] );
			$this->db->update ( 'tbl_mailling_address', $data_mailing );
				$data_activity = array (
					'company_id' => $_POST ["user_company_id"],
					'activity' => "Your Details Updated",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id , 
					'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}else {
					var_dump ( $this->db->_error_message () );die ();
				}
		} else if (isset ( $_POST ["billing_id"] )) {
			// var_dump($order->location);die("3");
			$comp_info = $this->user->getCompanyinfo($_POST ["billing_company_id"]);
			$billing_updated_data['updated_billing'] = $this->user->getBilling($_POST ["billing_id"],$_POST);			
			
			$data = array (
					'billing_name' => $_POST ["billing_name"],
					'street' => $_POST ["billing_street"],
					'city' => $_POST ["billing_city"],
					'country' => $_POST ["billing_country"],
					'county' => $_POST ["billing_county"],
					'postcode' => $_POST ["billing_post_code"] 
			);
			$this->db->where ( 'id', $_POST ["billing_id"] );
			if($this->db->update ( 'tbl_billing_address', $data )){
				if($billing_updated_data['updated_billing']){
					$to = "update@thelondonoffice.com";
					$header .= 'MIME-Version: 1.0' . "\r\n";
					$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$subject ="Update > ".$comp_info->company_name;
					$new_msg ="Company name: ".$comp_info->company_name."<br>";
					$new_msg .="Time of update: ".date ( "h:i:s")."<br>";
					$new_msg .="Date of update:".date ( "Y-m-d")."<br>";
					$new_msg .="What was updated:"."<br>";
					foreach($billing_updated_data['updated_billing'] as $billing_updated_data){
						$new_msg .= $billing_updated_data."<br>";
					}
					mail ( $to, $subject, $new_msg, $header );		
				}			
				$data_activity = array (
					'company_id' => $_POST ["billing_company_id"],
					'activity' => "Billing Information Updated",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id , 
					'email' => $email 
				);
					if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					}else {
							var_dump ( $this->db->_error_message () );
							die ();
					}
			}else {
					var_dump ( $this->db->_error_message () );die ();
			}
		} else if (isset ( $_POST ["mailing_id"] )) {
			/* var_dump($order->location);die("4"); */
			$data = array (
					'mailing_name' => $_POST ["mailing_name"],
					'street' => $_POST ["mailing_street"],
					'city' => $_POST ["mailing_city"],
					'country' => $_POST ["mailing_country"],
					'county' => $_POST ["mailing_county"],
					'postcode' => $_POST ["mailing_postcode"] 
			);
			$this->db->where ( 'id', $_POST ["mailing_id"] );
			if($this->db->update ( 'tbl_mailling_address', $data )){
				$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Mailing Information updated",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id , 
				 'email' => $email 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
			
			}
		} else if (isset ( $_POST ["c_id"] )) {
			/* var_dump($order->location);die("5"); */
			$data = array (
					'mailing_name' => $_POST ["mailing_name"],
					'street' => $_POST ["mailing_street"],
					'city' => $_POST ["mailing_city"],
					'country' => $_POST ["mailing_country"],
					'county' => $_POST ["mailing_county"],
					'postcode' => $_POST ["mailing_postcode"] 
			);
			$this->db->where ( 'id', $_POST ["mailing_id"] );
			$this->db->update ( 'tbl_mailling_address', $data );
		} elseif (isset ( $_POST ['state_id'] )) {
			// var_dump($_POST);die();
			$company_id = $_POST ['status_company_id'];
			$data_company = array (
					'state_id' => $_POST ['state_id'] 
			);
			$this->db->where ( 'id', $company_id );
			$this->db->update ( 'tbl_order', $data_company );
		}elseif (isset ( $_POST ['type_id'] )) {
			/* var_dump($order->location);die("6"); */
			$company_id = $_POST ['type_company_id'];
			$pc_id = $_POST ['pc_id'];
			$data_company = array (
					'type_id' => $_POST ['type_id'] 
			);
			$this->db->where ( 'id', $company_id );
			$this->db->update ( 'tbl_order', $data_company );
				if($_POST ['type_id']== '0'){
					$data_activity = array (
					'company_id' => $pc_id,
					'activity' => "Payment Status Changed",
					'Description' => "Annual Payment Selected",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id , 
					'email' => $email 
					);
				}else{
					$data_activity = array (
					'company_id' => $pc_id,
					'activity' => "Payment Status Changed",
					'Description' => "Monthly Payment Selected",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id , 
					'email' => $email 
					);
				}
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
		}
		
		if($data['new_search_bar'] != ""){
			$page = $this->input->get_post('page');
			$pageeses = $this->session->userdata('pageno');
			if($page==''){
				$data['pagef']  = '50';
			}else{
				$data['pagef']  = $page;
			}
			$config["base_url"] = base_url() . "dashboard/searchResult";
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
			$data['search_new'] = $this->input->get_post('query_string_2');
			if($data['search_new']=="search_all_new"){
				$type="search_all_new";
				$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
			}else if($data['search_new']=="search_company_new"){
				$type="search_company_new"; die("search_company_new");
				$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
			}elseif($data['search_new']=="search_director_new"){
				$type="search_director_new";die("search_director_new");
				$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
			}
			
			$config["base_url"] = base_url() . "dashboard/searchResult";
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['username'] = $session_data ['first_name'];			
			$data['role_id'] = $arr['role_id'];
			$data['user_orders'] = $show_companies;
			$data['record_count'] = $config["total_rows"] ;
			$this->load->view('home_view',$data);
		}else{
			redirect ( $com_uri, 'refresh' );		
		}
	}
	
	public function do_upload() {
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$comp_id = $this->input->get_post('comp_id');
		//var_dump($comp_id);die();
		$pdf_handel_ltd = $this->input->get_post('pdf_handel_ltd');
		$order = $this->search_model->getOrderData_new($comp_id);
		
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$company_num = trim($this->input->get_post('compnay_number'));
		if($company_num != ''){
			$arr2['id'] = $comp_id;
			$arr2['company_number'] = $company_num;
			$arr2['company_old_name'] = trim($this->input->get_post('comp_update_name'));
			//var_dump($arr2['company_old_name']);die();
			$updateCompanyAddress = $this->home_model->updateCompanyAddress($arr2);
			if($updateCompanyAddress){
		$arr1['company_id'] = $arr2['id'];   
        $arr1['company_number'] = $arr2['company_number'];
		//var_dump($arr1);die("jbjm");
		$qr = file_get_contents('https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk/company/'.$arr1['company_number'].'.json');
		$data = json_decode($qr);
		if($data  == ""){
			$res = array('Status'=>'0','message'=>'No data available');
		}else{
			$get_data = $this->home_model->Get_company_data($arr1['company_id']);
			 $register =  $data->date_of_creation;
    	    
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 
			 $relatedSlides = array();
			 $sitext= $data->sic_codes;
			
			 $i=0;
			  foreach($sitext as $key=>$v)
			  {
			 $relatedSlides[] = $v;
			  }
			 $relatedSlides = implode(",",$relatedSlides);
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 $account_ref_date = $AccountRefDay."/".$AccountRefMonth;
			 $li="https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk";
			 $links = $data->links->persons_with_significant_control;
			 $demo = $li.$links.".json";
			 $qr1 = file_get_contents($demo);
			 $data1 = json_decode($qr1);
			 $psc_data = $data1->items;
			 $get_data_1 = $this->home_model->check_psc_company_data($arr1['company_id']);
			 if(count($get_data_1) > 0){
                 $deletePscData = $this->home_model->deletePscData($arr1['company_id']);
			  }
			 foreach($psc_data as $item)
			  {
				$natures_of_control = $item->natures_of_control;
				$dob = $item->date_of_birth;
				$year = $dob->year;
				$month = $dob->month;
				$array2 = array( 
				  'company_id'=>$arr1['company_id'],
				  'first_name'=>$item->name_elements->forename,
				  'last_name'=>$item->name_elements->surname,
				  'name'=>$item->name,
				  'birth_month'=>$month,
				  'birth_year'=>$year,
				  'natures_of_control'=>$natures_of_control['0'],
				  'res_building_country'=>$item->address->country,
				  'res_building_postcode'=>$item->address->postal_code,
				  'building_address'=>$item->address->address_line_2,
				  'building_street'=>$item->address->address_line_1,
				  'building_name'=>$item->address->premises,
				  'nationality'=>$item->nationality,
				  'res_building_country'=>$item->address->locality,
				  'appoint_board'=>$item->notified_on,
				  'legal_authority'=>$item->identification->legal_authority,
				  'legal_form'=>$item->identification->legal_form,
				  'api'=>1,
				);
				$insert_person_data = $this->home_model->company_psc_data($array2);
			 }
			
			 $links1 = $data->links->officers;
			 $demo12 = $li.$links1.".json";
			 $qr2 = file_get_contents($demo12);
			 $data12 = json_decode($qr2);
			 $link1 = $data12->items;
			 $get_data_2 = $this->home_model->check_director_company_data($arr1['company_id']);
			 $get_data_sec = $this->home_model->check_sectory_company_data($arr1['company_id']);
			   if(count($get_data_2) > 0){
                 $deletedirectorData = $this->home_model->deletedirectorData($arr1['company_id']);
			  }
			  if(count($get_data_sec) > 0){
                 $deletesectoryData = $this->home_model->deletesectoryData($arr1['company_id']);
			  }
			 foreach($link1 as $item)
			  {
			  $names = $item->former_names;
			  $role = $item->officer_role;
			  $country = $item->address->country;
			  $postal_code = $item->address->postal_code;
			  $address_line1 = $item->address->address_line_1;
			  $address_line2 = $item->address->address_line_2;
			  $region = $item->address->region;
			  $locality = $item->address->locality;
			  $country_of_residence = $item->country_of_residence;
			  $nationality = $item->nationality;
			  $month = $item->date_of_birth->month;
			  $year = $item->date_of_birth->year;
			  $occupation = $item->occupation;
			  $appoint_date = $item->appointed_on;
			  $resigned_on = $item->resigned_on;
			  $building_street = $item->address->address_line_1;
			  $building_address = $item->address->address_line_2;
			  $building_name = $item->address->premises;
			  $name = $item->name;
			  $fname = substr($name, strpos($name, ",") + 1);
			  $lname = substr($name, 0, strpos($name, ','));
			  //var_dump($role);die();
			  if($name="")
			  {
				  foreach($names as $nam)
				  {
				  $fname = $nam->forenames;
				  $lname = $nam->surname;
				  }
			  }
			     $array1 = array( 
			     'company_id'=>$arr1['company_id'],
			      'first_name'=>$fname,
			     'last_name'=>$lname,
			     'nationality'=>$nationality,
			     'occupation'=>$occupation,
			     'res_building_address'=>$address_line1,
			     'country_residence'=>$country_of_residence,
			     'res_building_county'=>$locality,
			     'res_building_postcode'=>$postal_code,
			     'res_building_country'=> $country,
			     'birth_month'=> $month, 
			     'birth_year'=> $year,
			     'appoint_date'=> $appoint_date,
			     'resigned_on'=> $resigned_on,
			     'building_street'=> $building_street,
				 'building_name'=> $building_name,
			     'building_address'=> $building_address,
			  
			   'api'=>1,
			  );
			  if($role == "director")
			  {
			  
			  $insert_director_data = $this->home_model->company_director_data($array1);
			  //var_dump($insert_director_data);
			  }
			  elseif($role == "secretary")
			  {
			  //var_dump("secretary");
				$insert_sec_data = $this->home_model->company_sec_data($array1);
			  }
			}			
		$share_link = $data->links->filing_history;
		$share_capital = $li.$share_link.".json";
		$qr1 = file_get_contents($share_capital);
		$share_capital = json_decode($qr1);
		$share_capital2 = $share_capital->items;
		$get_data_3 = $this->home_model->check_shareholder_company_data($arr1['company_id']);
		if(count($get_data_3) > 0){
		$deleteshareholderData = $this->home_model->deleteshareholderData($arr1['company_id']);
						}
					foreach($share_capital2 as $item)
					{
					$dd = $item->associated_filings;
						foreach($dd as $d)
						{
						$capital = ($d->description_values->capital);
						$dd1 = ($d->type);
							foreach($capital as $d1)
							{
							$currency = $d1->currency;
							$figure = $d1->figure;
							}
						}
						$array3 = array( 
						'company_id'=>$arr1['company_id'],
						'share_class'=>$dd1,
						'currency'=>$currency,
						'per_person_shares'=>$figure,
						'api'=>1,
						);
						$insert_shareholder_data = $this->home_model->company_shareholder_data($array3);
				   }
				  
				$array = array( 
					'company_id'=>$arr1['company_id'],
					'company_name'=>$data->company_name,
					'incorporation_date'=>$register,
					'company_number'=>$data->company_number,
					'company_status'=>$data->company_status,
					'address_line1'=>$data->registered_office_address->address_line_1,
					'next_due_date'=>$data->confirmation_statement->last_made_up_to,
					'next__due_return_dated'=>$data->confirmation_statement->next_made_up_to,
					'address_line2'=>$data->registered_office_address->address_line_2,
					'country'=>$data->registered_office_address->country,
					'post_town'=>$data->registered_office_address->postal_code,
					'locality'=>$data->registered_office_address->locality, 
					'account_reference_date'=>$account_ref_date,
					'last_made_update'=>$data->accounts->last_accounts->made_up_to,
					'account_next_due'=>$data->accounts->next_accounts->period_end_on,
					'SIC_Codes'=>$relatedSlides, 
					'creation_date'=>$data->registered_office_address->country
			);
			if($get_data == 0){
			$new_company_name = trim($data->company_name);
			$dataupdate = array(
			'id'=>$arr1['company_id'],
			'company_name'=>$new_company_name,
			);
			//var_dump($dataupdate);die("if");
			$update_data_name = $this->home_model->update_company_data_name_import($dataupdate);
			//var_dump($update_data_name);die("if");
				$inserdata = $this->home_model->company_data($array);
				if($inserdata){
				$res = array('Status'=>'1','message'=>'Data imported');
				}else{
					$res = array('Status'=>'0','message'=>'error in insertion');
				} 
			}else{
				
			    $new_company_name = trim($data->company_name);
				$dataupdate = array(
				'id'=>$arr1['company_id'],
				'company_name'=>$new_company_name,
				);
				
				//var_dump($dataupdate);die("else");
				$update_data_name = $this->home_model->update_company_data_name_import($dataupdate);
				//var_dump($update_data_name);die("else");
				$update_data = $this->home_model->update_company_data($array);
				
			if($update_data){
				$res = array('Status'=>'1','message'=>'Data imported');
			}else{
				$res = array('Status'=>'0','message'=>'error in updation');
			}
			}
		}
		}
		}else{
			$new_company_name = trim($this->input->get_post('comp_update_name'));
			$arr4['id'] = $comp_id;
			$arr4['company_name'] = trim($this->input->get_post('comp_update_name'));
			$updateCompanyAddress = $this->home_model->updateCompanyAddress($arr4);
		}

		$items = []; 
		if(isset ( $_POST ["file_type_id"] )){
			$type_id = $_POST ["file_type_id"];
		}
		$check = false;
		$items = [];
		
		
		for($i=0;$i<=4;$i++){
			$cpt = count ( $_FILES ["userfile$i"] ['name'] );
			$ch = false;
			for($k = 0; $k < $cpt; $k ++) {
				if($_FILES["userfile$i"]['name'][$k]!=''){
					$check = true;
					$ch = true;
				}
			}
			if($ch){
				if($i==0){
						$items[] = 'Certificate of Incorporation ';	
				}if($i==1){
					$items[] = 'Memorandum';
				}if($i==2){
					$items[] = 'Articles of Association';
				}if($i==3){
					$items[] = 'First Minutes';
				}if($i==4){
					$items[] = 'Share Certificate';
				}
			}
		}
		
		if($check){
			$type_name = $this->search->getTypeOptions ( $type_id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_company' );
			$this->db->where ( 'tbl_company.id', $comp_id );
			$query_company = $this->db->get ();
			$detail_company = $query_company->row ();
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_alt_email' );
			$this->db->where ( 'tbl_alt_email.company_id', $comp_id );
			$query_emails = $this->db->get ();
			$alt_emails = $query_emails->result ();
			
			$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
			$this->db->from ( 'tbl_user' );
			$this->db->where ( 'tbl_user.id', $detail_company->create_user_id );
			$query_user = $this->db->get ();
			$user = $query_user->row ();
			//var_dump($user->first_name);die();
			$emails = array ();
			$emails ['email_1'] = $user->email;
			foreach ( $alt_emails as $alt_email){
				$emails ['email_2'] = $alt_email->email_2;
				$emails ['email_3'] = $alt_email->email_3;
				$emails ['email_4'] = $alt_email->email_4;
			}
			$files = $_FILES;
			
			
			$email_to = implode ( ',', $emails );
			$email_to = $email_to; // = implode(',', $emails);
			$subject = 'The Office Support >'. $new_company_name .'> You Have Mail!'; // subject of email
			$message ='';
					$message .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support  - You Have Mail!</span></br>
					<p style="">Dear '.$email123->first_name." ".$email->email123.',</p>
					<p style="">We are writing to let you know that you mail!</p>
					<p style="">We have scanned and attached your mail as requested.</p>
					<p style="">Should you require the original please <a href="https://theoffice.support">login to your online account and request the original.</a></p>
					<p style="">Should you wish to change your preference to receive mail withou attachments login to your account and follow these steps:</a></p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alerts" from the menu</p>
					<p>Step 3: Change to “Email Alerts Without Attachments"</p>
					';
					$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
			$boundary = md5 ( "sanwebe" );
			
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			$body = "--$boundary\r\n";
			$body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";
			$body .= chunk_split ( base64_encode ( $message ) );
			$this->load->library('upload');
			$files = $_FILES;
			
			for($i=0;$i<=4;$i++){
			
				if($i==0){
					$type_id= 15;
				}if($i==1){
					
					$type_id= 16;
				}if($i==2){
					
					$type_id= 17;
				}if($i==3){
					
					$type_id= 18;
				}if($i==4){
					
					$type_id= 19;
				}
			$cpt = count ( $_FILES ["userfile$i"] ['name'] );
			for($k = 0; $k < $cpt; $k ++) {
				if($files["userfile$i"]['name'][$k]!=''){
				
					$ext = pathinfo($files["userfile$i"]['name'][$k], PATHINFO_EXTENSION);
					$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files["userfile$i"]['name'][$k]);
					$new_data = str_replace  ("'", "", $withoutExt);
					$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
					
				//$filenamee = $files["userfile$i"]['name'][$k];
				//$filenameeOrigional = str_replace(' ', '', $filenamee);
				$_FILES ['userfile'] ['name'] = time().$new_data1.".".$ext;
				$_FILES ['userfile'] ['type'] = $files ["userfile$i"] ['type'] [$k];
				$_FILES ['userfile'] ['tmp_name'] = $files ["userfile$i"] ['tmp_name'] [$k];
				$_FILES ['userfile'] ['error'] = $files ["userfile$i"] ['error'] [$k];
				$_FILES ['userfile'] ['size'] = $files ["userfile$i"] ['size'] [$k];
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$company_name = $new_company_name;
					$UpdateCompanyName = $this->ltd_model->UpdateByCompnayName($comp_id,$company_name);
					$Get_company_data_formation =  $this->search_model->Get_company_data_formation($comp_id);
					//var_dump($Get_company_data_formation);die("data");
					$authcode = trim($this->input->get_post('authcode'));
					$orderstate_id_new = 1;
					$orderstate_id_new1 = $this->order->orderstate_id_new($comp_id,$orderstate_id_new);
					if($Get_company_data_formation)
					{
					//die("update");
					$authcode = $this->order->authcode($comp_id,$authcode);
					}
					else
					{
					//die("insert");
					$authcode = $this->order->insert_auth_code($comp_id,$authcode);
					}
					//var_dump($authcode);die();
					$data_file_info = array ( 
							'company_id' =>$comp_id,
							'file_name' => $_FILES ['userfile'] ['name'],
							'create_user_id' => $user_id,
							'create_time' => date ( 'Y-m-d h:i:s' ),
							'type_id' => $type_id,
							'compnay_name'=>trim($this->input->get_post('compnay_name')),
							'compnay_number'=>trim($this->input->get_post('compnay_number')),
							//'authcode'=>trim($this->input->get_post('authcode')),
					);
					//var_dump($data_file_info);die("jkhjhjh");
					$data_file_info1 = $this->order->data_file_info($data_file_info);
					
					if ($data_file_info1) {
					//echo "hello";die();
						
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$target_url = 'https://thelondonoffice.com/admin/fileupload.php';
								$post = array(
								'file_contents' => new CurlFile($file_tmp_name, $file_type, $file_name),
								);
								// Prepare the cURL call to upload the external script
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, $target_url);
								curl_setopt($ch, CURLOPT_HEADER, false);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT]']);
								curl_setopt($ch, CURLOPT_POST, true);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
						  		$result = curl_exec($ch);
								curl_close($ch);
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
					echo "Please upload mail";
				}
			}
			}
			}
			if($pdf_handel_ltd=="1")
			{
			$mail = mail($emails ['email_1'], $subject, $body, $headers );
			}
			else{
				$subject1 = 'The Office Support > Service Invoice'; // subject of email
					
					$headers1 = 'MIME-Version: 1.0' . "\r\n";					
					$headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers1 .= 'From: The Office Support<post@theoffice support>'. "\r\n"; 
			        $body1 ='';
					$body1 .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office support – Service Invoice</span></br>
					<p style="">Dear '.$email123->first_name." ".$email123->last_name.',</p>
					<p style="">Thank you for your order and payment.</p>
					<p style="">Please login to your account to view your invoice.</p>
					<p style="">To view the invoice <a href="https://theoffice.support/">login to your online account</a>, select the company then mail.</p>
					<p style="">Should you wish to change your preference to receive mail with attachments login to your account and follow these steps:</p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alert" from the menu</p>
					<p>Step 3: Change to "Email Alerts With Attachments"</p>
					';
					
					$body1 .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
					$mail = mail ($emails ['email_1'],$subject1,$body1,$headers1 );
			}
			if($mail){
			$data_activity = array ( 
						'company_id' => $comp_id,
						'activity' => "Document Upload (LTD)",
						'Description' => "Company Formation Documents Uploaded",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id,
						'email' => $email 
					);
					if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					$message = 'You Have Mail!';
					if($user->device_type == '1'){
							
							$msg_arr['device_token']=$user->device_token;
							$msg_arr['message']=$message;
							$msg_arr['code']='101';
							$sc = $this->iospushNotification($msg_arr,$message);
						}else if($user->device_type == '2'){
							$message1 = array('Status'=>'1','Code'=>'101','data'=>$message);
							$sc = $this->Androidnotification($user->device_token,$message1);
							//var_dump($sc);die();
						}
					}else{
						var_dump ( $this->db->_error_message () );
						die ();
					}
			//if(mail ('prinka.tuffgeekers@gmail.com', $subject, $body, $headers )){
				$page = $this->input->get_post('page');
				$pageeses = $this->session->userdata('pageno');
				if($page==''){
					$data['pagef']  = '50';
				}else{
					$data['pagef']  = $page;
				}
				$config["base_url"] = base_url() . "dashboard/searchResult";
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
				$data['search_new'] = $this->input->get_post('query_string_2');
				//var_dump($data);die("Gdf");
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				//echo "<script> alert('document uploaded')</script>";
				//echo $this->session->flashdata('message');
				$this->session->set_flashdata('message', 'Sucessfully uploaded.');
				$data['message'] = "Sucessfully uploaded.";
				$this->load->view('home_view',$data);
					
			}else{
				echo "Error throwing data";
			}
		} else {
			echo "Please upload mail";
		}
	}
	
	public function do_upload1() {
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email']; 
		$comp_id = $_POST ["comp_id"]; 
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id']; 
		
		$items = [];  
		if(isset ( $_POST ["file_type_id"] )){
			$type_id = $_POST ["file_type_id"];
		}
		
		if (! empty ( $_FILES ['userfile'] ['name']  )) {
			$type_name = $this->search->getTypeOptions ( $type_id );
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_company' );
			$this->db->where ( 'tbl_company.id', $comp_id );
			$query_company = $this->db->get ();
			$detail_company = $query_company->row ();
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_alt_email' );
			$this->db->where ( 'tbl_alt_email.company_id', $comp_id );
			$query_emails = $this->db->get ();
			$alt_emails = $query_emails->result ();
			
			$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
			$this->db->from ( 'tbl_user' );
			$this->db->where ( 'tbl_user.id', $detail_company->create_user_id );
			$query_user = $this->db->get ();
			$user = $query_user->row ();
			//var_dump($user->first_name);die();
			$emails = array ();
			$emails ['email_1'] = $user->email;
			foreach ( $alt_emails as $alt_email){
				$emails ['email_2'] = $alt_email->email_2;
				$emails ['email_3'] = $alt_email->email_3;
				$emails ['email_4'] = $alt_email->email_4;
			}
			$files = $_FILES;
			$cpt = count ( $_FILES ['userfile'] ['name'] );
			for($i = 0; $i < $cpt; $i ++) {
				if($files['userfile']['name'][$i]!=''){
					
				}
			}
			$email_to = implode ( ',', $emails );
			$email_to = $email_to; // = implode(',', $emails);
			$from_email = $team.' (donotreply@'.$mail_at.'.com)'; // sender email
			
			//$subject = $detail_company->company_name."> You have mail!"; // subject of email
			$subject ="Incorporation Accepted > ".trim($this->input->get_post('compnay_name')). " ( "  .trim($this->input->get_post('compnay_number'))." ) ";
			$message = "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Dear ".$user->first_name."</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Congratulations!" . "</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>We are pleased to advise you that your company," .trim($this->input->get_post('compnay_name'))." has been accepted for incorporation by Companies House.</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>NOTE: Although your company is now formed and we have received a registration number it can still take between 24 and 72 hours for Companies House to update their records." . "</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Your company details are shown below:"."</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Company Name:".trim($this->input->get_post('compnay_name'))."</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Company Number:".trim($this->input->get_post('compnay_number'))."</p>";
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Auth. Code:".trim($this->input->get_post('authcode'))."</p>";
			
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Status: Accepted</p>";
			
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Company Document's Attached:"."<br><br>";
			if(!empty($items)){
				$message .= "<ul>";
				foreach($items as $it){
					$message .= "<li>{$it}</li>";
				}
				$message .= "</ul>";
			}
	
			$message .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Thank you for using our service and we would like to wish you the best of luck with your new venture.<br>
           Best regards,"."</p>";
		    $message .= '<div style="display: inline-block; width: 100%;">';
			$message .= "<p style='width: 50%; float: left; font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>
			The London Office<br>
			85 Great Portland Street<br>
			First Floor<br>
			London<br>
			W1W 7LT<br><br>
			 
			Tel: +44 (0)207 183 3787<br>
			Email: mail@thelondonoffice.com<br>
			Web:  thelondonoffice.com<br>
			Skype: theoffice.support "."</p>";
			$message .= "<p style='width: 50%; float: left; font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The Edinburgh Office<br>
			Rose Street South Lane<br>
			Edinburgh<br>
			EH2 3JG<br><br>
			
			Tel: +44 (0)131 516 2337<br>
			Email: mail@theedinburghoffice.com<br>
			Web:  theedinburghoffice.com<br>
			Skype: theoffice.support</p>";
			$message .='</div>';
			$message .= '<div style="display: inline-block; width: 100%;">';
			$message .= '<img border="0" width="120" height="35" style="width:1.25in;height:.368in" src="https://thelondonoffice.com/beta//assets/images/iphon.png">';
			$message .= "<img border='0' width='120' height='35' style='width:1.25in;height:.368in' src='https://thelondonoffice.com/beta//assets/images/android.png'>";
			$message .='</div>';
			$message .= "<Disclaimer";
			$message .= "<p> <b>E-mails and any attachments from The London Office are confidential.</b>"."</p>";
			$message .= "<p>If you are not the intended recipient, please notify the sender immediately by replying to the e-mail, and then delete it without making copies or using it in any way."."</p>";
			$message .= "<p>Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before"."</p>";
			$message .= "<p>opening attachments, since The Registered Office  accepts no responsibility for loss or damage caused by software viruses."."</p>";
			$boundary = md5 ( "sanwebe" );
			
			$headers = "MIME-Version: 1.0\r\n";
			
			$headers = 'From: The London Office<post@thelondonoffice.com>' . "\r\n" .
						'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			$body = "--$boundary\r\n";
			$body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";
			$body .= chunk_split ( base64_encode ( $message ) );
			
			$userfiles = $_FILES["userfile"];
			$this->load->library('upload');
			$files = $_FILES;
			$cpt = count ( $_FILES ['userfile'] ['name'] );
			$items = []; 
			for($i = 0; $i < $cpt; $i ++) {
				if($files['userfile']['name'][$i]!=''){
				$ext = pathinfo($files['userfile']['name'][$i], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][$i]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				//$filenamee = $files['userfile']['name'][$i];
				//$filenameeOrigional = str_replace(' ', '', $filenamee);
				$_FILES ['userfile'] ['name'] = time().$new_data1.".".$ext;
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [$i];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$data_file_info = array (
							'company_id' => $comp_id,
							'file_name' => $_FILES ['userfile'] ['name'],
							'create_user_id' => $user_id,
							'create_time' => date ( 'Y-m-d h:i:s' ),
							'type_id' => $type_id,
							'compnay_name'=>trim($this->input->get_post('compnay_name')),
							'compnay_number'=>trim($this->input->get_post('compnay_number')),
							'authcode'=>trim($this->input->get_post('authcode')),
					);
					if ($this->db->insert ( 'tbl_file_info', $data_file_info )) {
						
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
					echo "Please upload mail";
				}
			}
			}
		
				 if(mail ( $email_to, $subject, $body, $headers )){
					$data_activity = array (
						'company_id' => $comp_id,
						'activity' => "Mail Uploaded",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id , 
						'email' => $email 
					);
					if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					$message = 'You Have Mail!';
					if($user->device_type == '1'){
							
							$msg_arr['device_token']=$user->device_token;
							$msg_arr['message']=$message;
							$msg_arr['code']='101';
							$sc = $this->iospushNotification($msg_arr,$message);
						}else if($user->device_type == '2'){
							$message1 = array('Status'=>'1','Code'=>'101','data'=>$message);
							$sc = $this->Androidnotification($user->device_token,$message1);
						}
					}else{
						var_dump ( $this->db->_error_message () );
						die ();
					}
				} 
			if(mail ($emails ['email_1'], $subject, $body, $headers )){
			//if(mail ('prinka.tuffgeekers@gmail.com', $subject, $body, $headers )){
				$page = $this->input->get_post('page');
				$pageeses = $this->session->userdata('pageno');
				if($page==''){
					$data['pagef']  = '50';
				}else{
					$data['pagef']  = $page;
				}
				$config["base_url"] = base_url() . "dashboard/searchResult";
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
				$data['search_new'] = $this->input->get_post('query_string_2');
				//var_dump($data);die("Gdf");
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				echo "<script> alert('documrnt uploaded')</script>";
				$this->load->view('home_view',$data);
					
			}else{
				echo "Error throwing data";
			}
		} else {
			echo "Please upload mail";
		}
		//redirect ( 'dashboard/index', 'refresh' );
	}
	public function doc_upload() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$comp_id = $_POST ["comp_id"];
		
		if (isset ( $_POST ["file_type_id"] )) {
			$type_id = $_POST ["file_type_id"];
		} else
			$type_id = 3;
		
		if (!empty ( $_FILES ['userfile'] ['name'] [0] )) {
			$type_name = $this->search->docTypeOptions( $type_id );
			$type_name_email ="";
			
			if($type_name == 'Identification')
				$type_name_email = 'Passport or ID Card';
			elseif($type_name == 'Proof of Address')
				$type_name_email = 'Utility Bill';
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_company' );
			$this->db->where ( 'tbl_company.id', $comp_id );
			$query_company = $this->db->get ();
			$detail_company = $query_company->row ();
			/* ======================================================= */
			/* ======================================================= */
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_alt_email' );
			$this->db->where ( 'tbl_alt_email.company_id', $comp_id );
			$query_emails = $this->db->get ();
			$alt_emails = $query_emails->result ();
			/* ======================User============================ */
			$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
			$this->db->from ( 'tbl_user' );
			$this->db->where ( 'tbl_user.id', $detail_company->create_user_id );
			$query_user = $this->db->get ();
			$user = $query_user->row ();
			$create_time = date ( 'Y-m-d h:i:s' );
			/* ======================User============================ */
			
			$subject = 'The Office Support >'. $detail_company->company_name .'> You Have Mail!'; // subject of email
			$message = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$message .= '<div>';
			$message .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
			$message .= '</div>';
			$message .= "<br>";
			$message .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$message .= "<br>";
			$message = 'Company Name : '.$detail_company->company_name. "<br><br>";
			$message .= "Date of Upload : " . date("Y-m-d",strtotime($create_time))."<br><br>";
			$message .= 'Time of Upload :'.date("h:i A",strtotime($create_time)).' <br><br>';
			$message .= $type_name_email.": <br><br><br>";
			$message .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red; ">The Office Support Team</p>
		<p style="font-size: 8.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 8.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
$message .= '</div>';
			$boundary = md5 ( "sanwebe" );
			// header
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n";
			// $headers .= "From: Nominee Services <mail@nomineeservices.co.uk>". "\r\n";
			$headers .= "Reply-To: Aditi" . "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			// plain text
			$body = "--$boundary\r\n";
			$body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";
			$body .= chunk_split ( base64_encode ( $message ) );
			
			/*=============*/
			
			$emails = array ();
			$emails ['email_1'] = $user->email;
			foreach ( $alt_emails as $alt_email ) {
				$emails ['email_2'] = $alt_email->email_2;
				$emails ['email_3'] = $alt_email->email_3;
				$emails ['email_4'] = $alt_email->email_4;
			}
			$email_to = implode ( ',', $emails );
			$email_to = $email_to; // = implode(',', $emails);
		
			$userfiles = $_FILES ["userfile"];
			$this->load->library ( 'upload' );
			$files = $_FILES;
			$cpt = count ( $_FILES ['userfile'] ['name'] );
			for($i = 0; $i < $cpt; $i ++) {
			    $ext = pathinfo($files['userfile']['name'][$i], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][$i]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				$_FILES['userfile']['name'] = time().'idcard' .$new_data1.".".$ext;
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [$i];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$data_file_info = array (
							'company_id' => $comp_id,
							'file_name' => $data['upload_data']["file_name"],
							'create_user_id' => $user_id,
							'create_time' => $create_time,
							'type_id' => $type_id 
					);
					if ($this->db->insert ( 'tbl_file_info', $data_file_info )) {
						/* ===================================== */
					
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						// attachment
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
						
						/* =================================================================== */
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
					echo "Please upload mail";
				}
			}
			if(mail ( 'id@thelondonoffice.com', $subject, $body, $headers ))
			{
			$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Document Uploaded",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id ,
				'email' => $email 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
			
			}
		} else {
			echo "Please upload mail";
		}
		redirect ( 'dashboard/index', 'refresh' );
	}
	
	public function uploadData(){
		if (!empty($_FILES['userfile']['name'])){
			var_dump($_FILES);
		}else{}
	}
	
	private function set_upload_options() {
		// upload an image options
		$config = array ();
		$path = $_SERVER['DOCUMENT_ROOT'];
		$config ['upload_path'] = $path.'/upload/';
		//$config ['upload_path'] = './../admin/uploads/';   //when the site is on the beta version use this path for uploading
		//$config ['upload_path'] = './uploads/';
		$config ['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
		$config ['max_size'] = '0';
		$config ['overwrite'] = FALSE;
		return $config;
	} 
	public function proof_upload() {
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data['id'];
		$email = $session_data['email'];
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$create_time = date('Y-m-d h:i:s');
		$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
		$data['search_new'] = $this->input->get_post('query_string_2');
		$c_id = $_POST["c_id"];
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		$order  = $this->search_model->getOrderData_new($c_id);
		//var_dump($order);die("gfd");
		if (!empty($_FILES['userfile']['name'])){
			
			//var_dump($data);die("Gfd");
			//$from_email = 'File Exchange<mail@theregisteredoffice.com>'; // sender email
			
			$message = 'Please find the attachment below' . "\r\n";
			$boundary = md5 ( "sanwebe" );
			
			$headers = "MIME-Version: 1.0\r\n";
			if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
				$headers = 'From: The London Office<post@theoffice support>'. "\r\n"; 
			}elseif($order->location=='EH2' || $order->location=='EH3' ){
				$headers = 'From: The Edinburgh Office<post@theoffice support>'. "\r\n"; 
			}else{
				$headers = 'From: The Office Support<post@theoffice support>'. "\r\n"; 
			}
			//$headers .= "From:" . $from_email . "\r\n";
			$headers .= "Reply-To: " . "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			
			$userfiles = $_FILES["userfile"];
			$company_id = $_POST["c_id"];
			$reseller_id = $_POST["r_id"];
			//var_dump($reseller_id);die();
			$company = $this->search->getCompany($company_id);
			$subject = 'The Office Support >'. $detail_company->company_name .'> You Have Mail!'; // subject of email
			$message .= '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$message .= '<div>';
			$message .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
			$message .= '</div>';
			$message .= "<br>";
			$message .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$message .= "<br>";
			$message .= "Company Name:".$company->company_name. "\r\n";
			$message .= "Date of Upload :".date("Y-m-d",strtotime($create_time)). "\r\n";
			$message .= "Time of Upload :".date("h:i A",strtotime($create_time)). "\r\n";
			$body = "--$boundary\r\n";
			$this->load->library('upload');
			$files = $_FILES;
			if (isset($_FILES['userfile'])){
				$ext = pathinfo($files['userfile']['name'][0], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][0]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				$_FILES['userfile']['name'] = time().'idcard' .$new_data1.".".$ext;
				//time().$new_data1.".".$ext;
				$_FILES['userfile']['type'] = $files['userfile']['type'][0];
				$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
				$_FILES['userfile']['error'] = $files['userfile']['error'][0];
				$_FILES['userfile']['size'] = $files['userfile']['size'][0];
				$configd['file_name'] = $_FILES['userfile']['name'];
				
				$this->upload->initialize($this->set_upload_options()); 

				if ($this->upload->do_upload()) {
					$data = array(
							'upload_data' => $this->upload->data() 
					);
					$data_file = array (
							'company_id' => $company_id,
							'reseller_id' => $reseller_id,
							'file_name' => $data['upload_data']["file_name"],
							'type_id' => '4',
							'create_user_id' => $company->create_user_id,
							'update_user_id' => $user_id,
							'create_time' => $create_time 
					);
					
					if($this->db->insert ( 'tbl_file_info', $data_file ))
					{
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$message .= "Passport or ID Card :".$file_name. "\r\n";
					
						$encoded_content = chunk_split ( base64_encode ( $content ) );
												
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
				}
				$ext = pathinfo($files['userfile']['name'][1], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][1]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				$_FILES['userfile']['name'] = time().'addproof' .$new_data1.".".$ext;
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [1];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [1];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [1];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [1];
				$configd ['file_name'] = $_FILES ['userfile'] ['name'];
				
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$data_file = array (
							'company_id' => $company_id,
							'reseller_id' => $reseller_id,
							'file_name' => $_FILES ['userfile'] ['name'],
							'type_id' => '5',
							'create_user_id' => $company->create_user_id,
							'update_user_id' => $user_id,
							'create_time' => $create_time 
					);
 					if($this->db->insert ( 'tbl_file_info', $data_file )){
					
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$target_url = 'https://thelondonoffice.com/admin/fileupload.php';
								$post = array(
								'file_contents' => new CurlFile($file_tmp_name, $file_type, $file_name),
								);
								// Prepare the cURL call to upload the external script
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, $target_url);
								curl_setopt($ch, CURLOPT_HEADER, false);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT]']);
								curl_setopt($ch, CURLOPT_POST, true);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
						  		$result = curl_exec($ch);
								curl_close($ch);
								$handle = fopen ( $file_tmp_name, "r" );
								$content = fread ( $handle, $file_size );
								fclose ( $handle );
						$message .= "Utility Bill :".$file_name. "\r\n";
						$message .= '</div>';
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
				}
			}
			$body .= "--$boundary\r\n";
			$body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";			
			$body .= chunk_split ( base64_encode ( $message ) );	
			if(mail ("id@thelondonoffice.com",$subject,$body,$headers )){
			$desc = $data_file['type_id'];
			if($data_file['type_id']=='4')
			{ $desc="Proof of id Uploaded";
				$data_activity = array (
					'company_id' => $company_id,
					'activity' => "ID Uploaded",
					'Description' => $desc,
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id ,
					'email' => $email 
				);
			}
			else
			{
				 $desc="Proof of Address  Uploaded";
				  $data_activity = array (
					'company_id' => $company_id,
					'activity' => "POA Upload",
					'Description' => $desc,
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id ,
					'email' => $email 
				);
			}
				
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					
				}else{
					var_dump ( $this->db->_error_message () );
					die ();
				}
			}
			//if($data['new_search_bar'] == ""){
				$page = $this->input->get_post('page');
					$pageeses = $this->session->userdata('pageno');
					if($page==''){
						$data['pagef']  = '50';
					}else{
						$data['pagef']  = $page;
					}
					$config["base_url"] = base_url() . "dashboard/searchResult";
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
					$data['search_new'] = $this->input->get_post('query_string_2');
					//var_dump($data);die("Gdf");
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						//var_dump($show_companies);die("search_all_new");
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new"; die("search_company_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}elseif($data['search_new']=="search_director_new"){
						$type="search_director_new";die("search_director_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					
					$config["base_url"] = base_url() . "dashboard/searchResult";
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['username'] = $session_data ['first_name'];			
					$data['role_id'] = $arr['role_id'];
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					$this->session->set_flashdata('message', 'Sucessfully uploaded.');
				    $data['message'] = "Sucessfully uploaded.";
					if($data['new_search_bar'] == "")
						redirect ($com_uri, 'refresh' );
					else				
						$this->load->view('home_view',$data);
					
					
			//}
		} else {
			echo "Please upload your identity";
		}
		
		//redirect ( 'dashboard/index', 'refresh' );
	}

	public function getUserResellerProof() {
		
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data['id'];
		$email = $session_data['id'];
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$create_time = date('Y-m-d h:i:s');
		$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
		$data['search_new'] = $this->input->get_post('query_string_2');
		if (!empty($_FILES['userfile']['name'])){
			
			//var_dump($data);die("Gfd");
			$from_email = 'File Exchange<mail@theregisteredoffice.com>'; // sender email
			
			$message = 'Please find the attachment below' . "\r\n";
			$boundary = md5 ( "sanwebe" );
			
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n";
			$headers .= "Reply-To: " . "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			
			$userfiles = $_FILES["userfile"];
			$company_id = $_POST["c_id"];
			$reseller_id = $_POST["r_id"];
			//var_dump($reseller_id);die();
			$company = $this->search->getCompany($company_id);
			$subject = 'The Office Support >'. $detail_company->company_name .'> You Have Mail!'; // subject of email
			$message .= '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$message .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$message .= "<br>";
			$message .= "Company Name:".$company->company_name. "\r\n";
			$message .= "Date of Upload :".date("Y-m-d",strtotime($create_time)). "\r\n";
			$message .= "Time of Upload :".date("h:i A",strtotime($create_time)). "\r\n";
			$body = "--$boundary\r\n";
			$this->load->library('upload');
			$files = $_FILES;
			if (isset($_FILES['userfile'])){
				$ext = pathinfo($files['userfile']['name'][0], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][0]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				$_FILES['userfile']['name'] = time().'idcard' .$new_data1.".".$ext;
				$_FILES['userfile']['type'] = $files['userfile']['type'][0];
				$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
				$_FILES['userfile']['error'] = $files['userfile']['error'][0];
				$_FILES['userfile']['size'] = $files['userfile']['size'][0];
				$configd['file_name'] = $_FILES['userfile']['name'];
				
				$this->upload->initialize($this->set_upload_options()); 

				if ($this->upload->do_upload()) {
					$data = array(
							'upload_data' => $this->upload->data() 
					);
					$data_file = array (
							'company_id' => $company_id,
							'reseller_id' => $reseller_id,
							'file_name' => $data['upload_data']["file_name"],
							'type_id' => '4',
							'create_user_id' => $company->create_user_id,
							'update_user_id' => $user_id,
							'create_time' => $create_time 
					);
					if($this->db->insert ( 'tbl_file_info', $data_file ))
					{
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$message .= "Passport or ID Card :".$file_name. "\r\n";
					
						$encoded_content = chunk_split ( base64_encode ( $content ) );
												
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
				}
				$_FILES ['userfile'] ['name'] = time () . 'addproof' . $files ['userfile'] ['name'] [1];
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [1];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [1];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [1];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [1];
				$configd ['file_name'] = $_FILES ['userfile'] ['name'];
				
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$data_file = array (
							'company_id' => $company_id,
							'reseller_id' => $reseller_id,
							'file_name' => $_FILES ['userfile'] ['name'],
							'type_id' => '5',
							'create_user_id' => $company->create_user_id,
							'update_user_id' => $user_id,
							'create_time' => $create_time 
					);
 					if($this->db->insert ( 'tbl_file_info', $data_file )){
					
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						$message .= "Utility Bill :".$file_name. "\r\n";
						$message .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red; ">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$message .= '</div>';
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
				}
			}
			$body .= "--$boundary\r\n";
			$body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";			
			$body .= chunk_split ( base64_encode ( $message ) );	
			if(mail ("id@thelondonoffice.com",$subject,$body,$headers )){
				$data_activity = array (
					'company_id' => $company_id,
					'activity' => "Proof Uploaded",
					'create_time' => date ( 'Y-m-d h:i:s' ),
					'create_user_id' => $user_id ,
					'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					
				}else{
					var_dump ( $this->db->_error_message () );
					die ();
				}
			}
			//if($data['new_search_bar'] == ""){
				$page = $this->input->get_post('page');
					$pageeses = $this->session->userdata('pageno');
					if($page==''){
						$data['pagef']  = '50';
					}else{
						$data['pagef']  = $page;
					}
					$config["base_url"] = base_url() . "dashboard/searchResult";
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
					$data['search_new'] = $this->input->get_post('query_string_2');
					//var_dump($data);die("Gdf");
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						//var_dump($show_companies);die("search_all_new");
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new"; die("search_company_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}elseif($data['search_new']=="search_director_new"){
						$type="search_director_new";die("search_director_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					
					$config["base_url"] = base_url() . "dashboard/searchResult";
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['username'] = $session_data ['first_name'];			
					$data['role_id'] = $arr['role_id'];
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					/*if($data['new_search_bar'] == "")
						redirect ( 'dashboard/index', 'refresh' );
					else*/
						redirect ( 'home/manageReseller', 'refresh' );
						//$this->load->view('manageReseller',$data);
			//}
		} else {
			echo "Please upload your identity";
		}
		
		//redirect ( 'dashboard/index', 'refresh' );
	}
	
	function message() {
		
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$email1 = $session_data ['email'];
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		//var_dump($email);die();
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$order_id = $_POST ["order_post_id"];
		$email = "";
		
		$this->db->select ( '*,tbl_order.id as id' );
		$this->db->from ( 'tbl_order' );
		$this->db->where ( 'tbl_order.id', $order_id );
		$query_order = $this->db->get ();
		$order = $query_order->row ();
		//var_dump($order);die("gfd");
		/* ======================User============================ */
		$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
		$this->db->from ( 'tbl_user' );
		$this->db->where ( 'tbl_user.id', $order->create_user_id );
		$query_user = $this->db->get ();
		$user = $query_user->row ();
		/* ======================User============================ */
		/* =====================Alt emails============== */
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_alt_email' );
		$this->db->where ( 'tbl_alt_email.company_id', $order->company_id );
		$company_id = $order->company_id;
		$query_emails = $this->db->get ();
		$alt_emails = $query_emails->result ();
		$emails = array ();
		$emails ['email_1'] = $user->email;
		foreach ( $alt_emails as $alt_email ) {
			$emails ['email_2'] = $alt_email->email_2;
			$emails ['email_3'] = $alt_email->email_3;			
			$emails ['email_4'] = $alt_email->email_4;
		}
		
		$email_to = implode ( ',', $emails );
		//var_dump($email_to);die();
		
		$email = $email_to;
	
		$data_file_info = array (
			'order_id' => $order_id,
			'message' => $_POST ["message_post"],
			'to_id' => $_POST ["to_id"],
			'from_id' => $_POST ["from_id"],
			'email_id' => $_POST ["_email"],
			'tel_no' => $_POST ["tel_no"],
			'create_time' => date ( 'Y-m-d h:i:s' ),
			'create_user_id' => $user_id 
		);
		//var_dump($data_file_info);die("hgf");
		if ($this->db->insert ( 'tbl_message', $data_file_info )) {
			
			$url = base_url();
			if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
				$header = 'From: The London Office<post@theoffice support>'. "\r\n";
				$team = 'The London Office';				
			}elseif($order->location=='EH2' || $order->location=='EH3' ){
				$header = 'From: The Edinburgh Office<post@theoffice support>'. "\r\n"; 
				$team = 'The Edinburgh Office';
			}else{
				$header = 'From: The London Office<post@theoffice support>'. "\r\n"; 
				$team = 'The London Office';
			}
			//$header = 'From: '.$team.'<noreply@thelondonoffice.com>' . "\r\n";
			$to = $email; // "mail@theregisteredoffice.com";//$this->search->getMessage ( $order_id );
			$header .= 'MIME-Version: 1.0' . "\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			//$subject = $_POST ["company_name"]." > You have a message!" ;
			$subject = 'The Office Support >'. $_POST ["company_name"] .'> You Have Mail!'; 
			$subject = 'The Office Support – You have a message!”'; 
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$new_msg .= '<div>';
			$new_msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
			$new_msg .= '</div>';
			$new_msg .= "<br>";
			$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$new_msg .= "<html>
						<head>
						<title>".$team."</title>
						</head>
						<body>
							<h3><p style='font-size: 22pt; font-family: Calibri; margin: 6px 0px;'>You have a message!</p></h3>
							<hr>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello " . $_POST ["to_id"] . ", 	</p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We're writing to let you know that you have a message. </p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The message details of the call are:</p>
	
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Call from:</b> " . $_POST ["from_id"] . "   </p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Call to:</b> " . $_POST ["to_id"] . "</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Telephone number:</b> " . $_POST ["tel_no"] . "</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Message:</b> " . $_POST ["message_post"] . "</p>

							
					</body>
					</html>";
				$new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo;  we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';	
			if (mail ( $to, $subject, $new_msg, $header )){
			
				$data_activity = array (
						'company_id' => $company_id,
						'activity' => "TAS Message",
						'Description' => $_POST ["from_id"],
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id ,
						'email' => $email1 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					$page = $this->input->get_post('page');
					$pageeses = $this->session->userdata('pageno');
					if($page==''){
						$data['pagef']  = '50';
					}else{
						$data['pagef']  = $page;
					}
					$config["base_url"] = base_url() . "dashboard/searchResult";
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
					$data['search_new'] = $this->input->get_post('query_string_2');
					//var_dump($data);die("Gdf");
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						//var_dump($show_companies);die("search_all_new");
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new"; die("search_company_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}elseif($data['search_new']=="search_director_new"){
						$type="search_director_new";die("search_director_new");
						$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					
					$config["base_url"] = base_url() . "dashboard/searchResult";
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['username'] = $session_data ['first_name'];			
					$data['role_id'] = $arr['role_id'];
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					$this->load->view('home_view',$data);
					$message = 'You Have Message!';
					if($user->device_token != ""){
						if($user->device_type == '1'){
							
							$msg_arr['device_token']=$user->device_token;
							$msg_arr['message']=$message;
							$msg_arr['code']='101';
							//die("gfd");
							$sc = $this->iospushNotification($msg_arr,$message);
							
						}else if($user->device_type == '2'){
							$message1 = array('Status'=>'1','Code'=>'101','data'=>$message);
							$sc = $this->Androidnotification($user->device_token,$message1);
						}
					}
				}
				else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
				
				//redirect ( 'dashboard/index', 'refresh' );
			}
		} else {
			die ( "not done" );
		} 
		
		redirect ($com_uri, 'refresh' );
	}
	
	function state_change() {
 		 $data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		 $data['title'] ="PENDING ORDERS";
		 if ($session_data = $this->session->userdata('logged_in')) {
		 $state_change = $this->input->get('state_change');
		$config["base_url"] = base_url() . "home/state_change";
		$config["total_rows"]  = $this->home_model->count_state_change_new('count',$state_change,$config["per_page"]);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
	    $show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];	
					if($data['new_search_bar']!=""){
						$show_companies=$this->search_model->getSearchAll_new_my('result',$config["per_page"],$data['new_search_bar'],$data['search_new']);
						//var_dump($show_companies);die();
					}
					
					if(!empty($show_companies)){
						
						$data['user_orders'] = $show_companies;
						$config["total_rows"] = count($show_companies);
						$data['record_count'] = $config["total_rows"] ;
						$this->load->view ( 'home_view', $data );
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$data['user_orders'] = $show_companies;
						$config["total_rows"] = count($show_companies);
						$data['record_count'] = $config["total_rows"] ;
						$this->load->view ( 'home_view', $data );
					}
					else if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$data['user_orders'] = ""; 
						$data['record_count'] = 0;
						$this->load->view ( 'home_view', $data );
					}
					else if($this->input->post('export1')=='export'){
						if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$show_companies = $this->home_model->count_state_change_new('result',$state_change,$config["per_page"],$page,'export');
					$data['user_orders'] = $show_companies;
					$this->load->view('spreadsheet_view', $data);
					}else{
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$show_companies = $this->home_model->count_state_change_new('result',$state_change,$config["per_page"],$page);
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					$this->load->view ( 'home_view', $data );
					}
					
					
		 }
		 else{
			 redirect ( 'login', 'refresh' );
			 }
	}
		function state_change_id() {
		$data['title'] ="PENDING ID";
 		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$state_change = $this->input->get('state_change');
		$url = $this->input->post('result');
		$sc = explode ( ',', $state_change );
	
		if($state_change !=''){
			$setstate = $this->session->set_userdata('state_change',$sc);
			$arr['state_change'] = $this->session->userdata('state_change');
			//var_dump($arr['state_change']);die();
		}else{
	 		$arr['state_change'] = $this->session->userdata('state_change');
		}
	$config["total_rows"]  = $this->home_model->count_state_change($arr);
		$config["base_url"] = base_url() . "home/state_change_id";
		
		//$config["total_rows"]  = $this->home_model->count_state_change($arr);
		
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
	     $show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}elseif($data['search_new']=="search_director_new"){
						$type="search_director_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->show_state_change($config["per_page"],$page,$arr,$export);
		if(!empty($show_companies)){
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					 if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$user_orders = "";
						$config["total_rows"] = 0;
					}
					$this->pagination->initialize($config);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
		$this->load->view ( 'home_view', $data );
/* 		if($url[1]=="companyView")
		{
		$this->load->view ( 'company_view', $data );
		}
		else
		{
 		$this->load->view ( 'home_view', $data );
		} */
		}
	}
	function companyStateChange() {
 		 $page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		 $data['title'] = "PENDING ORDERS";
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$state_change = $this->input->get('state_change');
		//var_dump($state_change);die("order");

		$url = $this->input->post('result');
		//var_dump($url[1]);die();
		$sc = explode ( ',', $state_change );
		
		//var_dump($sc);die("Fd");
		if($state_change !=''){
			$setstate = $this->session->set_userdata('state_change',$sc);
			$arr['state_change'] = $this->session->userdata('state_change');
		}else{
	 		$arr['state_change'] = $this->session->userdata('state_change');
		}
	
		$config["base_url"] = base_url() . "home/companyStateChange";
		
		
		$config["total_rows"]  = $this->home_model->count_state_change($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
	    $show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];	
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new"; die("search_company_new");
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}elseif($data['search_new']=="search_director_new"){
						$type="search_director_new";die("search_director_new");
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			
					$user_orders = $this->home_model->show_state_change($config["per_page"],$page,$arr);
					$export='1';
					$user_orders1 = $this->home_model->show_state_change($config["per_page"],$page,$arr,$export);
					
					if(!empty($show_companies)){
						$user_orders = $show_companies; 
						$config["total_rows"] = count($show_companies);
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					 if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$user_orders = "";
						$config["total_rows"] = 0;
					}
					$this->pagination->initialize($config);
					if($_POST['export1']=='export'){
						$data1['user_orders'] = $user_orders1;
						$this->load->view('spreadsheet_view', $data1);
					}else{
					//count_reseller
					$data ['role_id'] = $arr['role_id'];
					$data['user_orders'] = $user_orders;
					$data['user_orders1'] = $user_orders1;
					$data['username'] = $session_data['first_name'];
					$data['record_count'] = $config["total_rows"] ;
					$data['state_change'] = $arr['state_change'];
					$this->load->view ( 'company_view', $data );

					}
	}
	function companyStateChange1() {
 		  $page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		 $data['title'] = "PENDING ID";
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$state_change = $this->input->get('state_change');
		$url = $this->input->post('result');
		$sc = explode ( ',', $state_change );
	
		if($state_change !=''){
			$setstate = $this->session->set_userdata('state_change',$sc);
			$arr['state_change'] = $this->session->userdata('state_change');
		}else{
	 		$arr['state_change'] = $this->session->userdata('state_change');
		}
	   $config["total_rows"]  = $this->home_model->count_state_change($arr);
		$config["base_url"] = base_url() . "home/companyStateChange1";
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
	     $show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->show_state_change($config["per_page"],$page,$arr,$export);
		if(!empty($show_companies)){
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					 if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$user_orders = "";
						$config["total_rows"] = 0;
					}
					$this->pagination->initialize($config);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
		$this->load->view ( 'company_view', $data );
		}
	}
	function companyNewFormation() {
 		 $page =$_POST['page'];
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page; 
		 }
		  $data['title'] = "NEW FORMATIONS";
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		
		$config["base_url"] = base_url() . "home/companyNewFormation";
		$config["total_rows"]  = $this->home_model->countNewFormStatus($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_director_new"){
						$type="search_director_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
	           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->newFormationData($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->newFormationData($config["per_page"],$page,$arr,$export);
		if(!empty($show_companies)){
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					 if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$user_orders = "";
						$config["total_rows"] = 0;
					}
					$this->pagination->initialize($config);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller 
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
 		$this->load->view ( 'company_view', $data );
		}
	}
	
	function state_change1() {
 		 $page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$arr['state_change'] = $this->input->get('state_change');
		//var_dump($state_change);die("state_change");
		$arr['query_string_1'] = $this->input->get('query_string_1');
		$arr['query_string_2'] = $this->input->get('query_string_2');
		if($arr['state_change'] !=''){
			$setstate = $this->session->set_userdata('state_change',$arr['state_change']);
			$arr['state_change'] = $this->session->userdata('state_change');
		}else{
	 		$arr['state_change'] = $this->session->userdata('state_change');
		} 
		
		$config["base_url"] = base_url() . "home/state_change1";
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config["total_rows"]  = $this->home_model->count_state_change($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->show_state_change($config["per_page"],$page,$arr,$export);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
 		$this->load->view ( 'home_view', $data );
		}
	}
	function new_formation() {
 		 $page =$_POST['page'];
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page; 
		 }
		 $data['title'] = "New Formations";
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		
		$config["base_url"] = base_url() . "home/new_formation";
		$config["total_rows"]  = $this->home_model->countNewFormStatus($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$show_companies = [];
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
					$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_company_new"){
						$type="search_company_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}else if($data['search_new']=="search_director_new"){
						$type="search_director_new";
						$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
	           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->newFormationData($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->newFormationData($config["per_page"],$page,$arr,$export);
		if(!empty($show_companies)){
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					else if(isset($data['search_new']) && empty($show_companies))
					{
						$user_orders = $show_companies;
						$config["total_rows"] = count($show_companies);
					}
					 if(isset($data['new_search_bar']) && $data['new_search_bar']=="")
					{
						$user_orders = "";
						$config["total_rows"] = 0;
					}
					$this->pagination->initialize($config);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller 
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
 		$this->load->view ( 'home_view', $data );
		}
	} 
	function addtional_services() {
 		 
 		$this->load->view ( 'home_view', $data );
		}
	
	
	public function updateOrder() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$email = $session_data['email'];
		$user_id = $session_data['$user_id'];
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		
		if (isset ( $_POST ['renew_company_id'] )) {
			$order_id = $_POST ["get_order_id"];
			$renewlcompid = $_POST ["renewlcompid"];
			$renewlname = $_POST ["renewlname"];
			$renewal_date_current = date ( 'Y-m-d h:i:s', strtotime ($renewlname) );
			//var_dump($renewal_date_current);die("current");die("kjhk");
			$renewal_date = date ( 'Y-m-d h:i:s', strtotime ( $_POST ['renew_company_id'] ) );
			$renew_date = date ( 'Y-m-d h:i:s');
			$data1 = array (
				'renewable_date_next' => $renewal_date_current,
				'renew_date' => $renew_date
				//'renewable_date_next' => $renewal_date				
			);
			$this->db->where ( 'id', $order_id );
			$this->db->update ( 'tbl_order', $data1 );
			//var_dump($data1);die();
			$data = array (
				'renewable_date' => $renewal_date
				//'renewable_date_next' => $renewal_date				
			);
			$this->db->where ( 'id', $order_id );
			$this->db->update ( 'tbl_order', $data );
			$renewal_date1 = date ("d-m-Y",strtotime($renewal_date));
			$desc= "Renewal Changed form: ".$renewlname." to " .$renewal_date1."";
				$data_activity = array (
				'company_id' => $renewlcompid,
				'activity' => "Renewal Date Change",
				'Description' =>$desc,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
		}
		if (isset ( $_POST ['location_company_id'] )) {
			$order_location_id = $_POST ["location_company_id"];
			$addresscomp_id = $_POST ["addresscomp_id"];
			$address = $_POST ["address"];
			//var_dump($addresscomp_id);die("flj");
			$radioGroup = $_POST ["radioGroup"];
			$va = '';
			if ($radioGroup == "option1")
				$va = "SE1";
			elseif ($radioGroup == "option2") 
				$va = "WC1";
			elseif ($radioGroup == "option3")
				$va = "EH2";
			elseif ($radioGroup == "option4")
				$va = "W1";
			elseif ($radioGroup == "option5")
			$va = "IP4";
			elseif ($radioGroup == "option6")
			$va = "EH3";
			elseif ($radioGroup == "option7")
			$va = "EC1";
			elseif ($radioGroup == "option8")
			$va = "DUB";
			
			$data_location = array (
					'location' => $va 
			);
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_order' );
			$this->db->where ( 'tbl_order.id', $order_location_id );
			$query = $this->db->get ();
			$orders_company = $query->row ();
			
			$this->db->where ( 'tbl_order.id', $order_location_id );
			if ($this->db->update ( 'tbl_order', $data_location )) {
				
				$this->db->where ( 'tbl_company.id', $orders_company->company_id );
				if ($this->db->update ( 'tbl_company', $data_location )) {
					// die("done");
				} else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
			} else {
				var_dump ( $this->db->_error_message () );
				die ();
			}
			$desc= "Office Changed from: ".$address." to " .$va."";
				$data_activity = array (
				'company_id' => $addresscomp_id,
				'activity' => "Office Location Changed",
				'Description' =>$desc,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
		}
		if (isset ( $_POST ['reseller_id'] )) {
		             
			$order_reseller_id = $_POST ["reseller_id"];
			$save_reseller_id = $_POST ["save_reseller_id"];
			$resellername = $_POST ["resellername"];
			$resellercom_id = $_POST ["resellercom_id"];
			$resellers =  $this->search->resellersbyid($save_reseller_id);
			//var_dump($resellers['company_name']);die("hjhgjg");
			$data = array (
					'reseller_id' => $save_reseller_id 
			);
			$this->db->where ( 'id', $order_reseller_id );
			$this->db->update ( 'tbl_order', $data );
			$desc= "Reseller Changed from: ".$resellername." to " .$resellers['company_name']."";
				$data_activity = array (
				'company_id' => $resellercom_id,
				'activity' => "Reseller Account Change",
				'Description' =>$desc,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
		}
			$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
			$data['search_new'] = $this->input->get_post('query_string_2');
		if($data['new_search_bar'] != ""){
				$page = $this->input->get_post('page');
				$pageeses = $this->session->userdata('pageno');
				if($page==''){
					$data['pagef']  = '50';
				}else{
					$data['pagef']  = $page;
				}
				$config["base_url"] = base_url() . "dashboard/searchResult";
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				
				//var_dump($data);die("Gdf");
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				redirect( $com_uri, 'refresh' );
				//$this->load->view('home_view',$data);
			
		}else 
			redirect( $com_uri, 'refresh' );
	}
	
	
	
	public function do_upload123() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$uri = $this->input->get_post('uri');
		$pdf_handel = $this->input->get_post('pdf_handel');
        $uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		$comp_id = $_POST ["comp_id"];
		$comp_user_id = $_POST ["final_comp_id"];
		$type_mail = $_POST ["type_of_mail"];
		$email123 = $this->home_model->user_email($comp_user_id);
		if (isset ( $_POST ["file_type_id"] )) {
			$type_id = $_POST ["file_type_id"];
		} else  
			$type_id = 3;
	 
		$order  = $this->search_model->getOrderData_new($comp_id);
		if (! empty ( $_FILES ['userfile'] ['name'] [0] )) {
			$type_name = $this->search->getTypeOptions ( $type_id );
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_company' );
			$this->db->where ( 'tbl_company.id', $comp_id );
			$query_company = $this->db->get ();
			$detail_company = $query_company->row ();
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_alt_email' );
			$this->db->where ( 'tbl_alt_email.company_id', $comp_id );
			$query_emails = $this->db->get ();
			$alt_emails = $query_emails->result ();
			$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
			$this->db->from ( 'tbl_user' );
			$this->db->where ( 'tbl_user.id', $detail_company->create_user_id );
			$query_user = $this->db->get ();
			$user = $query_user->row ();
			$emails = $user->email;
			$email_to = $emails;
			//$from_email = 'Office Support<orders@theoffice.support>';
			$subject = 'The Office Support >'. $detail_company->company_name .'> You Have Mail!'; // subject of email
			if($pdf_handel==1)
			{
			        $message ='';
					$message .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support  - You Have Mail!</span></br>
					<p style="">Dear '.$email123->first_name." ".$email->email123.',</p>
					<p style="">We are writing to let you know that you mail!</p>
					<p style="">We have scanned and attached your mail as requested.</p>
					<p style="">Should you require the original please <a href="https://theoffice.support">login to your online account and request the original.</a></p>
					<p style="">Should you wish to change your preference to receive mail withou attachments login to your account and follow these steps:</a></p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alerts" from the menu</p>
					<p>Step 3: Change to “Email Alerts Without Attachments"</p>
					';
					$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
			
			$boundary = md5 ( "sanwebe" );
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n";
			$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
			$body = "--$boundary\r\n";
			$body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body .= "Content-Transfer-Encoding: base64\r\n\r\n";
			$body .= chunk_split ( base64_encode ( $message ) );
			$userfiles = $_FILES ["userfile"];
			$this->load->library ( 'upload' );
			$files = $_FILES;
			$cpt = count ( $_FILES ['userfile'] ['name'] );
			for($i = 0; $i < $cpt; $i ++) {
				$ext = pathinfo($files['userfile']['name'][$i], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][$i]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				//$filenamee = $files['userfile']['name'][$i];
				//$filenameeOrigional = str_replace(' ', '', $filenamee);
				//$filenameeOrigional = str_replace(' ', '', $filenamee);
				$_FILES ['userfile'] ['name'] = time().$new_data1.".".$ext;
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [$i];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [$i];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [$i];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [$i];
				$this->upload->initialize ( $this->set_upload_options () );
				if ($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
					$data_file_info = array (
							'company_id' => $comp_id,
							'file_name' => $_FILES ['userfile'] ['name'],
							'create_user_id' => $comp_user_id,
							'create_time' => date ( 'Y-m-d h:i:s' ),
							'type_id' => $type_id 
					);
					if ($this->db->insert ( 'tbl_file_info', $data_file_info )) {
						$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
						$file_name = $_FILES ['userfile'] ['name'];
						$file_size = $_FILES ['userfile'] ['size'];
						$file_type = $_FILES ['userfile'] ['type'];
						$file_error = $_FILES ['userfile'] ['error'];
						$target_url = 'https://thelondonoffice.com/admin/fileupload.php';
								$post = array(
								'file_contents' => new CurlFile($file_tmp_name, $file_type, $file_name),
								);
								// Prepare the cURL call to upload the external script
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, $target_url);
								curl_setopt($ch, CURLOPT_HEADER, false);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT]']);
								curl_setopt($ch, CURLOPT_POST, true);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
						  		$result = curl_exec($ch);
								curl_close($ch);
						$handle = fopen ( $file_tmp_name, "r" );
						$content = fread ( $handle, $file_size );
						fclose ( $handle );
						
						$encoded_content = chunk_split ( base64_encode ( $content ) );
						$body .= "--$boundary\r\n";
						$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
						$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
						$body .= "Content-Transfer-Encoding: base64\r\n";
						$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
						$body .= $encoded_content;
					}
				} else {
					$error = array (
							'error' => $this->upload->display_errors () 
					);
					echo "Please upload mail";
				}
			}
			}
			else{
				    $headers = 'MIME-Version: 1.0' . "\r\n";					
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n"; 
			        $body ='';
					$body .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support  - You Have Mail!</span></br>
					<p style="">Dear '.$email123->first_name." ".$email123->last_name.',</p>
					<p style="">We are writing to let you know that you mail!</p>
					<p style="">We have scanned and uploaded the item of mail to your online account.</p>
					<p style="">To view the mail <a href="https://theoffice.support/">login to your online account</a>, select the company then mail.</p>
					<p style="">Should you wish to change your preference to receive mail with attachments login to your account and follow these steps:</p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alert" from the menu</p>
					<p>Step 3: Change to "Email Alerts With Attachments"</p>
					';
					
					$body .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
			}
			if(mail ($email_to, $subject, $body, $headers )){
				if($_POST ["type_of_mail"]=="omail"){
					$data_activity = array (
						'company_id' => $comp_id,
						'activity' => "Scan and Email (official)",
						'description' => "Official Mail Scanned and Emailed",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id ,
						'email' => $email 
					);
				}else{
					$data_activity = array (
						'company_id' => $comp_id,
						'activity' => "Scan and Email (Business)",
						'description' => "Business Mail Scanned and Emailed",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id , 
						'email' => $email , 
					);
				}
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					$message = 'You Have Mail!';
					if($user->device_type == '1'){
						$msg_arr['device_token']=$user->device_token;
						$msg_arr['message']=$message;
						$msg_arr['code']='108';
						$sc = $this->iospushNotification($msg_arr,$message);
					}else if($user->device_type == '2'){
							$message1 = array('Status'=>'1','Code'=>'108','data'=>$message);
							$sc = $this->Androidnotification($user->device_token,$message1);
					}
					$page = $this->input->get_post('page');
				$pageeses = $this->session->userdata('pageno');
				if($page==''){
					$data['pagef']  = '50';
				}else{
					$data['pagef']  = $page;
				}
				$config["base_url"] = base_url() . "dashboard/searchResult";
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
				$data['search_new'] = $this->input->get_post('query_string_2');
				//var_dump($data);die("Gdf");
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				//echo "<script> alert('document uploaded')</script>";
				//echo $this->session->flashdata('message');
				$this->session->set_flashdata('message', 'Sucessfully uploaded.');
				$data['message'] = "Sucessfully uploaded.";
				//$this->load->view('home_view',$data);
				redirect ($com_uri, 'refresh' );
				}else {
					var_dump ( $this->db->_error_message () );
					die ();
				} 
			} 
			//@mail ( 'testaccount@tibilling.uk,jasdeep93.st@gmail.com', $subject, $body, $headers );
		} else {
			echo "Please upload mail";
		}
			
	}
	
	
	function getStatusOptions($id = null) {
		$list = array (
				"Active",
				"Suspend",
				"Cancelled" 
		);
		if ($id == null)
			return $list;
		
		if (is_numeric ( $id ))
			return $list [$id];
		return $id;
	}
	
	public	function addCompany() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$role_id = $session_data ['role_id']; 
		$time = time();
		$create_time = date ( 'Y-m-d h:i:s' );
		$extend_time = date ( "Y-m-d h:i:s", strtotime ( date ( "Y-m-d h:i:s", strtotime ( $create_time ) ) . " + 1 year" ) );
		
		if($_POST ["form_fields"]) {
		//var_dump($_POST ["form_fields"]);
			$service_name = $_POST ['order_name'];
			$mode = '';
			
			if($_POST['mode'] == 'pay_now')
				$mode = 'Pay Now';
			else
				$mode = 'Pay by Invoice';
			//var_dump($mode);die;
			$package_value = $this->order->getOrders ( $_POST ['order_name'] );
			
			$product = $package_value;
			$price = $_POST['price'];
			$data = urldecode( $_POST['form_fields']);
			
			$pieces = explode( "&", $data );
			for($a = 0; $a < count($pieces); $a ++){
				$profile_key = strstr ( $pieces [$a], "=", true );
				$profile[$profile_key] = substr ( strstr ( $pieces [$a], "=" ), 1 );
			}
			
			$company_contact_name = addslashes($_POST ["company_contact_name"]);
			$add_company_services_popup = addslashes($profile["add_company_services_popup"]);
			$add_company_location_popup = addslashes($profile["add_company_location_popup"]);
			$add_company_reseller = addslashes($profile["add_company_reseller"]);
			$add_company_postal_deposit = addslashes($profile["add_company_postal_deposit"]);
			$add_company_formation = $profile["add_company_formation"];
			
			/* if Reseller logged in use Reseller Data to add new company */
			$user_email = $this->search->userSearch ( $user_id );
			$reseller_user_id = $this->search->resellerByEmail ( $user_email->email );
			$rellerInfo = $this->search->userSearch ( $reseller_user_id->user_id );
			
			/* if company is added by admin user data of Reseller selected */
			$user_reseller = $this->search->getResellerById ( $add_company_reseller );
			$rellerInfo = $this->search->userSearch ( $user_reseller->user_id );
			
			
			
			
				if ($role_id == 1) {
					$reseller_id = $profile ["add_company_reseller"];
				if($reseller_id == 0 )
				{
					$reseller_id = 17;
					
				}

				} elseif ($role_id == 0) {
					$this->db->select ( '*' );
					$this->db->from ( 'tbl_order' );
					$this->db->like ( "tbl_order.create_user_id ", $user_id );
					$query_order = $this->db->get ();
					$this->db->order_by ( "id", "desc" );
					$order_reseller = $query_order->row ();
					if($order_reseller)
					$reseller_id = $order_reseller->reseller_id;
					else
					$reseller_id = 17;
				} else
					$reseller_id = $reseller_user_id->id;
			
			
			
			
			if ($role_id == 1)
			{
				$user_reseller_id = $this->search->getResellerById ( $reseller_id );
				$user_id = $user_reseller_id->user_id;
			}
			
			
			$milling = $this->search->getMaillingAdd ( $user_id );
			$billing = $this->search->getbillingAdd ( $user_id );
			
			$this->db->select ( '*,tbl_company.id as id,tbl_company.mailing_adress_id as mailing_adress_id,tbl_company.billing_adress_id as billing_adress_id ' );
			$this->db->from ( 'tbl_company' );
			
			$this->db->where ( 'tbl_company.company_name', $company_contact_name );
			
			$query_company = $this->db->get ();
			$company = $query_company->row ();
			
			$data_mailling = array (
					'mailing_name' => $milling->mailing_name,
					'street' => $milling->street,
					'country' => $milling->country,
					'city' => $milling->city,
					'postcode' => $milling->postcode,
					'county' => $milling->county,
					'create_user_id' => $user_id,
 					'update_user_id' => $user_id,
					'create_time' => $create_time 
			);
			
			$data_billing = array (
					'billing_name' => $billing->mailing_name,
					'street' => $billing->street,
					'country' => $billing->country,
					'city' => $billing->city,
					'postcode' => $billing->postcode,
					'county' => $billing->county,
					'create_user_id' => $user_id,
					'update_user_id' => $user_id,
					'create_time' => $create_time 
			);
			
			$this->db->insert('tbl_mailling_address', $data_mailling);
			$this->db->insert('tbl_billing_address', $data_billing);
			
			$mailling_id = $this->search->getMaillingByTime($create_time);
			$billing_id = $this->search->getBillingByTime($create_time);
			
			$data_company = array('company_name' => $company_contact_name,
									'create_user_id' => $user_id,
									'location' => $add_company_location_popup,
									'mailing_adress_id'=> $mailling_id->id,
									'billing_adress_id' => $billing_id->id,
									'create_time' => $create_time);
									//print_r($data_company);
			
			if ($this->db->insert('tbl_company', $data_company)) {
			
				$this->db->select('*,tbl_company.id as id');
				$this->db->from('tbl_company');
				$this->db->where('tbl_company.create_time', $create_time);
				$this->db->where('tbl_company.create_user_id', $user_id);
				$this->db->where('tbl_company.company_name', $company_contact_name);
				
				$query_company = $this->db->get ();
				$company = $query_company->row ();
				
			
			
				
				if ($product == 'complete_package') {
					$data_order = array (
							'company_id' => $company->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,
							'price' => $price,
							'quantity' => '1', 
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_id,
							'create_time' => $create_time 
					);
				} elseif ($product == 'both') {
					$data_order = array (
							'company_id' => $company->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,
							'price' => $price,
							'quantity' => '1',
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_id,
							'create_time' => $create_time 
					);
				} 
				elseif($product == 'partner')
				{
					$data_order = array (
							'company_id' => $company->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,
							'price' => $price,
							'quantity' => '1',
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_id,
							'create_time' => $create_time 
					);
				}
				else {
					
					$data_order = array (
							'company_id' => $company->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,
							'price' => $price,
							'quantity' => '1',
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_id,
							'create_time' => $create_time 
					);
				}
				if ($this->db->insert ( 'tbl_order', $data_order )) {
					$this->db->select ( '*,tbl_order.id as id' );
					$this->db->from ( 'tbl_order' );
					
					$this->db->where ( 'tbl_order.create_time', $create_time );
					$this->db->where ( 'tbl_order.create_user_id', $user_id );
					$this->db->where ( 'tbl_order.company_id', $company->id );
					
					$query_order = $this->db->get ();
					$order = $query_order->row ();
					
					$data_order_detail = array (
							'order_summery_id' => $order->id,
							'total' => $price+$add_company_postal_deposit,
							'price' => $price,
							'quantity' => '1',
							'create_user_id'=>$user_id,
							'create_time'=>$create_time 
					);
					if($this->db->insert('tbl_order_detail', $data_order_detail)) {
						
						$reseller = $this->order->getReseller($reseller_id);
						$ltd_option = $this->order->getLtdOptions($add_company_formation);
						
						/*========send mail to admin=======*/
					$header = 'From: The London Office<post@theoffice support>' . "\r\n";
					$header .= 'MIME-Version: 1.0'."\r\n";
					$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$subject = "The Office Support > ".$company_contact_name. " > You Have Mail!";
					$new_msg ='<h3>You have a New order from '.$company_contact_name.'</h3><hr>'."<br><br>";
					$new_msg .='Hello ,'."<br><br>";		
					$new_msg .= 'We`re writing to let you know that you have a new order.'."<br><br>";
					$new_msg .= 'The order details of the request are:'."<br><br>";
					$new_msg .= 'Company Name : '.$company_contact_name."<br><br>";
					$new_msg .= 'Reseller Name : '.$reseller."<br><br>";
					$new_msg .= 'Service : '. $service_name."<br><br>";
					$new_msg .= 'Amount : £'. $price."<br><br>";
					$new_msg .= 'Quantity : 1'."<br><br>";
					$new_msg .= 'Company Formation : '.$ltd_option."<br><br>";
					$new_msg .= 'Date of Order : '. date ( "Y-m-d", strtotime ( $create_time ) ) ."<br><br>";
					$new_msg .= 'Time of Order :  '. date ( "h:i A", strtotime ( $create_time ) )."<br><br>";
					$new_msg .= 'Type : '. $mode."<br><br>";
					$new_msg .= '<b>Best Regards,'."<br><br>";
					$new_msg .= 'The London Office'."<br><br>";
					$new_msg .='The Admin Team</b>'."<br><br>";

						if(mail ("order@thelondonoffice.com",$subject,$new_msg,$header ))
						{
							
							$data_activity = array (
										'company_id' => $company->id,
										'activity' => "New Order Placed",
										'create_time' => $create_time,
										'create_user_id' => $user_id,
										'email' => $email
								);
								if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
								redirect('dashboard/index','refresh');
									}
									else {
											var_dump ( $this->db->_error_message () );
											die ();
											}
							
							}
					} 

					else {
						var_dump ( $this->db->_error_message () );
						die ();
					}
				} else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
			}
			else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
		} elseif (! empty ( $_POST )) {
		
			if (isset ( $_FILES ['userfile'] )) {
				$userfiles = $_FILES ["userfile"];
				
				$this->load->library ( 'upload' );
				$files = $_FILES;
				
				$_FILES ['userfile'] ['name'] = $time . 'idcard' . $files ['userfile'] ['name'] [0];
				$_FILES ['userfile'] ['type'] = $files ['userfile'] ['type'] [0];
				$_FILES ['userfile'] ['tmp_name'] = $files ['userfile'] ['tmp_name'] [0];
				$_FILES ['userfile'] ['error'] = $files ['userfile'] ['error'] [0];
				$_FILES ['userfile'] ['size'] = $files ['userfile'] ['size'] [0];
				$configd ['file_name'] = $_FILES ['userfile'] ['name'];
				// var_dump($_FILES['userfile']['name']);
				
				$this->upload->initialize($this->set_upload_options());
				if($this->upload->do_upload()) {
					$data = array ( 'upload_data' => $this->upload->data() 
					);
				}else{
					$error = array(
							'error' => $this->upload->display_errors() 
					);
 				}
				
				$_FILES['userfile']['name'] = $time.'addproof'.$data['upload_data']['file_name'][1];
				$_FILES['userfile']['type'] = $files['userfile']['type'][1];
				$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][1];
				$_FILES['userfile']['error'] = $files['userfile']['error'][1];
				$_FILES['userfile']['size'] = $files['userfile']['size'][1];
				$configd['file_name'] = $_FILES['userfile']['name'];
				// var_dump($_FILES['userfile']['name']);
				
				$this->upload->initialize($this->set_upload_options());
				if($this->upload->do_upload ()) {
					$data = array (
							'upload_data' => $this->upload->data () 
					);
				}else {
					$error = array (
											
							'error' => $this->upload->display_errors () 
					);
					var_dump ( $error );
					die ();
				}
				
				$data_reseller = array (
						'company_name' => $_POST["company_name"],
						'company_number' => $_POST["company_phone"],
						'email_address' => $_POST["company_email_address"],
						'town' => $_POST["company_town"],
						'country' => $_POST ["company_country"],
						'id_proof' => $time . 'idcard' . $files['userfile']['name'][0],
						'address_proof' => $time . 'idcard' . $files['userfile']['name'][1],
 						'create_user_id' => $user_id,
						'create_time' => date ( 'Y-m-d h:i:s' ) 
				);
				#newly added on 17/11/2016  jasdeep
				if($data_reseller['create_user_id']==""){
					var_dump($error);
				}else{
					$this->db->insert('tbl_company', $data_reseller);
				}
				
			}
		}
		
		$this->load->view('company');
	}
	private function set_xsl_options() {
		
		// upload an image options
		$config = array ();
		$config ['upload_path'] = './uploads/xsl/';
		$config ['allowed_types'] = 'xls|xlsx';
		$config ['max_size'] = '0';
		$config ['overwrite'] = FALSE;
		
		return $config;
	}
	function reseller_change() {
		
		 $data="";
		 $page = $this->input->get_post('page');
		
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$role_id = $session_data ['role_id'];
		$reseller_change = $this->input->post('reseller_change'); 
		//var_dump($reseller_change);die("Gfd");
		if($reseller_change !=''){
			$set_reseller = $this->session->set_userdata('reseller',$reseller_change);
			$reseller_change2 = $this->session->userdata('reseller');
		}else{
	 		$reseller_change2 = $this->session->userdata('reseller');
		}
		/*----pagination start----*/
		$config["base_url"] = base_url() . "home/reseller_change";
		$config["total_rows"]  = $this->home_model->count_reseller($reseller_change2);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$show_reseller= $this->home_model->show_reseller($config["per_page"],$page,$reseller_change2);
		$export ='1';
		$show_reseller2= $this->home_model->show_reseller($config["per_page"],$page,$reseller_change2,$export);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $show_reseller2;
			//$this->downloadCsv($data1['user_orders']);
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			
		$data['role_id'] = $role_id;
		$data['user_id'] = $user_id;
		$data['username'] = $session_data['first_name'];
		$data['user_orders'] = $show_reseller;
		$data['user_orders1'] = $show_reseller2;
		$data['record_count'] = $config["total_rows"];
		
		
		$this->load->view ( 'home_view', $data );
		}
	}
	
	public function serviceSearch() {
		echo "ASd";
		//$this->session->unset_userdata ( 'service' );
		$user_orders ='';
 		$page = $this->input->get_post('page');
		if($page==''){ $data['pagef']= '10'; }else{ $data['pagef']= $page; }
  		$session_data = $this->session->userdata ( 'logged_in' );
		$email = $session_data ['email'];
		$arr['user_id'] = $session_data ['id'];
		$arr['role_id'] = $session_data ['role_id'];
		if($page == '' && $_POST['export1']==''){
			foreach ( $_POST as $key => $value ) {
				$ses['key_name'] = $key;
				$ses['value_name'] = $value;
			}
		}
  		if($ses !=''){
			$sesval = $this->session->set_userdata('service',$ses);
			$sesval2 = $this->session->userdata('service');
			$arr['key_name'] = $sesval2['key_name'];
			$arr['value_name'] =  $sesval2['value_name'];
		}else{
			$sesval2 = $this->session->userdata('service');
			$arr['key_name'] = $sesval2['key_name'];
			$arr['value_name'] =  $sesval2['value_name'];
		}
 		/*----pagination start----*/
		$config["base_url"] = base_url() . "home/serviceSearch";
		$config["total_rows"]  = $this->home_model->count_service_search($arr);
		$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_service_search($config["per_page"],$page,$arr);
		$export ='1';
		$user_orders1 = $this->home_model->show_service_search($config["per_page"],$page,$arr,$export);
		/*----pagination end ----*/
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
		$data['role_id'] = $arr['role_id'];
		$data['user_orders'] = $user_orders;
 		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data ['first_name'];
		$data['record_count'] = $config["total_rows"] ;
 		$this->load->view ( 'home_view', $data );
		}
		
	}
	
	function manageReseller() {
	//var_dump("hello");die("jhbg");
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data['id'];
		$role_id = $session_data['role_id'];
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$data['username'] = $session_data['first_name'];
		$data['role_id'] = $role_id;
		$data['resellers'] = $this->search->resellers();
		
		$data['resellersCount'] = $this->search->resellersCount();
		//var_dump($data);die();
		$data['CountAll'] = $this->home_model->count_companies_total_reseller($arr);//$this->search->CountAll();
		$data['Countactive'] = $this->home_model->count_companies($arr);
		
 		$this->load->view('manageReseller', $data);
	}
	 
	function exportshhet($id) {
		$data_query = $this->search_model->ressler_data($id);
		
		$data['user_orders'] = $data_query;
		$data['record_count'] = count($user_orders);
		$this->load->view ( 'reseller_view', $data );
	}
	function active_exportshhet($id) {
		$data_query = $this->search_model->ressler_data_active($id);
		$data['user_orders'] = $data_query;
		$data['record_count'] = count($user_orders);
		$this->load->view ( 'reseller_view', $data );
	}
	function inactive_exportshhet($id) {
		$data_query = $this->search_model->ressler_data_inactive($id);
		$data['user_orders'] = $data_query;
		$data['record_count'] = count($user_orders);
		$this->load->view ( 'reseller_view', $data );
	}
	function resellerEdit() {
	$user_id = $page = $this->input->get_post('user_id_reseeler');
	//var_dump($user_id);die;
		$password = md5 ( $_POST ["reseller_admin_password"] );
		$data_reseller = array (
				'company_name' => $_POST ["reseller_company_name"],
				'admin_email' => $_POST ["reseller_admin_email"],
				'contact_name' => $_POST ["reseller_contact_name"],
				'mail_email' => $_POST ["reseller_email"],
				'phone_number' => $_POST ["reseller_phone"],
				'address1' => $_POST ["reseller_address1"],
				'address2' => $_POST ["reseller_address2"],
				'county' => $_POST ["reseller_county"],
				'country' => $_POST ["reseller_country"],
				'web_address' => $_POST ["reseller_weburl"],
				'town' => $_POST ["reseller_town"],
				'post_code' => $_POST ["reseller_post_code"],
				'logo_text' => $_POST ["reseller_logo_name"] 
		);
		$data_reseller1 = array (
		'email' => $_POST ["reseller_email"],
		'password' => $password,
		);
		$reseller_data  = $this->search_model->resellerUpdateData($user_id,$data_reseller1);
		$this->db->where ( 'id', $_POST ["reseller_id"] );
		if ($this->db->update ( 'tbl_reseller', $data_reseller )) {
			
			redirect ( 'home/manageReseller', 'refresh' );
		} else {
			
			var_dump ( $this->db->_error_message () );
			die ();
		}
	}
	
	function filterSearch() {
		 $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		 
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data ['id'];
		$arr['role_id'] = $session_data ['role_id'];
		$config["base_url"] = base_url() . "home/filterSearch";
		$location_select = $this->input->post('location_select');
		//var_dump($location_select);die();
		$upload_change = $this->input->post('upload_change');
		if($location_select !=''){
			$setstate = $this->session->set_userdata('location_select',$location_select);
			$arr['location_select'] = $this->session->userdata('location_select');
		}
 		if($upload_change !=''){
			$setstate = $this->session->set_userdata('upload_change',$upload_change);
			$arr['upload_change'] = $this->session->userdata('upload_change');
		}
 	 	$config["total_rows"]  = $this->home_model->count_filter_search($arr);
		$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		/*---pagination end----*/
		$user_orders= $this->home_model->show_filter_search($config["per_page"],$page,$arr);
		$export ='1';
		$user_orders1= $this->home_model->show_filter_search($config["per_page"],$page,$arr,$export); // for export
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
		}else{
			$data['username'] = $session_data ['first_name'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['role_id'] = $arr['role_id'];
			$data['record_count'] =$config["total_rows"];
			$this->load->view ( 'home_view', $data );
		}
 	}
	function filterSearchbyid() {
		 $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		 
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data ['id'];
		$arr['role_id'] = $session_data ['role_id'];
		$config["base_url"] = base_url() . "home/filterSearch";
		//$location_select = $this->input->post('location_select');
		$arr['upload_change'] = $this->input->post('upload_change');
 	 	$config["total_rows"]  = $this->home_model->count_filter_search($arr);
		$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		/*---pagination end----*/
		$user_orders= $this->home_model->show_filter_searchbyid($config["per_page"],$page,$arr);
		$export ='1';
		$user_orders1= $this->home_model->show_filter_searchbyid($config["per_page"],$page,$arr,$export); // for export
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
		}else{
			$data['username'] = $session_data ['first_name'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['role_id'] = $arr['role_id'];
			$data['record_count'] =$config["total_rows"];
			$this->load->view ( 'home_view', $data );
		}
 	}

	
	/* function addAltEmail() {
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$role_id = $session_data ['role_id'];
		$company_id = $_POST ['company_id'];
		$emails='';
		$emails = $this->search->alterEmails ( $company_id );
		if ($emails) {
			$email_2 = $_POST['email_2'];
			$email_3 = $_POST['email_3'];
			$email_4 = $_POST['email_4'];
			
			$data_alt_emails = array (
					'company_id' => $company_id,
					'email_2' => $email_2,
					'email_3' => $email_3,
					'email_4' => $email_4,
					'create_user_id' => $user_id,
					'create_time' => date ( 'Y-m-d h:i:s' ) 
			);
			if(	$this->db->update ( 'tbl_alt_email', $data_alt_emails ))
			{
				
			$data_activity = array (
						'company_id' => $company_id,
						'activity' => "Alternate Email Added",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id 
				);
			if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
			else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
				
			}
		} 

		else {
			// var_dump($_POST);die;
			
			$email_2 = $_POST ['email_2'];
			$email_3 = $_POST ['email_3'];
			$email_4 = $_POST ['email_4'];
			
			$data_alt_emails = array (
					'company_id' => $company_id,
					'email_2' => $email_2,
					'email_3' => $email_3,
					'email_4' => $email_4,
					'create_user_id' => $user_id,
					'create_time' => date ( 'Y-m-d h:i:s' ) 
			);
			if($this->db->insert ( 'tbl_alt_email', $data_alt_emails ))
			{
				
			$data_activity = array (
						'company_id' => $company_id,
						'activity' => "Alternate Email Updated",
						'create_time' => date ( 'Y-m-d h:i:s' ),
						'create_user_id' => $user_id 
				);
			if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
				else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
			}
		}
		redirect ('dashboard/index', 'refresh');
	} */

public function download($file_name) {
		$this->load->helper ( 'download' ); // load helper
		$url = "./uploads/" . $file_name;
		// $url = "./uploads/";
		$data = file_get_contents ( $url ); // Read the file's contents
		
		$name = $file_name;
		
		force_download ( $name, $data );
		

	}
	public function mailRequestOriginal() {
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$company_id = $_GET['comp_id'];
		$order  = $this->search_model->getOrderData_new($company_id);
		$create_time = date ( 'Y-m-d h:i:s' );
		$type_id = $_GET['type_id'];
		$file_name = $_GET['file_name'];
		$file_id = $_GET['id'];
		$url = base_url();
		
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}else{
			$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}
		
		
		
		$company_name = $this->search->filterSearch($company_id)->company_name;
		$to = 'post@thelondonoffice.com';
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = "The Office Support > ".$company_name." > You Have Mail!";
		
		$new_msg ='<h3>You have a Request Original!</h3><hr>'."<br><br>";
	    $new_msg .='Hello ,'."<br><br>";		
		$new_msg .= 'We`re writing to let you know that you have a request original.'."<br><br>";
		$new_msg .= 'The message details of the request are:'."<br><br>";
		$new_msg .= 'Company Name : '. $company_name ."<br><br>";
		$new_msg .= 'File Name : '. $file_name ."<br><br>";
		$new_msg .= 'Date of Request : '. date ( "Y-m-d", strtotime ( $create_time ) ) ."<br><br>";
		$new_msg .= 'Time of Request :  '. date ( "h:i A", strtotime ( $create_time ) ) ."<br><br>";
		$new_msg .= 'Type : '. $type_id ."<br><br>";
		$new_msg .= 'Best Regards,'."<br><br>";
		$new_msg .= $team."<br><br>";
		$new_msg .='The Admin Team'."<br><br>";
 		//$new_msg .= '</body></html>'."\r\n";
		if (mail ($to,$subject,$new_msg,$header )) {
			$req_time = date ( 'Y-m-d h:i:s' );
			$data = array (
					'state_id' => 1,
	
					'req_time' => $req_time 
			);
			
			$this->db->where ( 'id', $file_id );
			if ($this->db->update ( 'tbl_file_info', $data ))
				{
						
						
	$data_activity = array (
				'company_id' => $_GET['comp_id'],
				'activity' => "Request Original",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id, 
				'email' => $email, 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
		$ref_url=$_GET['comp_id'];
         redirect('dashboard/showCompanyResult?id='.$ref_url);
			}
			else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
						
				}
			else
				var_dump ( $this->db->_error_message () );
			die ();
		} else {
			die ( "not done" );
		}
	}
	function message_according_id($id) {
		$id = $_POST['message_id'];
		$this->db->select('*');
		$this->db->from('tbl_message');
		$this->db->where('tbl_message.id', $id);
		
		$query = $this->db->get();
		$message_company = $query->row();
		return $message_company->message;
	}
	function deleteRecord() {
	
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data ['id'];
		$company_id = $_POST['company_id'];
		$id = $_POST['record_id'];
		
		if ($_POST['model'] == 'order_detail') {
			$order_id = $_POST['order_id'];
			
			$result = $this->search->orderDetails ( $order_id );
			$count = count($result);
			//var_dump($count);die;			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_order_detail' );
			$this->db->where ( 'tbl_order_detail.id', $id );
			$query_order_detail = $this->db->get ();
			$order_detail = $query_order_detail->row ();
			$order_summery_price = $order_detail->price;
			$order_summery_id = $order_detail->order_summery_id;			
			$order_product = $order_detail->product;			
			$package_value = $this->order->getOrders ( $order_product );
			// var_dump($order_product,$package_value);die;			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_order' );
			$this->db->where ( 'tbl_order.id', $order_summery_id );			
			$query_order = $this->db->get();
			$main_order = $query_order->row();
			$order_price = $main_order->price;			
			$price = $order_price-$order_summery_price;			
			$this->db->where ('id',$id);
			//var_dump($package_value);die;
				
			if ($this->db->delete('tbl_order_detail')) {
				if ($package_value == 'complete_package') {
				/*=======================*/
				if($count >1)
				{
					$data = array (
								'price' => $price,	
								'business_address' => 'No', 
						);
				}
			else
				{
				
					$data = array (
							'price' => $price,
							'registered_office' => 'No',
							'director_service_address' => 'No',
							'business_address' => 'No',
					);
				}
				/*========================*/
				} elseif($package_value == 'both')
				{
					$data = array (
							'price' => $price,
							'registered_office' => 'No',
							'director_service_address' => 'No',
					);
				}else {
					$data = array (
							'price' => $price,	
							$package_value => 'No' 
					);
				}
				
				$this->db->where ( 'id', $order_summery_id );
				$this->db->update ( 'tbl_order', $data );
				
				
				
						$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Order Deleted",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
				var_dump ( $this->db->_error_message () );
				die ();
				}
			}
				else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
		} elseif ($_POST ['model'] == 'reseller') {
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_reseller' );
			$this->db->where ( 'tbl_reseller.id', $id );
	
			
			$query = $this->db->get ();
			$message_company = $query->row ();
			
			$data = array (
					'state_id' => 1 
			);
			
			$this->db->where ( 'id', $id );
			$this->db->update ( 'tbl_reseller', $data );
		} elseif ($_POST ['model'] == 'message') {
			$this->db->where ( 'id', $id );
			if($this->db->delete ( 'tbl_message' ))
			{
			
				$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Message Deleted",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else { 
				var_dump ( $this->db->_error_message () );
				die ();
				}
			
			}
			
			
			
		} elseif ($_POST ['model'] == 'file_mail') {
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_file_info' );
			$this->db->where ( 'tbl_file_info.id', $id );
 			$query = $this->db->get ();
			$file_mail = $query->row ();
			 //var_dump($file_mail->file_name);die;
			if (unlink("./uploads/".$file_mail->file_name)) {
				$this->db->where ( 'id', $id );
				if($this->db->delete ( 'tbl_file_info' ))
				{
				
					$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Mail Deleted",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
				var_dump ( $this->db->_error_message () );
				die ();
				}
				
				}
				//echo $this->db->last_query();die;
			}else{
				var_dump($this->db->_error_message () );
				die ();
			}
		}
		elseif ($_POST ['model'] == 'file_doc') {
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_file_info' );
			$this->db->where ( 'tbl_file_info.id', $id );
 			$query = $this->db->get ();
			$file_mail = $query->row ();
			 //var_dump($file_mail->file_name);die;
			if (unlink("./uploads/".$file_mail->file_name)) {
				$this->db->where ( 'id', $id );
				if($this->db->delete ( 'tbl_file_info' ))
				{
				
					$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Document Deleted",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
				var_dump ( $this->db->_error_message () );
				die ();
				}
				
				}
				//echo $this->db->last_query();die;
			}else{
				var_dump($this->db->_error_message () );
				die ();
			}
		}
	}
	function orderDetailUpdate() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$user_id = $session_data['id'];
		$email = $session_data['email'];
		
		if (isset ( $_POST["product_id"] )) {
			// die("ddff");
			$product_name = $_POST["product_name"];
			$company_id = $_POST["order_compaqny_id"];
			$str_arr = explode('£',$_POST["product_price"]);
			if(!$str_arr[1])
				$str_arr_price = $str_arr[0];
			else
				$str_arr_price =$str_arr[1];
				
			$product_price = $str_arr_price;
			$product_quantity = $_POST["product_quantity"];
			$product_date = date( 'Y-m-d h:i:s', strtotime ( $_POST ["product_date"] ) );	
			$product_id = $_POST["product_id"];
			
			$data = array(
					'product' => $product_name,
					'price' => $product_price,
					'total' => $product_price,
					'quantity' => $product_quantity,
					'create_time' => $product_date,
					'update_user_id' => $user_id 
			);
			
			$this->db->where ( 'id', $product_id );
			if ($this->db->update ( 'tbl_order_detail', $data ))
				{
					$data_activity = array (
				'company_id' => $company_id,
				'activity' => "Order Updated",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id ,
				'email' => $emai 
		); 
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			redirect ( 'dashboard/index', 'refresh' );
			}
			else {
				var_dump ( $this->db->_error_message () );
				die ();
				}
			
				}
			else
				var_dump ( $this->db->_error_message () );
			die ();
		}
		redirect ( 'dashboard/index', 'refresh' );
	}
	function xsl_upload() {
		$session_data = $this->session->userdata( 'logged_in' );
		$user_id = $session_data['id'];
		$role_id = $session_data['role_id'];		
		$time = time();
		$company_data="";
		if (isset ( $_FILES['userfile'] )) {
			// var_dump ( $_FILES );die;
			$userfiles = $_FILES["userfile"];			
			$this->load->library( 'upload' );
			$files = $_FILES;
			$_FILES['userfile']['name'] = $time . 'xsl' . $files ['userfile']['name'][0];
			$_FILES['userfile']['type'] = $files['userfile']['type'][0];
			$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
			$_FILES['userfile']['error'] = $files['userfile']['error'][0];
			$_FILES['userfile']['size'] = $files['userfile']['size'][0];
			$configd['file_name'] = $_FILES['userfile']['name'];
			
			 var_dump ($_FILES ['userfile'] ['name']);die;
			
			$this->upload->initialize( $this->set_xsl_options());
			if ($this->upload->do_upload()) {
				$data = array (
						'upload_data' => $this->upload->data() 
				);
				$this->load->library('excel');
				$file = './uploads/xsl/'.$_FILES['userfile']['name'];
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				
				$arrayCount = count($allDataInSheet);
				
				for($i = 2; $i <= $arrayCount; $i++) {
					$getdata1 = $allDataInSheet[$i];
					$create_time = date("Y-m-d h:i:s");
					$d = str_replace( "/", "-",$getdata1['C']);
					$renewal = date("Y-m-d h:i:s",strtotime($d));					
					$reseller = $this->search->getResellerId($getdata1['B']);					
					// var_dump($reseller);die;					
					$user = $this->search->userEmail($getdata1['AA']);					
					$first_name = $getdata1['T'];
					$last_name = $getdata1['U'];
					$email = $getdata1['AA'];
					$password = md5($getdata1['AE']);
					$ph_no = $getdata1['AB'];
					$mobile = $getdata1['AC'];
					$skpe_address = $getdata1['AD'];
					$street = $getdata1['V'];
					$city = $getdata1['W'];
					$counrty = $getdata1['Z'];
					$postcode = $getdata1['Y'];
					$county = $getdata1['X'];					
					if (!$user) {						
						$user_data = array (
								'first_name' => $first_name,
								'last_name' => $last_name,
								'email' => $email,
								'password' => $password,
								'ph_no' => $ph_no,
								'mobile' => $mobile,
								'skpe_address' => $skpe_address,
								'street' => $street,
								'city' => $city,
								'counrty' => $counrty,
								'postcode' => $postcode,
								'county' => $county,
								'role_id' => 0,
								'create_time' => $create_time 
						);						
						/* ==============================User =========================================================== */
						if ($this->db->insert( 'tbl_user', $user_data )) {							
							 var_dump('tbl_user');
							$user_detail = $this->search->getUserCreateTime ($create_time);
							
							/* ======================================Mailing=============================================== */
							$mailling_data = array (
									'mailing_name' => $getdata1 ['N'],
									'street' => $getdata1 ['O'],
	
									'country' => $getdata1 ['S'],
									'city' => $getdata1 ['P'],
									'postcode' => $getdata1 ['R'],
									'county' => $getdata1 ['Q'],
									'create_user_id' => $user_detail->id,
									'create_time' => date ( "Y-m-d h:i:s" ) 
							);
							if ($this->db->insert ( 'tbl_mailling_address', $mailling_data )) {								
								 var_dump('tbl_mailling_address');
								$mailling_detail = $this->search->getMaillingByTime ( $create_time );
								
								/* =========================================Billing================================================ */
								$billing_data = array (
										'billing_name' => $getdata1['N'],
										'street' => $getdata1['O'],
										'country' => $getdata1['Q'],
										'city' => $getdata1['P'],
										'postcode' => $getdata1['R'],
										'county' => $getdata1['S'],
										'create_user_id' => $user_detail->id,
										'create_time' => date("Y-m-d h:i:s") 
								);
								if ($this->db->insert ('tbl_billing_address',$billing_data)) {
									
									 var_dump('tbl_billing_address');
									$billing_detail = $this->search->getBillingByTime($create_time);
									
									/* =========================================Company================================================ */
									
									$company_data = array(
											'company_name' => $getdata1['A'],
											'location' => $getdata1['E'],
		
											'mailing_adress_id' => $mailling_detail->id,
											'billing_adress_id' => $billing_detail->id,
											'create_user_id' => $user_detail->id,
											'create_time' => date( "Y-m-d h:i:s" ) 
									);
									
									if ($this->db->insert('tbl_company', $company_data)){
										 var_dump('tbl_company');
										$company_detail = $this->search->getCompanyByTime($create_time);										
										/* ==========================order=============================================================== */
										$order_data = array (
												'company_id' => $company_detail->id,
												'renewable_date' => $renewal,
												'location' => $getdata1['E'],
												'registered_office' => $getdata1['F'],
												'director_service_address' => $getdata1['G'],
												'business_address' => $getdata1['H'],
												'telephone_service' => $getdata1['I'],												
												// 'complete_package'=>$getdata1['J'],
												'hosting' => $getdata1['J'],
												'deposit' => $getdata1['K'],
												'price' => str_replace( '£', '', $getdata1['L'] ),
												'quantity' => '1',
												'reseller_id' => $reseller,
												'state_id' => '0',
												'create_user_id' => $user_detail->id,
												'create_time' => date("Y-m-d h:i:s") 
										);
										
										if ($this->db->insert ('tbl_order',$order_data)) {
											
											 var_dump('tbl_order');
											/* ===========================order detail===================================================== */
											
											$order_detail = $this->search->getOrderByTime($create_time);
											
											$order_detail_data = array (
													'order_summery_id' => $order_detail->id,
													'product' => 'Registered Office Address',
													'price' => str_replace( '£', '', $getdata1 ['L'] ),
													'quantity' => '1',
													'total' => str_replace( '£', '', $getdata1 ['L'] ),
													'create_user_id' => $user_detail->id,
													'create_time' => date("Y-m-d h:i:s") 
											)
											;
											if ($this->db->insert('tbl_order_detail',$order_detail_data)) {
												 var_dump('tbl_order_detail');
											} 

											else
												var_dump ( $this->db->_error_message () );die;
										} else
											var_dump ( $this->db->_error_message () );die;
									} else
										var_dump ( $this->db->_error_message () );die;
								} else
									var_dump ( $this->db->_error_message () );die;
							} else
								var_dump ( $this->db->_error_message () );die;
						} else
							var_dump ( $this->db->_error_message () );die;
					} 

					else 

					{
						/* ======================================Mailing=============================================== */
						$mailling_data = array (
								'mailing_name' => $getdata1 ['N'],
								'street' => $getdata1 ['O'],
								'country' => $getdata1 ['S'],
								'city' => $getdata1 ['P'],
								'postcode' => $getdata1 ['R'],
								'county' => $getdata1 ['Q'],
								'create_user_id' => $user->id,
								'create_time' => date("Y-m-d h:i:s") 
						);
						if ($this->db->insert ( 'tbl_mailling_address', $mailling_data )) {
							
							var_dump('tbl_mailling_address else',$mailling_data );die;
							$mailling_detail = $this->search->getMaillingByTime ( $create_time );
							
							/* =========================================Billing================================================ */
							$billing_data = array (
									'billing_name' => $getdata1 ['N'],
	
									'street' => $getdata1 ['O'],
									'country' => $getdata1 ['Q'],
									'city' => $getdata1 ['P'],
									'postcode' => $getdata1 ['R'],
									'county' => $getdata1 ['S'],
									'create_user_id' => $user->id,
									'create_time' => date("Y-m-d h:i:s") 
							);
							if ($this->db->insert ( 'tbl_billing_address', $billing_data )) {
								
								 var_dump('tbl_billing_address else');
								$billing_detail = $this->search->getBillingByTime ( $create_time );
								
								/* =========================================Company================================================ */
								
								$company_data = array (
										'company_name' => $getdata1 ['A'],
										'location' => $getdata1 ['E'],
										'mailing_adress_id' => $mailling_detail->id,
										'billing_adress_id' => $billing_detail->id,
										'create_user_id' => $user->id,
										'create_time' => date("Y-m-d h:i:s") 
								);
								
								if ($this->db->insert ( 'tbl_company', $company_data )) {
									 var_dump('tbl_company else');
									$company_detail = $this->search->getCompanyByTime ( $create_time );
									
									/* ==========================order=============================================================== */
									$order_data = array (
											'company_id' => $company_detail->id,
											'renewable_date' => $renewal,
											'location' => $getdata1 ['E'],
	
											'registered_office' => $getdata1 ['F'],
											'director_service_address' => $getdata1 ['G'],
											'business_address' => $getdata1 ['H'],
											'telephone_service' => $getdata1 ['I'],
											
											// 'complete_package'=>$getdata1['J'],
											'hosting' => $getdata1 ['J'],
											'deposit' => $getdata1 ['K'],
											'price' => str_replace ( '£', '', $getdata1 ['L'] ),
											'quantity' => '1',
											'reseller_id' => $reseller,
											'state_id' => '0',
											'create_user_id' => $user->id,
											'create_time' => date("Y-m-d h:i:s") 
									);
									
									if ($this->db->insert ( 'tbl_order', $order_data )) {
										
										// var_dump('tbl_order else');
										/* ===========================order detail===================================================== */
										
										$order_detail = $this->search->getOrderByTime ( $create_time );
										
										$order_detail_data = array (
												'order_summery_id' => $order_detail->id,
												'product' => 'Registered Office Address',
												'price' => str_replace ( '£', '', $getdata1 ['L'] ),
												'quantity' => '1',
												'total' => str_replace ( '£', '', $getdata1 ['L'] ),
												'create_user_id' => $user->id,
												'create_time' => date ( "Y-m-d h:i:s" ) 
										)
										;
										// var_dump($order_detail,$order_detail_data);die;
										if ($this->db->insert ( 'tbl_order_detail', $order_detail_data )) {
											// var_dump('tbl_order_detail else');
										} else {
											var_dump ( $this->db->_error_message () );
											die ();
										}
									} else {
										var_dump ( $this->db->_error_message () );
										die ();
									}
								} else {
									var_dump ( $this->db->_error_message () );
									die ();
								}
							} else {
								var_dump ( $this->db->_error_message () );
								die ();
							}
						} else {
							var_dump ( $this->db->_error_message () );
							die ();
						}
					}
				}
				redirect('dashboard/index','refresh');
			} else {
				$error = array (
						'error' => $this->upload->display_errors () 
				);
				echo "Please upload file";
				die ();
			}
			redirect ( 'dashboard/index', 'refresh' );
		}
		$data ['role_id'] = $role_id;
		$this->load->view ( 'xsl', $data );
		// $this->load->view('xsl');
	}
	public function location() {
		$id = $_POST ["id"];		
		$this->db->select ( '*' ); 
		$this->db->from ( 'tbl_order' );
		$this->db->where ( 'tbl_order.id', $id );		
		$query = $this->db->get ();
		$order = $query->row ();
		$data =  $order->location;
		if($data=='EC1'){
			$res = ['status'=>0];
		}
		else if($data=='SE1'){
			$res = ['status'=>1];
		}
		else if($data=='WC1'){
			$res = ['status'=>2];
		}
		else if($data=='EH2'){
			$res = ['status'=>3];
		}
		else if($data=='W1'){
			$res = ['status'=>4];
		}
		else if($data=='IP1'){
			$res = ['status'=>5];
		}
		else if($data=='EH3'){
			$res = ['status'=>6];
		}
		else if($data=='DUB'){
			$res = ['status'=>7];
		}
		echo json_encode($res);
	}
	public function ExportExcel($order) {
		//die("here fdgfdg");
		
		$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,
				tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
				tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
		$this->db->from ( 'tbl_order' );
		$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
		$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
		$this->db->order_by ( "tbl_company.company_name", "asc" );
		$query = $this->db->get ();
		$user_orders = $query->result ();
		if ($order)
			$user_orders = $order;
		else
			
			$data['user_orders'] = $user_orders;
			$data['record_count'] = count($user_orders);
		$this->load->view ( 'spreadsheet_view', $data );
	}
	public function filterExportExcel() {
		//die("here");
		
		$str_var = $_POST["str_var"];
		$array_var =unserialize(base64_decode($str_var)); //255
		echo "<pre>";
		print_r($array_var);die;
		// print_r($array_var);
		if(empty($array_var)){		
				$session_data = $this->session->userdata('logged_in');
				$user_id = $session_data['id'];
				$role_id = $session_data['role_id'];
				$reseller = '';
				$reseller_ids = array ();
				if ($role_id == 2) {
					$resellers = $this->search->resellerUser($user_id );
					
					foreach($resellers as $reseller){
						$reseller_ids[] = $reseller->id;
					}
				}
				
				$this->db->select ( '*,tbl_order.id as id,tbl_order.company_id as company_id,
					tbl_order.create_user_id as create_user_id ,tbl_order.state_id as state_id,
					tbl_company.company_name as company_name , tbl_reseller.company_name as reseller' );
				$this->db->from ( 'tbl_order' );
				$this->db->join ( 'tbl_company', 'tbl_company.id=tbl_order.company_id' );
				$this->db->join ( 'tbl_reseller', 'tbl_reseller.id=tbl_order.reseller_id' );
				$this->db->order_by ( "tbl_company.company_name", "asc" );
				$this->db->where ( 'tbl_order.state_id !=2 AND tbl_order.state_id !=4' );
				$this->db->where ( 'tbl_reseller.state_id !=1' );
				
				if($role_id == 0){
					$this->db->where('tbl_order.create_user_id', $user_id);
				}else if($role_id == 2){
					$this->db->where_in('tbl_reseller.id', $reseller_ids);
				}
				
				$query = $this->db->get ();
				//echo $this->db->last_query();die;
				$user_orders = $query->result ();
				
				$data ['username'] = $session_data ['first_name'];
				$data ['user_orders'] = $user_orders;
				$data ['role_id'] = $role_id;
				$this->load->view ( 'spreadsheet_view', $data );
		
		} else {
			
		  	$data ['user_orders'] = $array_var;
			$this->load->view ( 'spreadsheet_view', $data );
		}
	}
	public function renewalRecords() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$renew_company_from = $_POST['renew_company_from'];
		$renew_company_to = $_POST['renew_company_to'];
		$reseller_id = $_POST['reseller_id'];
		//var_dump($reseller_id);die();
 		if($renew_company_from !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_from',$renew_company_from);
			$arra['renew_company_from'] = $this->session->userdata('renew_company_from');
		}else{
	 		$arra['renew_company_from']  = $this->session->userdata('renew_company_from');
		}
		if($renew_company_to !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_to',$renew_company_to);
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}
		else if($renew_company_to !='Show All'){
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}else{
	 		$arra['renew_company_to']  = $this->session->userdata('renew_company_to');
		}
		
		if($reseller_id !=''){
			$reseller_id1 = $this->session->set_userdata('reseller_id',$reseller_id);
			$arra['reseller_id'] = $this->session->userdata('reseller_id');
		}else{
	 		$arra['reseller_id']  = $this->session->userdata('reseller_id');
		
		}
		
		
		$config["base_url"] = base_url() . "home/renewalRecords";
		$config["total_rows"]  = $this->home_model->count_renewal_records($arra);
		 
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records($config["per_page"],$page,$arra);
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records($config["per_page"],$page,$arra,$export);
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('home_view', $data);
		}
	}
	public function renewalRecordsNew() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$renew_company_from = $_GET['renew_company_from1'];
		$renew_company_to = $_GET['renew_company_to1'];
		$reseller_id = $_GET['reseller_id1'];
		$arra['search_new_all'] = $_GET['search_new_all'];
		//var_dump($search_new1);die("hg");
 		if($renew_company_from !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_from',$renew_company_from);
			$arra['renew_company_from'] = $this->session->userdata('renew_company_from');
		}else{
	 		$arra['renew_company_from']  = $this->session->userdata('renew_company_from');
		}
		if($renew_company_to !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_to',$renew_company_to);
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}
		else if($renew_company_to !='Show All'){
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}else{
	 		$arra['renew_company_to']  = $this->session->userdata('renew_company_to');
		}
		
		if($reseller_id !=''){
			$reseller_id1 = $this->session->set_userdata('reseller_id',$reseller_id);
			$arra['reseller_id'] = $this->session->userdata('reseller_id');
		}else{
	 		$arra['reseller_id']  = $this->session->userdata('reseller_id');
		
		}
		
		
		$config["base_url"] = base_url() . "home/renewalRecordsNew";
		$config["total_rows"]  = $this->home_model->count_renewal_records($arra);
		 
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records($config["per_page"],$page,$arra);
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records($config["per_page"],$page,$arra,$export);
		//var_dump($user_orders1);die("kjjjjj");
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('home_view', $data);
		}
	}
	/* Search new order*/
	public function OrdersRecordsNew() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$new_order_from = $_GET['new_order_from'];
		$new_order_to = $_GET['new_order_to'];
		$uri_orders = $_GET['uri_orders'];
		$arra['new_order_from'] = $new_order_from;
		$arra['new_order_to'] = $new_order_to;
		$config["base_url"] = base_url() . "home/OrdersRecordsNew";
		$config["total_rows"]  = $this->home_model->count_renewal_records1($arra);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records1($config["per_page"],$page,$arra);
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records1($config["per_page"],$page,$arra,$export);
		//var_dump($user_orders1);die("kjjjjj");
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('home_view', $data);
		}
	}
	public function companyOrdersRecordsNew() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$new_order_from = $_GET['new_order_from'];
		$new_order_to = $_GET['new_order_to'];
		$uri_orders = $_GET['uri_orders'];
		$arra['new_order_from'] = $new_order_from;
		$arra['new_order_to'] = $new_order_to;
		$config["base_url"] = base_url() . "home/OrdersRecordsNew";
		$config["total_rows"]  = $this->home_model->count_renewal_records1($arra);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records1($config["per_page"],$page,$arra);
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records1($config["per_page"],$page,$arra,$export);
		//var_dump($user_orders1);die("kjjjjj");
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('company_view', $data);
		}
	}
	/* Search new order*/
	public function companyRenewalRecordsNew() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$renew_company_from = $_GET['renew_company_from1'];
		$renew_company_to = $_GET['renew_company_to1'];
		$reseller_id = $_GET['reseller_id1'];
		$arra['search_new_all'] = $_GET['search_new_all'];
		///var_dump($search_new1);die("hg");
 		if($renew_company_from !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_from',$renew_company_from);
			$arra['renew_company_from'] = $this->session->userdata('renew_company_from');
		}else{
	 		$arra['renew_company_from']  = $this->session->userdata('renew_company_from');
		}
		if($renew_company_to !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_to',$renew_company_to);
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}
		else if($renew_company_to !='Show All'){
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}else{
	 		$arra['renew_company_to']  = $this->session->userdata('renew_company_to');
		}
		
		if($reseller_id !=''){
			$reseller_id1 = $this->session->set_userdata('reseller_id',$reseller_id);
			$arra['reseller_id'] = $this->session->userdata('reseller_id');
		}else{
	 		$arra['reseller_id']  = $this->session->userdata('reseller_id');
		
		}
		
		
		$config["base_url"] = base_url() . "home/renewalRecords";
		$config["total_rows"]  = $this->home_model->count_renewal_records($arra);
		 
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records($config["per_page"],$page,$arra);
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records($config["per_page"],$page,$arra,$export);
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ( 'company_view', $data );
		}
	}
	public function delete_row() {
		$order_id = $_POST ['delete_order_id'];		
		$data = array (
				'state_id' => 4 
		);
		$this->db->where ( 'id', $order_id );	
		$this->db->update ( 'tbl_order', $data );		
		redirect ( 'dashboard/index', 'refresh' );
	}
	public function addOrder() {
		$session_data = $this->session->userdata( 'logged_in' );
		$user_id = $session_data['id'];
		$email = $session_data['email'];
		
		if (isset($_POST )) {		
		$mode = '';
		$create_time = date ( 'Y-m-d h:i:s' );
		if($_POST['mode'] == 'pay_now')
			$mode = 'Pay Now';
		else
			$mode = 'Pay by Invoice';
		
			$data_order = array();			
			$data = urldecode($_POST['user_services'] );
			$field_id = $_POST['order_id'];
			$pieces = explode( "&", $data );
			for($a = 0; $a < count($pieces ); $a ++) {
				$profile_key = strstr ( $pieces [$a], "=", true );
				$profile [$profile_key] = substr ( strstr ( $pieces [$a], "=" ), 1 );
			}
			
			$old_order_id = $profile ['add_order_id'];
			$package_name = $profile ['service_name'];
			$amount = $profile ['amount'];
			$package_value = $this->order->getOrders ( $package_name, $field_id );
			$quantity = $profile ['service_quantity'];	
			
			$service_name = '';
			$service_name = preg_replace('/\d+/u', '', $package_name);
						
			if($service_name == "Complete Package")
				$service_name = "Virtual Business Plus";
			else if($service_name == "Business Address")
				$service_name = "Virtual Business Address";
			else
			   $service_name =  $service_name;
			
			
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_order' );
			$this->db->where ( 'tbl_order.id', $old_order_id );
			$query = $this->db->get ();
			$user_order = $query->row ();
			
			/*company details*/
			$this->db->select ( '*' );
			$this->db->from ( 'tbl_company' );
			$this->db->where ( 'tbl_company.id', $user_order->company_id );
			$query_company = $this->db->get ();
			$user_company = $query_company->row ();
			$company_name = $user_company->company_name;			
			/*================*/		
			
			$old_price = $user_order->price;
			$new_price = $old_price + $amount;			
			if ($package_value != "both" && $package_value != "complete_package" && $package_value != "deposit" && $package_value != "partner") {				
				$data_order = array (
						$package_value => 'Yes',
						'price' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id 
				);	
			} elseif ($package_value == "both") {
				$data_order = array (
						'registered_office' => 'Yes',
						'director_service_address' => 'Yes',
						'price' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id 
				);
			} elseif ($package_value == "partner") {
				$data_order = array (
						'registered_office' => 'Yes',
						'director_service_address' => 'Yes',
						'business_address' => 'Yes',
						'price' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id 
				);
			} elseif ($package_value == "complete_package") {
				$data_order = array (
						'registered_office' => 'Yes',
						'director_service_address' => 'Yes',
						'business_address' => 'Yes',
						'complete_package' => 'Yes',
						'price' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id,
				);
			} elseif ($package_value == "deposit") {
				$data_order = array ( 
						'deposit' => $amount,
						'price' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id 
				);
			}
			$this->db->where('id', $old_order_id );
			if ($this->db->update('tbl_order', $data_order )) {
				$order_reseller = $this->order->getOrderById($old_order_id);
				$reseller = $this->order->getReseller($order_reseller);
				$product = trim($package_name, $field_id );
				
				$data_order_details = array(
						'order_summery_id' => $old_order_id,
						'product' => $product,
						'price' => $amount,
						'quantity' => $quantity,
						'total' => $new_price,
						'create_user_id' => $user_order->create_user_id,
						'update_user_id' => $user_id,
						'create_time' => $create_time 
				);
				
				
/*====================================================================================*/
/*====================================================================================*/				
				if($profile ['order_options'] == 1)
				{
				
					
					$add_company_postal_deposit = "0.00";
					$add_company_formation = 2;
					$extend_time = date ( "Y-m-d h:i:s", strtotime ( date ( "Y-m-d h:i:s", strtotime ( $create_time ) ) . " + 1 year" ) );
	/*===================company====================*/
	
	$this->db->select ( '*' );
	$this->db->from ( 'tbl_company' );
	$this->db->where ( 'tbl_company.id ',$user_order->company_id );
	$query_company = $this->db->get ();
	$user_company = $query_company->row();
	
	$add_company_location_popup = $user_company->location;
	
	/*===================mailling====================*/
	
	$this->db->select ( '*' );
	$this->db->from ( 'tbl_mailling_address' );
	$this->db->where ( 'tbl_mailling_address.id ',$user_company->mailing_adress_id );
	$query_mailling = $this->db->get ();
	$user_mailling = $query_mailling->row();

	$mailling_data = array(
		'street'=>$user_mailling->street,
		'country'=>$user_mailling->country,
		'city'=>$user_mailling->city,
		'postcode'=>$user_mailling->postcode,
		'county'=>$user_mailling->county,		
		'create_user_id'=>$user_mailling->create_user_id,
		'update_user_id'=>$user_id,
		'create_time'=>$create_time
		
	); 
	

/*===================billing====================*/
	
	$this->db->select ( '*' );
	$this->db->from ( 'tbl_billing_address' );
	$this->db->where ( 'tbl_billing_address.id ',$user_company->billing_adress_id );
	$query_billing = $this->db->get ();
	$user_billing = $query_billing->row();
	
	$billing_data = array(
	    'billing_name'=>$billing_name->street,
		'street'=>$user_billing->street,
		'country'=>$user_billing->country,
		'city'=>$user_billing->city,
		'postcode'=>$user_billing->postcode,
		'county'=>$user_billing->county,		
		'create_user_id'=>$user_billing->create_user_id,
		'update_user_id'=>$user_id,
		'create_time'=>$create_time
		
	); 		
	
	/*=================save company========================*/
		$this->db->insert('tbl_mailling_address', $mailling_data);
		$this->db->insert('tbl_billing_address', $billing_data);
	
	
	$mailling_id = $this->search->getMaillingByTime($create_time);
	$billing_id = $this->search->getBillingByTime($create_time);
	
	$company_data = array(
		'company_name'=>$user_company->company_name,
		'trading'=>$user_company->trading,
		'director1'=>$user_company->director1,
		'director2'=>$user_company->director2,
		'director3'=>$user_company->director3,
		'director4'=>$user_company->director4,
		'location' => $user_company->location,
		'mailing_adress_id'=> $mailling_id->id,
		'billing_adress_id' => $billing_id->id,
		'create_user_id'=>$user_company->create_user_id,
		'update_user_id'=>$user_id,
		'create_time'=>$create_time
		
	); 

			
			if ($this->db->insert('tbl_company', $company_data)) {
			
				$this->db->select('*,tbl_company.id as id');
				$this->db->from('tbl_company');
				$this->db->where('tbl_company.create_time', $create_time);
				$this->db->where('tbl_company.create_user_id', $user_company->create_user_id);
				$this->db->where('tbl_company.company_name', $user_company->company_name);
				
				$query_company2 = $this->db->get ();
				$company2 = $query_company2->row ();
				
				$package_value = $this->order->getOrders ( $profile ['service_name'],$old_order_id );
				$product = $package_value;
				//var_dump($profile ['service_name'],$package_value);die;
				$reseller_id = $user_order->reseller_id;
						//$product = trim($package_name, $field_id );
				if ($product == 'complete_package') {
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'business_address' => 'Yes',
							'price' => $amount,
							'quantity' => $quantity,
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				} elseif ($product == 'both') {
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'price' => $amount,
							'quantity' => $quantity,
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				} 
				elseif($product == 'partner')
				{
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'business_address' => 'Yes',
							'price' => $amount,
							'quantity' => $quantity,
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				}
				else {
					
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit, // '0.00',
							$product => 'Yes',
							'price' => $amount,
							'quantity' => $quantity,
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				}
				
				if ($this->db->insert ( 'tbl_order', $data_order )) {
				$this->db->select ( '*,tbl_order.id as id' );
					$this->db->from ( 'tbl_order' );
					
					$this->db->where ( 'tbl_order.create_time', $create_time );
					$this->db->where ( 'tbl_order.create_user_id', $user_company->create_user_id );
					$this->db->where ( 'tbl_order.company_id', $company2->id );
					
					$query_order = $this->db->get ();
					$order = $query_order->row ();
					
					$data_order_detail = array (
							'order_summery_id' => $order->id,
							
							// 'product' => $product,
							'product' => trim($package_name, $field_id ),
							'total' => $price+$add_company_postal_deposit,
							'price' => $amount,
							'quantity' => $quantity,
							'create_user_id'=> $user_company->create_user_id ,
							'create_time'=>$create_time 
					);
					if($this->db->insert('tbl_order_detail', $data_order_detail)) {
						
						$this->db->select ( '*' );
					$this->db->from ( 'tbl_alt_email' );					
					$this->db->where ( 'tbl_alt_email.company_id',$user_order->company_id );
					
					$query_alert = $this->db->get ();
					$alert = $query_alert->row ();
					$alert_data= array(
					'company_id'=>$company2->id,
					'email_2' => $alert->email_2,
					'email_3' => $alert->email_3,
					'email_4' => $alert->email_4,
					'create_user_id'=> $user_company->create_user_id ,
					'create_time'=>$create_time					
					);
					if($this->db->insert('tbl_alt_email', $alert_data)) {
						$this->db->select ( '*' );
						$this->db->from ( 'tbl_message' );					
						$this->db->where ( 'tbl_message.order_id',$field_id );						
						$query_message = $this->db->get ();
						$msgs = $query_message->result ();
						if($msgs){
						foreach($msgs as $msg){
							$msg_data= array(
						'order_id'=>$order->id,
						'message' => $msg->message,	
						'to_id'=>$msg->to_id,
						'from_id' => $msg->from_id,	
						'tel_no'=>$msg->tel_no,
						'email_id' => $msg->email_id,						
						'create_user_id'=> $user_company->create_user_id ,
						'create_time'=>$create_time					
						);
							if($this->db->insert('tbl_message', $msg_data)) {
								echo "done";
							}
							else
							 {
								var_dump ( $this->db->_error_message () );
								die ();
							}
						
						}
						$this->db->select ( '*' );
					$this->db->from ( 'tbl_file_info' );					
					$this->db->where ( 'tbl_file_info.company_id',$user_order->company_id );
						$query_file = $this->db->get ();
					$file_infos = $query_file->result();
						
						foreach($file_infos as $file_info)
						{
							$file_info_data= array(
							'company_id'=>$company2->id,
							'file_name' => $file_info->file_name,	
							'type_id' => $file_info->type_id,
							'state_id' => $file_info->state_id,		
							'req_time' => $file_info->req_time,						
							'create_user_id'=> $user_company->create_user_id ,
							'create_time'=>$create_time					
							);
							if($this->db->insert('tbl_file_info', $file_info_data)) {
							}
								else {
								var_dump ( $this->db->_error_message () );
								die ();
							}
						
						}
						
						}
						else
						
						{
						
								$this->db->select ( '*' );
					$this->db->from ( 'tbl_file_info' );					
					$this->db->where ( 'tbl_file_info.company_id',$user_order->company_id );
						$query_file = $this->db->get ();
					$file_infos = $query_file->result();
						
						foreach($file_infos as $file_info)
						{
								$file_info_data= array(
							'company_id'=>$company2->id,
							'file_name' => $file_info->file_name,	
							'type_id' => $file_info->type_id,
							'state_id' => $file_info->state_id,		
							'req_time' => $file_info->req_time,						
							'create_user_id'=> $user_company->create_user_id ,
							'create_time'=>$create_time					
							);
							if($this->db->insert('tbl_file_info', $file_info_data)) {
							}
								else {
								var_dump ( $this->db->_error_message () );
								die ();
							}
						
						}
						
						
						
						}
						
					
					
					}
							else {
						var_dump ( $this->db->_error_message () );
						die ();
					}
						
						
					}
					else {
						var_dump ( $this->db->_error_message () );
						die ();
					}
				
				}
				else {
					var_dump ( $this->db->_error_message () );
					die ();
					}
				
			
			
			}
		else {
					var_dump ( $this->db->_error_message () );
					die ();
			}

					
					
				}
else
	{
				
					if ($this->db->insert ( 'tbl_order_detail', $data_order_details )) {
				
					/*========send mail to admin=======*/
					$header = 'From: The London Office<post@theoffice support>' . "\r\n";
					$header .= 'MIME-Version: 1.0'."\r\n";
					$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$subject = "The Office Support > ".$company_name." > You Have Mail!";
					$new_msg ='<h3>You have a New order from '.$company_name.'</h3><hr>'."<br><br>";
					$new_msg .='Hello ,'."<br><br>";		
					$new_msg .= 'We`re writing to let you know that you have a new order.'."<br><br>";
					$new_msg .= 'The order details of the request are:'."<br><br>";
					$new_msg .= 'Company Name : '.$company_name."<br><br>";
					$new_msg .= 'Reseller Name : '.$reseller."<br><br>";
					$new_msg .= 'Service : '. $service_name."<br><br>";
					$new_msg .= 'Amount : £'. $amount."<br><br>";
					$new_msg .= 'Quantity : '. $quantity."<br><br>";
					$new_msg .= 'Date of Order : '. date ( "Y-m-d", strtotime ( $create_time ) ) ."<br><br>";
					$new_msg .= 'Time of Order :  '. date ( "h:i A", strtotime ( $create_time ) )."<br><br>";
					$new_msg .= 'Type : '. $mode."<br><br>";
					$new_msg .= '<b>Best Regards,'."<br><br>";
					$new_msg .= 'The London Office'."<br><br>";
					$new_msg .='The Admin Team</b>'."<br><br>";

						if(mail ("order@thelondonoffice.com",$subject,$new_msg,$header ))
						{
							$data_activity = array (
				'company_id' => $user_order->company_id,
				'activity' => "New Order Placed",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id ,
				'email' => $email 
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			else {
				var_dump ( $this->db->_error_message () );
				die ();
				}
						}
					
					/*==============================*/
				} else { // echo "not done";
				}
				
				}
			}
		}
		redirect ( 'dashboard/index', 'refresh' );
	}
	
	function partner() {
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$email = $_POST ["reseller_email"];
		
		if (! empty ( $_POST )) {			
			$company_name = $_POST ["reseller_company_name"];
			$contact_name = $_POST ["reseller_contact_name"];
			$mail_email = $email;
			$phone_number = $_POST ["reseller_phone"];
			$address1 = $_POST ["reseller_address1"];
			$address2 = $_POST ["reseller_address2"];
			$county = $_POST ["reseller_county"];
			$post_code = $_POST ["reseller_post_code"];
			$web_address = $_POST ["reseller_weburl"];
			$admin_email = $_POST ["reseller_admin_email"];
			$reseller_parent = $_POST ["reseller_parent"];
			//var_dump($reseller_parent);die("$reseller_parent");
			$town = $_POST ["reseller_town"];
			$country = $_POST ["reseller_country"];
			$logo_text = $_POST ["reseller_logo_name"];
			$user_id = $arr['user_id'];
			$create_user_id = $user_id;
			$create_time = date ( 'Y-m-d h:i:s' );
			$password = md5 ( $_POST ["reseller_admin_password"] );
			
			if(!empty ($email))
			$user_email = $email;
		else
			$user_email = $_POST ["reseller_admin_email"];
		
		$user_res_id = $this->search->getUserByEmail($user_email);	
		if(!empty($user_res_id))
		{ 
			$user_id_reseller = $user_res_id->id;
		}
		else
		{
			$data_reseller = array (
							'first_name' => $contact_name,
							'email' => $user_email,
							'password' => $password,
							'ph_no' => $phone_number,
							'county' => $county,
							'postcode' => $post_code,
							'city' => $town,
							'counrty' => $country,
							'role_id' =>2,
							'create_user_id' =>$create_user_id,
							'create_time' => $create_time);
					if($this->db->insert ( 'tbl_user', $data_reseller ))
					{
						$user_res_id = $this->search->getUserByEmail($user_email);
						$user_id_reseller = $user_res_id->id;
					}
					else
					{
					
					var_dump ( $this->db->_error_message () );
					die ();
					}
		}
			if ($admin_email = "") {
				$admin_email = $mail_email;
			} else {
				$admin_email = $_POST ["reseller_admin_email"];
			}
			
			$user_data = $this->search->resellerEmail($email);
			if ($user_data) {
				$data = array (
					'role_id' => "2",
					'password' => $password
			);
				
				$this->db->where('id', $user_data->id);
				if ($this->db->update('tbl_user', $data)) {				
			$this->db->select('*');
			$this->db->from('tbl_reseller');
			//$this->db->where('company_name ='$company_name' AND admin_email='$admin_email' AND contact_name ='$contact_name);
			$this->db->where('company_name',$company_name);
			$this->db->where('admin_email',$admin_email);
			$this->db->where('contact_name',$contact_name);
			$query_user = $this->db->get ();
			$user_reseller = $query_user->row ();			
			
			if($user_reseller)
			{
				$data_order = array('state_id'=>0);
				$this->db->where ( 'id', $user_reseller->id );
			 $this->db->update ( 'tbl_reseller', $data_order );
			}
			else
			{	$data_reseller = array (
							'company_name' => $company_name,
							'contact_name' => $contact_name,
							'mail_email' => $mail_email,
							'phone_number' => $phone_number,
							'address1' => $address1,
							'address2' => $address2,
							'county' => $county,
							'post_code' => $post_code,
							'web_address' => $web_address,
							'admin_email' => $admin_email,
							'town' => $town,
							'country' => $country,
							'parent_reseller' => $reseller_parent,
							'logo_text' => $logo_text,
							'user_id' => $user_id_reseller,
							'create_user_id' =>$create_user_id,
							'create_time' => $create_time);
					$this->db->insert ( 'tbl_reseller', $data_reseller );
			
			}
			
				
				} else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
			} else {
				$data_user = array (
						'first_name' => $contact_name,
						'email' => $mail_email,
						'password' => $password,
						'ph_no' => $phone_number,
						'street' => $address1,
						'city' => $town,
						'counrty' => $country,
						'postcode' => $post_code,
						'county' => $county,
						'role_id' => '2',
						'state_id' => '1',
						'create_user_id' => $user_id,
						'create_time' => $create_time	/*	*/
					);
			if ($this->db->insert ( 'tbl_user', $data_user )) {
			$user_id_inserted = $this->db->insert_id();
			$this->db->select('*');
			$this->db->from('tbl_reseller');
			//$this->db->where('company_name ='$company_name' AND admin_email='$admin_email' AND contact_name ='$contact_name);
			$this->db->where('company_name',$company_name);
			$this->db->where('admin_email',$admin_email);
			$this->db->where('contact_name',$contact_name);
			$query_user = $this->db->get();
			$user_reseller = $query_user->row ();				
			if($user_reseller)
			{
				$data_order = array('state_id'=>0);
				$this->db->where ( 'id', $user_reseller->id );
			 $this->db->update ( 'tbl_reseller', $data_order );
			}
			else
			{	$data_reseller = array (
							'company_name' => $company_name,
							'contact_name' => $contact_name,
							'mail_email' => $mail_email,
							'phone_number' => $phone_number,
							'address1' => $address1,
							'address2' => $address2,
							'county' => $county,
							'post_code' => $post_code,
							'web_address' => $web_address,
							'admin_email' => $admin_email,
							'town' => $town,
							'country' => $country,
							'parent_reseller' => $reseller_parent,
							'logo_text' => $logo_text,
							'user_id' => $user_id_inserted,
							'create_user_id' =>$create_user_id,
							'create_time' => $create_time);
					$this->db->insert ( 'tbl_reseller', $data_reseller );
			
			}
				redirect ( 'dashboard/index', 'refresh' );
				
				} 

				else {
					var_dump ( $this->db->_error_message () );
					die ();
				}
			}
		}
		
		$data['username'] = $session_data ['first_name'];
		$data ['user_orders'] = $user_orders;
		$data ['record_count'] = count($user_orders);
		$data['role_id'] = $arr['role_id'];
		// var_dump($data ['role_id']);die;
		//$this->load->view ( 'home_view', $data );
		
		redirect ( 'home/manageReseller', 'refresh' );
	}
public function deleteCompany($comp_id)
{
	$delete_id = $comp_id;
		/*===========================================GET ORDER COMPANY ID========================================================*/
		/*$this->db->select ( '*,tbl_order.id as id' );
		$this->db->from ( 'tbl_order' );
		$this->db->where ( 'tbl_order.id', $delete_id );
		$query_company_id = $this->db->get ();
		$company_id = $query_company_id->row ();
		$delete_id = $company_id->company_id;*/
		/*=======================================================================================================================*/
		//var_dump($delete_id,$company_id->company_id);
		/* ==============company=============== */
		$this->db->select ( '*,tbl_company.id as id,tbl_company.mailing_adress_id as mailing_adress_id,tbl_company.billing_adress_id as billing_adress_id ' );
		$this->db->from ( 'tbl_company' );

		$this->db->where ( 'tbl_company.id', $delete_id );

		$query_company = $this->db->get ();
		$company = $query_company->row ();
		// var_dump($company);die;
		/* ==============orders=============== */
		$this->db->select ( '*,tbl_order.id as id' );
		$this->db->from ( 'tbl_order' );

		$this->db->where ( 'tbl_order.company_id', $delete_id );

		$query = $this->db->get ();
		$orders = $query->row ();

		/* ================order details============= */
		$this->db->where ( 'tbl_order_detail.order_summery_id', $orders->id );
		if ($this->db->delete ( 'tbl_order_detail' )) {
				//var_dump('tbl_order_detail');
			$this->db->where ( 'tbl_order.company_id', $delete_id );
			if ($this->db->delete ( 'tbl_order' )) {
			//var_dump('tbl_order');

				$this->db->where ( 'tbl_file_info.company_id', $delete_id );
				if ($this->db->delete ( 'tbl_file_info' )) {
				//var_dump('tbl_file_info');

					$this->db->where ( 'tbl_company.id', $delete_id );
					if ($this->db->delete ( 'tbl_company' )) {
						//var_dump('tbl_company');
						$this->db->where ( 'tbl_billing_address.id', $company->billing_adress_id );
						if ($this->db->delete ( 'tbl_billing_address' )) {

						//var_dump('tbl_billing_address');

						/* ================mailing Address=====2======== */
							$this->db->where ( 'id', $company->mailing_adress_id );
							if ($this->db->delete ( 'tbl_mailling_address' )) {
								//var_dump('tbl_mailling_address');
								/* ================messages=====2======== */
								$this->db->where ( 'tbl_message.order_id', $orders->id );

								if ($this->db->delete ( 'tbl_message' )) {
								//var_dump('tbl_message');die;
									redirect ( 'home/state_change', 'refresh' );

								} else {
									echo $this->db->_error_message ();die;
								}
							} else {
								echo $this->db->_error_message ();die;
							}
						} else {
							echo $this->db->_error_message ();die;
						}
					} else {
						echo $this->db->_error_message ();die;
					}
				} else {
					echo $this->db->_error_message ();die;
				}
			} else {
				echo $this->db->_error_message ();die;
			}
		} else {
			echo $this->db->_error_message ();die;
		}
}
public function advanceSearch()
{
	$page = $this->input->get_post('page');
	$pageeses = $this->session->userdata('pageno');	
		if($page==''){
				$data['pagef']='50';
		}else{
				$data['pagef']=$page;
		}
	$session_data = $this->session->userdata('logged_in');
	$arr['user_id'] = $session_data['id'];
	$arr['role_id'] = $session_data['role_id'];	
	/*	$setstate = $this->session->set_userdata('string',$state_change);
			$arr['state_change'] = $this->session->userdata('state_change');*/
	$data_posted= urldecode($_SERVER['QUERY_STRING']);
	$pieces = explode("&", $data_posted);
	for($a=0;$a<count($pieces);$a++)
	{
		$profile_key=strstr($pieces[$a],"=",true);
		$profile[$profile_key] = substr(strstr($pieces[$a],"="),1);
	}	
		$arr['company_name'] = $this->input->get_post('advance_company_name');
		$arr['state_id'] = $profile['advance_state_id'];
		if($profile['advance_state_id']=='10')
		$arr['state_id']='0';
		$arr['type_id'] = $profile['advance_type_id'];
		$arr['reseller_id'] = $profile['advance_reseller_id'];
		$arr['location_id'] = $this->search->getLocationOptions($profile['advance_location_id']);
		$arr['register_id'] = $this->search->getServiceOptions($profile['advance_register_id']);
		$arr['director_id'] = $this->search->getServiceOptions($profile['advance_director_id']);
		$arr['business_id'] = $this->search->getServiceOptions($profile['advance_business_id']);
		$arr['telephone_id'] = $this->search->getServiceOptions($profile['advance_telephone_id']);
		/* $arr['host_id'] = $this->search->getServiceOptions($profile['advance_host_id']); */		
		$arr['website_id'] = $this->search->getServiceOptions($profile['website_id']);		
		$arr['register__id'] = $this->search->getServiceOptions($profile['register__id']);		
		$arr['legal_id'] = $this->search->getServiceOptions($profile['legal_id']);		
		$arr['load_id'] = $profile['advance_load_id'];
		$arr['renewal'] = $profile['advance_renew_company_from'];	
		$arr['renewal_to'] = $profile['advance_renew_company_to'];	
//var_dump($arr['website_id']);die("jkjk");		
		/*---pagination start----*/
		$config["base_url"] = base_url()."home/advanceSearch";
		//$config['permitted_uri_chars'] =$data_posted;
		$config['suffix'] = '?'.$data_posted;
		$config["total_rows"]  = $this->home_model->count_advance_search_companies($arr);
		//var_dump($config["total_rows"]);die;
		$config["per_page"] = $data['pagef'];	
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		/*---pagination end----*/
		$show_advance_search_companies= $this->home_model->show_advance_search_companies($config["per_page"],$page,$arr);
		$export ='1';
		$show_advance_search_companies1= $this->home_model->show_advance_search_companies($config["per_page"],$page,$arr,$export); // for export
		//print_r($show_advance_search_companies);die;
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $show_advance_search_companies1;
			$this->load->view('spreadsheet_view', $data1);
		}else{
			$data['username'] = $session_data ['first_name'];
			$data['user_orders'] = $show_advance_search_companies;
			$data['user_orders1'] = $show_advance_search_companies1;
			$data['role_id'] = $arr['role_id'];
			//$data['data_posted']=$data_posted;
			$data['record_count'] = $config["total_rows"] ;
			$this->load->view ( 'home_view', $data );
		}
}
	function editTeleService()
	{
	
		$session_data = $this->session->userdata('logged_in');
		$id = $session_data ['id'];
		$data ['username'] = $session_data ['first_name'];
	
		$company_id = $this->input->post('company_id');
	
		$tele_answer_as  =$this->input->post('call_as');
		$tele_number = $this->input->post('tele_numb');
		$tele_fax_number  = $this->input->post('fax_numb');
		$tele_msg_to = $this->input->post('tele_msg');
		$tele_instruction1  = $this->input->post('tele_inst');
	
		$create_time = date ('Y-m-d h:i:s');
		$create_user_id  = $id;
		$update_user_id = $id;
	
		/*=============find company and update================*/
		$telephone_service = $this->search->comp_tele_service($company_id);
	
		if(! empty($telephone_service))
		{
			$data = array(
					'answer_as'=>$tele_answer_as,
					'tele_number'=>$tele_number,
					'fax_number'=>$tele_fax_number,
					'msg_to'=>$tele_msg_to,
					'instruction1'=>$tele_instruction1,
					'update_user_id'=>$update_user_id,
			);
			//var_dump($data);die;
			$this->db->where ('id', $telephone_service->id);
			$this->db->update ('tbl_company_tele_service',$data);
		}
		else
		{
			$data = array(
					'company_id' => $company_id,
					'answer_as'=>$tele_answer_as,
					'tele_number'=>$tele_number,
					'fax_number'=>$tele_fax_number,
					'msg_to'=>$tele_msg_to,
					'instruction1'=>$tele_instruction1,
					'create_user_id' => $id,
					'create_time'=> $create_time
			);
			//var_dump($data);die;
			if($this->db->insert ( 'tbl_company_tele_service', $data ))
			{
	         
			}
			else {
				var_dump($this->db->_error_message ());
				die ();
			}
		}
		redirect ( 'dashboard/index', 'refresh' );
	}
	
	function editCompanySecratory()
	{
	
		$session_data = $this->session->userdata('logged_in');
		$id = $session_data ['id'];
		$data ['username'] = $session_data ['first_name'];	
		$company_id = $_POST['company_id'];		
		$secratory_name  = $_POST['sec_name'];
		$secratory_appoint = date ('Y-m-d h:i:s',strtotime($_POST['sec_appoint']));
		$create_time = date ('Y-m-d h:i:s');
		$create_user_id  = $id;
		$update_user_id = $id;
	
		$secretries = $this->search->comp_secretary($company_id);
	
		if(! empty($secretries)){
			$data = array(
					'name'=>$secratory_name,
					'appoint_date'=>$secratory_appoint,
					'state_id' => 1,
					'update_user_id'=>$update_user_id,
			);
			$this->db->where ('id', $secretries->id);
			$this->db->update ('tbl_company_secretaries',$data);
		}else{
			$data = array(
					'company_id' => $company_id,
					'name'=>$secratory_name,
					'appoint_date'=>$secratory_appoint,
					'state_id' => 1,
					'create_user_id' => $id,
					'create_time'=> $create_time
			);
			if($this->db->insert ( 'tbl_company_secretaries', $data )){
	
			}else{
				var_dump($this->db->_error_message ());
				die ();
			}
		}
		
		redirect ( 'dashboard/index', 'refresh' );
	}
	public function editDiposite()
	{
		$order_d= $this->input->get_post('order_deposite_id');
		$deposite_value=$this->input->get_post('order_deposite_value');
		if($deposite_value !="")
			$deposite_value= $deposite_value;
		else
			$deposite_value = 0;
		$data = array(
					'deposit'=>$deposite_value,
			);
			$this->db->where ('id', $order_d);
			$this->db->update ('tbl_order',$data);
		redirect ( 'dashboard/index', 'refresh' );
		
	}
	function ltdSearch() {
 		 $page =$_POST['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$ltd_search = $this->input->post('comp_ltd_select');
		
		if($ltd_search !=''){
			$setstate = $this->session->set_userdata('ltd_search',$ltd_search);
			$arr['comp_ltd_select'] = $this->session->userdata('ltd_search');
		}else{
	 		$arr['comp_ltd_select'] = $this->session->userdata('ltd_search');
		}
		
		$config["base_url"] = base_url() . "home/ltdSearch";
		$config["total_rows"]  = $this->home_model->count_ltd_change($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_ltd_change($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->show_ltd_change($config["per_page"],$page,$arr,$export);
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
		//var_dump($user_orders);die;
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['ltdSearch'] = $arr['ltdSearch'];
 		$this->load->view ( 'home_view', $data );
		}
	}
	
	function newMail(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
	    $comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		$to = $userid['email'];
	
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}else{
			$header = 'From: The London Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = "The Office Support > ".$comp_name." > You Have Mail!";
		
		$msg = "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name.",</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We're writing to let you know that you have business mail!"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The mail has been sorted and will be forwarded to you by the end of this week."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Should you require your mail sooner please do let us know."."</p>";
		
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Best regards,"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The London Office Support Team"."</p>";
		
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Disclaimer"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>E-mails and any attachments from The London Office are confidential."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>If you are not the intended recipient, please notify the sender immediately by replying to the e-mail, and then delete it without making copies or using it in any way."."<br><br>";
		$msg .= "Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening "."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>attachments, since The Registered Office  accepts no responsibility for loss or damage caused by software viruses.</p>";
		
		if(mail($to,$subject,$msg,$header)){
				echo "1";
				
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Business Mail Alert",
				'Description' => "Business Mail Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
		
	}
	
	function newMailO(){
	   $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		//var_dump($email);die();
		$comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		//var_dump($order);die("fgd");
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		$to = $userid['email'];
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@thelondonoffice.com>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}else{
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
		}
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = 'The Office Support >'. $comp_name .'> You Have Mail!'; // subject of email
		$msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
		$msg .= '<div>';
		$msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
		$msg .= '</div>';
		$msg .= "<br>";
		$msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
		$msg .= "<br>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name.",</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We're writing to let you know that you have official mail!"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The mail has been sorted and will be forwarded to you by the end of this week."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Should you require your mail sooner please do let us know."."</p>";
		$msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$msg .= '</div>';
		
		if(mail($to,$subject,$msg,$header)){
				echo "1";
				
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Official  Mail Alert",
				'Description' => "Business Mail Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
		
	}
	
	function newbusinessemail(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
	    $comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		$to = $userid['email'];
		
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@thelondonoffice.com>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}else{
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
		}
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = 'The Office Support >'. $comp_name .'> You Have Mail!'; // subject of email
		$msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
		$msg .= '<div>';
		$msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
		$msg .= '</div>';
		$msg .= "<br>";
		$msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
		$msg .= "<br>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name.",</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We're writing to let you know that you have official mail!"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The mail has been sorted and will be forwarded to you by the end of this week."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Should you require your mail sooner please do let us know."."</p>";
		$msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt;color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$msg .= '</div>';
		
		if(mail($to,$subject,$msg,$header)){
				echo "1";
				
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Official  Mail Alert",
				'Description' => "Business Mail Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
		
	}
	function upgradeEmail(){
		$session_data = $this->session->userdata('logged_in');
		$email = $session_data ['email'];
		$comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		$typeby = $this->input->post('typeby');
		$to = $userid['email'];
		
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@thelondonoffice.com>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Admin Copy<admincopy@thelondonoffice.com>' . "\r\n";
		}else{
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
		}
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = 'The Office Support >'. $comp_name .'> You Have Mail!'; // subject of email
		$msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
		$msg .= '<div>';
		$msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
		$msg .= '</div>';
		$msg .= "<br>";
		$msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
		$msg .= "<br>";
		$msg = "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name.",</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We have received an item (or items) of mail which falls outside the mail service you have with us."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>There are two available options:"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Option 1</b>: We can return the mail to sender and ask that you only use our address for the service ordered."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'><b>Option 2</b>: Change your account to include the mail received."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>It would be appreciated if you could contact us within 5 days so we can either return the mail to sender or upgrade your account."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Should we not receive a reply within 5 days all mail received will be returned to sender without further notice."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Thank you in advance."."</p>";
		$msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
		$msg .= '</div>';
		if(mail($to,$subject,$msg,$header)){
				echo "1";
				if($typeby == "vba")
				{
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "VBA Upgrade Request",
				'Description' => "VBA Upgrade Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $userid['id'],
				'email' => $email
		);
		}
		else if($typeby == "dsa"){
		$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "DSA Upgrade Request",
				'Description' => "DSA Upgrade Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $userid['id'],
				'email' => $email
		);
		}
		else
		{
		$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "ROA Upgrade Request",
				'Description' => "ROA Upgrade Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $userid['id'],
				'email' => $email
		);
	}
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
		
	}
	

public function compnay_update_new(){
$update_compnay_id = $this->input->post('update_compnay_id');
$comp_new_up = $this->input->post('comp_new_up');
//$create_user_id = $this->input->post('create_user_id');
$notes = $this->input->post('comment');
//var_dump($create_user_id );die();
$UpdateCompanyName = $this->ltd_model->UpdateByCompnayName($update_compnay_id,$comp_new_up);
//var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}
/*trading new */
public function trading_update_new(){
$update_compnay_id = $this->input->post('update_compnay_id');
$comp_new_up = $this->input->post('comp_new_up');
//$create_user_id = $this->input->post('create_user_id');
$notes = $this->input->post('comment');
//var_dump($create_user_id );die();
$UpdateCompanyName = $this->ltd_model->UpdateByTradingName($update_compnay_id,$comp_new_up);
//var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}
/*xm companya name update */
public function xmlcompany_update_name(){
$update_compnay_xmlid = $this->input->post('update_compnay_xmlid');
$comp_new_upxml = $this->input->post('comp_new_upxml');
//$create_user_id = $this->input->post('create_user_id');
//$notes = $this->input->post('comment');
//var_dump($comp_new_upxml );die();
$UpdateCompanyName = $this->ltd_model->UpdateByXmlCompanyName($update_compnay_xmlid,$comp_new_upxml);
//var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}


	function topup(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$comp_id = $this->input->get_post('company_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$comp_name = $this->search->getCompany($comp_id);

		$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
		$this->db->from ( 'tbl_user' );
		$this->db->where ( 'tbl_user.id', $comp_name->create_user_id );
		$query_user = $this->db->get ();
		$user = $query_user->row ();
		
		$this->db->select ( '*' );
		$this->db->from ( 'tbl_alt_email' );
		$this->db->where ( 'tbl_alt_email.company_id', $comp_id );
		
		$query_emails = $this->db->get ();
		$alt_emails = $query_emails->result ();
		
		$emails = array ();
		$emails ['email_1'] = $user->email;
		foreach ( $alt_emails as $alt_email ) {
			$emails ['email_2'] = $alt_email->email_2;
			$emails ['email_3'] = $alt_email->email_3;
			$emails ['email_4'] = $alt_email->email_4;
		}
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@theoffice support>'. "\r\n"; 
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>'. "\r\n"; 
		}else{
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
		}
		$header .= 'MIME-Version: 1.0'."\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$subject = 'The Office Support >'. $comp_name->company_name .'> You Have Mail!'; // subject of email
		$msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
		$msg .= '<div>';
		$msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
		$msg .= '</div>';
		$msg .= "<br>";
		$msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
		$msg .= "<br>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name->company_name.",<p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We're writing to let you know that your postal deposit has fallen below &#163;5.00 and ask that you top up your account."."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Please use the following link to make a payment of &#163;20.00 (&#163;50.00 if overseas): <a href='https://thelondonoffice.com/index/make_payment'>https://thelondonoffice.com/index/make_payment</a>"."</p>";
		$msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Should you need to discuss this before proceeding please do not hesitate to contact us."."</p>";
		$msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$msg .= '</div>';
		$email_to = implode ( ',', $emails );
		$email_to = $email_to;
		//var_dump($email_to);die("mkj");
		if(mail($email_to,$subject,$msg,$header)){
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Deposit Request",
				'Description' => "Deposit Request Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $userid['id'],
				'email' => $email
				);
				 
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
					echo "1";
				}
			//redirect ( 'dashboard/index', 'refresh' );
			
		}else
			die("email not sent");
	}
	
	
/*function editHost()
	{
	
		$session_data = $this->session->userdata('logged_in');
		$id = $session_data ['id'];
		$data ['username'] = $session_data ['first_name'];
	
		$company_id = $_POST['host_company_id'];	
	
		$location  = $_POST['host_location'];
		$user_name = $_POST['host_username'];
		
		$create_time = date ('Y-m-d h:i:s');
		$create_user_id  = $id;
		$update_user_id = $id;
	
		
		$hosting = $this->company->comp_host($company_id);
	
		if(! empty($hosting))
		{
			$password  = (!empty($_POST['host_password']))?md5($_POST['host_password']):$hosting->password;
			$data = array(
					'location'=>$location,
					'username'=>$user_name,
					'password'=>$password,
					'update_user_id'=>$update_user_id,
			);
			$this->db->where ('id', $hosting->id);
			$this->db->update ('tbl_company_hosting',$data);
		}
		else
		{
			$password  = md5($_POST['host_password']);
			$data = array(
					'company_id' => $company_id,
					'location'=>$location,
					'username'=>$user_name,
					'password'=>$password,
					'create_user_id' => $id,
					'create_time'=> $create_time
			);
			if($this->db->insert ( 'tbl_company_hosting', $data ))
			{
	
			}
			else {
				var_dump($this->db->_error_message ());
				die ();
			}
		}
		redirect ( 'users/hosts', 'refresh' );
	}*/
	function billing_change() {
 		 $page =$_POST['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$type_change = $this->input->post('type_change');
		if($type_change !=''){
			$setstate = $this->session->set_userdata('type_change',$type_change);
			$arr['type_change'] = $this->session->userdata('type_change');
		}else{
	 		$arr['type_change'] = $this->session->userdata('type_change');
		}
		$config["base_url"] = base_url() . "home/billing_change";
		$config["total_rows"]  = $this->home_model->count_type_change($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_type_change($config["per_page"],$page,$arr);
		$export='1';
		$user_orders1 = $this->home_model->show_type_change($config["per_page"],$page,$arr,$export);
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			//count_reseller
			$data ['role_id'] = $arr['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['username'] = $session_data['first_name'];
			$data['record_count'] = $config["total_rows"] ;
			$data['type_change'] = $arr['type_change'];
			$this->load->view ( 'home_view', $data );
		}
	}
	function idRequired(){
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$comp_name = $this->search->getCompany($this->input->post( "new_required_id"));
		$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
		$this->db->from ( 'tbl_user' );
		$this->db->where ( 'tbl_user.id', $comp_name->create_user_id );
		$query_user = $this->db->get ();
		$user = $query_user->row ();
		    $to = $user->email;
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
			$header .= 'MIME-Version: 1.0' . "\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$subject = 'The Office Support >'. $comp_name->company_name .'> You Have Mail!';
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$new_msg .= "<html>
						<head>
						<title>We need your ID</title>
						</head>
						<body>
							<h3><p style='font-size: 22pt; font-family: Calibri; margin: 6px 0px;'>We need your ID!</p></h3>
							<hr>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name->company_name.", 	</p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>We have received mail for you which is currently being held as we have not received your ID.</p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Please log in to your account and upload your ID or send your ID to id@thelondonoffice.com</p>
					</body>
					</html>";
					$new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt;color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
			if (mail ( $to, $subject, $new_msg, $header )){
				//redirect ( 'dashboard/index', 'refresh' );
				$page = $this->input->get_post('page');
				$pageeses = $this->session->userdata('pageno');
				if($page==''){
					$data['pagef']  = '50';
				}else{
					$data['pagef']  = $page;
				}
				$config["base_url"] = base_url() . "dashboard/searchResult";
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['new_search_bar'] = trim($this->input->get_post('query_string_1'));
				$data['search_new'] = $this->input->get_post('query_string_2');
				//var_dump($data);die("Gdf");
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					//var_dump($show_companies);die("search_all_new");
				}else if($data['search_new']=="search_company_new"){
					$type="search_company_new"; die("search_company_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}elseif($data['search_new']=="search_director_new"){
					$type="search_director_new";die("search_director_new");
					$config["total_rows"]  = $this->search_model->countSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies=$this->search_model->getSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				
				$config["base_url"] = base_url() . "dashboard/searchResult";
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
				$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
				$config["per_page"] = $data['pagef'];
				$config["uri_segment"] = 3;
				$this->pagination->initialize($config);
				$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['username'] = $session_data ['first_name'];			
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view('home_view',$data);
			}else{
				var_dump("not done");die;
			}
	}
	
	public function ltd_company()
	{
		if ($this->session->userdata ( 'logged_in' )) {
		$session_data = $this->session->userdata ( 'logged_in' );
		$data['company_id'] = $this->input->get_post('company');
		$role_id = $session_data ['role_id'];
		$data ['role_id'] = $role_id;
		
		$data['comp_info'] = $this->search->filterSearch($data['company_id']);
		$data['comp_reg'] = $this->ltd_company->ltd_company_register($data['company_id']);
		
		$data['comp_director'] = $this->ltd_company->ltd_company_director($data['company_id']);
			/*$data['comp_director_corp'] = $this->ltd_company->ltd_company_director_corp($data['company_id']);		
		$data['comp_shareholder'] = $this->ltd_company->ltd_company_shareholder($data['company_id']);
		$data['comp_shareholder_corp'] = $this->ltd_company->ltd_company_shareholder_corp($data['company_id']);		
		$data['comp_psc'] = $this->ltd_company->ltd_company_psc($data['company_id']);
		$data['comp_psc_corp'] = $this->ltd_company->ltd_company_psc_corp($data['company_id']);		
		$data['comp_secratory'] = $this->ltd_company->ltd_company_secratory($data['company_id']);
		$data['comp_secratory_corp'] = $this->ltd_company->ltd_company_secratory_corp($data['company_id']);
		$data['comp_mna'] = $this->ltd_company->ltd_company_mna($data['company_id']);*/
		
		//var_dump($data['comp_director']);die;
			$this->load->view ( 'ltd_company', $data );
			}
	
	}
	
	public function editPrice(){ 
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$arr['order_price_id'] = $this->input->post('order_price_id');
		$arr['renew'] = $this->input->post('renew');
		$arr['new_order'] = $this->input->post('new_order');
		$comp_id = $this->input->post('edit_price_company_id');
		$uri = $this->input->get_post('uri');
        $uri1 = ltrim($uri, '/');
        $com_uri= base_url() . $uri1;
		$comp_name = $this->search->getCompany($comp_id);
		$order_data  = $this->search_model->getOrderData_new($comp_id);
		//var_dump($arr);die("renew");
		$this->db->select('*');
		$this->db->from('tbl_order');
		$this->db->where('company_id',$comp_name->id);
		$query_order1 = $this->db->get();
		$orders1 = $query_order1->row_array();
		//$date = date('Y-m-d', strtotime($orders1["renewable_date"]));
		$date2 = date('d-M-Y', strtotime($orders1["renewable_date"]));
		$this->db->select('*');
		$this->db->from('tbl_order_detail');
		$this->db->where('order_summery_id',$arr['order_price_id']);
		$query_order = $this->db->get();
		$orders = $query_order->result();
		$pprr=0;
		$text=NULL;
		foreach($orders as $order){
			$total_order_price = $order->price;
			$pprr = $pprr+$total_order_price;
			$title = $order->product;
			$text = $text .' With '.$title;
		}
		$test1 = ltrim($text,' With');
		
		$this->db->select ( '*,tbl_user.id as id,tbl_user.email as email' );
		$this->db->from ( 'tbl_user' );
		$this->db->where ( 'tbl_user.id', $comp_name->create_user_id );
		$query_user = $this->db->get ();
		$user = $query_user->row ();
		$to = $user->email;
		
		if($arr['renew']){
			//var_dump($order_data);die("GDf");
			if($order_data->location=='W1' || $order_data->location=='WC1' || $order_data->location=='SE1' || $order_data->location=='IP4'){
				$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}elseif($order_data->location=='EH2' || $order_data->location=='EH3' ){
				$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}else{
				$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
			}
			$header .= 'MIME-Version: 1.0'."\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$subject = 'The Office Support >'. $comp_name->company_name .'> You Have Mail!'; // subject of email
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			
			$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$new_msg .= "<br>";
			$new_msg .= "<html>
						<head>
						</head>
						<body>
							<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name->company_name.", 	</p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>The service you have with us expires : ".$date2."</p> 
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Please check the details and instructions below to renew your service.</p>
								<p><ul type='disc'><li style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Product or service: ".$test1." - Cost:".$pprr."</li></ul></p>
														
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>To renew your service you should <a href='http://thelondonoffice.com/login'>login to your account</a> and make the payment using the 'Make a Payment' tab or alternatively you can make the <a href='http://thelondonoffice.com/index/make_a_payment'>payment by clicking here</a>.</p>
								
								<p style='font-size: 12pt; font-family: Calibri; margin: 16px 0px;'><b>NOTES:</b></p>
								
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'> <b>(1)</b> The renewal of the service you have with us is shortly due. Please renew to ensure your service remains active. If your service remains overdue for 7 days it will be terminated and all mail received will be returned to sender.</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'> <b>(2)</b> If you no longer require to use our address as the registered office or business address of the Company , you must change the address before the end of the 7 day period. You can change the registered office address and director service address online directly with <a href='https://ewf.companieshouse.gov.uk/seclogin?tc=1'>companies house</a> or alternatively you can fill out <a href='https://www.gov.uk/government/publications/change-a-registered-office-address-ad01'>paper form AD01</a> and send it to Companies House.</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'> If you would like any further information about your order or our other services please do not hesitate to call or email us.</p>
					</body>
					</html>";
                    $new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size: 14.0pt;color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
			//if(mail($to,$subject,$new_msg,$header)){
			
			if(mail($to,$subject,$new_msg,$header)){
			$desc= "Product or service: ".$test1." - Cost:".$pprr."";
			     $data_activity = array (
				'company_id' => $comp_id,
				'activity' => "Renewal Email",
				'Description' => $desc,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				redirect ( $com_uri, 'refresh' );
			}else{
				var_dump("Not Send");die;
			}
		}elseif($arr['new_order']){
			$new_order_info = array();
			 $array = array(
			 'data1'=>array('value_field'=>trim($this->input->post('value_field')),
				'value_name'=>trim($this->input->post('value_name')),),
				
				'data2'=>array(	'value_field'=>trim($this->input->post('value_field1')),
				'value_name'=>trim($this->input->post('value_name1')),),
				
				'data3'=>array('value_field'=>trim($this->input->post('value_field2')),
				'value_name'=>trim($this->input->post('value_name2')),),
				
				'data4'=>array('value_field'=>trim($this->input->post('value_field3')),
				'value_name'=>trim($this->input->post('value_name3')),),
			 
				'data5'=>array('value_field'=>trim($this->input->post('custom_amount')),
				'value_name'=>trim($this->input->post('custom_value')),),
				
			);
			//var_dump($array);die("array");
			$temp_arr = array();
		
			foreach($array as $data_selected){
				//if(!empty($data_selected["value_name"])){
					
					$temp_arr1 = array();
					$temp_arr1['value_name']=$data_selected["value_name"];
					$temp_arr1['value_price']=$data_selected["value_field"];
					$temp_arr[] = $temp_arr1;
					
				//}
				
			}
			//var_dump($temp_arr);
			if($order_data->location=='W1' || $order_data->location=='WC1' || $order_data->location=='SE1' || $order_data->location=='IP4'){
				$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}elseif($order_data->location=='EH2' || $order_data->location=='EH3' ){
				$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}else{
				$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}
			$header .= 'MIME-Version: 1.0'."\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$subject = 'The Office Support >'. $comp_name->company_name .'> You Have Mail!'; // subject of email
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$new_msg .= "<br>";
			$new_msg .= "<html>
						<head>
						</head>
						<body>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Hello ".$comp_name->company_name.", 	</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>Thank you for confirming your new order, details below:</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'></p>";
								foreach($temp_arr as $emial_dat)
			                    {
									if($emial_dat["value_name"]!="" || $emial_dat ["value_price"]!="")
			  					$new_msg .= "<p><ul type='disc'><li style='font-size: 12pt; font-family: Calibri; margin: 0px 0px;'>Product or service: ".$emial_dat ["value_name"]." - Cost: £".$emial_dat ["value_price"]."</li></ul></p>";
								//$new_msg .= "<p style='font-size: 12pt; font-family: Calibri; margin: 0px 0px;'>Cost: ".$emial_dat ["value_price"]."</p>";
									
								}
								$new_msg .="
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'>To pay for your new service you should <a href='http://thelondonoffice.com/login'>login to your account</a> and make the payment using the 'Make a Payment' tab or alternatively you can make the <a href='http://thelondonoffice.com/index/make_a_payment'>payment by clicking here</a>.</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 6px 0px;'> If you would like any further information about your order or our other services please do not hesitate to call or email us.</p>
								
								
					</body>
					</html>";
                $new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
			if(mail($to,$subject,$new_msg,$header)){
			foreach($temp_arr as $emial_dat)
			{
			if($emial_dat["value_name"]=="" || $emial_dat ["value_price"]=="")
			{
			
			}else{
			 //var_dump($emial_dat);
			 $desc= "Product or service: ".$emial_dat["value_name"]." - Cost: £".$emial_dat ["value_price"]."";
			 
			// var_dump($desc);die();
			     $data_activity = array (
				'company_id' => $comp_id,
				'activity' => "New Order Email",
				'Description' => $desc,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $comp_name->create_user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
			}
		}
		
				redirect ($com_uri, 'refresh' );
			}else{
				var_dump("Not Send");die;
			}
		}
		
		//var_dump($arr); 
		
	}
	public function sendRequirement(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		$to = $userid['email'];
		if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
			$header = 'From: The London Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
		}elseif($order->location=='EH2' || $order->location=='EH3' ){
			$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
					'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;

		}else{
			$header = 'From: The Office Support<post@theoffice support>'. "\r\n";
		}					
			$header .= 'MIME-Version: 1.0'."\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$subject = 'The Office Support >'. $comp_name .'> You Have Mail!'; // subject of email
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$new_msg .= "<br>";
			$new_msg .= "<html>
						<head>
						</head>
						<body>
								<h1>We need your ID!</'h1>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Hello ".$comp_name.", 	</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>We have received mail for you which is currently being held as we have not received your ID.</p>
								
					</body>
					</html>";
                 $new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
			if(mail($to,$subject,$new_msg,$header)){
				echo "1";
				
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "ID Required",
				'Description' => "ID Request Email Sent",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				//'create_user_id' => $userid['id']
				'create_user_id' => $user_id,
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
			
	}
	public function sendcancelRequirement(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		//var_dump($email);die();
		$comp_id = $this->input->post('comp_id');
		$order  = $this->search_model->getOrderData_new($comp_id);
		$userid = $this->search->getUser($comp_id);
		$comp_name = $this->input->post('comp_name');
		//var_dump($userid);
			$to = $userid['email'];
			if($order->location=='W1' || $order->location=='WC1' || $order->location=='SE1' || $order->location=='IP4'){
				$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}elseif($order->location=='EH2' || $order->location=='EH3' ){
				$header = 'From: The Edinburgh Office<post@theoffice support>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;

			}else{
				$header = 'From: The London Office<id@thelondonoffice.com>' . "\r\n" .
						'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;
			}				
			$header .= 'MIME-Version: 1.0'."\r\n";
			$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$subject = 'The Office Support >'. $comp_name .'> You Have Mail!';
			$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
			$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
			$new_msg .= "<br>";
			$new_msg .= "<html>
						<head>
						</head>
						<body>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Hello ".$comp_name.", 	</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Further to our previous renewal reminders we have now cancelled the telephone answering service you have with us.</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>The telephone number and service is no longer available to use.</p>
								
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>Should you need our service again in the future please do not hesitate to call or email us.</p>
								<p style='font-size: 12pt; font-family: Calibri; margin: 20px 0px;'>IIf you need any further information regarding the above please do not hesitate to let us know.</p>
					</body>
					</html>";
					$new_msg .=  '<p style="">Should you require the original please <a href="https://theoffice.support/">login to your online account</a> and request the original.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
		<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
		<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';
			if(mail($to,$subject,$new_msg,$header)){
				echo "1";
				
				$data_activity = array (
				'company_id' => $comp_id,
				'activity' => "TAS Service Cancelled ",
				'Description' => "TAS Service Cancelled ",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $userid['id'],
				'email' => $email
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
				
				
			}else{
				echo "0";
			}
			
	}
	/*prinka deposit function*/
	//
	Public function deposit_details() {
	   $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
	    $arr['order_id'] = trim($this->input->get_post('order_id'));
	    $arr['type_id'] = trim($this->input->get_post('deposit_mail_type'));
		$arr['deposit_no_items'] = trim($this->input->get_post('deposit_no_items'));
		$arr['deposit_post_charge'] = trim($this->input->get_post('deposit_post_charge'));
		$arr['company_id'] = trim($this->input->get_post('deposit_company_id'));
		$arr['order_price']= trim($this->input->get_post('orderPrice'));
		
		$result= $this->home_model->add_deposit_details($arr);
		$deposit_id = $this->db->insert_id();
		$result= $this->home_model->getDepositById($deposit_id);
		$mainBalance = $this->home_model->order_deposit_details($arr);
		//$mainBalance->deposit;
		$mainBalance1 = number_format((float)$mainBalance->deposit, 2, '.', '');
		if($result){
			$result = array('status' => 1, 'message' => $result, 'mainBalance'=> $mainBalance1);
				$data_activity = array (
				'company_id' => $this->input->get_post('deposit_company_id'),
				'activity' => "Mail Charge",
				'Description' => "Mail Forwarding Charged",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
				);
				
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
			
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
	}
	/////fetch deposit details
	Public function fetching_deposit_details(){
	
		$array['deposit_company_id'] = $this->input->post('deposit_company_id');
		$post = $this->input->post('export');
		$result = $this->search->fetch_deposit_details($array['deposit_company_id']);
		//var_dump($array['deposit_company_id'],$post,$result);die();
		//var_dump($result[]);
		if($result){
			//die('main if result');
			//var_dump($_POST);die('post');
					if($post=='Export'){
					
						//die('if result');
						$data['export_deposit'] = $result;
						//var_dump($data['export_deposit']);die();
						$this->load->view('spreadsheet_view1', $data);
					}
					else
					{
						//die('else result');
					foreach($result as $res){
						
					$date=$res->created_date;
					$deposit_mail= $res->type_id;
					if($deposit_mail==1)
					{
						$deposit_mail1= "Business Mail";
					}
					elseif($deposit_mail==2)
					{
				     $deposit_mail1="Parcel";
					}
					elseif($deposit_mail==3)
					{
					
					$deposit_mail1="Official Mail";
					}
					    //$adminfee = $res->deposit_admin_fee;
						$adminfee= number_format((float)$res->deposit_admin_fee, 2, '.', '');
						//$deposit_post_charge = $res->deposit_post_charge;
						$deposit_post_charge= number_format((float)$res->deposit_post_charge, 2, '.', '');
						//$total_amount = $res->total_amount;
						$total_amount= number_format((float)$res->total_amount, 2, '.', '');
						//$deposit_price = $res->deposit_price;
						$deposit_price= number_format((float)$res->new_balance , 2, '.', '');
						//var_dump($adminfee);
						  $data[] = array(
								'deposit_id'=> $res->deposit_id,
								'deposit_mail_type'=> $deposit_mail1,
								'deposit_no_items'=> $res->deposit_no_items,
								'deposit_post_charge'=> $deposit_post_charge,
								'deposit_admin_fee'=> $adminfee,
								'total_amount'=> $total_amount,
								'deposit_price'=> $deposit_price,
								'created_date'=> $date,
							);
							//$data1[]=$res->deposit_price;
							$result = array('status' => 1, 'message' => $data);
							//$result = array('status' => 1, 'message' => $data, 'main' => $data1);
					  }
					echo json_encode($result); 
					}
		 }
		 
	}
	Public function fetching_deposit_new_details(){
		$array['company_id'] = trim($this->input->get_post('deposit_company_id1'));
		//var_dump($array['company_id']);die();
		$array['price'] = trim($this->input->get_post('price'));
		$post = $this->input->post('export1');
		
		$result = $this->search->new_fetch_deposit_details($array['company_id']);
		//var_dump($result);die();
		$post = $this->input->post('export1');
		if($result){
	     
	  		if($post=='Export'){
			
				$data['export_deposit1'] = $result;
				$this->load->view('spreadsheet_view2', $data);
			}else{
				foreach($result as $res){
				  $date=$res->created_date;
				  $deposit_mail1 = $res->type_id;
				  if($deposit_mail==4)
					{
						$deposit_mail1= "Deposit funds";
					}
					elseif($deposit_mail==5)
					{
				     $deposit_mail1="Refund Deposit";
					}
				  // $amount = $res->amount;
				  $amount= number_format((float)$res->amount, 2, '.', '');
				  /*$deposit_price = $res->deposit_price;
				  $deposit_price= number_format((float)$deposit_price, 2, '.', ''); */
				  
				  // $new_balance = $res->new_balance;
				  $new_balance= number_format((float)$res->new_balance, 2, '.', ''); /**/
								  $data[] = array(
										'deposit_id'=> $res->deposit_id,
										'type_id'=> $deposit_mail1 ,
										'deposit_post_charge'=> $res->deposit_post_charge ,
										'amount'=> $amount,
										//'new_balance'=> $array['price'],
										'new_balance'=> $new_balance,
										'created_date'=> $date,
									);
									$data1[]=$res->deposit_price;
									//var_dump($data1);die();
				  $result = array('status' => 1, 'message' => $data,'main' => $data1);
				  
			  } 
				echo json_encode($result);   
			} 
		} 
	}
	
	//insert depostdetails  
	Public function deposit_details1() {
		//$arr['deposit_refund'] = trim($this->input->get_post('deposit_refund', TRUE));
		$arr['type_id'] = trim($this->input->get_post('deposit_refund'));
		$arr['deposit_post_charge'] = trim($this->input->get_post('amount', TRUE));
		$arr['company_id'] = trim($this->input->get_post('deposit_company_id'));
		//var_dump($arr['company_id']);die("");
		$arr['amount'] = trim($this->input->get_post('price'));
		$arr['order_id'] = trim($this->input->get_post('order_id'));
		//var_dump($arr);die("hgfh1");
		$result= $this->home_model->add_deposit_details1($arr);
		//$userid = $this->db->insert_id();
		$deposit_id = $this->db->insert_id();
		$result= $this->home_model->getDepositById($deposit_id);
		$mainBalance = $this->home_model->order_deposit_details($arr);
	   $mainBalance1 = number_format((float)$mainBalance->deposit, 2, '.', '');
		if($result){
			$result = array('status' => 1, 'message' => $result,'mainBalance'=> $mainBalance1);
			if($arr['type_id']==4)
			{
			$data_activity = array (
				'company_id' => ($this->input->get_post('deposit_company_id')),
				'activity' => "Deposit Added",
				'Description' => "Mail Desposit Added to Account",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $create_user_id,
				);
			}
			else{
				$data_activity = array (
				'company_id' => $this->input->get_post('deposit_company_id'),
				'activity' => "Refund Deposit",
				'Description' => "Mail Deposit Refunded",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $create_user_id,
				);
			}
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
	}

	public function selectPreference(){
		$id= trim($this->input->post('id'));
		if($id != ""){
			$getPreference = $this->order->selectPreferences($id);
			if($getPreference != ""){
				$result = array('status' => 1, 'message' => $getPreference);
			}else{
				$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
			}
		}
		echo json_encode($result);
	}
	
	/* update preferences*/
	
	public function mail_formation_preference(){
	    $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$array = array(
			'create_user_id' => $this->input->post('create_user_id1'),
			'officail_mail_option1' => $this->input->post('official_options1'),
			'officail_mail_option2' => $this->input->post('official_options2'),
			'officail_mail_option3' => $this->input->post('official_options3'),
		);
		$create_user_id = $this->input->post('create_user_id1');
		$officail_mail_option1 = $this->input->post('official_options1');
		$officail_mail_option2 = $this->input->post('official_options2');
		$officail_mail_option3 = $this->input->post('official_options3');
		$precompany_id = $this->input->post('precompany_id');
		$getPreference = $this->order->selectPreferences($array['create_user_id']);
		//var_dump($create_user_id);die("jb");
		if(count($getPreference) == 0){
			$insertMailPreferences = $this->order->insertMailPreferences($array);
			if($insertMailPreferences){
				$result = array('status' => 1, 'message' => 'added');
			}else{
				$result = array('status' => 0, 'message' => 'error');
			}
		}
		else{
			$updateMailPreferences = $this->order->UpdatePreferences($array);
			
			if($updateMailPreferences){
				$result = array('status' => 1, 'message' => $updateMailPreferences);
			}else{
				$result = array('status' => 0, 'message' => 'error');
			}
		}
		if($officail_mail_option1== "yes")
		{
		$data_activity = array (
				'company_id' => $precompany_id,
				'activity' => "Official Mail Preference Change ",
				'Description' => "Status Change - Scan and email only",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email,
		);
		}
		else if($officail_mail_option2== "yes")
		{
		$data_activity = array (
				'company_id' => $precompany_id,
				'activity' => "Official Mail Preference Change ",
				'Description' => "Status Change -  mail by forwarding only",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email,
		);
		}
		else
		{
		$data_activity = array (
				'company_id' => $precompany_id,
				'activity' => "Official Mail Preference Change ",
				'Description' => "Status Change -  mail held for collection.",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $create_user_id,
				'email' => $email,
		);
		}
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
		echo json_encode($result);
	}
	        
	
	public function mail_formation_preference1(){
	     $session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$array = array(
			'create_user_id' => $this->input->post('create_user_id'),
			'business_mail_option1' => $this->input->post('business_options1'),
			'business_mail_option2' => $this->input->post('business_options2'),
			'business_mail_option3' => $this->input->post('business_options3'),
		);
		//var_dump($array);die("hjh");
		$create_user_id = $this->input->post('create_user_id');
		$business_mail_option1 = $this->input->post('business_options1');
		$business_mail_option2 = $this->input->post('business_options2');
		$business_mail_option3 = $this->input->post('business_options3');
		$precompany_id1 = $this->input->post('precompany_id1');
		//var_dump($business_mail_option1);die("GFd");
		$getPreference = $this->order->selectPreferences($array['create_user_id']);
		if(count($getPreference) == 0){
			$insertMailPreferences = $this->order->insertMailPreferences($array);
				if($insertMailPreferences){
					$result = array('status' => 1, 'message' => 'added');
				}else{
					$result = array('status' => 0, 'message' => 'error');
				}
		}else{
			$updateMailPreferences = $this->order->UpdatePreferences($array);
			if($updateMailPreferences){
				$result = array('status' => 1, 'message' => $updateMailPreferences);
			}else{
				$result = array('status' => 0, 'message' => 'error');
			}
		}
		if($business_mail_option1== "yes")
		{
		$data_activity = array (
				'company_id' => $precompany_id1,
				'activity' => "Business Mail Preference Change ",
				'Description' => "Status Change - Scan and email only",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		}
		else if($business_mail_option2== "yes")
		{
		$data_activity = array (
				'company_id' => $precompany_id1,
				'activity' => "Business Mail Preference Change ",
				'Description' => "Status Change -  mail by forwarding only",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		}
		else
		{
		$data_activity = array (
				'company_id' => $precompany_id1,
				'activity' => "Business Mail Preference Change ",
				'Description' => "Status Change -  mail held for collection.",
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email
		);
		}
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
		echo json_encode($result);
	}


/*update notes and company info*/

public function message_notes(){
$id = $this->input->post('compa_id');
$notes = $this->input->post('comment');
//var_dump($id );die();
$result= $this->home_model->Updatemessage_notes($id,$notes);
//var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}
public function message_notes_ByuserId(){
$id = $this->input->post('compa_id');
$create_user_id = $this->input->post('com_create_user_id');
//$create_user_id = $this->input->post('create_user_id');
$notes = $this->input->post('comment');
//var_dump($create_user_id );die();
$result= $this->home_model->Updatemessage_notes_userbyid($id,$notes,$create_user_id);
//var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}
public function message_comp_info(){
$id = $this->input->post('compa_id');
$comp_info = $this->input->post('comp_info');
//var_dump($id );die();
$result= $this->home_model->Updatemessage_comp_info($id,$comp_info);
var_dump($result);die();
if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
}




/* delete order row*/
 Public function Deletedeposit_info() {
	$id= $this->input->get_post('id');
	$type= $this->input->get_post('type');
	$total_amount= $this->input->get_post('total_amount');
	$price= $this->input->get_post('price');
	$order_id = trim($this->input->get_post('order_id'));
	//var_dump($type);die();
	if(($type=="Business Mail") ||($type=="Parcel") ||($type=="Official Mail")||($type=="Refund Deposit"))
	{
	$balance=($total_amount)+($price);
	$bal = number_format((float)$balance, 2, '.', '');
	//var_dump($balance);die("if");
	}
	elseif($type=="Deposit funds")
	{
	$balance=($total_amount)-($price);
    $bal = number_format((float)$balance, 2, '.', '');
	//var_dump($balance);die("elseif");
	}
	
	//$this->UpdateOrderDep($order_id,$arr['order_id']);
	$result = $this->home_model->UpdateOrderDep($bal,$order_id);
	//var_dump($balance);die();
	$result = $this->home_model->deleteDeposit($id);
	if($result){
	  $res = array('status' => 1,'message' => "delete data",'mainBalance'=> $bal);
	}
	else
	{
		$res = array('status' => 0, 'message' => "Error");
	}
	echo json_encode($res);
}







	public function renewalRecordsNew123() {
 		$data='';
		// $page =$_POST['page']; //$this->input->post('page');
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		
		$renew_company_from = $_GET['renew_company_from'];
		$renew_company_to = $_GET['renew_company_to'];
		$reseller_id = $_GET['reseller_id'];
		//$arra['search_new_all'] = $_GET['search_new_all'];
		//var_dump($renew_company_from);die("hg");
 		if($renew_company_from !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_from',$renew_company_from);
			$arra['renew_company_from'] = $this->session->userdata('renew_company_from');
		}else{
	 		$arra['renew_company_from']  = $this->session->userdata('renew_company_from');
		}
		if($renew_company_to !=''){
			$renew_company_to1 = $this->session->set_userdata('renew_company_to',$renew_company_to);
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}
		else if($renew_company_to !='Show All'){
			$arra['renew_company_to'] = $this->session->userdata('renew_company_to');
		}else{
	 		$arra['renew_company_to']  = $this->session->userdata('renew_company_to');
		}
		
		if($reseller_id !=''){
			$reseller_id1 = $this->session->set_userdata('reseller_id',$reseller_id);
			$arra['reseller_id'] = $this->session->userdata('reseller_id');
		}else{
	 		$arra['reseller_id']  = $this->session->userdata('reseller_id');
		
		}
		
		
		$config["base_url"] = base_url() . "home/renewalRecords";
		$config["total_rows"]  = $this->home_model->count_renewal_records($arra);
		 //var_dump($config["total_rows"]);die("ojki");
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_renewal_records($config["per_page"],$page,$arra);
		//var_dump($user_orders);die("ojki");
		$export='1';
		$user_orders1 = $this->home_model->show_renewal_records($config["per_page"],$page,$arra,$export);
		//var_dump($user_orders1);die("kjhj");
		 if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('home_view', $data);
		}
	}

////update company_number in comoany table
	public function add_company_number(){
	$session_data = $this->session->userdata('logged_in');
	$user_id = $session_data ['id'];
	$arr['id'] = $this->input->post("com_data_id");
	$importcnamedata = $this->input->post("importcnamedata");
	$arr['company_number'] = $this->input->post("company_number");
	$arr['company_old_name'] = $importcnamedata;
	$updateCompanyAddress = $this->home_model->updateCompanyAddress($arr);
	if($updateCompanyAddress){
		$arr1['company_id'] = $arr['id'];   
        $arr1['company_number'] = $arr['company_number'];
		$qr = file_get_contents('https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk/company/'.$arr['company_number'].'.json');
		$data = json_decode($qr);
		if($data  == ""){
			$res = array('Status'=>'0','message'=>'No data available');
		}else{
			$get_data = $this->home_model->Get_company_data($arr1['company_id']);
			 $register =  $data->date_of_creation;
    	    
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 
			 $relatedSlides = array();
			 $sitext= $data->sic_codes;
			
			 $i=0;
			  foreach($sitext as $key=>$v)
			  {
			 $relatedSlides[] = $v;
			  }
			 $relatedSlides = implode(",",$relatedSlides);
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 $account_ref_date = $AccountRefDay."/".$AccountRefMonth;
			 $li="https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk";
			 $links = $data->links->persons_with_significant_control;
			 $demo = $li.$links.".json";
			 $qr1 = file_get_contents($demo);
			 $data1 = json_decode($qr1);
			 $psc_data = $data1->items;
			 $get_data_1 = $this->home_model->check_psc_company_data($arr1['company_id']);
			 if(count($get_data_1) > 0){
                 $deletePscData = $this->home_model->deletePscData($arr1['company_id']);
			  }
			 //var_dump($psc_data);die();
			 foreach($psc_data as $item)
			  {
				$natures_of_control = $item->natures_of_control;
				$dob = $item->date_of_birth;
				$year = $dob->year;
				$month = $dob->month;
			  //var_dump($fname);die();
			   $array2 = array( 
			  'company_id'=>$arr1['company_id'],
			  'first_name'=>$item->name_elements->forename,
			  'last_name'=>$item->name_elements->surname,
			  'name'=>$item->name,
			  'birth_month'=>$month,
			  'birth_year'=>$year,
			  'natures_of_control'=>$natures_of_control['0'],
			  'res_building_country'=>$item->address->country,
			  'res_building_postcode'=>$item->address->postal_code,
			  'building_address'=>$item->address->address_line_2,
			  'building_street'=>$item->address->address_line_1,
			  'building_name'=>$item->address->premises,
			  'nationality'=>$item->nationality,
			  'res_building_country'=>$item->address->locality,
			  'appoint_board'=>$item->notified_on,
			  //'natures_of_control'=>$item->natures_of_control,
			  'legal_authority'=>$item->identification->legal_authority,
			  'legal_form'=>$item->identification->legal_form,
			  'api'=>1,
			  );
			   //var_dump($array2);die();
			 $insert_person_data = $this->home_model->company_psc_data($array2);
			 }
			
			 $links1 = $data->links->officers;
			 $demo12 = $li.$links1.".json";
			 $qr2 = file_get_contents($demo12);
			 $data12 = json_decode($qr2);
			 $link1 = $data12->items;
			 $get_data_2 = $this->home_model->check_director_company_data($arr1['company_id']);
			 $get_data_sec = $this->home_model->check_sectory_company_data($arr1['company_id']);
			   if(count($get_data_2) > 0){
                 $deletedirectorData = $this->home_model->deletedirectorData($arr1['company_id']);
			  }
			  if(count($get_data_sec) > 0){
                 $deletesectoryData = $this->home_model->deletesectoryData($arr1['company_id']);
			  }
			 //var_dump($link1);die("jkh");
			 foreach($link1 as $item)
			  {
			  $names = $item->former_names;
			  $role = $item->officer_role;
			  $country = $item->address->country;
			  $postal_code = $item->address->postal_code;
			  $address_line1 = $item->address->address_line_1;
			  $address_line2 = $item->address->address_line_2;
			  $region = $item->address->region;
			  $locality = $item->address->locality;
			  $country_of_residence = $item->country_of_residence;
			  $nationality = $item->nationality;
			  $month = $item->date_of_birth->month;
			  $year = $item->date_of_birth->year;
			  $occupation = $item->occupation;
			  $appoint_date = $item->appointed_on;
			  $resigned_on = $item->resigned_on;
			  $building_street = $item->address->address_line_1;
			  $building_address = $item->address->address_line_2;
			  $building_name = $item->address->premises;
			  $name = $item->name;
			  $fname = substr($name, strpos($name, ",") + 1);
			  $lname = substr($name, 0, strpos($name, ','));
			  //var_dump($role);die();
			  if($name="")
			  {
				  foreach($names as $nam)
				  {
				  $fname = $nam->forenames;
				  $lname = $nam->surname;
				  }
			  }
			     $array1 = array( 
			     'company_id'=>$arr1['company_id'],
			      'first_name'=>$fname,
			     'last_name'=>$lname,
			     'nationality'=>$nationality,
			     'occupation'=>$occupation,
			     'res_building_address'=>$address_line1,
			     'country_residence'=>$country_of_residence,
			     'res_building_county'=>$locality,
			     'res_building_postcode'=>$postal_code,
			     'res_building_country'=> $country,
			     'birth_month'=> $month, 
			     'birth_year'=> $year,
			     'appoint_date'=> $appoint_date,
			     'resigned_on'=> $resigned_on,
			     'building_street'=> $building_street,
				 'building_name'=> $building_name,
			     'building_address'=> $building_address,
			  
			   'api'=>1,
			  );
			  if($role == "director")
			  {
			  
			  $insert_director_data = $this->home_model->company_director_data($array1);
			  //var_dump($insert_director_data);
			  }
			  elseif($role == "secretary")
			  {
			  //var_dump("secretary");
				$insert_sec_data = $this->home_model->company_sec_data($array1);
			  }
			}			  
			  //die();
		$share_link = $data->links->filing_history;
		$share_capital = $li.$share_link.".json";
		$qr1 = file_get_contents($share_capital);
		$share_capital = json_decode($qr1);
		$share_capital2 = $share_capital->items;
		$get_data_3 = $this->home_model->check_shareholder_company_data($arr1['company_id']);
		if(count($get_data_3) > 0){
		$deleteshareholderData = $this->home_model->deleteshareholderData($arr1['company_id']);
						}
					foreach($share_capital2 as $item)
					{
						
						//var_dump(count($get_data));
						
					$dd = $item->associated_filings;
						foreach($dd as $d)
						{
						$capital = ($d->description_values->capital);
						$dd1 = ($d->type);
							foreach($capital as $d1)
							{
							$currency = $d1->currency;
							$figure = $d1->figure;
							}
						}
						$array3 = array( 
						'company_id'=>$arr1['company_id'],
						'share_class'=>$dd1,
						'currency'=>$currency,
						'per_person_shares'=>$figure,
						'api'=>1,
						);	
						 //var_dump($array3);die();
						$insert_shareholder_data = $this->home_model->company_shareholder_data($array3);
				   }
				  
				$array = array( 
					'company_id'=>$arr1['company_id'],
					'company_name'=>$data->company_name,
					'incorporation_date'=>$register,
					'company_number'=>$data->company_number,
					'company_status'=>$data->company_status,
					'address_line1'=>$data->registered_office_address->address_line_1,
					'next_due_date'=>$data->confirmation_statement->last_made_up_to,
					'next__due_return_dated'=>$data->confirmation_statement->next_made_up_to,
					'address_line2'=>$data->registered_office_address->address_line_2,
					'country'=>$data->registered_office_address->country,
					'post_town'=>$data->registered_office_address->postal_code,
					'locality'=>$data->registered_office_address->locality, 
					'account_reference_date'=>$account_ref_date,
					'last_made_update'=>$data->accounts->last_accounts->made_up_to,
					'account_next_due'=>$data->accounts->next_accounts->period_end_on,
					'SIC_Codes'=>$relatedSlides, 
					'creation_date'=>$data->registered_office_address->country
			);
			    
			//var_dump($array);die("gfg");die();
			if($get_data == 0){
			$new_company_name = $data->company_name;
			$name_update = array (
				'id' => $arr['id'],
				'company_name' => $new_company_name,
				);
				$update_data_name = $this->home_model->update_company_data_name_import($name_update);
				//var_dump($update_data_name);die("gfg");die();
				$inserdata = $this->home_model->company_data($array);
				
				//var_dump($update_data_name);die();
				if($inserdata){
				$res = array('Status'=>'1','message'=>'Data imported');
				}else{
					$res = array('Status'=>'0','message'=>'error in insertion');
				} 
			}else{
			   $new_company_name = $data->company_name;
				$name_update = array (
				'id' => $arr['id'],
				'company_name' => $new_company_name,
				);
				$update_data_name = $this->home_model->update_company_data_name_import($name_update);
				//var_dump($update_data_name);die();
				$update_data = $this->home_model->update_company_data($array);
				
			if($update_data){
				$res = array('Status'=>'1','message'=>'Data imported');
			}else{
				$res = array('Status'=>'0','message'=>'error in updation');
			}
			}
			
			//var_dump($get_data);die("gfdsg");
		}
		$compnay_name = $data->company_name;
		$compa_name = "company name Changed to ".$compnay_name. "From " .$importcnamedata;
		//echo $compa_name;
		$data_activity = array (
				'company_id' => $arr['id'],
				'activity' => "Company Name Updated",
				'Description' => $compa_name,
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
		);
		if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
			}
		
	    
    }
	
	else{
		$res = array('Status'=>'0','message'=>'Database error');
	}
	echo json_encode($res);
}

	public function iospushNotification($arr,$arr2){
		$deviceToken =  $arr['device_token'];
		$passphrase = '1234';
		$message =  $arr['message'];
		$ctx = stream_context_create();
		$pemfile =  'ck.pem';
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfile);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		//ssl://gateway.sandbox.push.apple.com:2195var_dump($fp);die("GFd");
		
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default',
			'code' => $arr['code'],
			'data' => $arr2
		);
		$payload = json_encode($body);
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		$result = fwrite($fp, $msg, strlen($msg));
		//var_dump($result);die("vcx");
		//var_dump($result);die("Gfd");
		if (!$result)
			return 0;
		else
			return 1;
		
		fclose($fp);
	}
	
	Public function Androidnotification($registrationIds,$msg){
		
		$api_key= 'AIzaSyDtSJTFnAgzzuyNO54MWykABpJiYZRM3NQ';
    
		$fields = array(
					'to'		=> $registrationIds,
					'data'	=> $msg
					);
		$headers = array(
					'Authorization: key=' . $api_key,
					'Content-Type: application/json'
				);
		
		#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt($ch,CURLOPT_POST, true );
		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch);
		//var_dump($result);die("FDgd");
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}else{
			//echo "send";
			return(1);
		} 
		curl_close( $ch );
 
	}  
/* new orders data*/
public function NewOrders() {
		$data='';
		$data['title'] ="New Orders";
		$page = $this->input->get_post('page');
		$data_order = $this->input->get('orderDate');
		//var_dump($data_order);die();
		if($data_order==NULL){
		$data_order = 1;
		}
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {
			$config["base_url"] = base_url() . "home/NewOrders";
			$config["total_rows"]  = $this->home_model->count_NewOdersCompanies_new('count',$data_order);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$config["base_url"] = base_url() . "home/NewOrders";
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->count_NewOdersCompanies_new('result',$data_order,$config["per_page"],$page,'export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->count_NewOdersCompanies_new('result',$data_order,$config["per_page"],$page);
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view ( 'home_view', $data );
 			}
		}else{
			redirect ( 'login', 'refresh' );
		} 
}
/*compnay ciew new order*/
public function NewOrdersCompany() {
$data='';
		$data['title'] ="New Orders";
		$page = $this->input->get_post('page');
		$data_order = $this->input->get('orderDate');
		//var_dump($data_order);die();
		if($data_order==NULL){
		$data_order = 1;
		}
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {
			$config["base_url"] = base_url() . "home/NewOrdersCompany";
			$config["total_rows"]  = $this->home_model->count_NewOdersCompanies_new('count',$data_order);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$config["base_url"] = base_url() . "home/NewOrdersCompany";
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->count_NewOdersCompanies_new('result',$data_order,$config["per_page"],$page,'export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->count_NewOdersCompanies_new('result',$data_order,$config["per_page"],$page);
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view ('company_view', $data);
 			}
		}else{
			redirect ( 'login', 'refresh' );
		} 
}
	public function add_company_number_new(){
	$arr['id'] = $this->input->post("com_data_id");
	$arr['company_number'] = $this->input->post("company_number");
	$updateCompanyAddress = $this->home_model->updateCompanyAddress($arr);
	if($updateCompanyAddress){
		$arr1['company_id'] = $arr['id'];   
        $arr1['company_number'] = $arr['company_number'];
		
		$qr = file_get_contents('http://data.companieshouse.gov.uk/doc/company/'.$arr['company_number'].'.json');
		
		if($qr  == ""){
			$res = array('Status'=>'0','message'=>'No data available');
		}else{
			$get_data = $this->home_model->Get_company_data($arr1['company_id']);
			$array = array( 
					'company_id'=>$arr1['company_id'],
					'text'=>$qr,
					
			);
			if(count($get_data) == 0){
				$inserdata = $this->home_model->company_data($array);
				$res = array('Status'=>'1','message'=>'Data imported');
				//insert
			}else{
			$update_data = $this->home_model->update_company_data($array);
			$res = array('Status'=>'1','message'=>'Data imported');
				//update
			}
		}
	    
    } 
	
	else{
		$res = array('Status'=>'0','message'=>'Database error');
	}
	echo json_encode($res);
} 
	public function xmldata_crone()
	{
	       $comp_overview12 =  $this->ltd_model->GetCompanyOverviewnumber();
			foreach($comp_overview12 as $overview)
			{
			$company_number = $overview->company_number;
			$company_id = $overview->company_id;
			if($company_number)
			{
			$url ='https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk/company/'.$company_number.'.json';
			
				$qr = file_get_contents($url);
				$data = json_decode($qr);
				
				$get_data_11 = $this->home_model->Get_company_data($company_id);
				//var_dump($get_data);
				$register =  $data->date_of_creation;
    	     $date =  $data->primaryTopic->IncorporationDate;
		     $dated = explode('/',$date);
		     $newdate = $dated['2'].'-'.$dated['1'].'-'.$dated['0'];
			 $next_due = $data->primaryTopic->Accounts->NextDueDate;
			 $next_dated = explode('/',$next_due);
			 $next__due_dated = $next_dated['2'].'-'.$next_dated['1'].'-'.$next_dated['0'];
			 $next_return_due = $data->primaryTopic->Returns->NextDueDate;
			 $next_return_dated = explode('/',$next_return_due);
			 $next__due_return_dated = $next_return_dated['2'].'-'.$next_return_dated['1'].'-'.$next_return_dated['0'];
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 $LastMadeUpDate= $data->primaryTopic->Accounts->LastMadeUpDate;
			 $next_dated_LastMadeUpDate = explode('/',$LastMadeUpDate);
			 $next__due_dated_LastMadeUpDate = $next_dated_LastMadeUpDate['2'].'-'.$next_dated_LastMadeUpDate['1'].'-'.$next_dated_LastMadeUpDate['0'];
			 $relatedSlides = array();
			 $sitext= $data->sic_codes;
			
			 $i=0;
			  foreach($sitext as $key=>$v)
			  {
			 $relatedSlides[] = $v;
			  }
			 $relatedSlides = implode(",",$relatedSlides);
			 $AccountRefDay= $data->accounts->accounting_reference_date->day;
			 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
			 $account_ref_date = $AccountRefDay."/".$AccountRefMonth;
			 $ReturnsLastMadeUpDate= $data->primaryTopic->Returns->LastMadeUpDate;
			 $Returns = explode('/',$ReturnsLastMadeUpDate);
			 $ReturnsLastMadeUpDate1 = $Returns['2'].'-'.$Returns['1'].'-'.$Returns['0'];
			 $li="https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk";
			 $links = $data->links->persons_with_significant_control;
			 $demo = $li.$links.".json";
			 $qr1 = file_get_contents($demo);
			 $data1 = json_decode($qr1);
			 
			 $psc_data = $data1->items;
			 $get_data = $this->home_model->check_psc_company_data($company_id);
			 if(count($get_data) > 0){
                 $get_datafs = $this->home_model->deletePscData($company_id);
			  }
			 //var_dump($psc_data);die();
			 foreach($psc_data as $item)
			  {
			  
			  //var_dump($fname);die();
			   $array2 = array( 
			  'company_id'=>$company_id,
			  'first_name'=>$item->name_elements->forename,
			  'last_name'=>$item->name_elements->surname,
			  'name'=>$item->name,
			  'res_building_country'=>$item->address->country,
			  'res_building_postcode'=>$item->address->postal_code,
			  'building_address'=>$item->address->address_line_2,
			  'building_street'=>$item->address->address_line_1,
			  'building_name'=>$item->address->premises,
			  'nationality'=>$item->nationality,
			  'res_building_country'=>$item->address->locality,
			  'appoint_board'=>$item->notified_on,
			  //'natures_of_control'=>$item->natures_of_control,
			  'legal_authority'=>$item->identification->legal_authority,
			  'legal_form'=>$item->identification->legal_form,
			  'api'=>1,
			  );
			  
			 $insert_person_data = $this->home_model->company_psc_data($array2);
			// var_dump($insert_person_data);die();
			 }	
			 $links1 = $data->links->officers;
			 $demo12 = $li.$links1.".json";
			 $qr2 = file_get_contents($demo12);
			 $data12 = json_decode($qr2);
			 $link1 = $data12->items;
			// var_dump($arr1['company_id']);
			 $get_data1 = $this->home_model->check_director_company_data($company_id);
			// var_dump($get_data1);die();
			 $get_data_sec = $this->home_model->check_sectory_company_data($company_id);
			   if(count($get_data1) > 0){
                 $deletedirectorData = $this->home_model->deletedirectorData($company_id);
			  }
			  if(count($get_data_sec) > 0){
                 $deletesectoryData = $this->home_model->deletesectoryData($company_id);
			  }
			 //var_dump($link1);die("jkh");
			 foreach($link1 as $item)
			  {
			  $names = $item->former_names;
			  $role = $item->officer_role;
			  $country = $item->address->country;
			  $postal_code = $item->address->postal_code;
			  $address_line1 = $item->address->address_line_1;
			  $address_line2 = $item->address->address_line_2;
			  $region = $item->address->region;
			  $locality = $item->address->locality;
			  $country_of_residence = $item->country_of_residence;
			  $nationality = $item->nationality;
			  $month = $item->date_of_birth->month;
			  $year = $item->date_of_birth->year;
			  $occupation = $item->occupation;
			  $appoint_date = $item->appointed_on;
			  $building_street = $item->address->address_line_1;
			  $building_address = $item->address->address_line_2;
			  $building_name = $item->address->premises;
			  $name = $item->name;
			  $fname = substr($name, strpos($name, ",") + 1);
			  $lname = substr($name, 0, strpos($name, ','));
			  //var_dump($role);die();
			  if($name="")
			  {
				  foreach($names as $nam)
				  {
				  $fname = $nam->forenames;
				  $lname = $nam->surname;
				  }
			  }
			     $array1 = array( 
			     'company_id'=>$company_id,
			      'first_name'=>$fname,
			     'last_name'=>$lname,
			     'nationality'=>$nationality,
			     'occupation'=>$occupation,
			     'res_building_address'=>$address_line1,
			     'country_residence'=>$country_of_residence,
			     'res_building_county'=>$locality,
			     'res_building_postcode'=>$postal_code,
			     'res_building_country'=> $country,
			     'birth_month'=> $month, 
			     'birth_year'=> $year,
			     'appoint_date'=> $appoint_date,
			     'building_street'=> $building_street,
			     'building_address'=> $building_address,
				 'building_name'=> $building_name,
			   'api'=>1,
			  );
			  //var_dump($array1);
			  if($role == "director")
			  {
			 
			  $insert_director_data = $this->home_model->company_director_data($array1);
			   //var_dump($insert_director_data);die("insert dir");
			  }
			  elseif($role == "secretary")
			  {
			  //var_dump("secretary");
				$insert_sec_data = $this->home_model->company_sec_data($array1);
				//var_dump($insert_director_data);die("insert sec");
			  }
			}			  
			  //die(); 
		$share_link = $data->links->filing_history;
		$share_capital = $li.$share_link.".json";
		$qr1 = file_get_contents($share_capital);
		$share_capital = json_decode($qr1);
		//var_dump($share_capital);
		$share_capital2 = $share_capital->items;
		$get_data2 = $this->home_model->check_shareholder_company_data($company_id);
		if(count($get_data2) > 0){
		$geer = $this->home_model->deleteshareholderData($company_id);
		}
					foreach($share_capital2 as $item)
					{
						
						//var_dump(count($get_data));
						
					$dd = $item->associated_filings;
						foreach($dd as $d)
						{
						$capital = ($d->description_values->capital);
						$dd1 = ($d->type);
							foreach($capital as $d1)
							{
							$currency = $d1->currency;
							$figure = $d1->figure;
							}
						}
						$array3 = array( 
						'company_id'=>$company_id,
						'share_class'=>$dd1,
						'currency'=>$currency,
						'per_person_shares'=>$figure,
						'api'=>1,
						);	
						 //var_dump($array3);die();
						$insert_shareholder_data = $this->home_model->company_shareholder_data($array3);
				   }
				  
				$array = array( 
					'company_id'=>$company_id,
					'company_name'=>$data->company_name,
					'creation_date'=>$data->registered_office_address->country,
					'company_number'=>$data->company_number,
					'company_status'=>$data->company_status,
					'address_line1'=>$data->registered_office_address->address_line_1,
					'next_due_date'=>$data->confirmation_statement->last_made_up_to,
					'next__due_return_dated'=>$data->confirmation_statement->next_made_up_to,
					'address_line2'=>$data->registered_office_address->address_line_2,
					'country'=>$data->registered_office_address->country,
					'post_town'=>$data->registered_office_address->postal_code,
					'locality'=>$data->registered_office_address->locality, 
					'account_reference_date'=>$account_ref_date,
					'last_made_update'=>$data->accounts->last_accounts->made_up_to,
					'account_next_due'=>$data->accounts->next_accounts->period_end_on,
					'SIC_Codes'=>$relatedSlides, 
			);
			//var_dump($array);die();
			$new_company_name = $data->company_name;
				$name_update = array (
				'id' => $company_id,
				'company_name' => $new_company_name,
				);
				$update_data_name = $this->home_model->update_company_data_name_import($name_update);
			    $update_data = $this->home_model->update_company_data($array);
			//var_dump($update_data);
			$res = array('Status'=>'1','message'=>'Data imported');
		
	
		
		}
	}
	echo json_encode($res);
	}   
	
	public function UpdateByCompanyRegisterdeliver()
	{
		$order_id = $this->input->get_post('get_order_id_deleiver');
		$deliver_date = $this->input->get_post('renew_company_id_deleiver');
		$document_status = $this->input->get_post('CompanyRegister_status');
		//var_dump($document_status);die();
		$update_data = $this->home_model->UpdateByCompanyRegisterdelivermodel($order_id,$deliver_date,$document_status);
		if($update_data)
		{
			redirect ( 'dashboard/company_register', 'refresh' );
		}
	}	
	
	public function UpdateByCompanydocumentdeliver()
	{
		$order_id = $this->input->get_post('get_order_id_deleiver_register');
		$deliver_date = $this->input->get_post('renew_company_id_deleiver');
		$document_status = $this->input->get_post('document_status');
		//var_dump($document_status);die("k");
		$update_data = $this->home_model->UpdateByCompanydocumentdelivermodel($order_id,$deliver_date,$document_status);
		//var_dump($update_data);die();
		if($update_data)
		{ 
			redirect ( 'dashboard/document_service', 'refresh' );
		}
	}
	public function UpdateDomainName()
	{
		$order_id = $this->input->get_post('domain_id');
		//var_dump($order_id);die();
		$domain_name = $this->input->get_post('domain_name');
		
		$update_data = $this->home_model->Updatedomainname($order_id,$domain_name);
		if($update_data)
		{
			redirect ( 'dashboard/Hosting', 'refresh' );
		}
	}
	public function documentExportservice($id)
	{
		//var_dump($id);die();
		$data_query = $this->search_model->ressler_data($id);
		$data['user_orders'] = $data_query;
		$data['record_count'] = count($user_orders);
		$this->load->view ( 'reseller_view', $data );
	}
	/*companynewsearch*/
	public function companynewsearch()
	{
			$data='';
		  $page = $this->input->get_post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arra['user_id'] = $session_data['id'];
		$arra['role_id'] = $session_data['role_id'];
		$arra['accountreturnsfrom'] = $_GET['accountreturnsfrom'];
		$arra['accountreturnsto'] = $_GET['accountreturnsto'];
		$arra['new_search_bar'] = $_GET['new_search_bar'];
		$arra['search_new_all'] = $_GET['search_new_all'];
		$config["base_url"] = base_url() . "home/companynewsearch";
		$config["total_rows"]  = $this->home_model->count_companysearch_records($arra);
		 if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_companysearch_records($config["per_page"],$page,$arra);
		//var_dump($user_orders);
		$export='1';
		$user_orders1 = $this->home_model->show_companysearch_records($config["per_page"],$page,$arra,$export);
        if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
			$data['role_id'] = $arra['role_id'];
			$data['user_orders'] = $user_orders;
			$data['user_orders1'] = $user_orders1;
			$data['record_count'] = $config["total_rows"] ;
			$data['username'] = $session_data['first_name'];
			$this->load->view ('company_view', $data);
		}
	}
	public function company_register_status()
	{
		$page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$arr['state_change'] = $this->input->get('company_register_state_change');
		//var_dump($state_change);die("state_change");
		$arr['query_string_1'] = $this->input->get('query_string_1');
		$arr['query_string_2'] = $this->input->get('query_string_2');
		$config["base_url"] = base_url() . "home/company_register_status";
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config["total_rows"]  = $this->home_model->count_state_change_company_register($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change_company_register($config["per_page"],$page,$arr);
		//var_dump($user_orders);die();
		$export='1';
		$user_orders1 = $this->home_model->show_state_change_company_register($config["per_page"],$page,$arr,$export);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
 		$this->load->view ( 'company_register', $data );
		}
	}public function document_service_status()
	{
		$page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$arr['state_change'] = $this->input->get('document_service_state_change');
		//var_dump($arr['state_change']);die("state_change");
		$arr['query_string_1'] = $this->input->get('query_string_1');
		$arr['query_string_2'] = $this->input->get('query_string_2');
		$config["base_url"] = base_url() . "home/document_service_status";
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config["total_rows"]  = $this->home_model->count_state_change_document_service($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change_document_service($config["per_page"],$page,$arr);
	  //var_dump($user_orders);die();
		$export='1';
		$user_orders1 = $this->home_model->show_state_change_document_service($config["per_page"],$page,$arr,$export);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
 		$this->load->view ( 'document_service', $data );
		}
	}
	
	public function hosting_status()
	{
		$page =$_GET['page']; //$this->input->post('page');
		 if($page==''){
				$data['pagef']= '50';
		 }else{
				$data['pagef']= $page;
		 }
		$session_data = $this->session->userdata ( 'logged_in' );
		$arr['user_id'] = $session_data['id'];
		$arr['role_id'] = $session_data['role_id'];
		$arr['state_change'] = $this->input->get('hosting_status_id');
		//var_dump($arr['state_change']);die("state_change");
		$arr['query_string_1'] = $this->input->get('query_string_1');
		$arr['query_string_2'] = $this->input->get('query_string_2');
		$config["base_url"] = base_url() . "home/hosting_status";
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config["total_rows"]  = $this->home_model->count_state_change_hosting($arr);
	 	$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$user_orders = $this->home_model->show_state_change_hosting($config["per_page"],$page,$arr);
	  //var_dump($user_orders);die();
		$export='1';
		$user_orders1 = $this->home_model->show_state_change_hosting($config["per_page"],$page,$arr,$export);
		if($_POST['export1']=='export'){
			$data1['user_orders'] = $user_orders1;
			$this->load->view('spreadsheet_view', $data1);
 		}else{
 		//count_reseller
 		$data ['role_id'] = $arr['role_id'];
 		$data['user_orders'] = $user_orders;
		$data['user_orders1'] = $user_orders1;
		$data['username'] = $session_data['first_name'];
		$data['record_count'] = $config["total_rows"] ;
		$data['state_change'] = $arr['state_change'];
 		$this->load->view ( 'hosting', $data );
		}
	}
	function allusercompanies()
	{
		
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
	    if ($session_data = $this->session->userdata('logged_in')) {
			$user_id = $this->input->get('alluser_id');
			$id = $user_id;
		    $config["base_url"] = base_url() . "home/allusercompanies";
			$config["total_rows"]  = $this->home_model->allusercompanies_model('count',$user_id);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);  
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->allusercompanies_model('result',$user_id,$config["per_page"],'','export'); // for export 
				
				$data['user_orders'] = $show_companies; 
				
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->allusercompanies_model('result',$user_id,$config["per_page"]);
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view ( 'home_view', $data );
 			}
		}else {
			redirect ( 'login', 'refresh' );
		}
	}

	
	function allusercompanies_user_id()
	{
		$arr['userid'] = $this->input->post('user_id');
		$user_orders = $this->home_model->allusercompanies_model($arr);
		$result = array('status' => 1, 'message' => $user_orders);
		echo json_encode($result);
	}
	function addAltEmail()
	{
	$user_id = $this->input->post('alt_id');
	$compnay_id = $this->input->post('alt_comm_id');
	$create_time = date ( 'Y-m-d h:i:s' );
	$alt_email = trim($this->input->post('alternate_email'));
	//var_dump($alt_email);die();
	$user_mail_check = $this->home_model->user_mail_check($alt_email);
	if(!$user_mail_check){
		$emails = $this->home_model->get_useremail_data($user_id);
		if($emails){
			if(empty($emails->email_2 ) || $emails->email_2 == '')
			{
				$data_email = array(
					'email_2' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);	
			}
			elseif(empty($emails->email_3 ) || $emails->email_3 == '' )
			{
				$data_email = array(
					'email_3' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}
			elseif(empty($emails->email_4 ) || $emails->email_4 == '' )
			{
				$data_email = array(
					'email_4' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}
			elseif(empty($emails->email_5 ) || $emails->email_5 == '' )
			{
				$data_email = array(
					'email_5' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}elseif(empty($emails->email_6 ) || $emails->email_6 == '' )
			{
				$data_email = array(
					'email_6' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}elseif(empty($emails->email_7 ) || $emails->email_7 == '' )
			{
				$data_email = array(
					'email_7' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}elseif(empty($emails->email_8 ) || $emails->email_8 == '' )
			{
				$data_email = array(
					'email_8' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}elseif(empty($emails->email_9 ) || $emails->email_9 == '' )
			{
				$data_email = array(
					'email_9' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}elseif(empty($emails->email_10 ) || $emails->email_10 == '' )
			{
				$data_email = array(
					'email_10' => trim($this->input->post('alternate_email')),
				);
				$update_altemail = $this->home_model->update_altemail($user_id,$data_email);
			}	
			}else{
	
				$data_email = array(
					'create_user_id'=>$user_id ,
					'email_2' => trim($this->input->post('alternate_email')),
					'create_time'=>$create_time
				);
				$update_altemail = $this->home_model->add_altemail($data_email);
			}
			//redirect('dashboard/showCompanyResult?id=$user_id');
			redirect ( 'dashboard/showCompanyResult?id='.$compnay_id, 'refresh' );
	}else{
		echo "Email already exist";
	}
	}
	public function alterEmail(){
		$user_id = $this->input->post('notification');
		$array['email']= $this->input->post('email');
		$array['number']= $this->input->post('number');
		$array['company_id']= $this->input->post('company_id');
		$array['create_user_id']= $user_id;
		$getuserdata = $this->home_model->getuserdata($user_id);
		$array['email_primary']= $getuserdata['email'];
		$make_primary = $array['email'];
		$arr = array(
			'id' => $user_id,
			'email' => $make_primary,
		);
		$updateUser = $this->home_model->updateUser($arr);
		//var_dump($updateUser);die();
		if($updateUser){
			$updateEmail = $this->home_model->updateEmail($array);
			//var_dump($updateEmail);
			if($updateUser){
				echo "1";
			}else{
				echo "error";
			}
			
		}else{
			echo "error";
		}
	}
	public function remove_email(){
		$arr["email"] = $this->input->post("email");
		$arr["field"] = $this->input->post("field");
		$arr["id"] = $this->input->post("id");
		$arr["create_user_id"] = $this->input->post("notification");
		if($arr["field"] == "email_2")
			$arr["email_2"] = "";
		else if($arr["field"] == "email_3")
			$arr["email_3"] = "";
		else if($arr["field"] == "email_4")
			$arr["email_4"] = "";
		$remove_email = $this->home_model->remove_email($arr);
		if($remove_email){
			echo "1";
		}else{
			echo "0";
		}
	}
	public function invoice()
	{
			$uri = $this->input->get_post('uri');
			$invoice_company_name = $this->input->get_post('invoice_company_name');
			$invoice_create_user_id = $this->input->get_post('invoice_create_user_id');
			$pdf_handel = $this->input->get_post('pdf_handel_invoice');
			$email123 = $this->home_model->user_email($invoice_create_user_id);
				$uri1 = ltrim($uri, '/');
				$com_uri= base_url() . $uri1;
				//var_dump($com_uri);die();
			//	var_dump($invoice_company_name);die();
		        $create_time = date('Y-m-d h:i:s');
				if (!empty($_FILES['userfile']['name'])){	
                $to = $email123->email;					
				$from_email = 'Office Support<orders@theoffice.support>'; // sender email
				$boundary = md5 ( "sanwebe" );
				// header
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "From:" . $from_email . "\r\n";
				$headers .= "Reply-To: " . "\r\n";
				$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
				/*=====================================================*/
				$userfiles = $_FILES["userfile"];
				$subject = 'The Office Support > Service Invoice'; // subject of email
				$message ='';
					$message .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support  - You Have Mail!</span></br>
					<p style="">Dear '.$email123->first_name." ".$email->email123.',</p>
					<p style="">Thank you for your order.</p>
					<p style="">Please find attached your service invoice.</p>
					<p style="">Should you require the original please <a href="https://theoffice.support">login to your online account and request the original.</a></p>
					<p style="">Should you wish to change your preference to receive mail without attachments login to your account and follow these steps:</a></p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alerts" from the menu</p>
					<p>Step 3: Change to “Email Alerts Without Attachments"</p>
					';
					$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
					$boundary = md5 ( "sanwebe" );
					$headers = "MIME-Version: 1.0\r\n";
					$headers .= 'From: The Office Support<post@theoffice support>'. "\r\n"; 

					$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
					$body = "--$boundary\r\n";
					$body .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$body .= "Content-Transfer-Encoding: base64\r\n\r\n";
					$body .= chunk_split ( base64_encode ( $message ) );
				$this->load->library('upload');
				$files = $_FILES;
				/*==============Remove Specila Char===============*/
				$ext = pathinfo($files['userfile']['name'][0], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][0]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				/*==============Remove Specila Char===============*/
				if (isset($_FILES['userfile'])){
					$_FILES['userfile']['name'] = time().'invoice' .$new_data1.'.'.$ext;
					$_FILES['userfile']['type'] = $files['userfile']['type'][0];
					$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
					$_FILES['userfile']['error'] = $files['userfile']['error'][0];
					$_FILES['userfile']['size'] = $files['userfile']['size'][0];
					$configd['file_name'] = $_FILES['userfile']['name'];
					$this->upload->initialize($this->set_upload_options());
					if($files['userfile']['name'][0])
					{
						if ($this->upload->do_upload()) {
							//die('upload');
							$data = array(
									'upload_data' => $this->upload->data() 
							);
							     $data_file = array (
									'company_id' => $this->input->post("com_invoice"),
									'file_name' => $data['upload_data']["file_name"],
									'type_id' => '51',
									'create_user_id' => $this->input->post("invoice_create_user_id"),
									'create_time' => $create_time,
									'status' => $this->input->post("radio1")
								);
								$invoice_model = $this->home_model->invoice_model($data_file);
								$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
								$file_name = $_FILES ['userfile'] ['name'];
								$file_size = $_FILES ['userfile'] ['size'];
								$file_type = $_FILES ['userfile'] ['type'];
								$file_error = $_FILES ['userfile'] ['error'];
								$target_url = 'https://thelondonoffice.com/admin/fileupload.php';
								$post = array(
								'file_contents' => new CurlFile($file_tmp_name, $file_type, $file_name),
								);
								// Prepare the cURL call to upload the external script
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL, $target_url);
								curl_setopt($ch, CURLOPT_HEADER, false);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT]']);
								curl_setopt($ch, CURLOPT_POST, true);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
						  		$result = curl_exec($ch);
								curl_close($ch);
								$handle = fopen ( $file_tmp_name, "r" );
								$content = fread ( $handle, $file_size );
								fclose ( $handle );
								$encoded_content = chunk_split ( base64_encode ( $content ) );
								$body .= "--$boundary\r\n";
								$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
								$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
								$body .= "Content-Transfer-Encoding: base64\r\n";
								$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
								$body .= $encoded_content;
						} else {
							$error = array (
									'error' => $this->upload->display_errors() 
							);
						}
					}
					
				}
				//id@thelondonoffice.com
				/* $body .= "--$boundary\r\n";
				$body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
				$body .= "Content-Transfer-Encoding: base64\r\n\r\n";			
				$body .= chunk_split ( base64_encode ( $message ) ); */
				//id@thelondonoffice.com
				if($pdf_handel==1)
				{
					if(mail ($to,$subject,$body,$headers ))
					{
						echo "<script>alert('Document uploaded');</script>";
						redirect ($com_uri, 'refresh' );
					}
				}
				else{
			        $subject1 = 'The Office Support > Service Invoice'; // subject of email
					
					$headers1 = 'MIME-Version: 1.0' . "\r\n";					
					$headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers1 .= 'From: The Office Support<post@theoffice support>'. "\r\n"; 
			        $body1 ='';
					$body1 .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office support – Service Invoice</span></br>
					<p style="">Dear '.$email123->first_name." ".$email123->last_name.',</p>
					<p style="">Thank you for your order and payment.</p>
					<p style="">Please login to your account to view your invoice.</p>
					<p style="">To view the invoice <a href="https://theoffice.support/">login to your online account</a>, select the company then mail.</p>
					<p style="">Should you wish to change your preference to receive mail with attachments login to your account and follow these steps:</p>
					<p>Step 1. Got to "Preferences"</p>
					<p>Step 2: Select "Email Alert" from the menu</p>
					<p>Step 3: Change to "Email Alerts With Attachments"</p>
					';
					
					$body1 .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
					if(mail ($to,$subject1,$body1,$headers1 ))
					{
						echo "<script>alert('Document uploaded');</script>";
						redirect ($com_uri, 'refresh' );
					}
				}
					
			} else {
			   echo "<script>alert('Please upload your identity');</script>";die("please upload ur id");
			}

			//redirect ($com_uri, 'refresh' );	
	}
	public function invoice_get()
	{
		$company_id = $this->input->post("id");
		$result = $this->home_model->get_invoice_model($company_id);
		
		if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result);
	}
	public function invoice_resseller(){
		
		       $uri = $this->input->get_post('uri');
				$uri1 = ltrim($uri, '/');
				$com_uri= base_url() . $uri1;
		        $create_time = date('Y-m-d h:i:s');
				if (!empty($_FILES['userfile']['name'])){		
				$from_email = 'Office Support<orders@theoffice.support>'; // sender email
				$message = 'Please find the attachment below' . "\r\n";
				$boundary = md5 ( "sanwebe" );
				// header
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "From:" . $from_email . "\r\n";
				$headers .= "Reply-To: " . "\r\n";
				$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
				/*=====================================================*/
				$userfiles = $_FILES["userfile"];
				$subject = 'ID Upload >'; // subject of email
				$message .= "Company Name:\r\n";
				$message .= "Date of Upload :".date("Y-m-d",strtotime($create_time)). "\r\n";
				$message .= "Time of Upload :".date("h:i A",strtotime($create_time)). "\r\n";
				$body = "--$boundary\r\n";
				$this->load->library('upload');
				$files = $_FILES;
				/*==============Remove Specila Char===============*/
				$ext = pathinfo($files['userfile']['name'][0], PATHINFO_EXTENSION);
				$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $files['userfile']['name'][0]);
				$new_data = str_replace  ("'", "", $withoutExt);
				$new_data1 = preg_replace ('/[^\p{L}\p{N}]/u', '_', $new_data);
				/*==============Remove Specila Char===============*/
				if (isset($_FILES['userfile'])){
					$_FILES['userfile']['name'] = time().'invoice' .$new_data1.'.'.$ext;
					$_FILES['userfile']['type'] = $files['userfile']['type'][0];
					$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
					$_FILES['userfile']['error'] = $files['userfile']['error'][0];
					$_FILES['userfile']['size'] = $files['userfile']['size'][0];
					$configd['file_name'] = $_FILES['userfile']['name'];
					$this->upload->initialize($this->set_upload_options());
					if($files['userfile']['name'][0])
					{
						if ($this->upload->do_upload()) {
							//die('upload');
							$data = array(
									'upload_data' => $this->upload->data() 
							);
							     $data_file = array (
									'reseller_id' => $this->input->post("com_invoice_reseller"),
									'file_name' => $data['upload_data']["file_name"],
									'type_id' => '51',
									'create_user_id' => $this->input->post("invoice_reseller_create_user_id"),
									'create_time' => $create_time,
									'status' => $this->input->post("radio1")
								);
								$invoice_reseller_model = $this->home_model->invoice_reseller_model($data_file);
								$file_tmp_name = $_FILES ['userfile'] ['tmp_name'];
								$file_name = $_FILES ['userfile'] ['name'];
								$file_size = $_FILES ['userfile'] ['size'];
								$file_type = $_FILES ['userfile'] ['type'];
								$file_error = $_FILES ['userfile'] ['error'];
								$target_url = 'https://thelondonoffice.com/admin/fileupload.php';
									//This needs to be the full path to the file you want to send.
								$file_name_with_full_path = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$file_name;
								$post = array('extra_info' => '123456','file_contents'=>'@'.$file_name_with_full_path);
								$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL,$target_url);
								curl_setopt($ch, CURLOPT_POST,1);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
								curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
								$result=curl_exec($ch);
								curl_close ($ch);
								$handle = fopen ( $file_tmp_name, "r" );
								$content = fread ( $handle, $file_size );
								fclose ( $handle );
								$message .= "Passport or ID Card :".$file_name. "\r\n";	
								$encoded_content = chunk_split ( base64_encode ( $content ) );
								$body .= "Content-Type: $file_type; name=\"$file_name\"\r\n";
								$body .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n";
								$body .= "Content-Transfer-Encoding: base64\r\n";
								$body .= "X-Attachment-Id: " . rand ( 1000, 99999 ) . "\r\n\r\n";
								$body .= $encoded_content;
						} else {
							$error = array (
									'error' => $this->upload->display_errors() 
							);
							var_dump($error);die;
						}
					}
					
				}
				//id@thelondonoffice.com
				$body .= "--$boundary\r\n";
				$body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
				$body .= "Content-Transfer-Encoding: base64\r\n\r\n";			
				$body .= chunk_split ( base64_encode ( $message ) );	//id@thelondonoffice.com
				if(mail ("prinka.st@gmail.com",$subject,$body,$headers ))
				{
					echo "<script>alert('Document uploaded');</script>";
					redirect ($com_uri, 'refresh' );
				}
					
			} else {
			   echo "<script>alert('Please upload your identity');</script>";die("please upload ur id");
			}

			redirect ($com_uri, 'refresh' );
}
	public function invoice_reseller_get()
	{
		$reseller_id = $this->input->get("id");
		$result = $this->home_model->get_invoice_reseller_model($reseller_id);
		$data['resellers']=$result;
		$this->load->view ( 'reseller_invoice', $data);
		/* if($result){
			$result = array('status' => 1, 'message' => $result);
		}else{
			$result = array('status' => 0, 'message' => 'Woops something technically went wrong.Please try again.'); 
		}
			echo json_encode($result); */
	}
	public function loginacess()
	{
	    $uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$company_id = $this->input->Post("acess_company_id");
		$acessradio = $this->input->Post("acessradio");
	    $user_id = $this->input->Post("acess_user_id");
		
	    $companyname = $this->input->Post("acess_user_company_name");
		
		$user = $this->search->userSearch($user_id);
		//var_dump($user->email);
		$acess = $this->home_model->acess_model($user_id,$acessradio);
		//var_dump($acess);die();
		if($acess)
		{
					$to = $user->email;
					$subject = 'The Office Support > '. $companyname .' > Service Update';
					$from = 'The Office Support (noreply@theoffice.support)';
					$headers .= 'MIME-Version: 1.0'."\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: The Office Support<noreply@theoffice.support>'. "\r\n";
					// Compose a simple HTML email message
					$message = '<html><body>';
					if($acessradio==1){ 
						$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support &#8208; Service Update!</span>
		<p style="">Dear '.$companyname.',</p>
		<p style="">Your online account is now active and ready to use.</p>
		<p style=""><b>Status Update:</b> Online Account Active.</p>
		<p style=""><b>Account Login:</b> <a href="https://theoffice.support/">theoffice.support</a></p>
		<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
		<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
		<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support &rsquo; we are open weekdays from 9am to 5.30pm.</p>
		<p style="font-size: 10.5pt;">Yours sincerely,</p>
		<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
		<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
					  }else{
						$message .= '<p style="color:#080;font-size:18px;">You are Not allowed to access your account</p>';
					  }			  			  
					$message .= '</body></html>'; 
					// Sending email
					if(mail($to, $subject, $message, $headers)){
						redirect ($com_uri, 'refresh' );
					} else{
						echo "somthing went wrong";
					}	 
			} else{
				echo "somthing went wrong";
			}
			
		
	}
	
	function access_list()
	{
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) 
		{
			$asess_form = $this->input->post('asess_form');
			$config["base_url"] = base_url() . "home/access_list";
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->home_model->show_acess_change('count',$asess_form);
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
					$show_companies= $this->home_model->show_acess_change('result',$asess_form,$config["per_page"],'','export'); // for export
					$data['user_orders'] = $show_companies;
					$this->load->view('spreadsheet_view', $data);
				}else{
					$show_companies= $this->home_model->show_acess_change('result',$asess_form,$config["per_page"]);
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					$this->load->view ( 'home_view', $data );
				}
		}
		else {
			redirect ( 'login', 'refresh' );
		}
	}
	
	
	/* send multiple attachment*/
	function multipleemails()
	{
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$alerts_user_id = $this->input->Post("alerts_user_id");
		$email_alert_with = $this->input->Post("email_alert_with");
		$email= $this->home_model->user_email($alerts_user_id);
		$alldocs= $this->home_model->alldocs($alerts_user_id);
		$status_update= $this->home_model->status_update($alerts_user_id,$email_alert_with);
		/* $server_file = [];
		foreach($alldocs as $doc)
		{
		$server_file[] = ['path'=>"http://thelondonoffice.com/admin/uploads/".$doc->file_name,'name'=>$doc->file_name];
		}
        $to = $email->email;
        $subject ="The Office Support > ".$email->first_name." ".$email->last_name." > You Have Mail!";
        
        
		if($email_alert_with =='1')
		{
        $semi_rand = md5(time());
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
		$headers = "From: The Office Support<post@theoffice.support>";
        $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
		$message ='';
					$message .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support</span></br>
					<p style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">You Have Mail!</p>
					<p style="">Dear '.$email->first_name." ".$email->last_name.',</p>
					<p style="">We are writing to let you know that you mail!</p>
					<p style="">We have scanned and attached your mail as requested.</p>
					<p style="">Should you require the original please <a href="https://theoffice.support">login to your online account and request the original.</a></p>
					<p style="">Should you require to receive email alerts without attachments login to your account, select preferences > Email Alerts > Email Alerts Without Attachments.</a></p>';
					$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
        $message .= "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
        $message .= "--{$mime_boundary}\n";
        $FfilenameCount = 0;
        foreach($server_file as $sl) {
			$file = $sl['path'];
			$content = file_get_contents( $file);
			$content = chunk_split(base64_encode($content));
            $name = $sl['name']; 
            $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$name\"\n" .
                "Content-Disposition: attachment;\n" . " filename=\"$name\"\n" .
                "Content-Transfer-Encoding: base64\n\n" . $content . "\n\n";
            $message .= "--{$mime_boundary}\n";
        }
		}
		else
		{			
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= "From: The Office Support<post@theoffice.support>";
			        $message ='';
					$message .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support</span></br>
					<p style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">You Have Mail!</p>
					<p style="">Dear '.$email->first_name." ".$email->last_name.',</p>
					<p style="">We are writing to let you know that you mail!</p>
					<p style="">We have scanned and uploaded the item of mail to your online account.</p>
					<p style="">To view the mail <a href="https://theoffice.support/">login to your online account</a>, select the company then mail.</p>
					<p style="">Should you wish to change your preference to receive mail with attachments login to your account, select preferences > Email alerts > Email Alerts With Attachments.</p>';
					
					$message .= '<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">We are here to help...</span>
					<p>Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
					<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to<a href="help@theoffice.support"> help@theoffice.support</a> &rsquo; we are open weekdays from 9am to 5.30pm.</p>
					<p style="font-size: 10.5pt;">Yours sincerely,</p>
					<p style="font-size:14.0pt; color: red;">The Office Support Team</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
					<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street&#44; First Floor, London, W1W 7LT.</p>
					</body></html>';
		}
        if(mail($to, $subject, $message, $headers)) {
            echo "<script>alert('Email Sent To Client');</script>"; */
            redirect($com_uri,'refresh');
        
		 
	}
	 function pdf_form()
	{
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) 
		{
			$asess_form = $this->input->post('pdf_form_id');
			$config["base_url"] = base_url() . "home/pdf_form";
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->home_model->show_pdf_change('count',$asess_form);
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
					$show_companies= $this->home_model->show_pdf_change('result',$asess_form,$config["per_page"],'','export'); // for export
					$data['user_orders'] = $show_companies;
	  				$this->load->view('spreadsheet_view', $data);
				}else{ 
					$show_companies= $this->home_model->show_pdf_change('result',$asess_form,$config["per_page"]);
					//var_dump($show_companies);die();
					$data['user_orders'] = $show_companies;
					$data['record_count'] = $config["total_rows"] ;
					$this->load->view ( 'home_view', $data );
				}
		}
		else {
			redirect ( 'login', 'refresh' );
		}
	} 
	/* function ediumnurgh_control()
	{
		$show_companies= $this->home_model->ediumnurgh();
		//var_dump ($show_companies);
		foreach($show_companies as $comp)
		{
			var_dump($comp["email"]);
		}
	} */
	function charge_update()
	{
		$charge_order_id = $this->input->post('charge_order_id');
		$charge_company_price = $this->input->post('charge_company_price');
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$update_charge = $this->home_model->charge_update_price($charge_order_id,$charge_company_price);
		if($update_charge)
		{
			redirect($com_uri,'refresh');
		}
		else{
			echo "somthing went wrong";
		}	
	}
	function edit_reseller_name()
	{
		$company_name_reseller = $this->input->post('company_name_reseller_name');
		//var_dump($company_name_reseller);die();
		$resellerr_update_id = $this->input->post('resellerr_update_id');
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$update_reseller = $this->home_model->reseller_update_name($company_name_reseller,$resellerr_update_id);
		if($update_reseller)
		{
			redirect($com_uri,'refresh');
		}
		else{
			echo "somthing went wrong";
		}	
	}
	function tas_column()
	{
		
		 $data_file = array (
									'company_id' => $this->input->post('get_number_comp_id'),
									'user_id' => $this->input->post('get_number_user_id'),
									'phone_number' => $this->input->post('add_new_number'),
									'name' => $this->input->post('number_name'),
								);
		$email123 = $this->home_model->user_email($this->input->post('get_number_user_id'));
		$add_number_company_name = $this->input->post('add_number_company_name');
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$select_phone_number = $this->home_model->select_phone_number($this->input->post('get_number_comp_id'));
		if($select_phone_number->company_id!="")
		{
			$update_phone_number = $this->home_model->update_phone_number($data_file);
		}
		else{
			$insert_phone_number = $this->home_model->insert_phone_number($data_file);
		}
		//$from_email = 'Office Support<orders@theoffice.support>'; // sender email
					$to = $email123->email;				
					$header = "From: Office Support <orders@theoffice.support>\r\n"; 				
					$header .= 'MIME-Version: 1.0'."\r\n";				
					$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$subject = "The Office Support > Telephone Answering Service";	
			        $body ='';
					$body .='<html><body>
					<span style="font-size: 14.0pt; font-family: &quot;Arial&quot;,sans-serif; color: red">The Office Support - Telephone Answering Service</span></br>
					<p style="">Hello '.$email123->first_name." ".$email123->last_name.',</p>
					<p style="">Thank you for using our telephone service.</p>
					<p style="">Hello '.$add_number_company_name.' is ' .$this->input->post('add_new_number').'(Fax Number 020 7183 3073)with calls being answered in the name of '.$this->input->post('number_name').' </p>
					<p style="">The line is live and ready to use.</p>
					<p style="">You can send us text to create an after-hours message or should you wish to create your own please send the recording as a "wav" or "mp3" file.</p>
					<p style="">Please note; we advise within the message to avoid saying ‘please leave a message’ as we do not have an out of hours messaging system and we would not want you to miss anything from your callers.</p>
					</body></html>';
					if(mail($to, $subject, $body, $header)){
						//var_dump($email123->email);die();
						redirect($com_uri,'refresh');
	                 }
					 else{
						 echo "error";
					 }
}  

public function update_order_table()
{
	    $edit_order_user_id = $this->input->post('edit_order_user_id');
		$order_company_id = $this->input->post('order_company_id');
		$orde_update = $this->input->post('orde_update');
		$order_yes_no = $this->input->post('order_yes_no');
		 $data_file = [
									$orde_update => $order_yes_no
							];
											
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$update_order_table = $this->home_model->update_order_table_model($order_company_id,$data_file);
		if($update_order_table)
		{
			redirect($com_uri,'refresh');
		}
		else{
						 echo "error";
					 }
}

public function update_order_table_ltd()
{
	    $edit_order_user_id_ltd = $this->input->post('edit_order_user_id_ltd');
		
		$order_company_id = $this->input->post('order_company_id_ltd');
		
		$orde_update = $this->input->post('orde_update_ltd');
		$order_yes_no_ltd = $this->input->post('order_yes_no_ltd');
		 $data_file = [
									$orde_update => $order_yes_no_ltd
							];
//var_dump($data_file);die();							
		$uri = $this->input->get_post('uri');
		$uri1 = ltrim($uri, '/');
		$com_uri= base_url() . $uri1;
		$update_order_table = $this->home_model->update_order_table_model($order_company_id,$data_file);
		if($update_order_table)
		{
			redirect($com_uri,'refresh');
		}
		else{
						 echo "error";
					 }
}
}
?> 