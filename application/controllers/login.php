<?php

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }
 private function getUserIP(){        
  $client  = @$_SERVER['HTTP_CLIENT_IP'];       
 $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];       
 $remote  = $_SERVER['REMOTE_ADDR'];           
 if(filter_var($client, FILTER_VALIDATE_IP))      
 {       
 $ip = $client;      
 }       
 elseif(filter_var($forward, FILTER_VALIDATE_IP))      
 {           
 $ip = $forward;       
 }      
 else        
 {            
 $ip = $remote;       
 }            
 return $ip;   
 }
 function index()
 {
   $this->load->helper(array('form'));
   $data['ip']= $this->getUserIP();
   $this->load->view('login_view',$data);
 }

}

?>