<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		 //$this->output->enable_profiler(TRUE);
		$this->load->model(['user','home_model','search','search_model','order','ltd_model']);
		$this->load->library(['calendar','pagination']);
		$this->load->helper('download');
		ini_set('memory_limit', '-1');
		//$this->output->delete_cache();

				
	}
	public	function index() {

		if ($session_data = $this->session->userdata('logged_in')) {
		 	if($session_data['role_id'] == '1'){
				$arr['data_order'] = 'today';
				$arr['state_change'] = '3';
				$arr['state_change1'] = array('0'=>'5','1'=>'7');
				$data['title'] = 'Company Manager';
				//for admin only

				 $data['total_companies'] = $this->home_model->countCompany('count');

				 $data['new_orders'] = $this->home_model->count_NewOdersCompanies_new('count',$arr['data_order']);

				 $data['state_change'] = $this->home_model->count_state_change_new('count',$arr['state_change']);
				 $data['pending_id'] = $this->home_model->count_state_change_new('count',$arr['state_change1']);

				 $data['newformations'] = $this->home_model->countNewFormStatus_new('count');
				 $data['renewals'] = $this->home_model->count_companies_newformation('count');
				 //echo "<pre>"; print_r($data['renewals']); die('testing');
				 $data['company_register'] = $this->home_model->company_register_count_companies_new('count');
				 $data['hosting'] = $this->home_model->hosting_count_companies_new('count');
				 $data['documents_service'] = $this->home_model->count_document_service_companies_new('count');
				 $data['patner_accounts']= $this->search->resellersCount();
				//echo $data['patner_accounts']; die;

				$this->load->view ( 'front_view',$data);
			}
		}
	}
	public	function dashboard_index() {

		
		$data['title'] ="Company Manager";
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {


 			$config["base_url"] = base_url() . "dashboard/dashboard_index";
			
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->home_model->countCompany('count');
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $result['result']; // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->countCompany('result',$config["per_page"],$page,'export');
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				 $data['types'] =  $this->order->getTypeOptions();
				 // echo "<pre>";
				 // print_r($data['user_orders']); die('testing');
				$this->load->view('home_view',$data);
 				}

		}else {
			redirect ( 'login', 'refresh' );
		}
	}

	public function searchResult(){
		$data['pagef'] = $this->input->get_post('page')==''?'50':$this->input->get_post('page');
		if ($session_data = $this->session->userdata('logged_in')) {
			$config["base_url"] = base_url() . "dashboard/searchResult";
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->search_model->getSearchAll_new_my('count',$config["per_page"],$data['new_search_bar'],$data['search_new']);
  	// 		$this->pagination->initialize($config);
			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['new_search_bar'] = trim($this->input->get_post('new_search_bar'));
			$data['search_new'] = $this->input->get_post('search_new');
			$config["base_url"] = base_url() . "dashboard/searchResult";
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$config["total_rows"]  = $this->search_model->getSearchAll_new_my('count',$config["per_page"],$data['new_search_bar'],$data['search_new']);
			if($_POST['export1']=='export'){
				$show_companies=$this->search_model->getSearchAll_new_my('result',$config["per_page"],$data['new_search_bar'],$data['search_new']);
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
			}else{			
				$show_companies=$this->search_model->getSearchAll_new_my('result',$config["per_page"],$data['new_search_bar'],$data['search_new']);
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				
				$this->load->view('home_view',$data);
			}
		}else
			echo "login to view data";
	}
	
/* function to display company overview */
	public function showCompanyResult(){
		$session_data = $this->session->userdata('logged_in');
		$data['role_id']=$session_data['role_id'];
		
		$data['new_company_id'] = $this->input->get('id');

		if($session_data){
			$data['getCompanyInfo_new']=$this->search_model->CustomOrderInfo('tbl_company',['id'=>$data['new_company_id']],'row');
			
			$data['getOrderData_new']=$this->search_model->CustomOrderInfo('tbl_order',['company_id'=>$data['new_company_id']],'row');
			
			$data['getShareholder']=$this->ltd_model->CustomGetShareholder('tbl_company_shareholders',['company_id'=>$data['getCompanyInfo_new']->id],'result');

			$data['GetcropShareholder']=$this->ltd_model->CustomGetShareholder('tbl_company_corp_shareholders',['company_id'=>$data['getCompanyInfo_new']->id],'result');
			




			$data['getSharecapital'] = $this->ltd_model->CustomFun('tbl_company_sharecapital',['company_id'=>$data['new_company_id']],'result');
			$data['getRegisteroffice']=$this->ltd_model->CustomFun('tbl_company_registred_office',['company_id'=>$data['new_company_id']],'row');
			$data['getdirector_corp']=$this->ltd_model->CustomFun('tbl_company_director_corp',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_director']=$this->ltd_model->CustomFun('tbl_company_director',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_secretaries']=$this->ltd_model->CustomFun('tbl_company_secretaries',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_secratory_corp']=$this->ltd_model->CustomFun('tbl_company_director',['company_id'=>$data['new_company_id']],'result');
			$data['company_psc']=$this->ltd_model->company_psc($data['new_company_id']);
			
			$data['company_psc_corp']= $this->ltd_model->CustomFun('tbl_company_psc_corp',['company_id'=>$data['new_company_id']],'result');

			
			$data['file_info_admin']= $this->ltd_model->CustomFun('tbl_file_info',['company_id'=>$data['new_company_id']],'result');
			$data['important_dates']= $this->ltd_model->CustomFun('tbl_company_formation_details',['company_id'=>$data['new_company_id']],'result');
			 $data['notes'] =  $this->search->CustomSearch('tbl_company',array('tbl_company.id'=>$data['getCompanyInfo_new']->id),'row');
			 $data['comp_overview'] =  $this->ltd_model->CustomFun('tbl_company_formation_details',array('company_id'=>$data['getCompanyInfo_new']->id),'row');


			 $data['orders'] =  $this->search->CustomSearch('tbl_order',['tbl_order.company_id'=>$data['getCompanyInfo_new']->id],'row'); 
			 $data['notification'] = $data['orders']->create_user_id;
			 $data['getAltEmail'] =  $this->home_model->CustomEmails('tbl_alt_email',array('tbl_alt_email.create_user_id'=>$data['notification']),'row');
			$data['user_email'] =  $this->home_model->CustomEmails('tbl_user',['tbl_user.id'=>$data['notification']],'row');
			
     		 $data['email'] = $data['user_email']->email;
			/* For company mails */
     		 $data['comp_mail'] = $this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['3','4','5','6','7','8','15','16','17','18','19'],'result');
     		 	 //var_dump( $data['comp_mail']); die('testing');
     		 
     		 	 /* For company documents */
     		   $data['comp_doc'] = $this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['0','1','2','8','9','10','11','12'],'result');
			/* For company ltd documents */
			 $data['comp_doc_ltd'] =	$this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14'],'result');

     		  /* For fetching sic_code */
     		  $data['sic_code'] =$this->ltd_model->CustomFun('tbl_sic_code',array('company_id'=>$data['getCompanyInfo_new']->id),'row');
     		  /* for getting messages from order table*/
     		  $data['comp_mesg'] =$this->ltd_model->CustomFun('tbl_order',array('company_id'=>$data['getCompanyInfo_new']->id),'row');
     		   /* for getting msg from tbl_message */
     		  
     		  $data['comp_message'] =$this->ltd_model->CustomFun('tbl_message',array('order_id'=>$data['comp_mesg']->id),'result');
     		  
     		  //$data['comp_order'] = $this->search->orderInfo($data['getCompanyInfo_new']->id);
     		   $data['comp_order'] = $this->search->CustomOrderDetails('tbl_order',['company_id'=>$data['getCompanyInfo_new']->id],'row_array');

     		   //echo "<pre>"; print_r($data['comp_order']); die('testing');
			 $data['comp_order_details'] = $this->search->orderInfoDetails($data['comp_order']->id);
			 $data['comp_RegisterOffice'] = $this->ltd_model->CustomFun('tbl_company_registred_office',['company_id'=>$data['getCompanyInfo_new']->id],'row');
			$this->load->view('overview',$data);
		}else
			echo "login to view data";
	}

#throw data as per company view
	public function companyView(){
		$data['title'] ="COMPANY MANAGER";
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {
 			$config["base_url"] = base_url() . "dashboard/companyView";
			$config["total_rows"]  = $this->home_model->countCompany('count');
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->countCompany('result',$config["per_page"],'','export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->countCompany('result',$config["per_page"]);
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				// 
				$this->load->view ( 'company_view', $data );
 			}
		}else {
			redirect ( 'login', 'refresh' );
		}
	}

function additional_serveics() {
	$page = $this->input->get_post('page');
			$pageeses = $this->session->userdata('pageno');
  				if($page==''){
						$data['pagef']  = '50';
 				}else{
						$data['pagef']  = $page;
 				}
	$data['title'] = "ADDTIONAL SERVEICS";
  	 if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			//var_dump($session_data);die();
		 	$arr['user_id'] = $session_data['id'];
			$arr['role_id'] = $session_data['role_id'];
			//var_dump($arr['role_id']);die();
			/*---pagination start----*/
 			$config["base_url"] = base_url() . "dashboard/additional_serveics";
			$config["total_rows"]  = $this->home_model->count_companies_add($arr);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$show_companies = [];
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					
					//var_dump($data);die("kjk");
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new_addtional($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies = $this->search_model->addtionalgetSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						//var_dump($show_companies);die("search_all_new");
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
	           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			/*---pagination end----*/
			$user_orders= $this->home_model->adtional_companies($config["per_page"],$page,$arr);
			$export ='1';
			$user_orders1= $this->home_model->adtional_companies($config["per_page"],$page,$arr,$export); // for export
			//var_dump($show_companies);die('ds');
			if(!empty($show_companies)){
				$user_orders = $show_companies;
				$config["total_rows"] = count($show_companies);
		    }else if(isset($data['search_new']) && empty($show_companies))
			{
				$user_orders = $show_companies;
				$config["total_rows"] = count($show_companies);
			}
			
			if($_POST['export1']=='export'){
				$data1['user_orders'] = $user_orders1;
				//var_dump($data1['user_orders']);die();
				$this->load->view('spreadsheet_view', $data1);
 			}else{
				$data['username'] = $session_data ['first_name'];
				$data['user_orders'] = $user_orders;
				$data['user_orders1'] = $user_orders1;
				$data['role_id'] = $arr['role_id'];
				$data['record_count'] = $config["total_rows"] ;
				//$this->load->view ( '');
				// echo "<pre>";
				// print_r($data['user_orders']); die('testing');
				$this->load->view ( 'additional_serveics', $data );
 			}
 	 }
	 else {
				// If no session, redirect to login page
				redirect ( 'login', 'refresh' );
			}
	
	}
	public function company_register() {
	$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
	if ($session_data = $this->session->userdata('logged_in')) {
	$config["base_url"] = base_url() . "dashboard/company_register";
	$config["total_rows"]  = $this->home_model->company_register_count_companies_new('count');
	$config["per_page"] = $data['pagef'];
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
	$show_companies = [];
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	                 $data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new_addtional($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies = $this->search_model->addtionalgetSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
						//var_dump($show_companies);die("search_all_new");
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
					$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
					$config["per_page"] = $data['pagef'];
					$config["uri_segment"] = 3;
					$this->pagination->initialize($config);
					$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->company_register_count_companies_new('result',$config["per_page"],'','export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}
			else if(!empty($show_companies)){
				$data['user_orders'] = $show_companies;
				$config["total_rows"] = count($show_companies);
		}
		else if(isset($data['search_new']) && empty($show_companies))
		{
			$data['user_orders']  = $show_companies;
			$config["total_rows"] = count($show_companies);
		}else{
				$show_companies= $this->home_model->company_register_count_companies_new('result',$config["per_page"]);
				$data['user_orders'] = $show_companies;
				
 			}
			$data['record_count'] = $config["total_rows"] ;

				$this->load->view ( 'company_register', $data );
		}else {
			redirect ( 'login', 'refresh' );
		}
}
/* Documents of compines*/
	public function document_service() {
	$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
	if ($session_data = $this->session->userdata('logged_in')) {
	$config["base_url"] = base_url() . "dashboard/document_service";
	$config["total_rows"]  = $this->home_model->count_document_service_companies_new('count');
	$config["per_page"] = $data['pagef'];
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config); 
	$show_companies = [];
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	
	$data['new_search_bar']=$_GET['new_search_bar'];
					$data['search_new']=$_GET['search_new'];
					if($data['search_new']=="search_all_new"){
						$type="search_all_new";
						$config["total_rows"]  = $this->search_model->countSearchAll_new_addtional($config["per_page"],$page,$data['new_search_bar'],$type);
						$show_companies = $this->search_model->addtionalgetSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
					}
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
	           $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
			$show_companies= $this->home_model->count_document_service_companies_new('result',$config["per_page"],'','export'); // for export
			$data['user_orders'] = $show_companies;
			$this->load->view('spreadsheet_view', $data);
		}else if(!empty($show_companies)){
		$data['user_orders'] = $show_companies;
		$config["total_rows"] = count($show_companies);
		}
		else if(isset($data['search_new']) && empty($show_companies))
		{
		$data['user_orders']  = $show_companies;
		$config["total_rows"] = count($show_companies);
		}
		else{
			$show_companies= $this->home_model->count_document_service_companies_new('result',$config["per_page"]);
			$data['user_orders'] = $show_companies;
			}
		   $data['record_count'] = $config["total_rows"] ;
			$this->load->view ( 'document_service', $data );
	}else {
		redirect ( 'login', 'refresh' );
	}

}


public function Hosting() {
$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
if ($session_data = $this->session->userdata('logged_in')) {
$config["base_url"] = base_url() . "dashboard/Hosting";
$config["total_rows"]  = $this->home_model->hosting_count_companies_new('count');;
$config["per_page"] = $data['pagef'];
$config["uri_segment"] = 3;
$this->pagination->initialize($config);
$show_companies = [];
$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
				$data['new_search_bar']=$_GET['new_search_bar'];
				$data['search_new']=$_GET['search_new'];
				if($data['search_new']=="search_all_new"){
					$type="search_all_new";
					$config["total_rows"]  = $this->search_model->countSearchAll_new_addtional($config["per_page"],$page,$data['new_search_bar'],$type);
					$show_companies = $this->search_model->addtionalgetSearchAll_new($config["per_page"],$page,$data['new_search_bar'],$type);
				}
				if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
		   $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
		$config["per_page"] = $data['pagef'];
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
          if($this->input->post('export1')=='export'){
			$show_companies= $this->home_model->hosting_count_companies_new('result',$config["per_page"],'','export'); // for export
			$data['user_orders'] = $show_companies;
			$this->load->view('spreadsheet_view', $data);
		}else if(!empty($show_companies)){
		$data['user_orders'] = $show_companies;
		$config["total_rows"] = count($show_companies);
		}
		else if(isset($data['search_new']) && empty($show_companies))
		{
		$data['user_orders']  = $show_companies;
		$config["total_rows"] = count($show_companies);
		}
		else{
			$show_companies= $this->home_model->hosting_count_companies_new('result',$config["per_page"]);
			$data['user_orders'] = $show_companies;
			
		}
		   $data['record_count'] = $config["total_rows"] ;
			$this->load->view ( 'hosting', $data );
	}else {
		redirect ( 'login', 'refresh' );
	}

}

/*end renewed data according to currrent month*/
		public	function comapnyRenewableNew() {
		$page = $this->input->get_post('page');
		$pageeses = $this->session->userdata('pageno');
		if($page==''){
				$data['pagef']  = '50';
		}else{
				$data['pagef']  = $page;
		}
		if ($this->session->userdata('logged_in')) {
			$session_data = $this->session->userdata('logged_in');
			$arr['user_id'] = $session_data['id'];
			$arr['role_id'] = $session_data['role_id'];
			/*---pagination start----*/
			$config["base_url"] = base_url() . "dashboard/comapnyRenewableNew";
			$config["total_rows"]  = $this->home_model->count_companies1($arr);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			/*---pagination end----*/
			$show_companies= $this->home_model->show_companies1($config["per_page"],$page,$arr);
			$export ='1';
			$show_companies1= $this->home_model->show_companies1($config["per_page"],$page,$arr,$export); // for export
			//var_dump($show_companies);die('ds');
			if($_POST['export1']=='export'){
				$data1['user_orders'] = $show_companies1;
				$this->load->view('spreadsheet_view', $data1);
			}else{
				$data['username'] = $session_data ['first_name'];
				$data['user_orders'] = $show_companies;
				$data['user_orders1'] = $show_companies1;
				$data['role_id'] = $arr['role_id'];
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view ( 'company_view', $data );
			}
		}else{
			// If no session, redirect to login page
			redirect ( 'login', 'refresh' );
		}
	}

	#get reseller as per renewable date
	public	function renewable_new() {
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {
			/*---pagination start----*/
			$config["base_url"] = base_url() . "dashboard/renewable_new";
			$config["total_rows"]  = $this->home_model->count_companies_newformation('count');
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->count_companies_newformation('result',$config["per_page"],$page,'export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->count_companies_newformation('result',$config["per_page"],$page);
				$data['user_orders'] = $show_companies;
				//echo "<pre>"; print_r($data['user_orders']); die('testing');
				$data['record_count'] = $config["total_rows"] ;
				$this->load->view ( 'home_view', $data );
 			}
		}else {
			redirect ( 'login', 'refresh' );
		}

	}

	function xsl_upload() {
		$session_data = $this->session->userdata( 'logged_in' );
		$user_id = $session_data['id'];
		$role_id = $session_data['role_id'];		
		$time = time();
		$company_data="";
		if (isset ($_FILES['userfile'])) {
			// var_dump ( $_FILES );die;
			$userfiles = $_FILES["userfile"];			
			$this->load->library( 'upload' );
			$files = $_FILES;
			$_FILES['userfile']['name'] = $time . 'xsl' . $files ['userfile']['name'][0];
			$_FILES['userfile']['type'] = $files['userfile']['type'][0];
			$_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][0];
			$_FILES['userfile']['error'] = $files['userfile']['error'][0];
			$_FILES['userfile']['size'] = $files['userfile']['size'][0];
			$configd['file_name'] = $_FILES['userfile']['name'];			
			// var_dump ($_FILES ['userfile'] ['name']);die;			
			$this->upload->initialize( $this->set_xsl_options());
			if ($this->upload->do_upload()) {
				$data = array ('upload_data' => $this->upload->data());
				$this->load->library('excel');
				$file = './uploads/xsl/'.$_FILES['userfile']['name'];
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);				
				$arrayCount = count($allDataInSheet);				
				for($i = 2; $i <= $arrayCount; $i++) {
					$getdata1 = $allDataInSheet[$i];				
					$create_time = date("Y-m-d h:i:s");
					$d = str_replace("/","-",$getdata1['D']);	
					$renewal = date("Y-m-d h:i:s",strtotime($d));		//var_dump($renewal);die;			
					$reseller = $this->search->getResellerId($getdata1['B']);					
					// var_dump($reseller);die;					
					$user = $this->search->userEmail($getdata1['AA']);					
					$first_name = $getdata1['T'];
					$last_name = $getdata1['U'];
					$email = $getdata1['AA'];
					$password = md5($getdata1['AE']);
					$ph_no = $getdata1['AB'];
					$mobile = $getdata1['AC'];
					$skpe_address = $getdata1['AD'];
					$street = $getdata1['V'];
					$city = $getdata1['W'];
					$counrty = $getdata1['Z'];
					$postcode = $getdata1['Y'];
					$county = $getdata1['X'];					
					if (!$user) {						
						$user_data = array (
								'first_name' => $first_name,
								'last_name' => $last_name,
								'email' => $email,
								'password' => $password,
								'ph_no' => $ph_no,
								'mobile' => $mobile,
								'skpe_address' => $skpe_address,
								'street' => $street,
								'city' => $city,
								'counrty' => $counrty,
								'postcode' => $postcode,
								'county' => $county,
								'role_id' => 0,
								'create_time' => $create_time 
						);						
						/* ==============================User =========================================================== */
						if ($this->db->insert( 'tbl_user', $user_data )) {							
							// var_dump('tbl_user');
							$user_detail = $this->search->getUserCreateTime ($create_time);
							
							/* ======================================Mailing=============================================== */
							$mailling_data = array (
									'mailing_name' => $getdata1 ['N'],
									'street' => $getdata1 ['O'],
									'country' => $getdata1 ['S'],
									'city' => $getdata1 ['P'],
									'postcode' => $getdata1 ['R'],
									'county' => $getdata1 ['Q'],
									'create_user_id' => $user_detail->id,
									'create_time' => $create_time
							);
							if ($this->db->insert ( 'tbl_mailling_address', $mailling_data )) {								
								// var_dump('tbl_mailling_address');
								$mailling_detail = $this->search->getMaillingByTime ( $create_time );
								
								/* =========================================Billing================================================ */
								$billing_data = array (
										'billing_name' => $getdata1['N'],
										'street' => $getdata1['O'],
										'country' => $getdata1['Q'],
										'city' => $getdata1['P'],
										'postcode' => $getdata1['R'],
										'county' => $getdata1['S'],
										'create_user_id' => $user_detail->id,
										'create_time' => $create_time
								);
								if ($this->db->insert ('tbl_billing_address',$billing_data)) {
									
									// var_dump('tbl_billing_address');
									$billing_detail = $this->search->getBillingByTime($create_time);
									
									/* =========================================Company================================================ */
									
									$company_data = array(
											'company_name' => $getdata1['A'],
											'location' => $getdata1['E'],
											'mailing_adress_id' => $mailling_detail->id,
											'billing_adress_id' => $billing_detail->id,
											'create_user_id' => $user_detail->id,
											'create_time' => $create_time 
									);
									
									if ($this->db->insert('tbl_company', $company_data)){
										// var_dump('tbl_company');
										$company_detail = $this->search->getCompanyByTime($create_time);										
										/* ==========================order=============================================================== */
										$order_data = array (
												'company_id' => $company_detail->id,
												'renewable_date' => $renewal,
												'location' => $getdata1['E'],
												'registered_office' => $getdata1['F'],
												'director_service_address' => $getdata1['G'],
												'business_address' => $getdata1['H'],
												'telephone_service' => $getdata1['I'],												
												// 'complete_package'=>$getdata1['J'],
												'hosting' => $getdata1['J'],
												'deposit' =>str_replace( '£', '', $getdata1['K'] ),
												'price' => str_replace( '£', '', $getdata1['L'] ),
												'quantity' => '1',
												'reseller_id' => $reseller,
												'state_id' => '0',
												'create_user_id' => $user_detail->id,
												'create_time' => $create_time 
										);
										
										if ($this->db->insert ('tbl_order',$order_data)) {
											
											// var_dump('tbl_order');
											/* ===========================order detail===================================================== */
											
											$order_detail = $this->search->getOrderByTime($create_time);
											
											$order_detail_data = array (
													'order_summery_id' => $order_detail->id,
													'product' => 'Registered Office Address',
													'price' => str_replace( '£', '', $getdata1 ['L'] ),
													'quantity' => '1',
													'total' => str_replace( '£', '', $getdata1 ['L'] ),
													'create_user_id' => $user_detail->id,
													'create_time' => $create_time 
											)
											;
											if ($this->db->insert('tbl_order_detail',$order_detail_data)) {
												// var_dump('tbl_order_detail');
											} 

											else
												var_dump ( $this->db->_error_message () );
										} else
											var_dump ( $this->db->_error_message () );
									} else
										var_dump ( $this->db->_error_message () );
								} else
									var_dump ( $this->db->_error_message () );
							} else
								var_dump ( $this->db->_error_message () );
						} else
							var_dump ( $this->db->_error_message () );
					} 

					else 

					{
						/* ======================================Mailing=============================================== */
						$mailling_data = array (
								'mailing_name' => $getdata1 ['N'],
								'street' => $getdata1 ['O'],
								'country' => $getdata1 ['S'],
								'city' => $getdata1 ['P'],
								'postcode' => $getdata1 ['R'],
								'county' => $getdata1 ['Q'],
								'create_user_id' => $user->id,
								'create_time' => $create_time 
						);
						if ($this->db->insert('tbl_mailling_address',$mailling_data)) {
							
							// var_dump('tbl_mailling_address else',$mailling_data );die;
							$mailling_detail = $this->search->getMaillingByTime ( $create_time );
							
							/* =========================================Billing================================================ */
							$billing_data = array (
									'billing_name' => $getdata1 ['N'],
									'street' => $getdata1 ['O'],
									'country' => $getdata1 ['Q'],
									'city' => $getdata1 ['P'],
									'postcode' => $getdata1 ['R'],
									'county' => $getdata1 ['S'],
									'create_user_id' => $user->id,
									'create_time' => $create_time 
							);
							if ($this->db->insert('tbl_billing_address',$billing_data)) {
								
								// var_dump('tbl_billing_address else');
								$billing_detail = $this->search->getBillingByTime ( $create_time );
								
								/* =========================================Company================================================ */
								
								$company_data = array (
										'company_name' => $getdata1 ['A'],
										'location' => $getdata1 ['E'],
										'mailing_adress_id' => $mailling_detail->id,
										'billing_adress_id' => $billing_detail->id,
										'create_user_id' => $user->id,
										'create_time' => $create_time 
								);
								
								if ($this->db->insert('tbl_company',$company_data)) {
									// var_dump('tbl_company else');
									$company_detail = $this->search->getCompanyByTime ( $create_time );
									
									/* ==========================order=============================================================== */
									$order_data = array (
											'company_id' => $company_detail->id,
											'renewable_date' => $renewal,
											'location' => $getdata1 ['E'],
											'registered_office' => $getdata1 ['F'],
											'director_service_address' => $getdata1 ['G'],
											'business_address' => $getdata1 ['H'],
											'telephone_service' => $getdata1 ['I'],
											
											// 'complete_package'=>$getdata1['J'],
											'hosting' => $getdata1 ['J'],
											'deposit' =>str_replace( '£', '', $getdata1['K'] ),
											'price' => str_replace ( '£', '', $getdata1 ['L'] ),
											'quantity' => '1',
											'reseller_id' => $reseller,
											'state_id' => '0',
											'create_user_id' => $user->id,
											'create_time' => $create_time 
									);
									
									if ($this->db->insert ('tbl_order',$order_data)) {
										
										// var_dump('tbl_order else');
										/* ===========================order detail===================================================== */
										
										$order_detail = $this->search->getOrderByTime($create_time);
										
										$order_detail_data = array (
												'order_summery_id' => $order_detail->id,
												'product' => 'Registered Office Address',
												'price' => str_replace ( '£', '', $getdata1 ['L'] ),
												'quantity' => '1',
												'total' => str_replace ( '£', '', $getdata1 ['L'] ),
												'create_user_id' => $user->id,
												'create_time' => $create_time
										)
										;
										// var_dump($order_detail,$order_detail_data);die;
										if ($this->db->insert ('tbl_order_detail',$order_detail_data)) {
											// var_dump('tbl_order_detail else');
										} else {
											var_dump ( $this->db->_error_message () );
											die ();
										}
									} else {
										var_dump ( $this->db->_error_message () );
										die ();
									}
								} else {
									var_dump ( $this->db->_error_message () );
									die ();
								}
							} else {
								var_dump ( $this->db->_error_message () );
								die ();
							}
						} else {
							var_dump ( $this->db->_error_message () );
							die ();
						}
					}
				}
				redirect('dashboard/index','refresh');
			} else {
				$error = array (
						'error' => $this->upload->display_errors () 
				);
				var_dump($error );
				echo "Please upload file";
				die ();
			}
			redirect ( 'dashboard/index', 'refresh' );
		}
		$data ['role_id'] = $role_id;
		$this->load->view ( 'xsl', $data );
		// $this->load->view('xsl');
	}
}