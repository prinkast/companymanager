<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function index()
 {
	$data['email'] = $this->input->post('email'); 
	$data['password'] = $this->input->post('password');
	$result = $this->user->login($data['email'], $data['password']);
	if($result){
		$sess_array = array(
         'id' => $result['id'],
         'email' => $result['email'],
       	 'first_name'=> $result['first_name'],
       	 'role_id'=> $result['role_id'],
       );
		if($result['Access']==1){
			//$this->session->sess_create();
			$this->session->set_userdata('logged_in', $sess_array);
			$res = array('status'=>'0');
		}else{
			$res = array('status'=>'3','msg'=>'not verified');
		}
		//redirect('dashboard', 'refresh');
	}else{
		$res = array('status'=>'1');
	}
	echo json_encode($res);
 
}
}
?>