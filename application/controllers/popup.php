<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
	// session_start(); //we need to call PHP's session object to access it through CI
class Popup extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model('home_model');
		$this->load->model('search_model');
		$this->load->model('search');
		$this->load->model('order');
		$this->load->model('ltd_company');
		$this->load->model('user');
		$this->load->library('calendar');
		$this->load->helper('download');
		$this->load->library('session');
		$this->load->library('pagination');
	}
	Public function userDetails_info() {
	$array['id'] = $this->input->get_post('user_company_id');
	$result = $this->search->getUserCompnay($array);
	$array['create_user_id']=$result['create_user_id'];
	$result = $this->search->getUserDetails($array);
	//var_dump($result);die();
	//$array['create_user_id'] = $this->input->get_post('create_user_id');
	if($result){
	  $result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}
Public function billing_info() {
	$array['id'] = $this->input->get_post('billing_company_id');
	$result = $this->search->getUserCompnay($array);
	//var_dump($result);die(0);
	$array['billing_adress_id']=$result['billing_adress_id'];
	//$result = $this->search->getUserDetails($array);
	//$array['id']=$result['id'];
	$result = $this->search->billingInfo($array);
	//var_dump($result);die();
	//var_dump($array['billing_adress_id']);die();
	//$array['create_user_id'] = $this->input->get_post('create_user_id');
	if($result){
	  $result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}
Public function order_info() {
	$array['id'] = $this->input->get_post('order_compaqny_id');
	
	$result = $this->search->orderInfo($array['id']);
	//var_dump($result);die(0);
    $array['id']=$result['id'];
	$result = $this->search->orderInfoDetails($array['id']);
	//var_dump($result);die();
	if($result){
	/* foreach($result as $res){
	$date2 = date('d-M-Y', strtotime($res["create_time"]));
	//var_dump($res['product']);die();
	//var_dump($res->product);die();
		 $data[] = array(
		 'product'=> $res['product'],
		 'price'=> $res['price'],
		 'quantity'=>$res['quantity'],
		 'create_time'=>$date2,
		 );
	} */
	$result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}

Public function update_userDetails_info() {
	//echo "hello";die("hello");
	$array['id'] = $this->input->get_post('User_id');
	//$array['id'] = $this->input->get_post('user_company_id');
	$array['first_name'] = trim($this->input->get_post('user_name', TRUE));
	$array['last_name'] = trim($this->input->get_post('user_last_name', TRUE));
	$array['email'] = trim($this->input->get_post('user_email', TRUE));
	$array['ph_no'] = trim($this->input->get_post('user_daytime_contact', TRUE));
	$array['mobile'] = trim($this->input->get_post('user_contact', TRUE));
	$array['skpe_address'] = trim($this->input->get_post('user_skype', TRUE));
	//$array['street'] = trim($this->input->get_post('user_street', TRUE));
	$array['city'] = trim($this->input->get_post('user_city', TRUE));
	$array['counrty'] = trim($this->input->get_post('user_country', TRUE));
	$array['postcode'] = trim($this->input->get_post('user_post_code', TRUE));
	$array['county'] = trim($this->input->get_post('user_county', TRUE)); 
	$array['password'] = trim($this->input->get_post('user_password', TRUE));
	//var_dump($array);die();
	$result = $this->home_model->UpdateUser($array);
	
	//$array['create_user_id'] = $this->input->get_post('create_user_id');
	if($result){ 
	  $result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}
Public function updateBilling_info() {
	    $array['id'] = $this->input->get_post('billing_id');
		$array['billing_name'] = $this->input->get_post('billing_name');
		$array['street'] = $this->input->get_post('billing_street');
		$array['city'] = $this->input->get_post('billing_city');
		$array['country'] = $this->input->get_post('billing_country');
		$array['postcode'] = $this->input->get_post('billing_post_code');
		$array['county'] = $this->input->get_post('billing_county');
		//var_dump($array);die();
	    $result = $this->home_model->UpdateBilling($array);
	    
	//$array['create_user_id'] = $this->input->get_post('create_user_id');
	if($result){
	  $result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}
 
 Public function updateOrder_info() {
	$array['id'] = $this->input->get_post('id');
		$array['product'] = $this->input->get_post('product_name');
		$array['price'] = $this->input->get_post('product_price');
		$array['quantity'] = $this->input->get_post('product_quantity');
		//var_dump($array);die();
		//$array['create_time'] = $this->input->get_post('billing_country');
	$result = $this->home_model->UpdateOrderinfo($array);
	//var_dump($result);die();
	//$array['create_user_id'] = $this->input->get_post('create_user_id');
	if($result){
	  $result = array('status' => 1, 'message' => $result);
	}
	else
	{
		$result = array('status' => 0, 'message' => "no data");
	}
	echo json_encode($result);
}
/* delete order row*/
 Public function DeleteOrder_info() {
	$id= $this->input->get_post('id');
	$result = $this->home_model->deleteOrder($id);
	if($result){
	  $res = array('status' => 1,'message' => "delete data");
	}
	else
	{
		$res = array('status' => 0, 'message' => "Error");
	}
	echo json_encode($res);
}

/* insert order*/
 Public function InserOrder_info() {
		$session_data = $this->session->userdata('logged_in');
		$user_id = $session_data ['id'];
		$email = $session_data ['email'];
		$selectdvalue = $this->input->get_post('selectdvalue');
		$order_compaqny_id = $this->input->get_post('order_compaqny_id');
		$oldOrderInfo_Id = $this->input->get_post('OrderInfo_Id');
		$compnayname = $this->input->get_post('compnayname');
		$array['order_summery_id'] = $this->input->get_post('OrderInfo_Id');
		$array['product'] = $this->input->get_post('payment_options');
		$product = $this->input->get_post('payment_options');
		$array['product'] = $product;
		//$arr_product = explode("-", $product, 2);
		$product_desc = trim(strstr($product, '-', true));
		//var_dump($product_desc);die();
		$array['price'] = $this->input->get_post('service_name');
		$array['quantity'] = $this->input->get_post('service_quantity');
		$create_time = date ( 'Y-m-d h:i:s' );
		//var_dump($product_desc);die("hjhj");
		$register_service11 = "No";
		$business_service = "No";
		$director_service = "No";
		$tele_service = "No";
		$complete_service = "No";
		$good_standing_app = "No";
		$document_pack = "No";
		$good_standing = "No";
		$hosting = "No";
		$legal = "No"; 
		$Company_reg = "No";			

		 if($selectdvalue==0) 
		 {  
		 if($product_desc === "Registered and Director Service Address" || $product_name ==="Registered Office and Director Service Address"){
						$director_service = "Yes";	
						$register_service11 = "Yes";
						$order = array(
				       'director_service_address'=>$director_service,
					   'registered_office'=>$register_service11,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
if($product_desc == "Registered Office Address")
{
							$register_service11 = "Yes";
						$order = array(
				       'registered_office'=>$register_service,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
if($product_desc === "Director Service Address")
{
							$director_service = "Yes";
						$order = array(
				       'director_service_address'=>$director_service,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
if($product_desc === "Virtual Business Address")
{
						$business_service = "Yes";
						$business_service = "Yes";
						$order = array(
				       'business_address'=>$business_service,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
							
					if($product_desc == "Telephone Answer Service")
					{
						$tele_service = "Yes";
						$order = array(
				       'telephone_service'=>$tele_service,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $update  = $this->db->update('tbl_order' ,$order);
						//var_dump($update);die("jh");
					}
						
					if($product_desc == "Website Hosting")
					{
						$hosting = "Yes";
						$order = array(
				       'hosting'=>$hosting,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
						
					if($product_desc == "Business Plus"){
						$register_service = "Yes";
						$director_service = "Yes";
						$business_service = "Yes";
						$order = array(
				       'director_service_address'=>$director_service,
				       'registered_office'=>$register_service,
				       'business_address'=>$business_service,
				        'id'=>$oldOrderInfo_Id
			             ); 
						$this->db->where('id', $order['id']);
		                $this->db->update('tbl_order' ,$order);
					}
		  //var_dump($array);die();
		  $result = $this->home_model->InsertOrder($array);
		
		
		//var_dump($result);
		}
		 else
		 {      
   
		$add_company_postal_deposit = "0.00";
		$add_company_formation = 2;
		$extend_time = date ( "Y-m-d h:i:s", strtotime ( date ( "Y-m-d h:i:s", strtotime ( $create_time ) ) . " + 1 year" ) );
        //var_dump("else");die();
        $user_company = $this->home_model->GetOrderByIdduplicate($order_compaqny_id);
        $user_company_id = $user_company->id;
        $user_company_name = $user_company->company_name;
		//var_dump($user_company_name);die("jhnj");
		$add_company_location_popup = $user_company->location;
		$mailing_adress_id = $user_company->mailing_adress_id;
		$billing_adress_id = $user_company->billing_adress_id;
		$user_mailling = $this->home_model->GetOrderByIdmailing($mailing_adress_id);
		//var_dump($mailling_data);die("mailing");
		$user_billing = $this->home_model->GetOrderByIdbiling($billing_adress_id);
			$mailling_data = array(
			'street'=>$user_mailling->street,
			'country'=>$user_mailling->country,
			'city'=>$user_mailling->city,
			'postcode'=>$user_mailling->postcode,
			'county'=>$user_mailling->county,		
			'create_user_id'=>$user_mailling->create_user_id,
			'update_user_id'=>$user_id,
			'create_time'=>$create_time	
			); 
	

/*===================billing====================*/
	
	
	
			$billing_data = array(
				'billing_name'=>$user_billing->street,
				'street'=>$user_billing->street,
				'country'=>$user_billing->country,
				'city'=>$user_billing->city,
				'postcode'=>$user_billing->postcode,
				'county'=>$user_billing->county,		
				'create_user_id'=>$user_billing->create_user_id,
				'update_user_id'=>$user_id,
				'create_time'=>$create_time
				
			); 		
	//var_dump($billing_data);die();
	/*=================save company========================*/
		$this->db->insert('tbl_mailling_address', $mailling_data);
		$this->db->insert('tbl_billing_address', $billing_data);
		$mailling_id = $this->search->getMaillingByTime($create_time);
		$billing_id = $this->search->getBillingByTime($create_time);
	//var_dump($billing_id->id);die("mailling");
		$company_data = array(
			'company_name'=>$user_company->company_name,
			'trading'=>$user_company->trading,
			'director1'=>$user_company->director1,
			'director2'=>$user_company->director2,
			'director3'=>$user_company->director3,
			'director4'=>$user_company->director4,
			'location' => $user_company->location,
			'mailing_adress_id'=> $mailling_id->id,
			'billing_adress_id' => $billing_id->id,
			'create_user_id'=>$user_company->create_user_id,
			'update_user_id'=>$user_id,
			'create_time'=>$create_time
			
		); 
				$user_company_data = $this->home_model->GetOrderByIdcomdup($company_data);
				//var_dump($user_company_data);die();
				$this->db->select('*,tbl_company.id as id');
				$this->db->from('tbl_company');
				$this->db->where('tbl_company.create_time', $create_time);
				$this->db->where('tbl_company.create_user_id', $user_company->create_user_id);
				$this->db->where('tbl_company.company_name', $user_company->company_name);
				$query_company2 = $this->db->get ();
				$company2 = $query_company2->row ();
				$oldOrderInfo_Id = $this->home_model->GetOrderByIdoldorderid($oldOrderInfo_Id);
				$service_name = preg_replace('/\d+/u', '', $array['product']);
				$service_name1= substr($service_name, 0, -6);
				//$service_name1 = preg_replace('/\d+/u', '', $service_name);
				$package_value = $this->order->getOrders($service_name1);
				$product = $package_value;
				$reseller_id = $oldOrderInfo_Id->reseller_id;
				if ($product == 'complete_package') {
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'business_address' => 'Yes',
							'price' => $array['price'],
							'quantity' => $array['quantity'],
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				}
				elseif ($product == 'both') {
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'price' => $array['price'],
							'quantity' => $array['quantity'],
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				} 
				elseif($product == 'partner')
				{
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit,//'0.00',
							'registered_office' => 'Yes',
							'director_service_address' => 'Yes',
							'business_address' => 'Yes',
							'price' => $array['price'],
							'quantity' => $array['quantity'],
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				}
				else {
					
					$data_order = array (
							'company_id' => $company2->id,
							'comp_ltd' => $add_company_formation,
							'renewable_date' => $extend_time,
							'location' => $add_company_location_popup,
							'deposit' => $add_company_postal_deposit, // '0.00',
							'price' => $array['price'],
							'quantity' => $array['quantity'],
							'reseller_id' => $reseller_id,
							'state_id' => '3',
							'create_user_id' => $user_company->create_user_id,
							'create_time' => $create_time 
					);
				}
				$data_order = $this->home_model->GetOrderByIdoldorderplace($data_order);
				//var_dump($data_order);die();
				$id = $this->db->insert_id();
			        $this->db->select ( '*,tbl_order.id as id' );
					$this->db->from ( 'tbl_order' );
					
					$this->db->where ( 'tbl_order.create_time', $create_time );
					$this->db->where ( 'tbl_order.create_user_id', $user_company->create_user_id );
					$this->db->where ( 'tbl_order.company_id', $company2->id );
					
					$query_order = $this->db->get ();
					$order = $query_order->row();
					//var_dump($order);die("hjh");
					$data_order_detail = array (
							'order_summery_id' => $order->id,
							
							// 'product' => $product,
							//'product' => trim($array['product'],$field_id),
							'product' => $product_desc,
							'total' => $array['price']+$add_company_postal_deposit,
							'price' => $array['price'],
							'quantity' => $array['quantity'],
							'create_user_id'=> $user_company->create_user_id ,
							'create_time'=>$create_time 
					);
					//var_dump($data_order_detail);die("hj");
					$result = $this->db->insert('tbl_order_detail', $data_order_detail);
					    
					}
					//var_dump($result);die("fgfg");
				if($result){
				
					$id = $this->db->insert_id();
					
					$result= $this->home_model->GetOrderById($id);
					//var_dump($result);die("result"); 
					$res = array('status' => 1,'message' => $result);
				}else{
				//var_dump("else");die("hjhj");
					$res = array('status' => 0, 'message' => "Error");
				}
				$header = 'From: The Office Support<noreply@theoffice.support>'. "\r\n";
				$header .= 'MIME-Version: 1.0'."\r\n";
				$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$subject = 'The Office Support >'. $compnayname .'You Have Mail!'; // subject of email
				$new_msg = '<div style="font-size: 11px;font-family: Arial,sans-serif;color: #5A5A5A;">';
				$new_msg .= '<div>';
				$new_msg .= '<a style="float: left;margin-top: 14px;color: red;text-decoration: none;font-size: 22px;" href="https://theoffice.support/">The Office Support</a>';
				$new_msg .= '</div>';
				$new_msg .= "<br>";
				$new_msg .= "<p style='font-size: 20.0pt;color: red;'>You have mail!</p>";
				$new_msg .= "<br>";
				$new_msg .='<h3>You have a New order from '. $compnayname.'</h3><hr>'."<br><br>";
				$new_msg .='Hello ,'."<br><br>";		
				$new_msg .= 'We`re writing to let you know that you have a new order.'."<br><br>";
				$new_msg .= 'The order details of the request are:'."<br><br>";
				$new_msg .= 'Company Name : '. $compnayname."<br><br>";
				//$new_msg .= 'Reseller Name : '.$reseller."<br><br>";
				$new_msg .= 'Service : '. $array['product']."<br><br>";
				$new_msg .= 'Amount : £'. $array['price']."<br><br>";
				$new_msg .= 'Quantity : '. $array['quantity']."<br><br>";
				$new_msg .= 'Date of Order : '. date ( "Y-m-d", strtotime ( $create_time ) ) ."<br><br>";
				$new_msg .= 'Time of Order :  '. date ( "h:i A", strtotime ( $create_time ) )."<br><br>";
			$new_msg .=  '<p style="">Should you require the original please <a href="#">login to your online account</a> and request the original.</p>
			<h2 style=""><span style="font-size:14.0pt; color: red;">We are here to help...</span></h2>
			<p style="">Our support team are here to help and will make sure you receive a professional and behind the scenes service at all times.</p>
			<p style="">If you have questions or need help call us on 020 7112 9184 (London) or 0131 581 8658 (Edinburgh) or 01 554 9727 (Dublin) or send us an email to help@theoffice.support – we are open weekdays from 9am to 5.30pm.</p>
			<h2 style=""><span style="font-size:14.0pt; color: red;">We do more..</span></h2>
			<p style="">We are continuously looking at additional services which may help you manage your business. Recently we have added a legal document service which includes obtaining a certificate of good standing to a full legal (Apostille) pack.</p>
			<p style="">We will keep you updated within your online account but should you need anything not available then please do let us know.</p>
			<p style="font-size: 10.5pt;">Yours sincerely,</p>
			<p style="font-size: 12.0pt;">The Office Support Team</p>
			<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">E-mails and any attachments from The Office Support are confidential. Although any attachments to the message will have been checked for viruses before transmission, you are urged to carry out your own virus check before opening attachments, since The Registered Office accepts no responsibility for loss or damage caused by software viruses.</p>
			<p style="font-size: 7.5pt; color: #A6A6A6; mso-style-textfill-fill-color: #A6A6A6; mso-style-textfill-fill-alpha: 100.0%">Registered Office (UK) Limited is registered in England No. 09347868. Registered Office: 85 Great Portland Street, First Floor, London, W1W 7LT.</p>
			<br><br>';
			$new_msg .= '</div>';

			if(mail ("order@thelondonoffice.com",$subject,$new_msg,$header )){
				$data_activity = array (
				'company_id' => $order_compaqny_id,
				'activity' => "New Order Placed",
				'Description' => $array['product'],
				'create_time' => date ( 'Y-m-d h:i:s' ),
				'create_user_id' => $user_id,
				'email' => $email 
				);
				if ($this->db->insert ( 'tbl_user_activity', $data_activity )) {
				}
			}
			echo json_encode($res);
		}





}