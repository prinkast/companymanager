<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Wp extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model('home_model');
		$this->load->model('search_model');
		$this->load->model('search');
		$this->load->model('order');
		$this->load->model('ltd_company');
		$this->load->model('user');
		$this->load->library('calendar');
		$this->load->helper('download');
		$this->load->library('session');
		$this->load->library('pagination');
	}
	Public function index() {
		$user_id = $this->input->post('user_id');
		$company_id = $this->input->post('company_id');
		$company_name = $this->input->post('company_name');
		$user_orders1 = $this->home_model->companydetail($company_id);
		$data= $this->input->post('value');
		$current_time = date('Y-m-d h:i:s');
		$create_date = date('Y-m-d');
		$create_time_mail = date('h:i:s');
		$cmp_no = $this->input->post('cmp_no');
		$trading_name = $this->input->post('trading_name');
		$amount = $this->input->post('amount');
		$fomation_vlaue = $this->input->post('fomation_vlaue');
		$loctn_req = $this->input->post('loctn_req');
		$import_data = $this->input->post('import_yes');
		$free_formation_include4 = $this->input->post('free_formation_include4');
		$free_formation_include2 = $this->input->post('free_formation_include2');
		$deposit = "0.00";
		$register_service11 = "No";
		$business_service = "No";
		$director_service = "No";
		$tele_service = "No";
		$complete_service = "No";
		$good_standing_app = "No";
		$document_pack = "No";
		$good_standing = "No";
		$hosting = "No";
		$legal = "No"; 
		$Company_reg = "No";
		$va="";
		$location_reqired="";
		if($loctn_req == "London (W1) Office,Great Portland Street,London,W1W 7LT"){
			$va= "W1";
			$location_reqired = "London (Westend)";
		}
		elseif($loctn_req == "London (WC1) Office,Bloomsbury Way,London,WC1A 2SE") {
			$va= "WC1";
			$location_reqired = "London (WC1)";
		}
		elseif($loctn_req == "Edinburgh (Central) Office,Rose Street South Lane,Edinburgh,EH2 3JG"){
			$va= "EH2";
			$location_reqired = "Edinburgh (Central)";
		}
		elseif($loctn_req == "Edinburgh (New Town) Office,Cumberland Street,Edinburgh,EH3 6RE"){
			$va= "EH3";
			$location_reqired = "Edinburgh (New Town)";
		}
		elseif($loctn_req == "London (City) Office,63/66 Hatton Garden,Suite 23,London,EC1N 8LE"){
			$va= "EC1";
			$location_reqired = "London (City)";
		}
		elseif($loctn_req == "Dublin (Central) Office,45 Dawson Street"){
			$va= "DUB";
			$location_reqired = "Dublin (Central)";
		}	
		$extend_time = date('Y-m-d h:i:s');
		foreach($data as $value){
			$product_desc = $value['desc'];
			if($product_desc ==="6 Months Subscription"){
				$extend_time = date("Y-m-d h:i:s", strtotime(date("Y-m-d h:i:s", strtotime($current_time)) . " + 6 month"));
			}elseif($product_desc ==="3 Months Subscription"){
				$extend_time = date("Y-m-d h:i:s", strtotime(date("Y-m-d h:i:s", strtotime($current_time)) . " + 3 month"));
			}elseif($product_desc ==="1 Month Subscription"){
				$extend_time = date("Y-m-d h:i:s", strtotime(date("Y-m-d h:i:s", strtotime($current_time)) . " + 1 month"));
			}else{
				$extend_time = date("Y-m-d h:i:s", strtotime(date("Y-m-d h:i:s", strtotime($current_time)) . " + 1 year"));
			}
		} 
		$comp_ltd =2;
		$Hot_Desk =2;
		$comp_reg_status = "0";
		if($fomation_vlaue=="My company is already registered."){
			$comp_reg_status = "1";
			$comp_reg_status1 = "Company Registered";
		}
		if($fomation_vlaue=="My Company is not yet registered."){
			$comp_reg_status = "2";
			$comp_reg_status1 = "New Company";
		}
		if($fomation_vlaue=="My Company is not a UK Limited Company."){
			$comp_reg_status = "3";
			$comp_reg_status1 = "Non-UK Company";
		}
		
		 $company_inforamtion = array(
				'company_name'	=>	$company_name,
				'company_number'	=>	$cmp_no,
				'trading'	=>	$trading_name,
				'com_reg_status'	=>	$comp_reg_status,
				'location'	=>	$va,
				'create_user_id'	=>	$user_id,
				'billing_adress_id'	=>	$user_orders1->billing_adress_id,
				'mailing_adress_id'	=>	$user_orders1->mailing_adress_id,
				'create_time'	=>	$current_time,
			);
			$insertCompany = $this->home_model->GetOrderByIdcomdup($company_inforamtion);
					foreach($data as $value){
						$product_name = $value['data'];
						$product_price = $value['price'];
						$product_desc = $value['desc'];
						
						if($product_name === "Registered and Director Service Address" || $product_name ==="Registered Office and Director Service Address"){
						$director_service = "Yes";	
						$register_service11 = "Yes";
						}
						if($product_name === "Hot Desk")
						$Hot_Desk =1;	

						if($product_name == "Registered Office Address")
							$register_service11 = "Yes";
								
						if($product_name === "Director Service Address")
							$director_service = "Yes";
								
						if($product_name === "Virtual Business Address")
							$business_service = "Yes";
							//$register_service = "No";
								
						if($product_name === "Telephone Answering Service")
							$tele_service = "Yes";
							
						if($product_name === "Telephone Answering")
							$tele_service = "Yes";
							
						if($product_name === "Website Hosting")
							$hosting = "Yes";
							
						if($product_name === "Virtual Business Plus"){
							$register_service11 = "Yes";
							$director_service = "Yes";
							$business_service = "Yes";
						}
				
						if($product_name === "Virtual Business Address & Telephone Answering service"){
							$business_service = "Yes";
							$tele_service = "Yes";
						}
						
						if($product_name === "Virtual Business Plus & Telephone Answering service"){
							$register_service11 = "Yes";
							$director_service = "Yes";
							$business_service = "Yes";
							$tele_service = "Yes";
						}
						
						if($product_name === "Virtual Business Plus & Website Hosting"){
							$register_service11 = "Yes";
							$director_service = "Yes";
							$business_service = "Yes";
							$hosting = "Yes";
						}
						
						if($product_name === "Virtual Business Plus & Legal Document Pack"){
							$register_service11 = "Yes";
							$director_service = "Yes";
							$business_service = "Yes";
						}
						 if($product_name === "Certificate of Good Standing with Apostille (Standard Service)"){
							$good_standing_app = "Yes";
						 }
					
						if($product_name === "Apostille Document Service"){
							$document_pack = "Yes"; 
						}
					
						if($product_name === "Certificate of Good Standing (Standard Service)"){
							$good_standing = "Yes"; 
						}
						if($product_name === "Company Register"){
							
							$Company_reg = "Yes";
						}
						if($product_name === "Legal Pack"){
							
							$legal = "Yes";
						}	
						if($product_name === "Virtual Business Plus / Telephone Answer Service"){
							$register_service11 = "Yes";
							$director_service = "Yes";
							$business_service = "Yes";
							$tele_service = "Yes";
						} 
						if($product_name == 'Virtual Business Address & Telephone Answering service' || $product_name == 'Virtual Business Plus & Telephone Answering service' ||	$product_name == 'Virtual Business Plus' ||	$product_name == 'Virtual Business Address'|| $product_name == 'Virtual Business Plus / Telephone Answer Service' ||	$product_name == 'Virtual Business Plus & Legal Document Pack' || $product_name == 'Virtual Business Plus & Website Hosting')
							$deposit = "20.00";
								
						}
						if($free_formation_include4 =="yes" || $free_formation_include2 =="yes"){
							$comp_ltd =1;
						}	
						$order_data = array(
						'company_id'=>$insertCompany,	
						'registered_office'=>$register_service11,
						'director_service_address'=>$director_service,		
						'business_address'=>$business_service,
						'telephone_service'=>$tele_service,					
						'complete_package'=>$complete_service,
						'hosting'=>$hosting,					
						'Company_Register'=>$Company_reg,	
						'Legal'=>$legal,	
						'document_pack'=>$document_pack,
						'good_standing'=>$good_standing,
						'good_standing_apostille'=>$good_standing_app,	
						'deposit'=>$deposit,
						'reseller_id'=>$user_orders1->reseller_id,					
						'price'=>$amount,
						'quantity'=>"1",			
						'location'=>$va,
						'amount'=>$amount,
						'state_id'=>3,				
						'create_time'=>$current_time,
						'renewable_date'=>$extend_time,				
						'create_user_id'=>$user_id,
						'comp_ltd'=>$comp_ltd,
						'hot_desk'=>$Hot_Desk,
						'type_id' =>'0',
					);
					$insertOrder= $this->home_model->GetOrderByIdoldorderplace($order_data);
					foreach($data as $value){
						$product_name = $value['data'];
						$product_price = $value['price'];
						$product_desc = $value['desc'];
				
					$order_details_summery = array(
						'order_summery_id'=>$insertOrder,
						'product'=>$product_name,
						'price'=>$product_price,
						'quantity'=>"1",
						'description'=> $product_desc,
						'total'=>$product_price,
						'create_time'=>$current_time,
						'create_user_id'=>$user_id,
					); 
					$insertOrderSummery = $this->home_model->InsertOrder($order_details_summery);
					$data_activity1 = array (
												'company_id' => $insertCompany,
												'activity' => "New company created ",
												'Description' => $company_name,
												'create_time' => date ( 'Y-m-d h:i:s' ),
												'create_user_id' => $user_id
												
										);
					$this->db->insert ( 'tbl_user_activity', $data_activity1 );
				}	
				if($insertOrderSummery){    
					$to  ="order@thelondonoffice.com";  
					//$to="himanshichauhan.st@gmail.com";
					$subject = "New Order > " .$company_name;
					$message = '
					<html>
					<head>
					<title>company manager </title> 
					</head>
					<body> 
					<h2>New Order: '.$company_name.'</h2>
					<h3>Ordered Details</h3>
					<table class="table table-cart" style="text-align:left;">
					<tbody>';
					foreach($data as $value){
						$product_name = $value['data'];
						$product_price = $value['price'];
						$product_desc = $value['desc'];
					$message .= '<tr>';
					$message .= '<th style="text-align: left; width: 200px;">Product: </th><td>' . $product_name . '</td>';
					$message .= '</tr>';
					
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Description: </th><td>' . $product_desc . '</td>';
					$message .= '</tr>';
					if($comp_ltd==1){
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Free LTD: </th><td>Yes</td>';
					$message .= '</tr>';
					}else{
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Free LTD: </th><td>No</td>';
					$message .= '</tr>';
					}
					
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Price: </th><td>&pound; ' . $product_price . '</td>';
					$message .= '</tr>';
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Order Date: </th><td> ' . $create_date . '</td>';
					$message .= '</tr>';
					$message .= '<tr>';
					$message .= '<th style="text-align: left;">Order Time: </th><td> ' . $create_time_mail . '</td>';
					$message .= '</tr>';
					$message .= '<tr><td colspan="2" style="border-bottom: 1px solid #ccc;"></td></tr>';
					}
					$message .= '</tbody></table>';
					
					/*======================================Orders======================================*/
					$message .= '<h3>Order Information</h3>

					<table>
					<tr>
						<td style="text-align: left; width: 200px;">Company Type:</td>
						<td>'.$comp_reg_status1.'</td>
					</tr>';
					if($comp_ltd ==1){
					$message .= '<tr>
						<td>Free LTD:</td>
						<td>Yes</td>
						</tr>';
						}else{
							$message .= '<tr>
						<td>Free LTD:</td>
						<td>No</td>
						</tr>';
						}
					
					if($company_name!='')
					{	
					$message .= '<tr>
						<td>Company Name:</td>
						<td>'.$company_name.'</td>
					</tr>';
					}
					if($trading_name!='')
					{	
					$message .= '<tr>
						<td>Trading Name:</td>
						<td>'.$trading_name.'</td>
					</tr>';
					}
					if($cmp_no!='')
					{	
					$message .= '<tr>
						<td>Company Number:</td>
						<td>'.$cmp_no.'</td>
					</tr>';
					}
					$message .= '<tr>
						<td>Reseller Name:</td>
						<td>'.$user_orders1->reseller.'</td>
					</tr>';
					
					$message .="<tr><td>Location Required	:</td><td>";
					$message .= $location_reqired;
					
					$message .="<td>
							</tr>
							</table>
							</body>
					</html>";
					$header = 'From: company manager<orders@theoffice.support>' . "\r\n" .
							'Bcc: Copy <admincopy@thelondonoffice.com>'. "\r\n" 	;					
					$header .= 'MIME-Version: 1.0' . "\r\n";
					$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				if(mail($to, $subject, $message, $header)){	
						if($cmp_no != "" && $import_data =="yes"){
						$arr['id'] = $insertCompany;
						$arr['company_number'] = $cmp_no;
						$updateCompanyAddress = $this->home_model->updateCompanyAddress($arr);
									if($updateCompanyAddress){
									$arr1['company_id'] = $arr['id'];   
									$arr1['company_number'] = $arr['company_number'];
									$qr = file_get_contents('https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk/company/'.$arr['company_number'].'.json');
									$data = json_decode($qr);
										 $register =  $data->date_of_creation;
										 $AccountRefDay= $data->accounts->accounting_reference_date->day;
										 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
										 $relatedSlides = array();
										 $sitext= $data->sic_codes;
										
										 $i=0;
										  foreach($sitext as $key=>$v)
										  {
										 $relatedSlides[] = $v;
										  }
										 $relatedSlides = implode(",",$relatedSlides);
										 $AccountRefDay= $data->accounts->accounting_reference_date->day;
										 $AccountRefMonth= $data->accounts->accounting_reference_date->month;
										 $account_ref_date = $AccountRefDay."/".$AccountRefMonth;
										 $li="https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk";
										  
										 $links = $data->links->persons_with_significant_control; 
										 $demo = $li.$links.".json";
										 $qr1 = file_get_contents($demo);
										 $data1 = json_decode($qr1);
										 $psc_data = $data1->items;
										 $get_data_1 = $this->home_model->psc($arr1['company_id'],'1');
										 if(count($get_data_1) > 0){
											 $deletePscData = $this->home_model->deletePscData($arr1['company_id']);
										  }
										 foreach($psc_data as $item)
										  {
											  $natures_of_control = $item->natures_of_control;
												$dob = $item->date_of_birth;
												$year = $dob->year;
												$month = $dob->month;
										   $array2 = array( 
											'company_id'=>$arr1['company_id'],
											'first_name'=>$item->name_elements->forename,
											'last_name'=>$item->name_elements->surname,
											'name'=>$item->name,
											'birth_month'=>$month,
											'birth_year'=>$year,
											'natures_of_control'=>$natures_of_control['0'],
											'res_building_country'=>$item->address->country,
											'res_building_postcode'=>$item->address->postal_code,
											'building_address'=>$item->address->address_line_2,
											'building_street'=>$item->address->address_line_1,
											'building_name'=>$item->address->premises,
											'nationality'=>$item->nationality,
											'res_building_country'=>$item->address->locality,
											'appoint_board'=>$item->notified_on,
											//'natures_of_control'=>$item->natures_of_control,
											'legal_authority'=>$item->identification->legal_authority,
											'legal_form'=>$item->identification->legal_form,
											'api'=>1,
										  );
										 $insert_person_data = $this->home_model->insert_psc_address($array2);
										 }
										  
										
										 $links1 = $data->links->officers;
										 $demo12 = $li.$links1.".json";
										 $qr2 = file_get_contents($demo12);
										 $data12 = json_decode($qr2);
										 $link1 = $data12->items;	
										 $get_data_2 = $this->home_model->directorData($arr1['company_id'],'1');	
										 $get_data_sec = $this->home_model->secretaries($arr1['company_id'],'1');
										   if(count($get_data_2) > 0){
											 $deletedirectorData = $this->home_model->deletedirectorData($arr1['company_id']);
										  }
										  if(count($get_data_sec) > 0){
											 $deletesectoryData = $this->home_model->deletesectoryData($arr1['company_id']);
										  }
										 foreach($link1 as $item)
										  {
										  $names = $item->former_names;
										  $role = $item->officer_role;
										  $country = $item->address->country;
										  $postal_code = $item->address->postal_code;
										  $address_line1 = $item->address->address_line_1;
										  $address_line2 = $item->address->address_line_2;
										  $region = $item->address->region;
										  $locality = $item->address->locality;
										  $country_of_residence = $item->country_of_residence;
										  $nationality = $item->nationality;
										  $month = $item->date_of_birth->month;
										  $year = $item->date_of_birth->year;
										  $occupation = $item->occupation;
										  $appoint_date = $item->appointed_on;
										  $resigned_on = $item->resigned_on;
										  $building_street = $item->address->address_line_1;
										  $building_address = $item->address->address_line_2;
										  $building_name = $item->address->premises;
										  $name = $item->name;
										  $fname = substr($name, strpos($name, ",") + 1);
										  $lname = substr($name, 0, strpos($name, ','));
										  if($name="")
										  {
											  foreach($names as $nam)
											  {
											  $fname = $nam->forenames;
											  $lname = $nam->surname;
											  }
										  }
											 $array1 = array( 
											 'company_id'=>$arr1['company_id'],
											 'first_name'=>$fname,
											 'last_name'=>$lname,
											 'nationality'=>$nationality,
											 'occupation'=>$occupation,
											 'res_building_address'=>$address_line1,
											 'country_residence'=>$country_of_residence,
											 'res_building_county'=>$locality,
											 'res_building_postcode'=>$postal_code,
											 'res_building_country'=> $country,
											 'birth_month'=> $month, 
											 'birth_year'=> $year,
											 'appoint_date'=> $appoint_date,
											 'resigned_on'=> $resigned_on,
											 'building_street'=> $building_street,
											 'building_name'=> $building_name,
											 'building_address'=> $building_address,
										  
										   'api'=>1,
										  );
										  if($role == "director")
										  {
										  
										  $insert_director_data = $this->home_model->insert_directer_address($array1);
										  //var_dump($insert_director_data);
										  }
										  elseif($role == "secretary")
										  {
										  //var_dump("secretary");
											$insert_sec_data = $this->home_model->insert_secretaries_address($array1);
										  }
										}			  
										  //die();
											$share_link = $data->links->filing_history;
											$share_capital = $li.$share_link.".json";
											$qr1 = file_get_contents($share_capital);
											$share_capital = json_decode($qr1);
											$share_capital2 = $share_capital->items;
											$get_data_4= $this->home_model->check_shareholder_company_data($arr1['company_id']);
											if(count($get_data_4) > 0){
											$get_data_4 = $this->home_model->deleteshareholderData($arr1['company_id']);
											}
												foreach($share_capital2 as $item)
												{
												$dd = $item->associated_filings;
													foreach($dd as $d)
													{
													$capital = ($d->description_values->capital);
													$dd1 = ($d->type);
														foreach($capital as $d1)
														{
														$currency = $d1->currency;
														$figure = $d1->figure;
														}
													}
													$array3 = array( 
													'company_id'=>$arr1['company_id'],
													'share_class'=>$dd1,
													'currency'=>$currency,
													'per_person_shares'=>$figure,
													'api'=>1,
													);	
													 //var_dump($array3);die();
													$insert_shareholder_data = $this->home_model->insert_shareholder_address($array3);
											   }
											  
											$array = array( 
												'company_id'=>$arr1['company_id'],
												'company_name'=>$data->company_name,
												'incorporation_date'=>$register,
												'creation_date'=>$data->registered_office_address->country,
												'company_number'=>$data->company_number,
												'company_status'=>$data->company_status,
												'address_line1'=>$data->registered_office_address->address_line_1,
												'next_due_date'=>$data->confirmation_statement->last_made_up_to,
												'next__due_return_dated'=>$data->confirmation_statement->next_made_up_to,
												'address_line2'=>$data->registered_office_address->address_line_2,
												'country'=>$data->registered_office_address->country,
												'post_town'=>$data->registered_office_address->postal_code,
												'locality'=>$data->registered_office_address->locality, 
												'account_reference_date'=>$account_ref_date,
												'last_made_update'=>$data->accounts->last_accounts->made_up_to,
												'account_next_due'=>$data->accounts->next_accounts->period_end_on,
												'SIC_Codes'=>$relatedSlides, 
										);
										$get_data = $this->home_model->Get_company_data($arr1['company_id']);	
										if($get_data == 0){
											$inserdata = $this->home_model->company_data($array);
										}else{
											$update_data = $this->home_model->update_company_data($array);
										} 
										
										$compnay_name = $data->company_name;
										$compa_name = "company name Changed to ".$compnay_name;
										//echo $compa_name;
										$data_activity = array (
												'company_id' => $arr['id'],
												'activity' => "Company Name Updated",
												'Description' => $compa_name,
												'create_time' => date ( 'Y-m-d h:i:s' ),
												'create_user_id' => $user_id
										);
										$this->db->insert ( 'tbl_user_activity', $data_activity );
								}
							
							}
							
							echo "1";
					}
						
					
			}

	}
	
	
	
	
	public function com_number_check(){
		$arr1['company_number'] = $this->input->post('cmp_no');
		//var_dump($arr1['company_number']);die("fdsf");
		$content = file_get_contents('https://fWADG_o2xBSMozgGovUFgLY_wCHzINHutQ6P9SyB:@api.companieshouse.gov.uk/company/'.$arr1['company_number'].'.json');
		$data_json = json_decode($content);
		$com_name = $data_json->company_name;
		//var_dump($date_of_creation);die();
		 if($data_json  == "" || $data_json ==NULL){
				$res = array('Status'=>'0','message'=>'No data available');
		}else{
			$res = array('Status'=>'1','com_name'=>$com_name);
		} 
		echo json_encode($res);
	}
}
