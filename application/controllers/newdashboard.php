<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newdashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		 //$this->output->enable_profiler(TRUE);
		$this->load->model(['user','home_model','search','search_model','order','ltd_model']);
		$this->load->library(['calendar','pagination']);
		$this->load->helper('download');
		ini_set('memory_limit', '-1');
		//$this->output->delete_cache();

				
	}
	public	function index() {

		if ($session_data = $this->session->userdata('logged_in')) {
		 	if($session_data['role_id'] == '1'){
				$arr['data_order'] = 'today';
				$arr['state_change'] = '3';
				$arr['state_change1'] = array('0'=>'5','1'=>'7');
				$data['title'] = 'Company Manager';
				//for admin only

				 $data['total_companies'] = $this->home_model->countCompany('count');

				 $data['new_orders'] = $this->home_model->count_NewOdersCompanies_new('count',$arr['data_order']);

				 $data['state_change'] = $this->home_model->count_state_change_new('count',$arr['state_change']);
				 $data['pending_id'] = $this->home_model->count_state_change_new('count',$arr['state_change1']);

				 $data['newformations'] = $this->home_model->countNewFormStatus_new('count');
				 $data['renewals'] = $this->home_model->count_companies_newformation('count');
				 //echo "<pre>"; print_r($data['renewals']); die('testing');
				 $data['company_register'] = $this->home_model->company_register_count_companies_new('count');
				 $data['hosting'] = $this->home_model->hosting_count_companies_new('count');
				 $data['documents_service'] = $this->home_model->count_document_service_companies_new('count');
				 $data['patner_accounts']= $this->search->resellersCount();
				//echo $data['patner_accounts']; die;

				$this->load->view ( 'front_view',$data);
			}
		}
	}
	public	function dashboard_index() {

		
		$data['title'] ="Company Manager";
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {


 			$config["base_url"] = base_url() . "dashboard/dashboard_index";
			
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->home_model->countCompany('count');
  			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $result['result']; // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->countCompany('result',$config["per_page"],$page,'export');
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				 $data['types'] =  $this->order->getTypeOptions();
				 $company_id = array();
				 $create_user_id = array();
				//  foreach ($data['user_orders'] as $key => $value) {
				//  	 array_push($company_id, $value->company_id)  ;
				//  	 array_push($create_user_id, $value->create_user_id)  ;
				//  }
				 
				//  $data['company']=  $this->search->CustomSearch('tbl_company','tbl_company.id',$company_id,'result');

				//   $data['files_info'] =  $this->search->CustomSearch('tbl_file_info','tbl_file_info.create_user_id',$create_user_id,'result');

				//  /*For search user */
				//   $data['user']= $this->search->CustomSearch('tbl_user','id',$create_user_id,'row');
				//   /* fetch company's file attachments */
				//   $data['files_info_attach'] =  $this->search->CustomSearch('tbl_file_info','tbl_file_info.create_user_id',$create_user_id,'row');
				  

				// $data['files_info_reseller'][] =  $this->search->fileInfo1($user_order->reseller_id);
				//   $data['total_price'][] = $this->order->orderPrice($user_order->id);		
				//   $data['total_price_updated'][] = $this->order->orderPrice_updated($user_order->id);






				 // echo "<pre>";
				 // print_r($data['company']); die('testing');
				//  var_dump($data['user_orders']); die();
				//  foreach ($data['user_orders'] as $key=>$user_order){
				//  	/* fetch company details */
				//  $data['company'][] =  $this->search->CustomSearch('tbl_company',['tbl_company.id'=>$user_order->company_id],'result');
				//  /* fetch company's files */
				//  $data['files_info'][] =  $this->search->CustomSearch('tbl_file_info',['tbl_file_info.create_user_id'=>$user_order->create_user_id],'result');

				//  /*For search user */
				//   $data['user'][] =  $this->search->CustomSearch('tbl_user',['id'=>$user_order->create_user_id],'row');
				//   /* fetch company's file attachments */
				//   $data['files_info_attach'][] =  $this->search->CustomSearch('tbl_file_info',['tbl_file_info.create_user_id'=>$user_order->create_user_id],'row');
				//   $data['files_info_reseller'][] =  $this->search->fileInfo1($user_order->reseller_id);
				//   $data['total_price'][] = $this->order->orderPrice($user_order->id);		
				//   $data['total_price_updated'][] = $this->order->orderPrice_updated($user_order->id);
				// }
				 
				// foreach ($data['company'] as $key => $companies) {
				//  foreach ($companies as $comp) {
				//  	 /*For search user */
				//   $data['update_company_user'][] = $this->search->CustomSearch('tbl_user',['id'=>$comp->create_user_id],'row');
				//   /* for fetching order */
				//   $data['orders'][] =  $this->search->CustomSearch('tbl_order',['tbl_order.company_id'=>$comp->id],'row');
				//  //echo "<pre>"; print_r($data['orders']); die('testing');
				//   $data['Get_company_data_formation'][] =  $this->search_model->CustomOrderInfo('tbl_company_formation_details',['company_id'=>$comp->id],'row');
				// }
				// }
				// //echo "string"; die('testing');
				// //echo "<pre>"; print_r($data['Get_company_data_formation']); die('testing');
				// foreach ($data['orders'] as $key => $order) {
				// 	$data['am'][] =  $this->order->getTypeOptions($order->type_id);
				// }
				
				// Deletes cache for the currently requested URI

				$this->load->view('home_view',$data);
 				}

		}else {
			redirect ( 'login', 'refresh' );
		}
	}

	public function searchResult(){
		$data['pagef'] = $this->input->get_post('page')==''?'50':$this->input->get_post('page');
		if ($session_data = $this->session->userdata('logged_in')) {
			$config["base_url"] = base_url() . "dashboard/searchResult";
		 	$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
			$config["total_rows"]  = $this->search_model->getSearchAll_new_my('count',$config["per_page"],$data['new_search_bar'],$data['search_new']);
  	// 		$this->pagination->initialize($config);
			// $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$data['new_search_bar'] = trim($this->input->get_post('new_search_bar'));
			$data['search_new'] = $this->input->get_post('search_new');
			$config["base_url"] = base_url() . "dashboard/searchResult";
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");	
			$config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			$config["total_rows"]  = $this->search_model->getSearchAll_new_my('count',$config["per_page"],$data['new_search_bar'],$data['search_new']);
			if($_POST['export1']=='export'){
				$show_companies=$this->search_model->getSearchAll_new_my('result',$config["per_page"],$data['new_search_bar'],$data['search_new']);
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
			}else{			
				$show_companies=$this->search_model->getSearchAll_new_my('result',$config["per_page"],$data['new_search_bar'],$data['search_new']);
				$data['role_id'] = $arr['role_id'];
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;
				$n=60;
				$this->output->cache($n);
				$this->load->view('home_view',$data);
			}
		}else
			echo "login to view data";
	}
	
/* function to display company overview */
	public function showCompanyResult(){
		$session_data = $this->session->userdata('logged_in');
		$data['role_id']=$session_data['role_id'];
		
		$data['new_company_id'] = $this->input->get('id');

		if($session_data){
			$data['getCompanyInfo_new']=$this->search_model->CustomOrderInfo('tbl_company',['id'=>$data['new_company_id']],'row');
			
			$data['getOrderData_new']=$this->search_model->CustomOrderInfo('tbl_order',['company_id'=>$data['new_company_id']],'row');
			
			$data['getShareholder']=$this->ltd_model->CustomGetShareholder('tbl_company_shareholders',['company_id'=>$data['getCompanyInfo_new']->id],'result');

			$data['GetcropShareholder']=$this->ltd_model->CustomGetShareholder('tbl_company_corp_shareholders',['company_id'=>$data['getCompanyInfo_new']->id],'result');
			$data['getSharecapital'] = $this->ltd_model->CustomFun('tbl_company_sharecapital',['company_id'=>$data['new_company_id']],'result');
			$data['getRegisteroffice']=$this->ltd_model->CustomFun('tbl_company_registred_office',['company_id'=>$data['new_company_id']],'row');
			$data['getdirector_corp']=$this->ltd_model->CustomFun('tbl_company_director_corp',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_director']=$this->ltd_model->CustomFun('tbl_company_director',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_secretaries']=$this->ltd_model->CustomFun('tbl_company_secretaries',['company_id'=>$data['new_company_id']],'result');
			$data['getcompany_secratory_corp']=$this->ltd_model->CustomFun('tbl_company_director',['company_id'=>$data['new_company_id']],'result');
			$data['company_psc']=$this->ltd_model->company_psc($data['new_company_id']);
			
			$data['company_psc_corp']= $this->ltd_model->CustomFun('tbl_company_psc_corp',['company_id'=>$data['new_company_id']],'result');

			
			$data['file_info_admin']= $this->ltd_model->CustomFun('tbl_file_info',['company_id'=>$data['new_company_id']],'result');
			$data['important_dates']= $this->ltd_model->CustomFun('tbl_company_formation_details',['company_id'=>$data['new_company_id']],'result');
			 $data['notes'] =  $this->search->CustomSearch('tbl_company',array('tbl_company.id'=>$data['getCompanyInfo_new']->id),'row');
			 $data['comp_overview'] =  $this->ltd_model->CustomFun('tbl_company_formation_details',array('company_id'=>$data['getCompanyInfo_new']->id),'row');


			 $data['orders'] =  $this->search->CustomSearch('tbl_order',['tbl_order.company_id'=>$data['getCompanyInfo_new']->id],'row'); 
			 $data['notification'] = $data['orders']->create_user_id;
			 $data['getAltEmail'] =  $this->home_model->CustomEmails('tbl_alt_email',array('tbl_alt_email.create_user_id'=>$data['notification']),'row');
			$data['user_email'] =  $this->home_model->CustomEmails('tbl_user',['tbl_user.id'=>$data['notification']],'row');
			
     		 $data['email'] = $data['user_email']->email;
			/* For company mails */
     		 $data['comp_mail'] = $this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['3','4','5','6','7','8','15','16','17','18','19'],'result');
     		 	 //var_dump( $data['comp_mail']); die('testing');
     		 
     		 	 /* For company documents */
     		   $data['comp_doc'] = $this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['0','1','2','8','9','10','11','12'],'result');
			/* For company ltd documents */
			 $data['comp_doc_ltd'] =	$this->ltd_model->CustomMail('tbl_file_info',['company_id'=>$data['getCompanyInfo_new']->id],'tbl_file_info.type_id',['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14'],'result');

     		  /* For fetching sic_code */
     		  $data['sic_code'] =$this->ltd_model->CustomFun('tbl_sic_code',array('company_id'=>$data['getCompanyInfo_new']->id),'row');
     		  /* for getting messages from order table*/
     		  $data['comp_mesg'] =$this->ltd_model->CustomFun('tbl_order',array('company_id'=>$data['getCompanyInfo_new']->id),'row');
     		   /* for getting msg from tbl_message */
     		  
     		  $data['comp_message'] =$this->ltd_model->CustomFun('tbl_message',array('order_id'=>$data['comp_mesg']->id),'result');
     		  
     		  //$data['comp_order'] = $this->search->orderInfo($data['getCompanyInfo_new']->id);
     		   $data['comp_order'] = $this->search->CustomOrderDetails('tbl_order',['company_id'=>$data['getCompanyInfo_new']->id],'row_array');

     		   echo "<pre>"; print_r($data['comp_order']); die('testing');
			 $data['comp_order_details'] = $this->search->orderInfoDetails($data['comp_order']->id);
			 $data['comp_RegisterOffice'] = $this->ltd_model->CustomFun('tbl_company_registred_office',['company_id'=>$data['getCompanyInfo_new']->id],'row');
			$this->load->view('overview',$data);
		}else
			echo "login to view data";
	}

#throw data as per company view
	public function companyView(){
		$data['title'] ="COMPANY MANAGER";
		$data['pagef'] = $this->input->get('page')==''?'50':$this->input->get('page');
		if ($session_data = $this->session->userdata('logged_in')) {
 			$config["base_url"] = base_url() . "dashboard/companyView";
			$config["total_rows"]  = $this->home_model->countCompany('count');
			$config["per_page"] = $data['pagef'];
			$config["uri_segment"] = 3;
  			$this->pagination->initialize($config);
			//$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			if($this->input->post('export1')=='export'){
				$show_companies= $this->home_model->countCompany('result',$config["per_page"],'','export'); // for export
				$data['user_orders'] = $show_companies;
				$this->load->view('spreadsheet_view', $data);
 			}else{
				$show_companies= $this->home_model->countCompany('result',$config["per_page"]);
				$data['user_orders'] = $show_companies;
				$data['record_count'] = $config["total_rows"] ;



				// foreach ($data['user_orders'] as $key=>$user_order)
				// {

				// $data['company'][] =  $this->search->CustomSearch('tbl_company',['tbl_company.id'=>$user_order->company_id],'result');
				// /* fetch company's files */
				//  $data['files_info'][] =  $this->search->CustomSearch('tbl_file_info',['tbl_file_info.create_user_id'=>$user_order->create_user_id],'result');

				//  /*For search user */
				//   $data['user'][] =  $this->search->CustomSearch('tbl_user',['id'=>$user_order->create_user_id],'row');
				// }
				// foreach ($data['company'] as $key => $companies) {
				//  foreach ($companies as $comp) {
				//  	 /*For search user */
				//   $data['update_company_user'][] = $this->search->CustomSearch('tbl_user',['id'=>$comp->create_user_id],'row');
				//   /* for fetching order */
				//   $data['orders'][] =  $this->search->CustomSearch('tbl_order',['tbl_order.company_id'=>$comp->id],'row');
				// // echo "<pre>"; print_r($data['orders']); die('testing');
				//   $data['Get_company_data_formation'][] =  $this->search_model->CustomOrderInfo('tbl_company_formation_details',['company_id'=>$comp->id],'row');

				// }
				// }
				 //echo "<pre>"; print_r($data['orders']); die('testing');
				$this->load->view ( 'company_view', $data );
 			}
		}else {
			redirect ( 'login', 'refresh' );
		}
	}



}