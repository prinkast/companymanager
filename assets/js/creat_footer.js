$(".edit_pencil").click(function() {
            $(".address_edit,.address_row").addClass("edit_office_add");
        });
        $(".form-actn-btn .btn-success").click(function() {
            $(".address_edit,.address_row").removeClass("edit_office_add");
        });
		
$(window).scroll(function(){ 
		  if ($(this).scrollTop() > 80) {
			  $('.right_package_box').addClass('package_box_fixed');
		  } else {
			  $('.right_package_box').removeClass('package_box_fixed');
		  }
	  });
	  
	  
	  $(window).scroll(function(){
		if ($(this).scrollTop() > 300) {
			$('.left-nav-tab').addClass('package_box_fixed');
		} else {
			$('.left-nav-tab').removeClass('package_box_fixed');
		}
	  });
	  $(document).ready(function() {
        window.history.pushState(null, "", window.location.href);        
        window.onpopstate = function() {
            window.history.pushState(null, "", window.location.href);
			//alert("you might lose important data");
		};
    });
		$(document).ready(function() {
			var base_url = window.location.origin;
			//alert(base_url);
           $('#deposit_submit').click(function(){
			   $('#deposit_submit').attr("disabled", "disabled");
				var order_deposite_id = $.trim($("#last_id").val()); 
				var deposit_mail_type = $.trim($("#deposit_mail_type").val()); 
				var deposit_no_items = $.trim($("#deposit_no_items").val()); 
				var deposit_post_charge = $.trim($("#deposit_post_charge").val()); 
				var deposit_company_id = $('#deposit_company_id').val();
				var orderPrice = $('#orderPrice').val();
				//alert(deposit_company_id);
				$.ajax({
				type: "POST",
				url: base_url+"/home/deposit_details",       
				data: {
					'order_id':order_deposite_id,
					'deposit_mail_type':deposit_mail_type,
					'deposit_no_items':deposit_no_items,
					'deposit_post_charge':deposit_post_charge,
					'deposit_company_id':deposit_company_id,
					'orderPrice':orderPrice,
				},
				success: function(res) {
				//alert(res);
				var data = JSON.parse(res)
				
					if (data.status == '1') {
						$("#deposit_form")[0].reset();
						$("#deposit_submit").removeAttr("disabled");
						//alert(data);
						//alert(data.message.type_id);
						var deposit= data.message.type_id ;
						if(deposit==1)
						{
						var deposit_method="Business Mail";
						}
						else if(deposit==2){
						var deposit_method="Parcel";
						}
						else
						{
						var deposit_method="Official Mail";
						} 
						
						var item = '';
						item += '<tr id="tr_id_'+data.message.deposit_id+'">';
						item += ('<td>' + data.message.created_date + '</td>');
						item += ('<td>' + deposit_method + '</td>');
						item += ('<td>' + data.message.deposit_no_items + '</td>');
						item += ('<td>' + '&pound;' + data.message.deposit_admin_fee + '</td>');
						item += ('<td>' + '&pound;' + data.message.deposit_post_charge + '</td>');
						item += ('<td>' + '&pound;' + data.message.total_amount + '</td>');
						item += ('<td>' + '&pound;' + data.message.new_balance + '</td>');
						item += ('<td class="custom_icon fa fa-trash-o">' + '<input type="button" value="" name="delete"  id="delete_record_'+data.message.deposit_id+'" data-no='+data.message.deposit_id+' onclick="deleteDeposit('+data.message.deposit_id+',\''+deposit_method+'\','+data.message.total_amount+','+data.message.new_balance+')">' + '</td>');
						item += ('</tr>');
					  // alert(data.mainBalance);
						$( ".data-print" ).prepend(item);
						$('#main_balance').html(data.mainBalance);
						$('#orderPrice').val(data.mainBalance); // check all test case
					} else {
						alert("error adding data for charge history");
					}
				}
				});
           });
		   $('#deposit_submit1').click(function(){
			   $('#deposit_submit1').attr("disabled", "disabled");
				var order_deposite_id = $.trim($("#last_id").val()); 
				var deposit_refund = $.trim($("#deposit_refund").val()); 
				var amount = $.trim($("#amount").val()); 
				var deposit_company_id = $('#deposit_company_id1').val();
				var price = $('#orderPrice').val();
				//alert(deposit_company_id);
				$.ajax({
				type: "POST",
				url: base_url+"/home/deposit_details1",       
				data: {
					'order_id':order_deposite_id,
					'deposit_refund':deposit_refund,
					'amount':amount,
					'deposit_company_id':deposit_company_id,
					'price' :price,
				},
				success: function(res) {
				//alert(res);
					var data = JSON.parse(res);
					if (data.status == '1') {
						$("#deposit_form1")[0].reset();
						$("#deposit_submit1").removeAttr("disabled");
					
						
						var deposit=data.message.type_id ;
						if(deposit==4){
							var deposit_method="Deposit funds";
						}else{
							var deposit_method="Refund Deposit";
						} 
							var item = '';
							item += '<tr id="tr_id_'+data.message.deposit_id+'">';
							item += ('<td>' + data.message.created_date + '</td>');
							item += ('<td>' +deposit_method+ '</td>');
							item += ('<td>' + '&pound;' + data.message.deposit_post_charge + '</td>');
							item += ('<td>' + '&pound;' + data.message.amount + '</td>');
							item += ('<td>' + '&pound;' + data.message.new_balance + '</td>');
							item += ('<td class="custom_icon fa fa-trash-o">' + '<input type="button" value="" name="delete"  id="delete_record_'+data.message.deposit_id+'" data-no='+data.message.deposit_id+' onclick="deleteDeposit('+data.message.deposit_id+',\''+deposit_method+'\','+data.message.deposit_post_charge+','+data.message.new_balance+')">' + '</td>');
							$( ".data-print1" ).prepend(item);
							$('#main_balance').html(data.mainBalance);
							$('#orderPrice').val(data.mainBalance); // check all test case
							
					} else {
						alert("Error Adding data");
					}
				}
				});
           });
		   
		   $('#Update_user').click(function(){
		   
				var User_id = $.trim($("#User_id").val()); 
				var user_name = $.trim($("#user_name").val()); 
				var user_last_name = $.trim($("#user_last_name").val()); 
				var user_email = $.trim($("#user_email").val()); 
				var user_daytime_contact = $.trim($("#user_daytime_contact").val()); 
				var user_contact = $.trim($("#user_contact").val()); 
				var user_skype = $.trim($("#user_skype").val()); 
				var user_street = $.trim($("#user_street").val());  
				var user_city = $.trim($("#user_city").val()); 
				var user_country = $.trim($("#user_country").val()); 
				var user_post_code = $.trim($("#user_post_code").val()); 
				var user_county = $.trim($("#user_county").val()); 
				var user_password = $.trim($("#user_password").val()); 
				var update_user_form= $("#update_user_form").serialize(); 
				$.ajax({
					type: "POST",
					url: base_url+"/popup/update_userDetails_info",       
					data: update_user_form,
					success: function(res) {
						var pr = JSON.parse(res)
					
						if (pr.status == '1') {
							$("#errorDiv").html("");
							$("#successDiv").html(res.message);
							console.log(pr);
							location.reload(true);
						} else {
							$("#successDiv").html("");
							$("#errorDiv").html(res.message);
						}
					}
				});
           });
		   
		   
		   $('#Update_user_billing').click(function(){
				var billing_id = $.trim($("#billing_id").val()); 
				var billing_name = $.trim($("#billing_name").val()); 
				var billing_street = $.trim($("#billing_street").val()); 
				var billing_city = $.trim($("#billing_city").val()); 
				var billing_country = $.trim($("#billing_country").val()); 
				var billing_post_code = $.trim($("#billing_post_code").val()); 
				var billing_county = $.trim($("#billing_county").val()); 
				var update_billing_form= $("#update_billing_form").serialize(); 
				$.ajax({
				type: "POST",
				url: base_url+"/popup/updateBilling_info",       
				data: update_billing_form,
				success: function(res) {
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						$("#errorDiv").html("");
						$("#successDiv").html(res.message);
						console.log(pr);
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#order_insert_form_submit').click(function(){
			   
				var OrderInfo_Id = $.trim($("#OrderInfo_Id").val()); 
				var payment_options = $.trim($("#payment_options").val()); 
				var service_name = $.trim($("#add_order_id_price").val()); 
				var service_quantity = $.trim($("#service_quantity").val()); 
				var selectdvalue = $.trim($("#selectdvalue").val()); 
				var order_compaqny_id = $.trim($("#order_compaqny_id").val()); 
				var compnayname = $.trim($("#compnayname").val()); 
				//alert(selectdvalue);
				$.ajax({
					type: "POST",
					url: base_url+"/popup/InserOrder_info",       
					data: {
						'OrderInfo_Id':OrderInfo_Id,
						'payment_options':payment_options,
						'service_name':service_name, 
						'selectdvalue':selectdvalue,
						'service_quantity':service_quantity,
						'order_compaqny_id':order_compaqny_id,
						'compnayname':compnayname,
					},
					success: function(res) {
					//alert(res);
						var data = JSON.parse(res);
						$('.pr_name').append('<input type="text" value="'+data.message.product+'" name="product_name[]" id="product_name_'+data.id+'" class="form-control new-field" >');
						$('.pr_price').append('<input type="text" value="'+data.message.price+'" name="product_price[]" id="product_price_'+data.id+'" class="form-control new-field" >');
						$('.pr_quantity').append('<input type="text" value="'+data.message.quantity+'" name="product_quantity[]" id="product_quantity_'+data.id+'" class="form-control new-field" >');
						$('.pr_date').append('<input type="text" value="'+data.message.create_time+'" name="product_quantity[]" id="product_date_'+data.id+'" class="form-control new-field" >');
						$('.btn-print').append('<input type="button" value="Edit" name="update" class="btn btn-success edit-pro new-field" data-no="'+data.id+'" id="edit_ajax_'+data.id+'"/><input type="button" value="Delete" name="update" class="btn btn-danger delete-pro new-field" id="delete_ajax_'+data.id+'" data-no="'+data.id+'" />');
						if(selectdvalue=='1')
						{
						   swal(" New order created");
							$('#order_insert_form').submit();
						}
						else
						{
						 $('#order_insert_form').submit();
						}
					}
				});
           }); 
		   $('#notes_save').click(function(){
			var compa_id = $.trim($("#compa_id").val()); 
			var comment = $.trim($("#comment").val()); 
			$.post(base_url+'/home/message_notes',{compa_id:compa_id,comment:comment}, function( data ) {
				swal("notes saved");
			});
		});
		$('#comp_save').click(function(){
			var compa_id = $.trim($("#compa_id").val());
			var comp_info = $.trim($("#comp_info").val()); 
			$.post(base_url+'/home/message_comp_info',{compa_id:compa_id,comp_info:comp_info}, function( data ) {
				//alert(data);
				swal("notes saved");
			});
		}); 
		$('#msg_notes_save').click(function(){
			var compa_id = $.trim($("#com_message_id").val()); 
			var comment = $.trim($("#comment_area").val()); 
			var com_create_user_id = $.trim($("#com_create_user_id").val()); 
			$.post(base_url+'/home/message_notes_ByuserId',{compa_id:compa_id,comment:comment,com_create_user_id:com_create_user_id}, function(data ) {
			swal("notes saved");
			});
		});
		$('#saveupdatecomp').click(function(){
			var update_compnay_id = $.trim($("#update_compnay_id").val()); 
			var comp_new_up = $.trim($("#comp_new_up").val());
			$.post(base_url+'/home/compnay_update_new',{update_compnay_id:update_compnay_id,comp_new_up:comp_new_up}, function(data ) {
			swal("Company Name Updated");
			});
		});
		$('#saveupdatecomp1').click(function(){
			var update_compnay_id = $.trim($("#update_compnay_id1").val()); 
			var comp_new_up = $.trim($("#comp_new_up1").val()); 
			$.post(base_url+'/home/trading_update_new',{update_compnay_id:update_compnay_id,comp_new_up:comp_new_up}, function(data ) {
			swal("Trading Name Updated");
			});
		});
		$('#saveupdatecompxml').click(function(){
			var update_compnay_xmlid = $.trim($("#update_compnay_xmlid").val()); 
			var comp_new_upxml = $.trim($("#comp_new_upxml").val()); 
			$.post(base_url+'/home/xmlcompany_update_name',{update_compnay_xmlid:update_compnay_xmlid,comp_new_upxml:comp_new_upxml}, function(data ) {
			swal("Company Name Updated");
			});
		});
		$('#msg_notes_save_new').click(function(){
			var compa_id = $.trim($("#com_message_id_new").val()); 
			var comment = $.trim($("#comment_area_new").val()); 
			$.post(base_url+'/home/message_notes',{compa_id:compa_id,comment:comment}, function(data) {
			var json = JSON.parse(data);
			$("#comment").val(json.message.notes);
			swal("notes saved");
			});
		});
	$("#director_select_pre_adrress").on("change",function(){
	var option_id =$(this).find('option:selected').attr('value');
	if(option_id == '1'){
		$('#dir_build_name').val('85');		$('#dir_build_street').val('Great Portland Street');		$('#dir_build_address').val('First Floor');
		$('#dir_build_town').val('London');		$('#dir_build_county').val('');		$('#dir_build_postcode').val('W1W 7LT');		$('#dir_build_country').val('England');
	}else if(option_id == '2'){
		$('#dir_build_name').val('101');		$('#dir_build_street').val('Rose Street South Lane');		$('#dir_build_address').val('');
		$('#dir_build_town').val('London');		$('#dir_build_county').val('');		$('#dir_build_postcode').val('EH2 3JG');		$('#dir_build_country').val('Scotland');

	}else if(option_id == '3'){
		$('#dir_build_name').val('40');		$('#dir_build_street').val('Bloomsbury Way');		$('#dir_build_address').val('Lower Ground Floor');
		$('#dir_build_town').val('Edinburgh');		$('#dir_build_county').val('');		$('#dir_build_postcode').val('WC1A 2SE');		$('#dir_build_country').val('England');

	}
});
$("#same_reg_address").click(function(){
  var dir_build_name = $("#dir_build_name").val();
  var dir_build_street = $("#dir_build_street").val();
  var dir_build_address = $("#dir_build_address").val();
  var dir_build_town = $("#dir_build_town").val();
  var dir_build_county = $("#dir_build_county").val();
  var dir_build_postcode = $("#dir_build_postcode").val();
  var dir_build_country = $("#dir_build_country").val();
  $("#dir_reg_name").val(dir_build_name);
  $("#dir_reg_street").val(dir_build_street);
  $("#selected_dir_reg_address").val(dir_build_address);
  $("#dir_reg_town").val(dir_build_town);
  $("#dir_reg_county").val(dir_build_county);
  $("#dir_reg_postcode").val(dir_build_postcode);
  $("#dir_reg_country").val(dir_build_country);
 });
$('#dir_button').click(function(){
				var id = $.trim($("#d_id").val()); 
				var sec_fname = $.trim($("#sec_fname").val()); 
				var d_title = $.trim($("#d_title").val()); 
				var d_Middle = $.trim($("#d_Middle").val()); 
				var d_last = $.trim($("#d_last").val()); 
				var dir_residence = $.trim($("#dir_residence").val()); 
				var dc_occu = $.trim($("#dc_occu").val()); 
				var dir_nationality = $.trim($("#dir_nationality").val()); 
				var dir_build_name = $.trim($("#dir_build_name").val()); 
				var dir_build_street = $.trim($("#dir_build_street").val()); 
				var dir_build_address = $.trim($("#dir_build_address").val()); 
				var dir_build_town = $.trim($("#dir_build_town").val()); 
				var dir_build_county = $.trim($("#dir_build_county").val()); 
				var dir_build_postcode = $.trim($("#dir_build_postcode").val()); 
				var dir_build_country = $.trim($("#dir_build_country").val()); 
				var dir_reg_name = $.trim($("#dir_reg_name").val()); 
				var dir_reg_street = $.trim($("#dir_reg_street").val()); 
				var selected_dir_reg_address = $.trim($("#selected_dir_reg_address").val()); 
				var dir_reg_town = $.trim($("#dir_reg_town").val()); 
				var dir_reg_name = $.trim($("#dir_reg_name").val()); 
				var dir_reg_county = $.trim($("#dir_reg_county").val()); 
				var dir_reg_postcode = $.trim($("#dir_reg_postcode").val()); 
				var dir_reg_country = $.trim($("#dir_reg_country").val()); 
				var dir_date = $.trim($("#dir_date").val()); 
				var dir_month = $.trim($("#dir_month").val()); 
				var dir_year = $.trim($("#dir_year").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updateDirector_info",       
				data: {'id':id,
				
				'sec_fname':sec_fname,
				'd_title':d_title,
				'd_Middle':d_Middle,
				'd_last':d_last,
				'dir_residence':dir_residence,
				'dc_occu':dc_occu,
				'dir_nationality':dir_nationality,
				'dir_build_name':dir_build_name,
				'dir_build_street':dir_build_street,
				'dir_build_address':dir_build_address,
				'dir_build_town':dir_build_town,
				'dir_build_county':dir_build_county,
				'dir_build_postcode':dir_build_postcode,
				'dir_build_country':dir_build_country,
				'dir_reg_name':dir_reg_name,
				'dir_reg_street':dir_reg_street,
				'selected_dir_reg_address':selected_dir_reg_address,
				'dir_reg_town':dir_reg_town,
				'dir_reg_county':dir_reg_county,
				'dir_reg_postcode':dir_reg_postcode,
				'dir_reg_country':dir_reg_country,
				'dir_date':dir_date,
				'dir_month':dir_month,
				'dir_year':dir_year,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });	
		    $('#dir_button1').click(function(){ 
				var id = $.trim($("#dc_id").val()); 
				var dcrop_cname = $.trim($("#dcrop_cname").val()); 
				var dcop_fname = $.trim($("#dcop_fname").val()); 
				var dcrop_lname = $.trim($("#dcrop_lname").val()); 
				var dcrop_address = $.trim($("#dcrop_address").val()); 
				var corp_dir_build_name = $.trim($("#corp_dir_build_name").val()); 
				var corp_dir_build_street = $.trim($("#corp_dir_build_street").val()); 
				var corp_pree_adrress = $.trim($("#corp_pree_adrress").val()); 
				var corp_dir_build_town = $.trim($("#corp_dir_build_town").val()); 
				var corp_dir_build_county = $.trim($("#corp_dir_build_county").val()); 
				var corp_dir_build_postcode = $.trim($("#corp_dir_build_postcode").val()); 
				var corp_dir_build_country = $.trim($("#corp_dir_build_country").val()); 
				var dir_corp_country = $.trim($("#dir_corp_country").val()); 
				var dir_corp_reg_numbr = $.trim($("#dir_corp_reg_numbr").val()); 
				var dir_corp_low = $.trim($("#dir_corp_low").val()); 
				var dir_corp_legal = $.trim($("#dir_corp_legal").val()); 
				var dir_corp_act = $.trim($("#dir_corp_act").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatecorpDirector_info",       
				data: {'id':id,
				'dcrop_cname':dcrop_cname,
				'dcop_fname':dcop_fname,
				'dcrop_lname':dcrop_lname,
				'dcrop_address':dcrop_address,
				'corp_dir_build_name':corp_dir_build_name,
				'corp_dir_build_street':corp_dir_build_street,
				'corp_pree_adrress':corp_pree_adrress,
				'corp_dir_build_town':corp_dir_build_town,
				'corp_dir_build_county':corp_dir_build_county,
				'corp_dir_build_postcode':corp_dir_build_postcode,
				'corp_dir_build_country':corp_dir_build_country,
				'dir_corp_country':dir_corp_country,
				'dir_corp_reg_numbr':dir_corp_reg_numbr,
				'dir_corp_low':dir_corp_low,
				'dir_corp_legal':dir_corp_legal,
				'dir_corp_act':dir_corp_act,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		    $('#add_corp_director').click(function(){
		   $(".directer").hide();
		   $(".directer_crop").show();
		   $('.directer_crop').trigger("reset");
		   $("#dir_button1").hide();
				$("#btn3").show();
           }); 
	 $('#btn3').click(function(){
		       var id = $.trim($("#dc_id").val()); 
				var comp_id = $.trim($("#dc_comp_id").val());
				var dcrop_cname = $.trim($("#dcrop_cname").val()); 
				var dcop_fname = $.trim($("#dcop_fname").val()); 
				var dcrop_lname = $.trim($("#dcrop_lname").val()); 
				var dcrop_address = $.trim($("#dcrop_address").val()); 
				var corp_dir_build_name = $.trim($("#corp_dir_build_name").val()); 
				var corp_dir_build_street = $.trim($("#corp_dir_build_street").val()); 
				var corp_pree_adrress = $.trim($("#corp_pree_adrress").val()); 
				var corp_dir_build_town = $.trim($("#corp_dir_build_town").val()); 
				var corp_dir_build_county = $.trim($("#corp_dir_build_county").val()); 
				var corp_dir_build_postcode = $.trim($("#corp_dir_build_postcode").val()); 
				var corp_dir_build_country = $.trim($("#corp_dir_build_country").val()); 
				var dir_corp_country = $.trim($("#dir_corp_country").val()); 
				var dir_corp_reg_numbr = $.trim($("#dir_corp_reg_numbr").val()); 
				var dir_corp_low = $.trim($("#dir_corp_low").val()); 
				var dir_corp_legal = $.trim($("#dir_corp_legal").val()); 
				var dir_corp_act = $.trim($("#dir_corp_act").val());
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertcorpDirector_info",       
				data: {
				'comp_id':comp_id,
				'dcrop_cname':dcrop_cname,
				'dcop_fname':dcop_fname,
				'dcrop_lname':dcrop_lname,
				'dcrop_address':dcrop_address,
				'corp_dir_build_name':corp_dir_build_name,
				'corp_dir_build_street':corp_dir_build_street,
				'corp_pree_adrress':corp_pree_adrress,
				'corp_dir_build_town':corp_dir_build_town,
				'corp_dir_build_county':corp_dir_build_county,
				'corp_dir_build_postcode':corp_dir_build_postcode,
				'corp_dir_build_country':corp_dir_build_country,
				'dir_corp_country':dir_corp_country,
				'dir_corp_reg_numbr':dir_corp_reg_numbr,
				'dir_corp_low':dir_corp_low,
				'dir_corp_legal':dir_corp_legal,
				'dir_corp_act':dir_corp_act,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
		   //alert("hello");
           });    
		   $("#dir_corpo_select_preeAddress").on("change",function(){
	var option_id =$(this).find('option:selected').attr('value');
	if(option_id == '1'){
		$('#corp_dir_build_name').val('85');	$('#corp_dir_build_street').val('Great Portland Street');	$('#corp_corp_pree_adrress').val('First Floor');
		$('#corp_dir_build_town').val('London');	$('#corp_dir_build_county').val('');	$('#corp_dir_build_postcode').val('W1W 7LT');	$('#corp_dir_build_country').val('England');
	}else if(option_id == '2'){
		$('#corp_dir_build_name').val('101');	$('#corp_dir_build_street').val('Rose Street South Lane');	$('#corp_pree_adrress').val('');
		$('#corp_dir_build_town').val('Edinburgh');	$('#corp_dir_build_county').val('');	$('#corp_dir_build_postcode').val('EH2 3JG');	$('#corp_dir_build_country').val('Scotland');
	}else if(option_id == '3'){
		$('#corp_dir_build_name').val('40');	$('#corp_dir_build_street').val('Bloomsbury Way');	$('#corp_pree_adrress').val('Lower Ground Floor');
		$('#corp_dir_build_town').val('Edinburgh');	$('#corp_dir_build_county').val('');	$('#corp_dir_build_postcode').val('WC1A 2SE');	$('#corp_dir_build_country').val('England');

	}
});
$("#select_secretary_pre_adrress").on("change",function(){
var s = $("#select_secretary_pre_adrress");
var option_id =$(this).find('option:selected').attr('value');
if(option_id == '1')
{	$('#secretary_building_name').val('85');
	$('#secretary_building_street').val('Great Portland Street');
	$('#secretary_pre_address').val('First Floor');
	$('#secretary_building_town').val('London');
	$('#secretary_building_county').val('');
	$('#secretary_building_postcode').val('W1W 7LT');
	$('#secretary_building_country').val('England');
}else if(option_id == '2'){
	$('#secretary_building_name').val('101');
	$('#secretary_building_street').val('Rose Street South Lane');
	$('#secretary_pre_address').val('');
	$('#secretary_building_town').val('Edinburgh');
	$('#secretary_building_county').val('');
	$('#secretary_building_postcode').val('EH2 3JG');
	$('#secretary_building_country').val('Scotland');
}else if(option_id == '3'){
	$('#secretary_building_name').val('40');
	$('#secretary_building_street').val('Bloomsbury Way');
	$('#secretary_pre_address').val('Lower Ground Floor');
	$('#secretary_building_town').val('Edinburgh');
	$('#secretary_building_county').val('');
	$('#secretary_building_postcode').val('WC1A 2SE');
	$('#secretary_building_country').val('England');
}

});
$("#corp_secratory_pre_address").on("change",function(){
 var option_id =$(this).find('option:selected').attr('value');
 if(option_id == '1'){
  $('#secratory_build_name').val('85');
  $('#corp_secratory_build_street').val('Great Portland Street');
  $('#secratory_corp_pree_adrress').val('First Floor');
  $('#corp_secratory_build_town').val('London');
  $('#corp_secratory_build_county').val('');
  $('#corp_secratory_build_postcode').val('W1W 7LT');
  $('#corp_secratory_build_country').val('England');
 }else if(option_id == '2'){
  $('#secratory_build_name').val('101');
  $('#corp_secratory_build_street').val('Rose Street South Lane');
  $('#secratory_corp_pree_adrress').val('');
  $('#corp_secratory_build_town').val('Edinburgh');
  $('#corp_secratory_build_county').val('');
  $('#corp_secratory_build_postcode').val('EH2 3JG');
  $('#corp_secratory_build_country').val('Scotland');
 }else if(option_id == '3'){
  $('#secratory_build_name').val('40');
  $('#corp_secratory_build_street').val('Bloomsbury Way');
  $('#secratory_corp_pree_adrress').val('Lower Ground Floor');
  $('#corp_secratory_build_town').val('Edinburgh');
  $('#corp_secratory_build_county').val('');
  $('#corp_secratory_build_postcode').val('WC1A 2SE');
  $('#corp_secratory_build_country').val('England');
 }
 
});
$("#psc_corp_directors_pre_address").on("change",function(){
 var option_id =$(this).find('option:selected').attr('value');
 if(option_id == '1'){
  $('#psc_dir_build_name').val('85');
  $('#psc_dir_build_street').val('Great Portland Street');
  $('#psc_dir_build_address').val('First Floor');
  $('#psc_dir_build_town').val('London');
  $('#psc_dir_build_county').val('');
  $('#psc_dir_build_postcode').val('W1W 7LT');
  $('#psc_dir_build_country').val('England');
 }else if(option_id == '2'){
  $('#psc_dir_build_name').val('101');
  $('#psc_dir_build_street').val('Rose Street South Lane');
  $('#psc_dir_build_address').val('');
  $('#psc_dir_build_town').val('Edinburgh');
  $('#psc_dir_build_county').val('');
  $('#psc_dir_build_postcode').val('EH2 3JG');
  $('#psc_dir_build_country').val('Scotland');
 }else if(option_id == '3'){
  $('#psc_dir_build_name').val('40');
  $('#psc_dir_build_street').val('Bloomsbury Way');
  $('#psc_dir_build_address').val('Lower Ground Floor');
  $('#psc_dir_build_town').val('Edinburgh');
  $('#psc_dir_build_county').val('');
  $('#psc_dir_build_postcode').val('WC1A 2SE');
  $('#psc_dir_build_country').val('England');
 }
});
$('#sec_button').click(function(){
				var id = $.trim($("#s_id").val()); 
				var s_fname = $.trim($("#s_fname").val()); 
				var s_title = $.trim($("#s_title").val()); 
				var s_mname = $.trim($("#s_mname").val()); 
				var s_lname = $.trim($("#s_lname").val()); 
				var select_secretary_pre_adrress = $.trim($("#select_secretary_pre_adrress").val()); 
				var secretary_building_name = $.trim($("#secretary_building_name").val()); 
				var secretary_building_street = $.trim($("#secretary_building_street").val()); 
				var secretary_pre_address = $.trim($("#secretary_pre_address").val()); 
				var secretary_building_town = $.trim($("#secretary_building_town").val()); 
				var secretary_building_county = $.trim($("#secretary_building_county").val()); 
				var secretary_building_postcode = $.trim($("#secretary_building_postcode").val()); 
				var secretary_building_country = $.trim($("#secretary_building_country").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updateSectory_info",       
				data: {'id':id,
				's_fname':s_fname,
				's_title':s_title,
				's_mname':s_mname,
				's_lname':s_lname,
				'select_secretary_pre_adrress':select_secretary_pre_adrress,
				'secretary_building_name':secretary_building_name,
				'secretary_building_street':secretary_building_street,
				'secretary_pre_address':secretary_pre_address,
				'secretary_building_town':secretary_building_town,
				'secretary_building_county':secretary_building_county,
				'secretary_building_postcode':secretary_building_postcode,
				'secretary_building_country':secretary_building_country,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#sec_button1').click(function(){
				var id = $.trim($("#sc_id").val()); 
				//var id = $.trim($("#s_id").val()); 
				var sc_fname = $.trim($("#sc_fname").val()); 
				var sc_lname = $.trim($("#sc_lname").val()); 
				var corp_secratory_pre_address = $.trim($("#corp_secratory_pre_address").val()); 
				var secratory_build_name = $.trim($("#secratory_build_name").val()); 
				var corp_secratory_build_street = $.trim($("#corp_secratory_build_street").val()); 
				var secratory_corp_pree_adrress = $.trim($("#secratory_corp_pree_adrress").val()); 
				var corp_secratory_build_town = $.trim($("#corp_secratory_build_town").val()); 
				var corp_secratory_build_county = $.trim($("#corp_secratory_build_county").val()); 
				var corp_secratory_build_postcode = $.trim($("#corp_secratory_build_postcode").val()); 
				var corp_secratory_build_country = $.trim($("#corp_secratory_build_country").val()); 
				var secratory_corp_country = $.trim($("#secratory_corp_country").val()); 
				var secratory_corp_reg_numbr = $.trim($("#secratory_corp_reg_numbr").val()); 
				var secratory_corp_low = $.trim($("#secratory_corp_low").val()); 
				var secratory_corp_legal = $.trim($("#secratory_corp_legal").val()); 
				//var secratory_corp_legal = $.trim($("#secratory_corp_legal").val());
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatecorpSectory_info",       
				data: {'id':id,
				'sc_fname':sc_fname,
				'sc_lname':sc_lname,
				'corp_secratory_pre_address':corp_secratory_pre_address,
				'secratory_build_name':secratory_build_name,
				'corp_secratory_build_street':corp_secratory_build_street,
				'secratory_corp_pree_adrress':secratory_corp_pree_adrress,
				'corp_secratory_build_town':corp_secratory_build_town,
				'corp_secratory_build_county':corp_secratory_build_county,
				'corp_secratory_build_postcode':corp_secratory_build_postcode,
				'corp_secratory_build_country':corp_secratory_build_country,
				'secratory_corp_country':secratory_corp_country,
				'secratory_corp_reg_numbr':secratory_corp_reg_numbr,
				'secratory_corp_low':secratory_corp_low,
				'secratory_corp_legal':secratory_corp_legal,
				
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#add_Secretary').click(function(){
				$(".Secretary").show();
				$('.Secretary').trigger("reset");
				$(".Secretarycorp").hide();
				$("#sec_button").hide();
				$("#sectorbtn2").show();
			
			});
			$('#sectorbtn2').click(function(){
			   var scom_id = $.trim($("#scom_id").val()); 
			   var s_fname = $.trim($("#s_fname").val()); 
				var s_title = $.trim($("#s_title").val()); 
				var s_mname = $.trim($("#s_mname").val()); 
				var s_lname = $.trim($("#s_lname").val()); 
				var select_secretary_pre_adrress = $.trim($("#select_secretary_pre_adrress").val()); 
				var secretary_building_name = $.trim($("#secretary_building_name").val()); 
				var secretary_building_street = $.trim($("#secretary_building_street").val()); 
				var secretary_pre_address = $.trim($("#secretary_pre_address").val()); 
				var secretary_building_town = $.trim($("#secretary_building_town").val()); 
				var secretary_building_county = $.trim($("#secretary_building_county").val()); 
				var secretary_building_postcode = $.trim($("#secretary_building_postcode").val()); 
				var secretary_building_country = $.trim($("#secretary_building_country").val());
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertSectory_info",       
				data: {'scom_id':scom_id,
				
				's_fname':s_fname,
				's_title':s_title,
				's_mname':s_mname,
				's_lname':s_lname,
				'select_secretary_pre_adrress':select_secretary_pre_adrress,
				'secretary_building_name':secretary_building_name,
				'secretary_building_street':secretary_building_street,
				'secretary_pre_address':secretary_pre_address,
				'secretary_building_town':secretary_building_town,
				'secretary_building_county':secretary_building_county,
				'secretary_building_postcode':secretary_building_postcode,
				'secretary_building_country':secretary_building_country,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
			});
			 $('#add_Secretarycorp').click(function(){
		   $(".Secretary").hide();
		   $(".Secretarycorp").show();
		   $('.Secretarycorp').trigger("reset");
		   $('#sec_button1').hide();
		   $('#sectorropbtn2').show();
		   //$(".sectcorpbtn").attr("id", "sectorropbtn2");
		   //alert("hi");
           });
		   
		   $('#sectorropbtn2').click(function(){
		  
			   var sccom_id = $.trim($("#sccom_id").val()); 
			    alert(sccom_id);
			   var sc_fname = $.trim($("#sc_fname").val()); 
				var sc_lname = $.trim($("#sc_lname").val()); 
				var corp_secratory_pre_address = $.trim($("#corp_secratory_pre_address").val()); 
				var secratory_build_name = $.trim($("#secratory_build_name").val()); 
				var corp_secratory_build_street = $.trim($("#corp_secratory_build_street").val()); 
				var secratory_corp_pree_adrress = $.trim($("#secratory_corp_pree_adrress").val()); 
				var corp_secratory_build_town = $.trim($("#corp_secratory_build_town").val()); 
				var corp_secratory_build_county = $.trim($("#corp_secratory_build_county").val()); 
				var corp_secratory_build_postcode = $.trim($("#corp_secratory_build_postcode").val()); 
				var corp_secratory_build_country = $.trim($("#corp_secratory_build_country").val()); 
				var secratory_corp_country = $.trim($("#secratory_corp_country").val()); 
				var secratory_corp_reg_numbr = $.trim($("#secratory_corp_reg_numbr").val()); 
				var secratory_corp_low = $.trim($("#secratory_corp_low").val()); 
				var secratory_corp_legal = $.trim($("#secratory_corp_legal").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertcorpSectory_info",       
				data: {'sccom_id':sccom_id,
				'sc_fname':sc_fname,
				'sc_lname':sc_lname,
				'corp_secratory_pre_address':corp_secratory_pre_address,
				'secratory_build_name':secratory_build_name,
				'corp_secratory_build_street':corp_secratory_build_street,
				'secratory_corp_pree_adrress':secratory_corp_pree_adrress,
				'corp_secratory_build_town':corp_secratory_build_town,
				'corp_secratory_build_county':corp_secratory_build_county,
				'corp_secratory_build_postcode':corp_secratory_build_postcode,
				'corp_secratory_build_country':corp_secratory_build_country,
				'secratory_corp_country':secratory_corp_country,
				'secratory_corp_reg_numbr':secratory_corp_reg_numbr,
				'secratory_corp_low':secratory_corp_low,
				'secratory_corp_legal':secratory_corp_legal,
				
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
				
			});
			$("#select_psc_pre_address").on("change",function(){
	var option_id =$(this).find('option:selected').attr('value');
	if(option_id == '1'){	
		$('#psc_building_name').val('85');	$('#psc_building_street').val('Great Portland Street');	$('#psc_pre_address').val('First Floor');
		$('#psc_building_town').val('London');	$('#psc_building_aounty').val('');	$('#psc_building_postcode').val('W1W 7LT');	$('#psc_building_country').val('England');
		$('#psc_building_res_name').val('85');	$('#psc_building_res_street').val('Great Portland Street');	$('#psc_building_res_address3').val('First Floor');
		$('#psc_building_res_town').val('London');	$('#psc_building_aounty').val('');	$('#psc_building_res_postcode').val('W1W 7LT');	$('#psc_building_res_country').val('England');
	}else if(option_id == '2'){
		$('#psc_building_name').val('101');	$('#psc_building_street').val('Rose Street South Lane');	$('#psc_pre_address').val('');
		$('#psc_building_town').val('Edinburgh');	$('#psc_building_aounty').val('');	$('#psc_building_postcode').val('EH2 3JG');	$('#psc_building_country').val('Scotland');
		
		$('#psc_building_res_name').val('101');	$('#psc_building_res_street').val('Rose Street South Lane');	$('#psc_building_res_address3').val('');
		$('#psc_building_res_town').val('Edinburgh');	$('#psc_building_res_county').val('');	$('#psc_building_res_postcode').val('EH2 3JG');	$('#psc_building_res_country').val('Scotland');
	}else if(option_id == '3'){
		$('#psc_building_name').val('40');	$('#psc_building_street').val('Bloomsbury Way');	$('#psc_pre_address').val('Lower Ground Floor');
		$('#psc_building_town').val('Edinburgh');	$('#psc_building_aounty').val('');	$('#psc_building_postcode').val('WC1A 2SE');
		$('#psc_building_country').val('England');
		
		$('#psc_building_res_name').val('40');	$('#psc_building_res_street').val('Bloomsbury Way');	$('#psc_building_res_address3').val('Lower Ground Floor');
		$('#psc_building_res_town').val('Edinburgh');	$('#psc_building_res_county').val('');	$('#psc_building_res_postcode').val('WC1A 2SE');	$('#psc_building_res_country').val('England');
	}
});
$('#psc_btn').click(function(){
				var id = $.trim($("#pc_id").val());
				var p_fname = $.trim($("#p_fname").val()); 
				var p_title = $.trim($("#p_title").val()); 
				var p_mname = $.trim($("#p_mname").val()); 
				var p_lname = $.trim($("#p_lname").val()); 
				var psc_date_select = $.trim($("#psc_date_select").val()); 
				var psc_month_select = $.trim($("#psc_month_select").val()); 
				var psc_year_select = $.trim($("#psc_year_select").val()); 
				var psc_nationality = $.trim($("#psc_nationality").val()); 
				var psc_occupation = $.trim($("#psc_occupation").val()); 
				var psc_country_res = $.trim($("#psc_country_res").val()); 
				var select_psc_pre_address = $.trim($("#select_psc_pre_address").val()); 
				var psc_building_name = $.trim($("#psc_building_name").val()); 
				var psc_building_street = $.trim($("#psc_building_street").val()); 
				var psc_pre_address = $.trim($("#psc_pre_address").val()); 
				var psc_building_town = $.trim($("#psc_building_town").val()); 
				var psc_building_aounty = $.trim($("#psc_building_aounty").val()); 
				var psc_building_postcode = $.trim($("#psc_building_postcode").val()); 
				var psc_building_country = $.trim($("#psc_building_country").val()); 
				var psc_building_res_name = $.trim($("#psc_building_res_name").val()); 
				var psc_building_res_street = $.trim($("#psc_building_res_street").val()); 
				var psc_building_res_address3 = $.trim($("#psc_building_res_address3").val()); 
				var psc_building_res_town = $.trim($("#psc_building_res_town").val()); 
				var psc_building_res_county = $.trim($("#psc_building_res_county").val()); 
				var psc_building_res_postcode = $.trim($("#psc_building_res_postcode").val()); 
				var psc_building_res_country = $.trim($("#psc_building_res_country").val()); 
				var show_id = $.trim($("#show_id").val()); 
				var show_id1 = $.trim($("#show_id1").val()); 
				var show_id2 = $.trim($("#show_id2").val()); 
				var show_id111 = $.trim($("#show_id111").val()); 
				var show_id112 = $.trim($("#show_id112").val()); 
				var show_id113 = $.trim($("#show_id113").val()); 
				var own_share13 = $.trim($("#own_share13").val()); 
				var own_share14 = $.trim($("#own_share14").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatePsc_info",       
				data: {'id':id,
				'p_fname':p_fname,
				'p_title':p_title,
				'p_mname':p_mname,
				'p_lname':p_lname,
				'psc_date_select':psc_date_select,
				'psc_month_select':psc_month_select,
				'psc_year_select':psc_year_select,
				'psc_nationality':psc_nationality,
				'psc_occupation':psc_occupation,
				'psc_country_res':psc_country_res,
				'select_psc_pre_address':select_psc_pre_address,
				'psc_building_name':psc_building_name,
				'psc_building_street':psc_building_street,
				'psc_pre_address':psc_pre_address,
				'psc_building_town':psc_building_town,
				'psc_building_aounty':psc_building_aounty,
				'psc_building_postcode':psc_building_postcode,
				'psc_building_country':psc_building_country,
				'psc_building_res_name':psc_building_res_name,
				'psc_building_res_street':psc_building_res_street,
				'psc_building_res_address3':psc_building_res_address3,
				'psc_building_res_town':psc_building_res_town,
				'psc_building_res_county':psc_building_res_county,
				'psc_building_res_postcode':psc_building_res_postcode,
				'psc_building_res_country':psc_building_res_country,
				'show_id':show_id,
				'show_id1':show_id1,
				'show_id2':show_id2,
				'show_id111':show_id111,
				'show_id112':show_id112,
				'show_id113':show_id113,
				'own_share13':own_share13,
				'own_share14':own_share14,
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#regupdateform').click(function(){
				var id = $.trim($("#roc_id").val()); 
				
				var reg_building_name = $.trim($("#reg_building_name").val()); 
				var reg_street = $.trim($("#reg_street").val()); 
				var reg_address1 = $.trim($("#reg_address1").val()); 
				var reg_town = $.trim($("#reg_town").val()); 
				var reg_county = $.trim($("#reg_county").val()); 
				var reg_postcode = $.trim($("#reg_postcode").val()); 
				var reg_country = $.trim($("#reg_country").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updateRegister_info",       
				data: {'id':id,
				'reg_building_name':reg_building_name,
				'reg_street':reg_street,
				'reg_address1':reg_address1,
				'reg_town':reg_town,
				'reg_county':reg_county,
				'reg_postcode':reg_postcode,
				'reg_country':reg_country,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#sharecorp').click(function(){
				var shcorp_id = $.trim($("#shcorp_id").val()); 
				var corp_comp_name = $.trim($("#corp_comp_name").val()); 
				var corp_first_name = $.trim($("#corp_first_name").val()); 
				var corp_last_name = $.trim($("#corp_last_name").val()); 
				var corp__build_name = $.trim($("#corp__build_name").val()); 
				var corp__street = $.trim($("#corp__street").val()); 
				var share_corp_pre_address = $.trim($("#share_corp_pre_address").val()); 
				var corp__town = $.trim($("#corp__town").val()); 
				var corp__county = $.trim($("#corp__county").val()); 
				var corp__postcode = $.trim($("#corp__postcode").val()); 
				var corp_build_country = $.trim($("#corp_build_country").val()); 
				var shareholder_currency1 = $.trim($("#shareholder_currency1").val()); 
				var shareholder_share_class1 = $.trim($("#shareholder_share_class1").val()); 
				 
				 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatecorpShare_info'",       
				data: {
				'shcorp_id':shcorp_id,
				
				'corp_comp_name':corp_comp_name,
				'corp_first_name':corp_first_name,
				'corp_last_name':corp_last_name,
				'corp_last_name':shareholder_last_name,
				'corp__build_name':corp__build_name,
				'corp__street':corp__street,
				'share_corp_pre_address':share_corp_pre_address,
				'shareholder_building_town':shareholder_building_town,
				'corp__town':corp__town,
				'corp__county':corp__county,
				'corp__postcode':corp__postcode,
				'corp_build_country':corp_build_country,
				'shareholder_currency1':shareholder_currency1,
				'shareholder_share_class1':shareholder_share_class1,
				
				
			}, 
				success: function(res) {
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#share').click(function(){
		   //alert("hello");
				var sh_id = $.trim($("#sh_id").val()); 
				var shareholder_title = $.trim($("#shareholder_title").val()); 
				var shareholder_first_name = $.trim($("#shareholder_first_name").val()); 
				var shareholder_middle_name = $.trim($("#shareholder_middle_name").val()); 
				var shareholder_last_name = $.trim($("#shareholder_last_name").val()); 
				var shareholder_building_name = $.trim($("#shareholder_building_name").val()); 
				var shareholder_building_street = $.trim($("#shareholder_building_street").val()); 
				var shareholder_building_address3 = $.trim($("#shareholder_building_address3").val()); 
				var shareholder_building_town = $.trim($("#shareholder_building_town").val()); 
				var shareholder_postcode = $.trim($("#shareholder_postcode").val()); 
				var shareholder_country = $.trim($("#shareholder_country").val()); 
				var shareholder_nationality = $.trim($("#shareholder_nationality").val()); 
				var shareholder_share_Currency = $.trim($("#shareholder_share_Currency").val()); 
				var shareholder_share_class = $.trim($("#shareholder_share_class").val()); 
				var shareholder_of_shares = $.trim($("#shareholder_of_shares").val()); 
				var shareholder_per_share = $.trim($("#shareholder_per_share").val()); 
				var shareholder_security_1 = $.trim($("#shareholder_security_1").val()); 
				var shareholder_security_2 = $.trim($("#shareholder_security_2").val()); 
				var shareholder_security_3 = $.trim($("#shareholder_security_3").val()); 
				 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updateShare_info",       
				data: {
				'sh_id':sh_id,
				
				'shareholder_title':shareholder_title,
				'shareholder_first_name':shareholder_first_name,
				'shareholder_middle_name':shareholder_middle_name,
				'shareholder_last_name':shareholder_last_name,
				'shareholder_building_name':shareholder_building_name,
				'shareholder_building_street':shareholder_building_street,
				'shareholder_building_address3':shareholder_building_address3,
				'shareholder_building_town':shareholder_building_town,
				'shareholder_postcode':shareholder_postcode,
				'shareholder_country':shareholder_country,
				'shareholder_nationality':shareholder_nationality,
				'shareholder_share_Currency':shareholder_share_Currency,
				'shareholder_share_class':shareholder_share_class,
				'shareholder_of_shares':shareholder_of_shares,
				'shareholder_per_share':shareholder_per_share,
				'shareholder_security_1':shareholder_security_1,
				'shareholder_security_2':shareholder_security_2,
				'shareholder_security_3':shareholder_security_3,
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#psc_btn').click(function(){
		   //alert("hello");
				var id = $.trim($("#pc_id").val()); 
				//alert(id);
				var p_fname = $.trim($("#p_fname").val()); 
				var p_title = $.trim($("#p_title").val()); 
				var p_mname = $.trim($("#p_mname").val()); 
				var p_lname = $.trim($("#p_lname").val()); 
				var psc_date_select = $.trim($("#psc_date_select").val()); 
				var psc_month_select = $.trim($("#psc_month_select").val()); 
				var psc_year_select = $.trim($("#psc_year_select").val()); 
				var psc_nationality = $.trim($("#psc_nationality").val()); 
				var psc_occupation = $.trim($("#psc_occupation").val()); 
				var psc_country_res = $.trim($("#psc_country_res").val()); 
				var select_psc_pre_address = $.trim($("#select_psc_pre_address").val()); 
				var psc_building_name = $.trim($("#psc_building_name").val()); 
				var psc_building_street = $.trim($("#psc_building_street").val()); 
				var psc_pre_address = $.trim($("#psc_pre_address").val()); 
				var psc_building_town = $.trim($("#psc_building_town").val()); 
				var psc_building_aounty = $.trim($("#psc_building_aounty").val()); 
				var psc_building_postcode = $.trim($("#psc_building_postcode").val()); 
				var psc_building_country = $.trim($("#psc_building_country").val()); 
				var psc_building_res_name = $.trim($("#psc_building_res_name").val()); 
				var psc_building_res_street = $.trim($("#psc_building_res_street").val()); 
				var psc_building_res_address3 = $.trim($("#psc_building_res_address3").val()); 
				var psc_building_res_town = $.trim($("#psc_building_res_town").val()); 
				var psc_building_res_county = $.trim($("#psc_building_res_county").val()); 
				var psc_building_res_postcode = $.trim($("#psc_building_res_postcode").val()); 
				var psc_building_res_country = $.trim($("#psc_building_res_country").val()); 
				var show_id = $.trim($("#show_id").val()); 
				var show_id1 = $.trim($("#show_id1").val()); 
				var show_id2 = $.trim($("#show_id2").val()); 
				var show_id111 = $.trim($("#show_id111").val()); 
				var show_id112 = $.trim($("#show_id112").val()); 
				var show_id113 = $.trim($("#show_id113").val()); 
				var own_share13 = $.trim($("#own_share13").val()); 
				var own_share14 = $.trim($("#own_share14").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatePsc_info",       
				data: {'id':id,
				'p_fname':p_fname,
				'p_title':p_title,
				'p_mname':p_mname,
				'p_lname':p_lname,
				'psc_date_select':psc_date_select,
				'psc_month_select':psc_month_select,
				'psc_year_select':psc_year_select,
				'psc_nationality':psc_nationality,
				'psc_occupation':psc_occupation,
				'psc_country_res':psc_country_res,
				'select_psc_pre_address':select_psc_pre_address,
				'psc_building_name':psc_building_name,
				'psc_building_street':psc_building_street,
				'psc_pre_address':psc_pre_address,
				'psc_building_town':psc_building_town,
				'psc_building_aounty':psc_building_aounty,
				'psc_building_postcode':psc_building_postcode,
				'psc_building_country':psc_building_country,
				'psc_building_res_name':psc_building_res_name,
				'psc_building_res_street':psc_building_res_street,
				'psc_building_res_address3':psc_building_res_address3,
				'psc_building_res_town':psc_building_res_town,
				'psc_building_res_county':psc_building_res_county,
				'psc_building_res_postcode':psc_building_res_postcode,
				'psc_building_res_country':psc_building_res_country,
				'show_id':show_id,
				'show_id1':show_id1,
				'show_id2':show_id2,
				'show_id111':show_id111,
				'show_id112':show_id112,
				'show_id113':show_id113,
				'own_share13':own_share13,
				'own_share14':own_share14,
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		   $('#pscrop_btn').click(function(){
				var pcr_id = $.trim($("#pcr_id").val()); 
				var psc_corp_dir_comp_name = $.trim($("#psc_corp_dir_comp_name").val()); 
				var psc_corp_dir_first_name = $.trim($("#psc_corp_dir_first_name").val()); 
				var psc_corp_dir_last_name = $.trim($("#psc_corp_dir_last_name").val()); 
				var psc_corp_directors_pre_address = $.trim($("#psc_corp_directors_pre_address").val()); 
				var psc_dir_build_name = $.trim($("#psc_dir_build_name").val()); 
				var psc_dir_build_street = $.trim($("#psc_dir_build_street").val()); 
				var psc_dir_build_address = $.trim($("#psc_dir_build_address").val()); 
				var psc_dir_build_town = $.trim($("#psc_dir_build_town").val()); 
				var psc_dir_build_county = $.trim($("#psc_dir_build_county").val()); 
				var psc_dir_build_postcode = $.trim($("#psc_dir_build_postcode").val()); 
				var psc_dir_build_country = $.trim($("#psc_dir_build_country").val()); 
				var psc_dir_corp_country = $.trim($("#psc_dir_corp_country").val()); 
				var psc_dir_corp_reg_numbr = $.trim($("#psc_dir_corp_reg_numbr").val()); 
				var psc_dir_corp_low = $.trim($("#psc_dir_corp_low").val()); 
				var psc_dir_corp_legal = $.trim($("#psc_dir_corp_legal").val()); 
				var show_id11 = $.trim($("#show_id11").val()); 
				var show_id12 = $.trim($("#show_id12").val()); 
				var show_id13 = $.trim($("#show_id13").val()); 
				var show_id3 = $.trim($("#show_id3").val()); 
				var show_id4 = $.trim($("#show_id4").val()); 
				var show_id5 = $.trim($("#show_id5").val()); 
				var own_share2 = $.trim($("#own_share2").val());
				var own_share3 = $.trim($("#own_share3").val()); 
				 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/updatecorpPsc_info",       
				data: {
				'pcr_id':pcr_id,
				
				'psc_corp_dir_comp_name':psc_corp_dir_comp_name,
				'psc_corp_dir_first_name':psc_corp_dir_first_name,
				'psc_corp_dir_last_name':psc_corp_dir_last_name,
				'psc_corp_directors_pre_address':psc_corp_directors_pre_address,
				'psc_dir_build_name':psc_dir_build_name,
				'psc_dir_build_street':psc_dir_build_street,
				'psc_dir_build_address':psc_dir_build_address,
				'psc_dir_build_town':psc_dir_build_town,
				'psc_dir_build_county':psc_dir_build_county,
				'psc_dir_build_postcode':psc_dir_build_postcode,
				'psc_dir_build_country':psc_dir_build_country,
				'psc_dir_corp_country':psc_dir_corp_country,
				'psc_dir_corp_reg_numbr':psc_dir_corp_reg_numbr,
				'psc_dir_corp_low':psc_dir_corp_low,
				'psc_dir_corp_legal':psc_dir_corp_legal,
				'show_id11':show_id11,
				'show_id12':show_id12,
				'show_id13':show_id13,
				'show_id4':show_id4,
				'show_id5':show_id5,
				'own_share2':own_share2,
				'own_share3':own_share3,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
           });
		$('#person_psc').click(function(){
		$("#psc_form").show();
		$('#psc_form').trigger("reset");
		$("#pscrop_form").hide();
		//$(".Secretarycorp").hide();
		$("#psc_btn").hide();
		$("#psc_inc").show();

		});
		$('#person_pscrop').click(function(){
$("#psc_form").hide();
$("#pscrop_form").show();
$('#pscrop_form').trigger("reset");
//$(".Secretarycorp").hide();
$("#pscrop_btn").hide();
$("#pscrop_inc").show();

});
$('#psc_inc').click(function(){
		  
			   var pccom_id = $.trim($("#pccom_id").val()); 
			   var p_fname = $.trim($("#p_fname").val()); 
				var p_title = $.trim($("#p_title").val()); 
				var p_mname = $.trim($("#p_mname").val()); 
				var p_lname = $.trim($("#p_lname").val()); 
				var psc_date_select = $.trim($("#psc_date_select").val()); 
				var psc_month_select = $.trim($("#psc_month_select").val()); 
				var psc_year_select = $.trim($("#psc_year_select").val()); 
				var psc_nationality = $.trim($("#psc_nationality").val()); 
				var psc_occupation = $.trim($("#psc_occupation").val()); 
				var psc_country_res = $.trim($("#psc_country_res").val()); 
				var select_psc_pre_address = $.trim($("#select_psc_pre_address").val()); 
				var psc_building_name = $.trim($("#psc_building_name").val()); 
				var psc_building_street = $.trim($("#psc_building_street").val()); 
				var psc_pre_address = $.trim($("#psc_pre_address").val()); 
				var psc_building_town = $.trim($("#psc_building_town").val()); 
				var psc_building_aounty = $.trim($("#psc_building_aounty").val()); 
				var psc_building_postcode = $.trim($("#psc_building_postcode").val()); 
				var psc_building_country = $.trim($("#psc_building_country").val()); 
				var psc_building_res_name = $.trim($("#psc_building_res_name").val()); 
				var psc_building_res_street = $.trim($("#psc_building_res_street").val()); 
				var psc_building_res_address3 = $.trim($("#psc_building_res_address3").val()); 
				var psc_building_res_town = $.trim($("#psc_building_res_town").val()); 
				var psc_building_res_county = $.trim($("#psc_building_res_county").val()); 
				var psc_building_res_postcode = $.trim($("#psc_building_res_postcode").val()); 
				var psc_building_res_country = $.trim($("#psc_building_res_country").val()); 
				var show_id = $.trim($("#show_id").val()); 
				var show_id1 = $.trim($("#show_id1").val()); 
				var show_id2 = $.trim($("#show_id2").val()); 
				var show_id111 = $.trim($("#show_id111").val()); 
				var show_id112 = $.trim($("#show_id112").val()); 
				var show_id113 = $.trim($("#show_id113").val()); 
				var own_share13 = $.trim($("#own_share13").val()); 
				var own_share14 = $.trim($("#own_share14").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertPsc_info",       
				data: {'pccom_id':pccom_id,
				'p_fname':p_fname,
				'p_title':p_title,
				'p_mname':p_mname,
				'p_lname':p_lname,
				'psc_date_select':psc_date_select,
				'psc_month_select':psc_month_select,
				'psc_year_select':psc_year_select,
				'psc_nationality':psc_nationality,
				'psc_occupation':psc_occupation,
				'psc_country_res':psc_country_res,
				'select_psc_pre_address':select_psc_pre_address,
				'psc_building_name':psc_building_name,
				'psc_building_street':psc_building_street,
				'psc_pre_address':psc_pre_address,
				'psc_building_town':psc_building_town,
				'psc_building_aounty':psc_building_aounty,
				'psc_building_postcode':psc_building_postcode,
				'psc_building_country':psc_building_country,
				'psc_building_res_name':psc_building_res_name,
				'psc_building_res_street':psc_building_res_street,
				'psc_building_res_address3':psc_building_res_address3,
				'psc_building_res_town':psc_building_res_town,
				'psc_building_res_county':psc_building_res_county,
				'psc_building_res_postcode':psc_building_res_postcode,
				'psc_building_res_country':psc_building_res_country,
				'show_id':show_id,
				'show_id1':show_id1,
				'show_id2':show_id2,
				'show_id111':show_id111,
				'show_id112':show_id112,
				'show_id113':show_id113,
				'own_share13':own_share13,
				'own_share14':own_share14,
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
				
			});
			$('#pscrop_inc').click(function(){
		  
			   var pcr_comp_id = $.trim($("#pcr_comp_id").val()); 
			   var psc_corp_dir_comp_name = $.trim($("#psc_corp_dir_comp_name").val()); 
				var psc_corp_dir_first_name = $.trim($("#psc_corp_dir_first_name").val()); 
				var psc_corp_dir_last_name = $.trim($("#psc_corp_dir_last_name").val()); 
				var psc_corp_directors_pre_address = $.trim($("#psc_corp_directors_pre_address").val()); 
				var psc_dir_build_name = $.trim($("#psc_dir_build_name").val()); 
				var psc_dir_build_street = $.trim($("#psc_dir_build_street").val()); 
				var psc_dir_build_address = $.trim($("#psc_dir_build_address").val()); 
				var psc_dir_build_town = $.trim($("#psc_dir_build_town").val()); 
				var psc_dir_build_county = $.trim($("#psc_dir_build_county").val()); 
				var psc_dir_build_postcode = $.trim($("#psc_dir_build_postcode").val()); 
				var psc_dir_build_country = $.trim($("#psc_dir_build_country").val()); 
				var psc_dir_corp_country = $.trim($("#psc_dir_corp_country").val()); 
				var psc_dir_corp_reg_numbr = $.trim($("#psc_dir_corp_reg_numbr").val()); 
				var psc_dir_corp_low = $.trim($("#psc_dir_corp_low").val()); 
				var psc_dir_corp_legal = $.trim($("#psc_dir_corp_legal").val()); 
				var show_id11 = $.trim($("#show_id11").val()); 
				var show_id12 = $.trim($("#show_id12").val()); 
				var show_id13 = $.trim($("#show_id13").val()); 
				var show_id3 = $.trim($("#show_id3").val()); 
				var show_id4 = $.trim($("#show_id4").val()); 
				var show_id5 = $.trim($("#show_id5").val()); 
				var own_share2 = $.trim($("#own_share2").val());
				var own_share3 = $.trim($("#own_share3").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertcorpPsc_info",       
				data: {'pcr_comp_id':pcr_comp_id,
				'psc_corp_dir_comp_name':psc_corp_dir_comp_name,
				'psc_corp_dir_first_name':psc_corp_dir_first_name,
				'psc_corp_dir_last_name':psc_corp_dir_last_name,
				'psc_corp_directors_pre_address':psc_corp_directors_pre_address,
				'psc_dir_build_name':psc_dir_build_name,
				'psc_dir_build_street':psc_dir_build_street,
				'psc_dir_build_address':psc_dir_build_address,
				'psc_dir_build_town':psc_dir_build_town,
				'psc_dir_build_county':psc_dir_build_county,
				'psc_dir_build_postcode':psc_dir_build_postcode,
				'psc_dir_build_country':psc_dir_build_country,
				'psc_dir_corp_country':psc_dir_corp_country,
				'psc_dir_corp_reg_numbr':psc_dir_corp_reg_numbr,
				'psc_dir_corp_low':psc_dir_corp_low,
				'psc_dir_corp_legal':psc_dir_corp_legal,
				'show_id11':show_id11,
				'show_id12':show_id12,
				'show_id13':show_id13,
				'show_id4':show_id4,
				'show_id5':show_id5,
				'own_share2':own_share2,
				'own_share3':own_share3,
				
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
				
			});
			
  $('#addshare').click(function(){
		$(".shareform").show();
		$('.shareform').trigger("reset");
		$("#share").hide();
		$("#share_inc").show();
		$(".sharecorpform").hide();
		/* 
			$(".Secretarycorp").hide();
		*/
	});
	 $('#share_inc').click(function(){
				var sh_comp_id = $.trim($("#sh_comp_id").val());  
				var shareholder_title = $.trim($("#shareholder_title").val()); 
				var shareholder_first_name = $.trim($("#shareholder_first_name").val()); 
				var shareholder_middle_name = $.trim($("#shareholder_middle_name").val()); 
				var shareholder_last_name = $.trim($("#shareholder_last_name").val()); 
				var shareholder_building_name = $.trim($("#shareholder_building_name").val()); 
				var shareholder_building_street = $.trim($("#shareholder_building_street").val()); 
				var shareholder_building_address3 = $.trim($("#shareholder_building_address3").val()); 
				var shareholder_building_town = $.trim($("#shareholder_building_town").val()); 
				var shareholder_postcode = $.trim($("#shareholder_postcode").val()); 
				var shareholder_country = $.trim($("#shareholder_country").val()); 
				var shareholder_nationality = $.trim($("#shareholder_nationality").val()); 
				var shareholder_share_Currency = $.trim($("#shareholder_share_Currency").val()); 
				var shareholder_share_class = $.trim($("#shareholder_share_class").val()); 
				var shareholder_of_shares = $.trim($("#shareholder_of_shares").val()); 
				var shareholder_per_share = $.trim($("#shareholder_per_share").val()); 
				var shareholder_security_1 = $.trim($("#shareholder_security_1").val()); 
				var shareholder_security_2 = $.trim($("#shareholder_security_2").val()); 
				var shareholder_security_3 = $.trim($("#shareholder_security_3").val());
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertShare_info",       
				data: {
				'sh_comp_id':sh_comp_id,
				'shareholder_title':shareholder_title,
				'shareholder_first_name':shareholder_first_name,
				'shareholder_middle_name':shareholder_middle_name,
				'shareholder_last_name':shareholder_last_name,
				'shareholder_building_name':shareholder_building_name,
				'shareholder_building_street':shareholder_building_street,
				'shareholder_building_address3':shareholder_building_address3,
				'shareholder_building_town':shareholder_building_town,
				'shareholder_postcode':shareholder_postcode,
				'shareholder_country':shareholder_country,
				'shareholder_nationality':shareholder_nationality,
				'shareholder_share_Currency':shareholder_share_Currency,
				'shareholder_share_class':shareholder_share_class,
				'shareholder_of_shares':shareholder_of_shares,
				'shareholder_per_share':shareholder_per_share,
				'shareholder_security_1':shareholder_security_1,
				'shareholder_security_2':shareholder_security_2,
				'shareholder_security_3':shareholder_security_3,
			}, 
				success: function(res) {
				//alert(res);
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
		   //alert("hello");
           });
		   $('#addsharecrop').click(function(){
		$(".shareform").hide();
		$(".sharecorpform").show();
		$("#sharecorp_inc").show();
		$("#sharecorp").hide();
	}); 
	$('#sharecorp_inc').click(function(){
		
				var id = $.trim($("#shcorp_id").val()); 
				var shcorp_comp_id = $.trim($("#shcorp_comp_id").val()); 
				var corp_comp_name = $.trim($("#corp_comp_name").val()); 
				var corp_first_name = $.trim($("#corp_first_name").val()); 
				var corp_last_name = $.trim($("#corp_last_name").val()); 
				var corp__build_name = $.trim($("#corp__build_name").val()); 
				var corp__street = $.trim($("#corp__street").val()); 
				var share_corp_pre_address = $.trim($("#share_corp_pre_address").val()); 
				var corp__town = $.trim($("#corp__town").val()); 
				var corp__county = $.trim($("#corp__county").val()); 
				var corp__postcode = $.trim($("#corp__postcode").val()); 
				var corp_build_country = $.trim($("#corp_build_country").val()); 
				var shareholder_currency1 = $.trim($("#shareholder_currency1").val()); 
				var shareholder_share_class1 = $.trim($("#shareholder_share_class1").val()); 
				$.ajax({
				type: "POST",
				url: base_url+"/ltd_controller/InsertcorpShare_info",       
				data: {
				'shcorp_comp_id':shcorp_comp_id,
				'corp_comp_name':corp_comp_name,
				'corp_first_name':corp_first_name,
				'corp_last_name':corp_last_name,
				'corp__build_name':corp__build_name,
				'corp__street':corp__street,
				'share_corp_pre_address':share_corp_pre_address,
				'corp__town':corp__town,
				'corp__county':corp__county,
				'corp__postcode':corp__postcode,
				'corp_build_country':corp_build_country,
				'shareholder_currency1':shareholder_currency1,
				'shareholder_share_class1':shareholder_share_class1,
			}, 
				success: function(res) {
					var pr = JSON.parse(res)
					if (pr.status == '1') {
						
						location.reload(true);
					} else {
						$("#successDiv").html("");
						$("#errorDiv").html(res.message);
					}
				}
				});
});
	
	function MyDirector(id,type,company_name)
	{ 
	//var base_url = window.location.origin;
	if(id == "" && type =="dir"){
		var p_id = $("#person_select_dir" ).val();
		id = p_id;
	}
	if(id == "" && type =="corp_dir"){
		var xc_id = $("#person_select_crop" ).val();
		id = xc_id;
	}
		$('#dc_name').val(company_name);
		$('#crop_company').val(company_name);
		$(".dirtab").click();
		$(".directer_crop").hide();
		$(".directer").hide();
		if(id=='0')
		{
			$(".directer").hide();
			alert("Please select one value");
		}
		else if(type=='dir')
		{
		$(".directer").show();
		
		}
		else if(type=='corp_dir')
		{
		
		$(".directer_crop").show();
		
		}
		$.ajax({
			url: base_url+"/ltd_controller/Director",
				  
			type: "POST", 
			data: {"id":id,"type":type},
			success: function(rep) {
			//alert(rep);
			var json = JSON.parse(rep);
			if(type=='dir')
			{
			$('#d_id').val(json.message.id);
			$('#sec_fname').val(json.message.first_name);
			$('#d_title').val(json.message.title);
			$('#d_Middle').val(json.message.middle_name);
			$('#d_last').val(json.message.last_name);
			$('#dir_residence').val(json.message.country_residence);
			$('#dc_occu').val(json.message.occupation);
			$('#dir_nationality').val(json.message.nationality);
			$('#dir_build_name').val(json.message.building_name);
			$('#dir_build_street').val(json.message.building_street);
			$('#dir_build_address').val(json.message.building_address);
			$('#dir_build_town').val(json.message.building_town);
			$('#dir_build_county').val(json.message.building_county);
			$('#dir_build_postcode').val(json.message.building_postcode);
			$('#dir_build_country').val(json.message.building_country);
			$('#dir_reg_name').val(json.message.res_building_name);
			$('#dir_reg_street').val(json.message.res_building_street);
			$('#selected_dir_reg_address').val(json.message.res_building_address);
			$('#dir_reg_town').val(json.message.res_building_town);
			$('#dir_reg_county').val(json.message.res_building_county);
			$('#dir_reg_postcode').val(json.message.res_building_postcode);
			$('#dir_reg_country').val(json.message.res_building_country);
			$('#dir_date').val(json.message.birth_date);
			$('#dir_month').val(json.message.birth_month);
			$('#dir_year').val(json.message.birth_year);
			if (json.message.type_id =="1"){
					$('#dir_type1').prop("checked", true);
			}else{
			$('#dir_type2').prop("checked", true);
			}

			}
			else if(type=='corp_dir')
		    {
			  $('#dc_id').val(json.message.id);
			  $('#dcrop_cname').val(json.message.company_name);
			  //$('#dcrop_cname').val(json.message.company_name);
			  $('#dcop_fname').val(json.message.first_name);
			  $('#dcrop_lname').val(json.message.last_name);
			  $('#dcrop_address').val(json.message.location);
			  //$('#dir_corpo_select_preeAddress').val(json.message.last_name);
			  $('#corp_dir_build_name').val(json.message.building_name);
			  $('#corp_dir_build_street').val(json.message.building_street);
			  $('#corp_pree_adrress').val(json.message.building_address);
			  $('#corp_dir_build_town').val(json.message.building_town);
			  $('#corp_dir_build_county').val(json.message.building_county);
			  $('#corp_dir_build_postcode').val(json.message.building_postcode);
			  $('#corp_dir_build_country').val(json.message.building_country);
			  $('#dir_corp_country').val(json.message.country_reg);
			  $('#dir_corp_reg_numbr').val(json.message.registration_number);
			  $('#dir_corp_low').val(json.message.governing_law);
			  $('#dir_corp_legal').val(json.message.legal_form);
			  $('#dir_corp_act').val(json.message.state_id);
			  if (json.message.type_id =="1"){
					$('#dir_type1').prop("checked", true);
			}else{
			$('#dir_type2').prop("checked", true);
			}
			}
			}
		});
	}
	function MySecterory(id,type,company_name)
	{ 
	if(id == "" && type =="sectr"){
		var p_id = $("#person_select_sect" ).val();
		id = p_id;
	}
	if(id == "" && type =="corp_sectr"){
		var xc_id = $("#person_select_sectcrop" ).val();
		id = xc_id;
	}
	//alert("hello");
	//alert(id);
	$(".sectrtab").click();
		$(".Secretarycorp").hide();
		$(".Secretary").hide();
		$('#sc_name').val(company_name);
		if(id=='0')
		{
			//$(".directer").hide();
			alert("Please select one value");
		}
		else if(type=='sectr')
		{
		$(".Secretary").show();
		
		}
		else if(type=='corp_sectr')
		{
		
		$(".Secretarycorp").show();
		
		}
		$.ajax({
			url: base_url+"/ltd_controller/Secterory",
			type: "POST", 
			data: {"id":id,"type":type},
			success: function(rep) {
			//alert(rep);
			var json = JSON.parse(rep);
			if(type=='sectr')
			{
			$('#s_id').val(json.message.id)
			$('#s_fname').val(json.message.name);
			$('#s_title').val(json.message.title);
			$('#s_mname').val(json.message.mid_name);
			$('#s_lname').val(json.message.last_name);
			$('#select_secretary_pre_adrress').val(json.message.pre_location);
			$('#s_lname').val(json.message.consent);
			$('#s_lname').val(json.message.appoint_date);
			$('#secretary_building_name').val(json.message.building_name);
			$('#secretary_building_street').val(json.message.building_street);
			$('#secretary_pre_address').val(json.message.building_address);
			$('#secretary_building_town').val(json.message.building_town);
			$('#secretary_building_county').val(json.message.building_county);
			$('#secretary_building_postcode').val(json.message.building_postcode);
			$('#secretary_building_country').val(json.message.building_country);
			if (json.message.type_id =="1"){
					$('#secratory_type1').prop("checked", true);
			}else{
			$('#secratory_type2').prop("checked", true);
			}
				//alert(json.message.first_name); 
				//window.location.href = base_url+"home/manageReseller"; 
			}
			else if(type=='corp_sectr')
		    {
			$('#sc_id').val(json.message.id);
			$('#sc_fname').val(json.message.first_name); 
			$('#sc_lname').val(json.message.last_name);
			$('#corp_secratory_pre_address').val(json.message.location);
			$('#secratory_build_name').val(json.message.building_name);
			$('#corp_secratory_build_street').val(json.message.building_street);
			$('#secratory_corp_pree_adrress').val(json.message.building_address);
			$('#corp_secratory_build_town').val(json.message.building_town);
			$('#corp_secratory_build_county').val(json.message.building_county);
			$('#corp_secratory_build_postcode').val(json.message.building_postcode);
			$('#corp_secratory_build_country').val(json.message.building_country);
			$('#secratory_corp_country').val(json.message.country_reg);
			$('#secratory_corp_reg_numbr').val(json.message.registration_number);
			$('#secratory_corp_low').val(json.message.governing_law);
			$('#secratory_corp_legal').val(json.message.legal_form);
			$('#secratory_corp_legal').val(json.message.state_id);
			if (json.message.type_id =="1"){
					$('#secratory_type1').prop("checked", true);
			}else{
			$('#secratory_type2').prop("checked", true);
			}
			}
		}
		});
	}
	function Mypsc(id,type,company_name)
	{ 
	if(id == "" && type =="psc"){
		var p_id = $("#person_select_psc" ).val();
		id = p_id;
	}
	if(id == "" && type =="corp_psc"){
		var xc_id = $("#person_select_psccrop" ).val();
		id = xc_id;
	}
	//alert(type);
		$('#pc_name').val(company_name);
		$(".psctab").click();
		$("#psc_form").hide();
		$("#pscrop_form").hide();
		if(id=='0')
		{
			//$(".directer").hide();
			alert("Please select one value");
		}
		else if(type=='psc')
		{
		$("#psc_form").show();
		
		}
		else if(type=='corp_psc')
		{
		
		$("#pscrop_form").show();
		
		}
		$.ajax({
			url:"/ltd_controller/psc",
			type: "POST", 
			data: {"id":id,"type":type},
			success: function(rep) {
			var json = JSON.parse(rep);
			if(type=='psc')
			{
			$('#pc_id').val(json.message.id);
			$('#p_fname').val(json.message.first_name);
			$('#p_title').val(json.message.title);
			$('#p_mname').val(json.message.middle_name);
			$('#p_lname').val(json.message.last_name);
			$('#psc_date_select').val(json.message.birth_date);
			$('#psc_month_select').val(json.message.birth_month);
			$('#psc_year_select').val(json.message.birth_year);
			$('#psc_nationality').val(json.message.nationality);
			$('#psc_occupation').val(json.message.occupation);
			$('#psc_country_res').val(json.message.country_residence);
			$('#select_psc_pre_address').val(json.message.service_location);
			$('#psc_building_name').val(json.message.building_name);
			$('#psc_building_street').val(json.message.building_street);
			$('#psc_pre_address').val(json.message.building_address);
			$('#psc_building_town').val(json.message.building_town);
			$('#psc_building_aounty').val(json.message.building_county);
			$('#psc_building_postcode').val(json.message.building_postcode);
			$('#psc_building_country').val(json.message.building_country);
			$('#psc_building_res_name').val(json.message.res_building_name);
			$('#psc_building_res_street').val(json.message.res_building_street);
			$('#psc_building_res_address3').val(json.message.res_building_address);
			$('#psc_building_res_town').val(json.message.res_building_town);
			$('#psc_building_res_county').val(json.message.res_building_county);
			$('#psc_building_res_postcode').val(json.message.res_building_postcode);
			$('#psc_building_res_country').val(json.message.res_building_country);
			$('#show_id').val(json.message.person_hold_share);
			$('#show_id1').val(json.message.member_hold_share);
			$('#show_id2').val(json.message.trustee_hold_share);
			$('#show_id111').val(json.message.person_voting_right);
			$('#show_id112').val(json.message.member_voting_right);
			$('#show_id113').val(json.message.trustee_voting_right);
			$('#own_share13').val(json.message.appoint_board);
			$('#own_share14').val(json.message.significant);
			if (json.message.type_id =="1"){
					$('#psc_corp_type1').prop("checked", true);
			}else{
			$('#psc_corp_type2').prop("checked", true);
			}
			}
			else if(type=='corp_psc')
		    {
				$('#pcr_id').val(json.message.id);
				$('#psc_corp_dir_comp_name').val(json.message.company_name);
				$('#psc_corp_dir_first_name').val(json.message.first_name);
				$('#psc_corp_dir_last_name').val(json.message.last_name);
				$('#psc_corp_directors_pre_address').val(json.message.location);
				$('#psc_dir_build_name').val(json.message.building_name);
				$('#psc_dir_build_street').val(json.message.building_street);
				$('#psc_dir_build_address').val(json.message.building_address);
				$('#psc_dir_build_town').val(json.message.building_town);
				$('#psc_dir_build_county').val(json.message.building_county);
				$('#psc_dir_build_postcode').val(json.message.building_postcode);
				$('#psc_dir_build_country').val(json.message.building_country);
				$('#psc_dir_corp_country').val(json.message.country_reg);
				$('#psc_dir_corp_reg_numbr').val(json.message.registration_number);
				$('#psc_dir_corp_low').val(json.message.governing_law);
				$('#psc_dir_corp_legal').val(json.message.legal_form);
				$('#show_id11').val(json.message.person_hold_share);
				$('#show_id12').val(json.message.member_hold_share);
				$('#show_id13').val(json.message.trustee_hold_share);
				$('#show_id3').val(json.message.person_voting_right);
				$('#show_id4').val(json.message.member_voting_right);
				$('#show_id5').val(json.message.trustee_voting_right);
				$('#own_share2').val(json.message.appoint_board);
				$('#own_share3').val(json.message.significant);
				if (json.message.type_id =="1"){
					$('#psc_corp_type1').prop("checked", true);
			}else{
			$('#psc_corp_type2').prop("checked", true);
			}
				
			}
			}
		});
	}
	function MyShare(id,type,company_name)
	{ 
	if(id == "" && type =="share"){
		var p_id = $("#person_select_share" ).val();
		id = p_id;
	}
	if(id == "" && type =="corp_share"){
		var xc_id = $("#person_select_sharecrop" ).val();
		id = xc_id;
	}
		$('#pc_name').val(company_name);
		$(".sharetab").click();
		$(".sharecorpform").hide();
		$(".shareform").hide();
		if(id=='0')
		{
			//$(".directer").hide();
			alert("Please select one value");
		}
		else if(type=='share')
		{
		$(".shareform").show();
		
		}
		else if(type=='corp_share')
		{
		
		$(".sharecorpform").show();
		
		}
		$.ajax({
			url:"/ltd_controller/Share",
			type: "POST", 
			data: {"id":id,"type":type},
			success: function(rep) {
			//alert(rep);
			var json = JSON.parse(rep);
			if(type=='share')
			{
				//alert("hello");
			$('#sh_id').val(json.message.id);
			$('#shareholder_title').val(json.message.title);
			$('#shareholder_first_name').val(json.message.name);
			$('#shareholder_middle_name').val(json.message.middle_name);
			$('#shareholder_last_name').val(json.message.last_name);
			$('#shareholder_building_name').val(json.message.building_name);
			$('#shareholder_building_street').val(json.message.building_street);
			$('#shareholder_building_address3').val(json.message.building_address);
			$('#shareholder_building_town').val(json.message.building_town);
			$('#shareholder_postcode').val(json.message.building_postcode);
			$('#shareholder_country').val(json.message.	building_country);
			$('#shareholder_nationality').val(json.message.nationality);
			$('#shareholder_share_Currency').val(json.message.currency);
			$('#shareholder_share_class').val(json.message.share_class);
			$('#shareholder_of_shares').val(json.message.shares);
			$('#shareholder_per_share').val(json.message.per_person_shares);
			$('#shareholder_security_1').val(json.message.security_1);
			$('#shareholder_security_2').val(json.message.security_2);
			$('#shareholder_security_3').val(json.message.security_3);
			
			}
			else if(type=='corp_share')
		    {
				$('#shcorp_id').val(json.message.id);
				$('#corp_comp_name').val(json.message.company_name);
				$('#corp_first_name').val(json.message.first_name);
				$('#corp_last_name').val(json.message.last_name);
				$('#corp__build_name').val(json.message.building_name);
				$('#corp__street').val(json.message.building_street);
				$('#share_corp_pre_address').val(json.message.building_address);
				$('#corp__town').val(json.message.building_town);
				$('#corp__county').val(json.message.building_county);
				$('#corp__postcode').val(json.message.building_postcode);
				$('#corp_build_country').val(json.message.building_country);
				$('#shareholder_currency1').val(json.message.shareholder_currency);
				$('#shareholder_share_class1').val(json.message.shareholder_share_class);
				$('#shareholder_of_shares1').val(json.message.shareholder_of_shares);
				$('#shareholder_per_share1').val(json.message.shareholder_per_share);
				
				
			}
			}
		});
	}
function showHide(id,hide_id,hide_id2){
$("#"+id).slideToggle();
}

function showHideFinal(id,hide_id,hide_id2,hide_id3){
$("#"+id).slideToggle();
}
