//**********************company information section********************************//
function company_info(div_id,id,active){
		$("#"+active).addClass("active");
		$('#'+div_id).css('display','block');
		if(id == "search_box" && active == "reg_com_2"){
			$('#search_box').css('display','none');
			$("#reg_com_1,#reg_com_3").removeClass("active");
			$("#fomation_vlaue").val("My Company is not yet registered.");
		}else if(id == "hide_search_box" && active == "reg_com_1"){
			$('#search_box').css('display','block');
			$("#reg_com_2,#reg_com_3").removeClass("active");
			$("#fomation_vlaue").val("My company is already registered.");
		}else if(id == "search_box" && active == "reg_com_3"){
			$('#search_box').css('display','none');
			$("#reg_com_1,#reg_com_2").removeClass("active");
			$("#fomation_vlaue").val("My Company is not a UK Limited Company.");
		}
}


//*****************************location required********************************************************//
function locationReq(id,map_id){
		$("#"+ map_id).addClass("active");
		if(map_id=="second_map"){
			$("#first_map,#third_map,#fourth_map,#fifth_map,#sixth_map").removeClass("active");
			$('#loctn_req').val('London (WC1) Office,Bloomsbury Way,London,WC1A 2SE');
			$('#instId').val("1068922");
			$('#cartId').val("RO1");
		}else if (map_id=="first_map"){
			$("#second_map,#third_map,#fourth_map,#fifth_map,#sixth_map").removeClass("active");	
			$('#loctn_req').val('London (W1) Office,Great Portland Street,London,W1W 7LT');	
			$('#instId').val("1068922");
			$('#cartId').val("RO1");
		}else if(map_id=="third_map"){
			$("#second_map,#first_map,#fourth_map,#fifth_map,#sixth_map").removeClass("active");
			$('#loctn_req').val('Edinburgh (New Town) Office,Cumberland Street,Edinburgh,EH3 6RE');
			$('#instId').val("1231924");
			$('#cartId').val("EO1");
		}else if(map_id=="fourth_map"){
			$("#third_map,#first_map,#second_map,#fifth_map,#sixth_map").removeClass("active");
			$('#loctn_req').val('Edinburgh (Central) Office,Rose Street South Lane,Edinburgh,EH2 3JG');	
			$('#instId').val("1231924");
			$('#cartId').val("EO1");
		}	else if(map_id=="fifth_map"){
			$("#third_map,#first_map,#second_map,#fourth_map,#sixth_map").removeClass("active");
			$('#loctn_req').val('London (City) Office,63/66 Hatton Garden,Suite 23,London,EC1N 8LE');	
			$('#instId').val("1231924");
			$('#cartId').val("RO1");
		}	else if(map_id=="sixth_map"){
			$("#third_map,#first_map,#second_map,#fifth_map,#fourth_map").removeClass("active");
			$('#loctn_req').val('Dublin (Central) Office,45 Dawson Street');	
			$('#instId').val("1231924");
			$('#cartId').val("EO1");
		}	
}


//*****************************location required********************************************************//
function select_service(select_id,items,product_price,subscription){
		var e = document.getElementById(select_id);
		var strUser1 = e.options[e.selectedIndex].value;	
		var strUser = strUser1.slice(0, strUser1.indexOf("#"));
		var subscription1 = strUser1.substring(strUser1.indexOf("#") + 1);
		$('#'+items).html('£'+strUser);
		$('#'+product_price).val(strUser);
		$('#'+subscription).val(subscription1);
		if(select_id=="select4" && subscription1=="1 Year Subscription"){
			$('#company_formation1').css('display','block');
		}else{
			$('#company_formation1').css('display','none');
		}
		pricecompany();
}


//*****************************services section *******************************************************//
function product_div(product_name,active_id,price_id,product_id,product_existance){ 
		var e = document.getElementById('select4');
		var strUser1 = e.options[e.selectedIndex].value;
		var subscription1 = strUser1.substring(strUser1.indexOf("#") + 1);
		//alert(subscription1=="1 Year Subscription");
		var div1Class = $('#'+active_id).attr('class');
		var reg_com_2 = $('#reg_com_2').attr('class');
		var price = $('#'+price_id).html();
		if(div1Class =="box_button active"){
			$('#'+product_id).val("");
			$("#"+ active_id).removeClass("active");
			$('#company_formation1').css('display','none');
			$('#company_formation').css('display','none');	
		}else if(div1Class =="box_button"){	
			$('#'+product_id).val(product_name);
			$("#"+ active_id).addClass("active");
			$('#company_formation1').css('display','none');
			$('#company_formation').css('display','none');
		}
		
		
		if(active_id =="box_button2" && reg_com_2=="form-group active"){
			if(div1Class =="box_button active"){
				$('#company_formation1').css('display','none');
				$('#company_formation').css('display','none');
			}else if(div1Class =="box_button"){
				$('#company_formation1').css('display','none');
				$('#company_formation').css('display','block');
				//$('#myModal_ltd').modal('show');
			}
		}else if(active_id=="box_button4" && reg_com_2=="form-group active" && subscription1=="1 Year Subscription"){
			if(div1Class =="box_button active"){
				$('#company_formation1').css('display','none');
				$('#company_formation').css('display','none');
			}else if(div1Class =="box_button"){	
				$('#company_formation1').css('display','block');
				$('#company_formation').css('display','none');
				$('#myModal_ltd').modal('show');
			}
		}
		if(active_id=="box_button7" || active_id=="box_button8" || active_id=="box_button9" || active_id=="box_button10" || active_id=="box_button4" || active_id=="box_button3"){
			 if(div1Class =="box_button"){	
				$('#myModal_ltd').modal('show');
				}
		}
		pricecompany();
		
}
function make_payment_div(name,active_id,remove_id1,remove_id2,remove_id3,enable,disable1,disable2,disable3) {
		/* if(enable=="amount4"){
			$('#amount4').val('20.00');
			$('#keyDiv').html('20.00');
			$('#amount').val('20.00');
		} */
		$('#'+enable).prop("disabled", false);
		$("#"+active_id).addClass("active");
		$("#"+remove_id1+",#"+remove_id2+ ",#"+remove_id3).removeClass("active");
		$("#"+disable1+",#"+disable2+ ",#"+disable3).prop("disabled", true);
		var data1 = $('#'+disable1).val();
		var data2 = $('#'+disable2).val();
		var data3 = $('#'+disable3).val();
		if(data1 !=""){
			$("#"+enable).val(data1);
		}else if(data2 !=""){
			$("#"+enable).val(data2);
		}else if(data3 !=""){
			$("#"+enable).val(data3);
		}
		var amnt = $("#"+enable).val();
		$('#keyDiv').text(amnt);
		$('#amount').val(amnt);
		$("#"+disable1+",#"+disable2+ ",#"+disable3).val('');
		var value = $('#'+name).html();
		var selected_value = $('#option').val(value);	
}
//*****************************include free company formation***********************************************//
function include_com_formation(include){
		$("#"+ include).addClass("active");	
		if(include=="include_yes"){
			$("#include_no").removeClass("active");	
			$("#free_formation_include2").val("yes");				
		}else if(include=="include_no"){
			$("#include_yes").removeClass("active");
			$("#free_formation_include2").val("no");		
		}else if(include=="include_yes1"){
			$("#include_no1").removeClass("active");
			$("#free_formation_include4").val("yes");		
		}else if(include=="include_no1"){
			$("#include_yes1").removeClass("active");
			$("#free_formation_include4").val("no");		
		}
}
function pricecompany(){
				//alert("gfdg");
				var array1 =[];
					$('a.active').each(function(i){
						if($(this).is('.active')){
							var id = $(this).attr('id');
							//var data = $("#"+id).find('.name').val();
							$("#"+id).each(function(key,value){
								var a = $("#"+id).children("input[name=product_name]").val(); 
								var b = $("#"+id).children("input[name=product_price]").val(); 
								var d = $("#"+id).children("input[name=subscription_time]").val(); 
								
								var c = {
									'data':a,
									'price':b,
									'desc':d,
								};
								
								array1.push(c);				
							});		
						}
					});
					var renw_sum = $('#renew_sum').val();
					var sum = 0;
					for(var i=0; i < array1.length; i++){
					sum += parseFloat(array1[i].price);
					}
					var n = sum.toFixed(2);
					var cost = $("#cost_amt").val();
					var postal = $('#deposite').val();
					if(cost==="yes" && postal==""){
					var amot = Number(n)+20.00;
					var fee = parseFloat(amot).toFixed(2);
					var pay_amt = $("#pay_amt").val(fee);
					$('#total_pric').html(" : £"+fee);
					$('#pay_1').html(fee);
					$('#pay_2').html(fee);
					}else if(postal==="20.00"){
						var sums = Number(n)+20.00;
						var sumspreamout = parseFloat(sums).toFixed(2);
						$('#total_pric').html(" : £"+sumspreamout);
						$('#pay_1').html(sumspreamout);
						$('#pay_2').html(sumspreamout);
						var pay_amt = $("#pay_amt").val(sumspreamout);						
					}else{
						$('#total_pric').html(" : £"+n);
						$('#pay_1').html(n);
						$('#pay_2').html(n);	
						var pay_amt = $("#pay_amt").val(n);
						
					}
										 
}
function xml_import_data(){
	var cmp_no = $("#cmp_no").val();
	var confirm_cmp_no = $("#confirm_cmp_no").val();
	if(!cmp_no){
		alert("Please fill Your company number");
	}
	else{
	$.ajax({ 
		'url' :'https://www.companymanager.online/wp/com_number_check',
		'type' : 'POST',
		'data' :{
			'cmp_no' : cmp_no,
		},
		'success' : function(data) {
			 var myObj = JSON.parse(data);	
			if(myObj.Status =="0"){
				swal("No data available");
				$("#import_yes").val("");
			}else if(myObj.Status =="1"){
				swal({
				  title: 'Are you sure you want to import the Company:',
				  text:myObj.com_name,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Import Company',
				  cancelButtonText: 'Cancel Import',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-danger',
				  buttonsStyling: false
				}).then(function () {
						$("#import_yes").val("yes");
						$('#company_name').val(myObj.com_name);
				}, function (dismiss) {
				  if (dismiss === 'cancel') {
					$("#import_yes").val("");
				  }
				})
			}  
		}
	});
	}
}
//************************************formsubmission******************************************************//
function order_service(){
	var base_url = window.location.origin;
	 var array =[];
	$('a.active').each(function(i){
		if($(this).is('.active')){
			var id = $(this).attr('id');
			//var data = $("#"+id).find('.name').val();
			$("#"+id).each(function(key,value){
				var a = $("#"+id).children("input[name=product_name]").val(); 
				var b = $("#"+id).children("input[name=product_price]").val(); 
				var d = $("#"+id).children("input[name=subscription_time]").val(); 
				
				var c = {
					'data':a,
					'price':b,
					'desc':d,
				};
				
				array.push(c);				
			});		
		}
	});
	var sum = 0;
	for(var i=0; i < array.length; i++){
		//console.log(array[i].price);
		sum += parseFloat(array[i].price);
		} 
	var company_name = $("#company_name").val(); 
	var cmp_no = $("#cmp_no").val();
		
	var cost = $("#cost_amt").val();
	if(cost=="yes"){
		var fee = Number(sum)+20.00;
		var sum1 = $("#amount").val(fee);
	}else{
		var sum1 = $("#amount").val(sum);
	}
	var trading_name = $("#trading_name").val(); 
	var fomation_vlaue = $("#fomation_vlaue").val(); 
	var loctn_req = $("#loctn_req").val(); 
	var product_existance = $("#product_existance").val(); 
	var user_id = $("#user_id").val(); 
	var company_id = $("#company_id").val(); 
	var import_yes = $("#import_yes").val(); 
	var free_formation_include4 = $("#free_formation_include4").val(); 
	var free_formation_include2 = $("#free_formation_include2").val();
	var amont = $("#amount").val();	
	if(!company_name){
		alert("Please fill Company Information Section - Company Name");
		return false;
	}else if(!$('#agree_tnc').is(':checked')){
		alert('Please agree our terms and conditions');
		return false;
	} else{
		   $.post('/wp',{
			 'value':array,
			 'company_name':company_name,
			 'cmp_no':cmp_no,
			 'loctn_req':loctn_req,
			 'fomation_vlaue':fomation_vlaue,
			 'trading_name':trading_name,
			 'user_id':user_id,
			 'import_yes':import_yes,
			 'free_formation_include4':free_formation_include4,
			 'free_formation_include2':free_formation_include2,
			 'amount':amont,
			 'company_id':company_id
			 
			 }, function( data,e ) {	
					 if(data=='1')
					{
					$('#my-payment-form').submit();	
					}
					else{
						console.log(data);
						alert(data);
					} 
					      
		});
	} 
}


//*********************************import js ****************************************//	

//********************************new-company-section-done*****************************************//
$(function() {
    $('.key').keyup(function() {
        $('#keyDiv').html($(this).val());
        $('#amount').val($(this).val());
    });
});


function modal_dismiss(){
	$('#myModal_ltd').modal('hide');
	var data = $('#total_pric').html();
		var subs = data.substring(data.indexOf("£") + 1);
		$("#cost_amt").val("yes");
		pricecompany();
	    
}
